

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qstackedwidget.h
// #include <qstackedwidget.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 10
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QStackedWidget {
pub:
  QFrame
}

pub interface QStackedWidgetITF {
//    QFrameITF
    get_cthis() voidptr
    toQStackedWidget() QStackedWidget
}
fn hotfix_QStackedWidget_itf_name_table(this QStackedWidgetITF) {
  that := QStackedWidget{}
  hotfix_QStackedWidget_itf_name_table(that)
}
pub fn (ptr QStackedWidget) toQStackedWidget() QStackedWidget { return ptr }

pub fn (this QStackedWidget) get_cthis() voidptr {
    return this.QFrame.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQStackedWidgetFromptr(cthis voidptr) QStackedWidget {
    bcthis0 := newQFrameFromptr(cthis)
    return QStackedWidget{bcthis0}
}
pub fn (dummy QStackedWidget) newFromptr(cthis voidptr) QStackedWidget {
    return newQStackedWidgetFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QStackedWidget(QWidget *)
type T_ZN14QStackedWidgetC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QStackedWidget) new_for_inherit_(parent  QWidget/*777 QWidget **/) QStackedWidget {
  //return newQStackedWidget(parent)
  return QStackedWidget{}
}
pub fn newQStackedWidget(parent  QWidget/*777 QWidget **/) QStackedWidget {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN14QStackedWidgetC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(4054824524, "_ZN14QStackedWidgetC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQStackedWidgetFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQStackedWidget)
    // qtrt.connect_destroyed(gothis, "QStackedWidget")
  return vthis
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QStackedWidget(QWidget *)

/*

*/
pub fn (dummy QStackedWidget) new_for_inherit_p() QStackedWidget {
  //return newQStackedWidgetp()
  return QStackedWidget{}
}
pub fn newQStackedWidgetp() QStackedWidget {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN14QStackedWidgetC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(4054824524, "_ZN14QStackedWidgetC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQStackedWidgetFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQStackedWidget)
    // qtrt.connect_destroyed(gothis, "QStackedWidget")
    return vthis
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:62
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int addWidget(QWidget *)
type T_ZN14QStackedWidget9addWidgetEP7QWidget = fn(cthis voidptr, w voidptr) int

/*

*/
pub fn (this QStackedWidget) addWidget(w  QWidget/*777 QWidget **/) int {
    mut conv_arg0 := voidptr(0)
    /*if w != voidptr(0) && w.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = w.QWidget_ptr().get_cthis()
      conv_arg0 = w.get_cthis()
    }
    mut fnobj := T_ZN14QStackedWidget9addWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1249653093, "_ZN14QStackedWidget9addWidgetEP7QWidget")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int insertWidget(int, QWidget *)
type T_ZN14QStackedWidget12insertWidgetEiP7QWidget = fn(cthis voidptr, index int, w voidptr) int

/*

*/
pub fn (this QStackedWidget) insertWidget(index int, w  QWidget/*777 QWidget **/) int {
    mut conv_arg1 := voidptr(0)
    /*if w != voidptr(0) && w.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = w.QWidget_ptr().get_cthis()
      conv_arg1 = w.get_cthis()
    }
    mut fnobj := T_ZN14QStackedWidget12insertWidgetEiP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2385140436, "_ZN14QStackedWidget12insertWidgetEiP7QWidget")
    rv :=
    fnobj(this.get_cthis(), index, conv_arg1)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void removeWidget(QWidget *)
type T_ZN14QStackedWidget12removeWidgetEP7QWidget = fn(cthis voidptr, w voidptr) /*void*/

/*

*/
pub fn (this QStackedWidget) removeWidget(w  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if w != voidptr(0) && w.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = w.QWidget_ptr().get_cthis()
      conv_arg0 = w.get_cthis()
    }
    mut fnobj := T_ZN14QStackedWidget12removeWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(598937263, "_ZN14QStackedWidget12removeWidgetEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:66
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * currentWidget() const
type T_ZNK14QStackedWidget13currentWidgetEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QStackedWidget) currentWidget()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK14QStackedWidget13currentWidgetEv(0)
    fnobj = qtrt.sym_qtfunc6(4210146573, "_ZNK14QStackedWidget13currentWidgetEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:67
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int currentIndex() const
type T_ZNK14QStackedWidget12currentIndexEv = fn(cthis voidptr) int

/*

*/
pub fn (this QStackedWidget) currentIndex() int {
    mut fnobj := T_ZNK14QStackedWidget12currentIndexEv(0)
    fnobj = qtrt.sym_qtfunc6(2777648257, "_ZNK14QStackedWidget12currentIndexEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOf(QWidget *) const
type T_ZNK14QStackedWidget7indexOfEP7QWidget = fn(cthis voidptr, arg0 voidptr) int

/*

*/
pub fn (this QStackedWidget) indexOf(arg0  QWidget/*777 QWidget **/) int {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QWidget_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZNK14QStackedWidget7indexOfEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2588077492, "_ZNK14QStackedWidget7indexOfEP7QWidget")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * widget(int) const
type T_ZNK14QStackedWidget6widgetEi = fn(cthis voidptr, arg0 int) voidptr/*666*/

/*

*/
pub fn (this QStackedWidget) widget(arg0 int)  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK14QStackedWidget6widgetEi(0)
    fnobj = qtrt.sym_qtfunc6(137583665, "_ZNK14QStackedWidget6widgetEi")
    rv :=
    fnobj(this.get_cthis(), arg0)
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int count() const
type T_ZNK14QStackedWidget5countEv = fn(cthis voidptr) int

/*

*/
pub fn (this QStackedWidget) count() int {
    mut fnobj := T_ZNK14QStackedWidget5countEv(0)
    fnobj = qtrt.sym_qtfunc6(3735907962, "_ZNK14QStackedWidget5countEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCurrentIndex(int)
type T_ZN14QStackedWidget15setCurrentIndexEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QStackedWidget) setCurrentIndex(index int)  {
    mut fnobj := T_ZN14QStackedWidget15setCurrentIndexEi(0)
    fnobj = qtrt.sym_qtfunc6(3030298062, "_ZN14QStackedWidget15setCurrentIndexEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCurrentWidget(QWidget *)
type T_ZN14QStackedWidget16setCurrentWidgetEP7QWidget = fn(cthis voidptr, w voidptr) /*void*/

/*

*/
pub fn (this QStackedWidget) setCurrentWidget(w  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if w != voidptr(0) && w.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = w.QWidget_ptr().get_cthis()
      conv_arg0 = w.get_cthis()
    }
    mut fnobj := T_ZN14QStackedWidget16setCurrentWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1926882632, "_ZN14QStackedWidget16setCurrentWidgetEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:78
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void currentChanged(int)
type T_ZN14QStackedWidget14currentChangedEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QStackedWidget) currentChanged(arg0 int)  {
    mut fnobj := T_ZN14QStackedWidget14currentChangedEi(0)
    fnobj = qtrt.sym_qtfunc6(3435686077, "_ZN14QStackedWidget14currentChangedEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qstackedwidget.h:79
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void widgetRemoved(int)
type T_ZN14QStackedWidget13widgetRemovedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QStackedWidget) widgetRemoved(index int)  {
    mut fnobj := T_ZN14QStackedWidget13widgetRemovedEi(0)
    fnobj = qtrt.sym_qtfunc6(3890011385, "_ZN14QStackedWidget13widgetRemovedEi")
    fnobj(this.get_cthis(), index)
}

[no_inline]
pub fn deleteQStackedWidget(this &QStackedWidget) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2840884496, "_ZN14QStackedWidgetD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QStackedWidget) freecpp() { deleteQStackedWidget(&this) }

fn (this QStackedWidget) free() {

  /*deleteQStackedWidget(&this)*/

  cthis := this.get_cthis()
  //println("QStackedWidget freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10251() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
