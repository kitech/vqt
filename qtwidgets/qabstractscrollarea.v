

module qtwidgets
// /usr/include/qt/QtWidgets/qabstractscrollarea.h
// #include <qabstractscrollarea.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 5
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QAbstractScrollArea {
pub:
  QFrame
}

pub interface QAbstractScrollAreaITF {
//    QFrameITF
    get_cthis() voidptr
    toQAbstractScrollArea() QAbstractScrollArea
}
fn hotfix_QAbstractScrollArea_itf_name_table(this QAbstractScrollAreaITF) {
  that := QAbstractScrollArea{}
  hotfix_QAbstractScrollArea_itf_name_table(that)
}
pub fn (ptr QAbstractScrollArea) toQAbstractScrollArea() QAbstractScrollArea { return ptr }

pub fn (this QAbstractScrollArea) get_cthis() voidptr {
    return this.QFrame.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQAbstractScrollAreaFromptr(cthis voidptr) QAbstractScrollArea {
    bcthis0 := newQFrameFromptr(cthis)
    return QAbstractScrollArea{bcthis0}
}
pub fn (dummy QAbstractScrollArea) newFromptr(cthis voidptr) QAbstractScrollArea {
    return newQAbstractScrollAreaFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qabstractscrollarea.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractScrollArea(QWidget *)
type T_ZN19QAbstractScrollAreaC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAbstractScrollArea) new_for_inherit_(parent  QWidget/*777 QWidget **/) QAbstractScrollArea {
  //return newQAbstractScrollArea(parent)
  return QAbstractScrollArea{}
}
pub fn newQAbstractScrollArea(parent  QWidget/*777 QWidget **/) QAbstractScrollArea {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN19QAbstractScrollAreaC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(538689117, "_ZN19QAbstractScrollAreaC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractScrollAreaFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractScrollArea)
    // qtrt.connect_destroyed(gothis, "QAbstractScrollArea")
  return vthis
}
// /usr/include/qt/QtWidgets/qabstractscrollarea.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractScrollArea(QWidget *)

/*

*/
pub fn (dummy QAbstractScrollArea) new_for_inherit_p() QAbstractScrollArea {
  //return newQAbstractScrollAreap()
  return QAbstractScrollArea{}
}
pub fn newQAbstractScrollAreap() QAbstractScrollArea {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN19QAbstractScrollAreaC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(538689117, "_ZN19QAbstractScrollAreaC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractScrollAreaFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractScrollArea)
    // qtrt.connect_destroyed(gothis, "QAbstractScrollArea")
    return vthis
}
// /usr/include/qt/QtWidgets/qabstractscrollarea.h:84
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * cornerWidget() const
type T_ZNK19QAbstractScrollArea12cornerWidgetEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QAbstractScrollArea) cornerWidget()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK19QAbstractScrollArea12cornerWidgetEv(0)
    fnobj = qtrt.sym_qtfunc6(1269609790, "_ZNK19QAbstractScrollArea12cornerWidgetEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qabstractscrollarea.h:85
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCornerWidget(QWidget *)
type T_ZN19QAbstractScrollArea15setCornerWidgetEP7QWidget = fn(cthis voidptr, widget voidptr) /*void*/

/*

*/
pub fn (this QAbstractScrollArea) setCornerWidget(widget  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN19QAbstractScrollArea15setCornerWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(322961687, "_ZN19QAbstractScrollArea15setCornerWidgetEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qabstractscrollarea.h:90
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * viewport() const
type T_ZNK19QAbstractScrollArea8viewportEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QAbstractScrollArea) viewport()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK19QAbstractScrollArea8viewportEv(0)
    fnobj = qtrt.sym_qtfunc6(3104358012, "_ZNK19QAbstractScrollArea8viewportEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qabstractscrollarea.h:91
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setViewport(QWidget *)
type T_ZN19QAbstractScrollArea11setViewportEP7QWidget = fn(cthis voidptr, widget voidptr) /*void*/

/*

*/
pub fn (this QAbstractScrollArea) setViewport(widget  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN19QAbstractScrollArea11setViewportEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2564963984, "_ZN19QAbstractScrollArea11setViewportEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQAbstractScrollArea(this &QAbstractScrollArea) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1253198226, "_ZN19QAbstractScrollAreaD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAbstractScrollArea) freecpp() { deleteQAbstractScrollArea(&this) }

fn (this QAbstractScrollArea) free() {

  /*deleteQAbstractScrollArea(&this)*/

  cthis := this.get_cthis()
  //println("QAbstractScrollArea freeing ${cthis} 0 bytes")

}


/*


*/
//type QAbstractScrollArea.SizeAdjustPolicy = int
pub enum QAbstractScrollAreaSizeAdjustPolicy {
  AdjustIgnored = 0
  AdjustToContentsOnFirstShow = 1
  AdjustToContents = 2
} // endof enum SizeAdjustPolicy

//  body block end

//  keep block begin


fn init_unused_10191() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
