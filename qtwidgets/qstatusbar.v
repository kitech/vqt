

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qstatusbar.h
// #include <qstatusbar.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 13
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QStatusBar {
pub:
  QWidget
}

pub interface QStatusBarITF {
//    QWidgetITF
    get_cthis() voidptr
    toQStatusBar() QStatusBar
}
fn hotfix_QStatusBar_itf_name_table(this QStatusBarITF) {
  that := QStatusBar{}
  hotfix_QStatusBar_itf_name_table(that)
}
pub fn (ptr QStatusBar) toQStatusBar() QStatusBar { return ptr }

pub fn (this QStatusBar) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQStatusBarFromptr(cthis voidptr) QStatusBar {
    bcthis0 := newQWidgetFromptr(cthis)
    return QStatusBar{bcthis0}
}
pub fn (dummy QStatusBar) newFromptr(cthis voidptr) QStatusBar {
    return newQStatusBarFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QStatusBar(QWidget *)
type T_ZN10QStatusBarC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QStatusBar) new_for_inherit_(parent  QWidget/*777 QWidget **/) QStatusBar {
  //return newQStatusBar(parent)
  return QStatusBar{}
}
pub fn newQStatusBar(parent  QWidget/*777 QWidget **/) QStatusBar {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN10QStatusBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2740779997, "_ZN10QStatusBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQStatusBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQStatusBar)
    // qtrt.connect_destroyed(gothis, "QStatusBar")
  return vthis
}
// /usr/include/qt/QtWidgets/qstatusbar.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QStatusBar(QWidget *)

/*

*/
pub fn (dummy QStatusBar) new_for_inherit_p() QStatusBar {
  //return newQStatusBarp()
  return QStatusBar{}
}
pub fn newQStatusBarp() QStatusBar {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN10QStatusBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2740779997, "_ZN10QStatusBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQStatusBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQStatusBar)
    // qtrt.connect_destroyed(gothis, "QStatusBar")
    return vthis
}
// /usr/include/qt/QtWidgets/qstatusbar.h:62
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addWidget(QWidget *, int)
type T_ZN10QStatusBar9addWidgetEP7QWidgeti = fn(cthis voidptr, widget voidptr, stretch int) /*void*/

/*

*/
pub fn (this QStatusBar) addWidget(widget  QWidget/*777 QWidget **/, stretch int)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN10QStatusBar9addWidgetEP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(2690176878, "_ZN10QStatusBar9addWidgetEP7QWidgeti")
    fnobj(this.get_cthis(), conv_arg0, stretch)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:62
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addWidget(QWidget *, int)

/*

*/
pub fn (this QStatusBar) addWidgetp(widget  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    // arg: 1, int=Int, =Invalid, , Invalid
    stretch := int(0)
    mut fnobj := T_ZN10QStatusBar9addWidgetEP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(2690176878, "_ZN10QStatusBar9addWidgetEP7QWidgeti")
    fnobj(this.get_cthis(), conv_arg0, stretch)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int insertWidget(int, QWidget *, int)
type T_ZN10QStatusBar12insertWidgetEiP7QWidgeti = fn(cthis voidptr, index int, widget voidptr, stretch int) int

/*

*/
pub fn (this QStatusBar) insertWidget(index int, widget  QWidget/*777 QWidget **/, stretch int) int {
    mut conv_arg1 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = widget.QWidget_ptr().get_cthis()
      conv_arg1 = widget.get_cthis()
    }
    mut fnobj := T_ZN10QStatusBar12insertWidgetEiP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(4143092170, "_ZN10QStatusBar12insertWidgetEiP7QWidgeti")
    rv :=
    fnobj(this.get_cthis(), index, conv_arg1, stretch)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int insertWidget(int, QWidget *, int)

/*

*/
pub fn (this QStatusBar) insertWidgetp(index int, widget  QWidget/*777 QWidget **/) int {
    mut conv_arg1 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = widget.QWidget_ptr().get_cthis()
      conv_arg1 = widget.get_cthis()
    }
    // arg: 2, int=Int, =Invalid, , Invalid
    stretch := int(0)
    mut fnobj := T_ZN10QStatusBar12insertWidgetEiP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(4143092170, "_ZN10QStatusBar12insertWidgetEiP7QWidgeti")
    rv :=
    fnobj(this.get_cthis(), index, conv_arg1, stretch)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addPermanentWidget(QWidget *, int)
type T_ZN10QStatusBar18addPermanentWidgetEP7QWidgeti = fn(cthis voidptr, widget voidptr, stretch int) /*void*/

/*

*/
pub fn (this QStatusBar) addPermanentWidget(widget  QWidget/*777 QWidget **/, stretch int)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN10QStatusBar18addPermanentWidgetEP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(1375597379, "_ZN10QStatusBar18addPermanentWidgetEP7QWidgeti")
    fnobj(this.get_cthis(), conv_arg0, stretch)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addPermanentWidget(QWidget *, int)

/*

*/
pub fn (this QStatusBar) addPermanentWidgetp(widget  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    // arg: 1, int=Int, =Invalid, , Invalid
    stretch := int(0)
    mut fnobj := T_ZN10QStatusBar18addPermanentWidgetEP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(1375597379, "_ZN10QStatusBar18addPermanentWidgetEP7QWidgeti")
    fnobj(this.get_cthis(), conv_arg0, stretch)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:65
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int insertPermanentWidget(int, QWidget *, int)
type T_ZN10QStatusBar21insertPermanentWidgetEiP7QWidgeti = fn(cthis voidptr, index int, widget voidptr, stretch int) int

/*

*/
pub fn (this QStatusBar) insertPermanentWidget(index int, widget  QWidget/*777 QWidget **/, stretch int) int {
    mut conv_arg1 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = widget.QWidget_ptr().get_cthis()
      conv_arg1 = widget.get_cthis()
    }
    mut fnobj := T_ZN10QStatusBar21insertPermanentWidgetEiP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(2100641601, "_ZN10QStatusBar21insertPermanentWidgetEiP7QWidgeti")
    rv :=
    fnobj(this.get_cthis(), index, conv_arg1, stretch)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:65
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int insertPermanentWidget(int, QWidget *, int)

/*

*/
pub fn (this QStatusBar) insertPermanentWidgetp(index int, widget  QWidget/*777 QWidget **/) int {
    mut conv_arg1 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = widget.QWidget_ptr().get_cthis()
      conv_arg1 = widget.get_cthis()
    }
    // arg: 2, int=Int, =Invalid, , Invalid
    stretch := int(0)
    mut fnobj := T_ZN10QStatusBar21insertPermanentWidgetEiP7QWidgeti(0)
    fnobj = qtrt.sym_qtfunc6(2100641601, "_ZN10QStatusBar21insertPermanentWidgetEiP7QWidgeti")
    rv :=
    fnobj(this.get_cthis(), index, conv_arg1, stretch)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:66
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void removeWidget(QWidget *)
type T_ZN10QStatusBar12removeWidgetEP7QWidget = fn(cthis voidptr, widget voidptr) /*void*/

/*

*/
pub fn (this QStatusBar) removeWidget(widget  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN10QStatusBar12removeWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1858783838, "_ZN10QStatusBar12removeWidgetEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:68
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setSizeGripEnabled(bool)
type T_ZN10QStatusBar18setSizeGripEnabledEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QStatusBar) setSizeGripEnabled(arg0 bool)  {
    mut fnobj := T_ZN10QStatusBar18setSizeGripEnabledEb(0)
    fnobj = qtrt.sym_qtfunc6(2512257864, "_ZN10QStatusBar18setSizeGripEnabledEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isSizeGripEnabled() const
type T_ZNK10QStatusBar17isSizeGripEnabledEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QStatusBar) isSizeGripEnabled() bool {
    mut fnobj := T_ZNK10QStatusBar17isSizeGripEnabledEv(0)
    fnobj = qtrt.sym_qtfunc6(1342450037, "_ZNK10QStatusBar17isSizeGripEnabledEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qstatusbar.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString currentMessage() const
type T_ZNK10QStatusBar14currentMessageEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QStatusBar) currentMessage() string {
    mut fnobj := T_ZNK10QStatusBar14currentMessageEv(0)
    fnobj = qtrt.sym_qtfunc6(3358295982, "_ZNK10QStatusBar14currentMessageEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qstatusbar.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMessage(const QString &, int)
type T_ZN10QStatusBar11showMessageERK7QStringi = fn(cthis voidptr, text voidptr, timeout int) /*void*/

/*

*/
pub fn (this QStatusBar) showMessage(text string, timeout int)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN10QStatusBar11showMessageERK7QStringi(0)
    fnobj = qtrt.sym_qtfunc6(3756771496, "_ZN10QStatusBar11showMessageERK7QStringi")
    fnobj(this.get_cthis(), conv_arg0, timeout)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMessage(const QString &, int)

/*

*/
pub fn (this QStatusBar) showMessagep(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, int=Int, =Invalid, , Invalid
    timeout := int(0)
    mut fnobj := T_ZN10QStatusBar11showMessageERK7QStringi(0)
    fnobj = qtrt.sym_qtfunc6(3756771496, "_ZN10QStatusBar11showMessageERK7QStringi")
    fnobj(this.get_cthis(), conv_arg0, timeout)
}
// /usr/include/qt/QtWidgets/qstatusbar.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clearMessage()
type T_ZN10QStatusBar12clearMessageEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QStatusBar) clearMessage()  {
    mut fnobj := T_ZN10QStatusBar12clearMessageEv(0)
    fnobj = qtrt.sym_qtfunc6(2515226392, "_ZN10QStatusBar12clearMessageEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qstatusbar.h:79
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void messageChanged(const QString &)
type T_ZN10QStatusBar14messageChangedERK7QString = fn(cthis voidptr, text voidptr) /*void*/

/*

*/
pub fn (this QStatusBar) messageChanged(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN10QStatusBar14messageChangedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2199278426, "_ZN10QStatusBar14messageChangedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQStatusBar(this &QStatusBar) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2231870532, "_ZN10QStatusBarD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QStatusBar) freecpp() { deleteQStatusBar(&this) }

fn (this QStatusBar) free() {

  /*deleteQStatusBar(&this)*/

  cthis := this.get_cthis()
  //println("QStatusBar freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10253() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
