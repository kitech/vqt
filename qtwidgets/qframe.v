

module qtwidgets
// /usr/include/qt/QtWidgets/qframe.h
// #include <qframe.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 25
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QFrame {
pub:
  QWidget
}

pub interface QFrameITF {
//    QWidgetITF
    get_cthis() voidptr
    toQFrame() QFrame
}
fn hotfix_QFrame_itf_name_table(this QFrameITF) {
  that := QFrame{}
  hotfix_QFrame_itf_name_table(that)
}
pub fn (ptr QFrame) toQFrame() QFrame { return ptr }

pub fn (this QFrame) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQFrameFromptr(cthis voidptr) QFrame {
    bcthis0 := newQWidgetFromptr(cthis)
    return QFrame{bcthis0}
}
pub fn (dummy QFrame) newFromptr(cthis voidptr) QFrame {
    return newQFrameFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qframe.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QFrame(QWidget *, Qt::WindowFlags)
type T_ZN6QFrameC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE = fn(cthis voidptr, parent voidptr, f int) 

/*

*/
pub fn (dummy QFrame) new_for_inherit_(parent  QWidget/*777 QWidget **/, f int) QFrame {
  //return newQFrame(parent, f)
  return QFrame{}
}
pub fn newQFrame(parent  QWidget/*777 QWidget **/, f int) QFrame {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN6QFrameC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(3925201311, "_ZN6QFrameC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQFrameFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQFrame)
    // qtrt.connect_destroyed(gothis, "QFrame")
  return vthis
}
// /usr/include/qt/QtWidgets/qframe.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QFrame(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QFrame) new_for_inherit_p() QFrame {
  //return newQFramep()
  return QFrame{}
}
pub fn newQFramep() QFrame {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN6QFrameC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(3925201311, "_ZN6QFrameC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQFrameFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQFrame)
    // qtrt.connect_destroyed(gothis, "QFrame")
    return vthis
}
// /usr/include/qt/QtWidgets/qframe.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QFrame(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QFrame) new_for_inherit_p1(parent  QWidget/*777 QWidget **/) QFrame {
  //return newQFramep1(parent)
  return QFrame{}
}
pub fn newQFramep1(parent  QWidget/*777 QWidget **/) QFrame {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN6QFrameC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(3925201311, "_ZN6QFrameC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQFrameFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQFrame)
    // qtrt.connect_destroyed(gothis, "QFrame")
    return vthis
}
// /usr/include/qt/QtWidgets/qframe.h:67
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int frameStyle() const
type T_ZNK6QFrame10frameStyleEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFrame) frameStyle() int {
    mut fnobj := T_ZNK6QFrame10frameStyleEv(0)
    fnobj = qtrt.sym_qtfunc6(226051958, "_ZNK6QFrame10frameStyleEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qframe.h:68
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFrameStyle(int)
type T_ZN6QFrame13setFrameStyleEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QFrame) setFrameStyle(arg0 int)  {
    mut fnobj := T_ZN6QFrame13setFrameStyleEi(0)
    fnobj = qtrt.sym_qtfunc6(1788913065, "_ZN6QFrame13setFrameStyleEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qframe.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int frameWidth() const
type T_ZNK6QFrame10frameWidthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFrame) frameWidth() int {
    mut fnobj := T_ZNK6QFrame10frameWidthEv(0)
    fnobj = qtrt.sym_qtfunc6(1132186928, "_ZNK6QFrame10frameWidthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qframe.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize sizeHint() const
type T_ZNK6QFrame8sizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QFrame) sizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK6QFrame8sizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(2037293193, "_ZNK6QFrame8sizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}

[no_inline]
pub fn deleteQFrame(this &QFrame) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1356824937, "_ZN6QFrameD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QFrame) freecpp() { deleteQFrame(&this) }

fn (this QFrame) free() {

  /*deleteQFrame(&this)*/

  cthis := this.get_cthis()
  //println("QFrame freeing ${cthis} 0 bytes")

}


/*


*/
//type QFrame.Shape = int
pub enum QFrameShape {
  NoFrame = 0
  Box = 1
  Panel = 2
  WinPanel = 3
  HLine = 4
  VLine = 5
  StyledPanel = 6
} // endof enum Shape


/*


*/
//type QFrame.Shadow = int
pub enum QFrameShadow {
  Plain = 16
  Raised = 32
  Sunken = 48
} // endof enum Shadow


/*


*/
//type QFrame.StyleMask = int
pub enum QFrameStyleMask {
  Shadow_Mask = 240
  Shape_Mask = 15
} // endof enum StyleMask

//  body block end

//  keep block begin


fn init_unused_10189() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
