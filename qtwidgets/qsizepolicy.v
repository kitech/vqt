

module qtwidgets
// /usr/include/qt/QtWidgets/qsizepolicy.h
// #include <qsizepolicy.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QSizePolicy {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QSizePolicyITF {
    get_cthis() voidptr
    toQSizePolicy() QSizePolicy
}
fn hotfix_QSizePolicy_itf_name_table(this QSizePolicyITF) {
  that := QSizePolicy{}
  hotfix_QSizePolicy_itf_name_table(that)
}
pub fn (ptr QSizePolicy) toQSizePolicy() QSizePolicy { return ptr }

pub fn (this QSizePolicy) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQSizePolicyFromptr(cthis voidptr) QSizePolicy {
    return QSizePolicy{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QSizePolicy) newFromptr(cthis voidptr) QSizePolicy {
    return newQSizePolicyFromptr(cthis)
}

[no_inline]
pub fn deleteQSizePolicy(this &QSizePolicy) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(331919931, "_ZN11QSizePolicyD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QSizePolicy) freecpp() { deleteQSizePolicy(&this) }

fn (this QSizePolicy) free() {

  /*deleteQSizePolicy(&this)*/

  cthis := this.get_cthis()
  //println("QSizePolicy freeing ${cthis} 0 bytes")

}


/*


*/
//type QSizePolicy.PolicyFlag = int
pub enum QSizePolicyPolicyFlag {
  GrowFlag = 1
  ExpandFlag = 2
  ShrinkFlag = 4
  IgnoreFlag = 8
} // endof enum PolicyFlag


/*


*/
//type QSizePolicy.Policy = int
pub enum QSizePolicyPolicy {
  Fixed = 0
  Minimum = 1
  Maximum = 4
  Preferred = 5
  MinimumExpanding = 3
  Expanding = 7
  Ignored = 13
} // endof enum Policy


/*


*/
//type QSizePolicy.ControlType = int
pub enum QSizePolicyControlType {
  DefaultType = 1
  ButtonBox = 2
  CheckBox = 4
  ComboBox = 8
  Frame = 16
  GroupBox = 32
  Label = 64
  Line = 128
  LineEdit = 256
  PushButton = 512
  RadioButton = 1024
  Slider = 2048
  SpinBox = 4096
  TabWidget = 8192
  ToolButton = 16384
} // endof enum ControlType

//  body block end

//  keep block begin


fn init_unused_10177() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
