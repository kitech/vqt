

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qcombobox.h
// #include <qcombobox.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 9
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QComboBox {
pub:
  QWidget
}

pub interface QComboBoxITF {
//    QWidgetITF
    get_cthis() voidptr
    toQComboBox() QComboBox
}
fn hotfix_QComboBox_itf_name_table(this QComboBoxITF) {
  that := QComboBox{}
  hotfix_QComboBox_itf_name_table(that)
}
pub fn (ptr QComboBox) toQComboBox() QComboBox { return ptr }

pub fn (this QComboBox) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQComboBoxFromptr(cthis voidptr) QComboBox {
    bcthis0 := newQWidgetFromptr(cthis)
    return QComboBox{bcthis0}
}
pub fn (dummy QComboBox) newFromptr(cthis voidptr) QComboBox {
    return newQComboBoxFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qcombobox.h:88
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QComboBox(QWidget *)
type T_ZN9QComboBoxC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QComboBox) new_for_inherit_(parent  QWidget/*777 QWidget **/) QComboBox {
  //return newQComboBox(parent)
  return QComboBox{}
}
pub fn newQComboBox(parent  QWidget/*777 QWidget **/) QComboBox {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QComboBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(400444357, "_ZN9QComboBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQComboBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQComboBox)
    // qtrt.connect_destroyed(gothis, "QComboBox")
  return vthis
}
// /usr/include/qt/QtWidgets/qcombobox.h:88
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QComboBox(QWidget *)

/*

*/
pub fn (dummy QComboBox) new_for_inherit_p() QComboBox {
  //return newQComboBoxp()
  return QComboBox{}
}
pub fn newQComboBoxp() QComboBox {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN9QComboBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(400444357, "_ZN9QComboBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQComboBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQComboBox)
    // qtrt.connect_destroyed(gothis, "QComboBox")
    return vthis
}
// /usr/include/qt/QtWidgets/qcombobox.h:91
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int maxVisibleItems() const
type T_ZNK9QComboBox15maxVisibleItemsEv = fn(cthis voidptr) int

/*

*/
pub fn (this QComboBox) maxVisibleItems() int {
    mut fnobj := T_ZNK9QComboBox15maxVisibleItemsEv(0)
    fnobj = qtrt.sym_qtfunc6(2452855331, "_ZNK9QComboBox15maxVisibleItemsEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qcombobox.h:92
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaxVisibleItems(int)
type T_ZN9QComboBox18setMaxVisibleItemsEi = fn(cthis voidptr, maxItems int) /*void*/

/*

*/
pub fn (this QComboBox) setMaxVisibleItems(maxItems int)  {
    mut fnobj := T_ZN9QComboBox18setMaxVisibleItemsEi(0)
    fnobj = qtrt.sym_qtfunc6(2605451080, "_ZN9QComboBox18setMaxVisibleItemsEi")
    fnobj(this.get_cthis(), maxItems)
}
// /usr/include/qt/QtWidgets/qcombobox.h:94
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int count() const
type T_ZNK9QComboBox5countEv = fn(cthis voidptr) int

/*

*/
pub fn (this QComboBox) count() int {
    mut fnobj := T_ZNK9QComboBox5countEv(0)
    fnobj = qtrt.sym_qtfunc6(484839320, "_ZNK9QComboBox5countEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qcombobox.h:95
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaxCount(int)
type T_ZN9QComboBox11setMaxCountEi = fn(cthis voidptr, max int) /*void*/

/*

*/
pub fn (this QComboBox) setMaxCount(max int)  {
    mut fnobj := T_ZN9QComboBox11setMaxCountEi(0)
    fnobj = qtrt.sym_qtfunc6(804866272, "_ZN9QComboBox11setMaxCountEi")
    fnobj(this.get_cthis(), max)
}
// /usr/include/qt/QtWidgets/qcombobox.h:96
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int maxCount() const
type T_ZNK9QComboBox8maxCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QComboBox) maxCount() int {
    mut fnobj := T_ZNK9QComboBox8maxCountEv(0)
    fnobj = qtrt.sym_qtfunc6(3494713860, "_ZNK9QComboBox8maxCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qcombobox.h:111
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool duplicatesEnabled() const
type T_ZNK9QComboBox17duplicatesEnabledEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QComboBox) duplicatesEnabled() bool {
    mut fnobj := T_ZNK9QComboBox17duplicatesEnabledEv(0)
    fnobj = qtrt.sym_qtfunc6(3571501781, "_ZNK9QComboBox17duplicatesEnabledEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qcombobox.h:112
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setDuplicatesEnabled(bool)
type T_ZN9QComboBox20setDuplicatesEnabledEb = fn(cthis voidptr, enable bool) /*void*/

/*

*/
pub fn (this QComboBox) setDuplicatesEnabled(enable bool)  {
    mut fnobj := T_ZN9QComboBox20setDuplicatesEnabledEb(0)
    fnobj = qtrt.sym_qtfunc6(1134441101, "_ZN9QComboBox20setDuplicatesEnabledEb")
    fnobj(this.get_cthis(), enable)
}
// /usr/include/qt/QtWidgets/qcombobox.h:159
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setEditable(bool)
type T_ZN9QComboBox11setEditableEb = fn(cthis voidptr, editable bool) /*void*/

/*

*/
pub fn (this QComboBox) setEditable(editable bool)  {
    mut fnobj := T_ZN9QComboBox11setEditableEb(0)
    fnobj = qtrt.sym_qtfunc6(1670976916, "_ZN9QComboBox11setEditableEb")
    fnobj(this.get_cthis(), editable)
}
// /usr/include/qt/QtWidgets/qcombobox.h:184
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int currentIndex() const
type T_ZNK9QComboBox12currentIndexEv = fn(cthis voidptr) int

/*

*/
pub fn (this QComboBox) currentIndex() int {
    mut fnobj := T_ZNK9QComboBox12currentIndexEv(0)
    fnobj = qtrt.sym_qtfunc6(3220285656, "_ZNK9QComboBox12currentIndexEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qcombobox.h:185
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString currentText() const
type T_ZNK9QComboBox11currentTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QComboBox) currentText() string {
    mut fnobj := T_ZNK9QComboBox11currentTextEv(0)
    fnobj = qtrt.sym_qtfunc6(1212789540, "_ZNK9QComboBox11currentTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qcombobox.h:188
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString itemText(int) const
type T_ZNK9QComboBox8itemTextEi = fn(sretobj voidptr, cthis voidptr, index int) voidptr

/*

*/
pub fn (this QComboBox) itemText(index int) string {
    mut fnobj := T_ZNK9QComboBox8itemTextEi(0)
    fnobj = qtrt.sym_qtfunc6(2984951329, "_ZNK9QComboBox8itemTextEi")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis(), index)
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qcombobox.h:192
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void addItem(const QString &, const QVariant &)
type T_ZN9QComboBox7addItemERK7QStringRK8QVariant = fn(cthis voidptr, text voidptr, userData voidptr) /*void*/

/*

*/
pub fn (this QComboBox) addItem(text string, userData  qtcore.QVariantITF)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if userData != voidptr(0) && userData.QVariant_ptr() != voidptr(0) */ {
        // conv_arg1 = userData.QVariant_ptr().get_cthis()
      conv_arg1 = userData.get_cthis()
    }
    mut fnobj := T_ZN9QComboBox7addItemERK7QStringRK8QVariant(0)
    fnobj = qtrt.sym_qtfunc6(931319215, "_ZN9QComboBox7addItemERK7QStringRK8QVariant")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1)
}
// /usr/include/qt/QtWidgets/qcombobox.h:192
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void addItem(const QString &, const QVariant &)

/*

*/
pub fn (this QComboBox) addItemp(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, const QVariant &=LValueReference, QVariant=Record, , Invalid
    mut conv_arg1 := qtcore.newQVariant()
    mut fnobj := T_ZN9QComboBox7addItemERK7QStringRK8QVariant(0)
    fnobj = qtrt.sym_qtfunc6(931319215, "_ZN9QComboBox7addItemERK7QStringRK8QVariant")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1)
}
// /usr/include/qt/QtWidgets/qcombobox.h:198
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void insertItem(int, const QString &, const QVariant &)
type T_ZN9QComboBox10insertItemEiRK7QStringRK8QVariant = fn(cthis voidptr, index int, text voidptr, userData voidptr) /*void*/

/*

*/
pub fn (this QComboBox) insertItem(index int, text string, userData  qtcore.QVariantITF)  {
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut conv_arg2 := voidptr(0)
    /*if userData != voidptr(0) && userData.QVariant_ptr() != voidptr(0) */ {
        // conv_arg2 = userData.QVariant_ptr().get_cthis()
      conv_arg2 = userData.get_cthis()
    }
    mut fnobj := T_ZN9QComboBox10insertItemEiRK7QStringRK8QVariant(0)
    fnobj = qtrt.sym_qtfunc6(3465040126, "_ZN9QComboBox10insertItemEiRK7QStringRK8QVariant")
    fnobj(this.get_cthis(), index, conv_arg1, conv_arg2)
}
// /usr/include/qt/QtWidgets/qcombobox.h:198
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void insertItem(int, const QString &, const QVariant &)

/*

*/
pub fn (this QComboBox) insertItemp(index int, text string)  {
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    // arg: 2, const QVariant &=LValueReference, QVariant=Record, , Invalid
    mut conv_arg2 := qtcore.newQVariant()
    mut fnobj := T_ZN9QComboBox10insertItemEiRK7QStringRK8QVariant(0)
    fnobj = qtrt.sym_qtfunc6(3465040126, "_ZN9QComboBox10insertItemEiRK7QStringRK8QVariant")
    fnobj(this.get_cthis(), index, conv_arg1, conv_arg2)
}
// /usr/include/qt/QtWidgets/qcombobox.h:204
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void removeItem(int)
type T_ZN9QComboBox10removeItemEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QComboBox) removeItem(index int)  {
    mut fnobj := T_ZN9QComboBox10removeItemEi(0)
    fnobj = qtrt.sym_qtfunc6(3369550016, "_ZN9QComboBox10removeItemEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qcombobox.h:206
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setItemText(int, const QString &)
type T_ZN9QComboBox11setItemTextEiRK7QString = fn(cthis voidptr, index int, text voidptr) /*void*/

/*

*/
pub fn (this QComboBox) setItemText(index int, text string)  {
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut fnobj := T_ZN9QComboBox11setItemTextEiRK7QString(0)
    fnobj = qtrt.sym_qtfunc6(198468933, "_ZN9QComboBox11setItemTextEiRK7QString")
    fnobj(this.get_cthis(), index, conv_arg1)
}
// /usr/include/qt/QtWidgets/qcombobox.h:210
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAbstractItemView * view() const
type T_ZNK9QComboBox4viewEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QComboBox) view()  QAbstractItemView/*777 QAbstractItemView **/ {
    mut fnobj := T_ZNK9QComboBox4viewEv(0)
    fnobj = qtrt.sym_qtfunc6(3310046969, "_ZNK9QComboBox4viewEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQAbstractItemViewFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qcombobox.h:224
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clear()
type T_ZN9QComboBox5clearEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QComboBox) clear()  {
    mut fnobj := T_ZN9QComboBox5clearEv(0)
    fnobj = qtrt.sym_qtfunc6(3991980318, "_ZN9QComboBox5clearEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qcombobox.h:225
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clearEditText()
type T_ZN9QComboBox13clearEditTextEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QComboBox) clearEditText()  {
    mut fnobj := T_ZN9QComboBox13clearEditTextEv(0)
    fnobj = qtrt.sym_qtfunc6(2293827830, "_ZN9QComboBox13clearEditTextEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qcombobox.h:226
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setEditText(const QString &)
type T_ZN9QComboBox11setEditTextERK7QString = fn(cthis voidptr, text voidptr) /*void*/

/*

*/
pub fn (this QComboBox) setEditText(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QComboBox11setEditTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3736805172, "_ZN9QComboBox11setEditTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qcombobox.h:227
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCurrentIndex(int)
type T_ZN9QComboBox15setCurrentIndexEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QComboBox) setCurrentIndex(index int)  {
    mut fnobj := T_ZN9QComboBox15setCurrentIndexEi(0)
    fnobj = qtrt.sym_qtfunc6(934993843, "_ZN9QComboBox15setCurrentIndexEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qcombobox.h:228
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCurrentText(const QString &)
type T_ZN9QComboBox14setCurrentTextERK7QString = fn(cthis voidptr, text voidptr) /*void*/

/*

*/
pub fn (this QComboBox) setCurrentText(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QComboBox14setCurrentTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3375923067, "_ZN9QComboBox14setCurrentTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qcombobox.h:231
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void editTextChanged(const QString &)
type T_ZN9QComboBox15editTextChangedERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QComboBox) editTextChanged(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QComboBox15editTextChangedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(681558797, "_ZN9QComboBox15editTextChangedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qcombobox.h:232
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void activated(int)
type T_ZN9QComboBox9activatedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QComboBox) activated(index int)  {
    mut fnobj := T_ZN9QComboBox9activatedEi(0)
    fnobj = qtrt.sym_qtfunc6(3913087343, "_ZN9QComboBox9activatedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qcombobox.h:233
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void textActivated(const QString &)
type T_ZN9QComboBox13textActivatedERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QComboBox) textActivated(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QComboBox13textActivatedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1528827286, "_ZN9QComboBox13textActivatedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qcombobox.h:234
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void highlighted(int)
type T_ZN9QComboBox11highlightedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QComboBox) highlighted(index int)  {
    mut fnobj := T_ZN9QComboBox11highlightedEi(0)
    fnobj = qtrt.sym_qtfunc6(1991475282, "_ZN9QComboBox11highlightedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qcombobox.h:235
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void textHighlighted(const QString &)
type T_ZN9QComboBox15textHighlightedERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QComboBox) textHighlighted(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QComboBox15textHighlightedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1845413289, "_ZN9QComboBox15textHighlightedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qcombobox.h:236
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void currentIndexChanged(int)
type T_ZN9QComboBox19currentIndexChangedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QComboBox) currentIndexChanged(index int)  {
    mut fnobj := T_ZN9QComboBox19currentIndexChangedEi(0)
    fnobj = qtrt.sym_qtfunc6(3753237607, "_ZN9QComboBox19currentIndexChangedEi")
    fnobj(this.get_cthis(), index)
}

[no_inline]
pub fn deleteQComboBox(this &QComboBox) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2960591526, "_ZN9QComboBoxD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QComboBox) freecpp() { deleteQComboBox(&this) }

fn (this QComboBox) free() {

  /*deleteQComboBox(&this)*/

  cthis := this.get_cthis()
  //println("QComboBox freeing ${cthis} 0 bytes")

}


/*


*/
//type QComboBox.InsertPolicy = int
pub enum QComboBoxInsertPolicy {
  NoInsert = 0
  InsertAtTop = 1
  InsertAtCurrent = 2
  InsertAtBottom = 3
  InsertAfterCurrent = 4
  InsertBeforeCurrent = 5
  InsertAlphabetically = 6
} // endof enum InsertPolicy


/*


*/
//type QComboBox.SizeAdjustPolicy = int
pub enum QComboBoxSizeAdjustPolicy {
  AdjustToContents = 0
  AdjustToContentsOnFirstShow = 1
  AdjustToMinimumContentsLength = 2
  AdjustToMinimumContentsLengthWithIcon = 3
} // endof enum SizeAdjustPolicy

//  body block end

//  keep block begin


fn init_unused_10219() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
