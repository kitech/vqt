

module qtwidgets
// /usr/include/qt/QtWidgets/qlayout.h
// #include <qlayout.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 5
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QLayout {
pub:
  qtcore.QObject
  QLayoutItem
}

pub interface QLayoutITF {
//    qtcore.QObjectITF
//    QLayoutItemITF
    get_cthis() voidptr
    toQLayout() QLayout
}
fn hotfix_QLayout_itf_name_table(this QLayoutITF) {
  that := QLayout{}
  hotfix_QLayout_itf_name_table(that)
}
pub fn (ptr QLayout) toQLayout() QLayout { return ptr }

pub fn (this QLayout) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
pub fn (this QLayout) set_cthis(cthis voidptr) {
    // this.QObject = qtcore.newQObjectFromptr(cthis)
    // this.QLayoutItem = newQLayoutItemFromptr(cthis)
}
[no_inline]
pub fn newQLayoutFromptr(cthis voidptr) QLayout {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    bcthis1 := newQLayoutItemFromptr(cthis)
    return QLayout{bcthis0, bcthis1}
}
pub fn (dummy QLayout) newFromptr(cthis voidptr) QLayout {
    return newQLayoutFromptr(cthis)
}

[no_inline]
pub fn deleteQLayout(this &QLayout) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(925367012, "_ZN7QLayoutD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QLayout) freecpp() { deleteQLayout(&this) }

fn (this QLayout) free() {

  /*deleteQLayout(&this)*/

  cthis := this.get_cthis()
  //println("QLayout freeing ${cthis} 0 bytes")

}


/*


*/
//type QLayout.SizeConstraint = int
pub enum QLayoutSizeConstraint {
  SetDefaultConstraint = 0
  SetNoConstraint = 1
  SetMinimumSize = 2
  SetFixedSize = 3
  SetMaximumSize = 4
  SetMinAndMaxSize = 5
} // endof enum SizeConstraint

//  body block end

//  keep block begin


fn init_unused_10207() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
