

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qabstractspinbox.h
// #include <qabstractspinbox.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 22
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QAbstractSpinBox {
pub:
  QWidget
}

pub interface QAbstractSpinBoxITF {
//    QWidgetITF
    get_cthis() voidptr
    toQAbstractSpinBox() QAbstractSpinBox
}
fn hotfix_QAbstractSpinBox_itf_name_table(this QAbstractSpinBoxITF) {
  that := QAbstractSpinBox{}
  hotfix_QAbstractSpinBox_itf_name_table(that)
}
pub fn (ptr QAbstractSpinBox) toQAbstractSpinBox() QAbstractSpinBox { return ptr }

pub fn (this QAbstractSpinBox) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQAbstractSpinBoxFromptr(cthis voidptr) QAbstractSpinBox {
    bcthis0 := newQWidgetFromptr(cthis)
    return QAbstractSpinBox{bcthis0}
}
pub fn (dummy QAbstractSpinBox) newFromptr(cthis voidptr) QAbstractSpinBox {
    return newQAbstractSpinBoxFromptr(cthis)
}

[no_inline]
pub fn deleteQAbstractSpinBox(this &QAbstractSpinBox) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1472121571, "_ZN16QAbstractSpinBoxD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAbstractSpinBox) freecpp() { deleteQAbstractSpinBox(&this) }

fn (this QAbstractSpinBox) free() {

  /*deleteQAbstractSpinBox(&this)*/

  cthis := this.get_cthis()
  //println("QAbstractSpinBox freeing ${cthis} 0 bytes")

}


/*


*/
//type QAbstractSpinBox.StepEnabledFlag = int
pub enum QAbstractSpinBoxStepEnabledFlag {
  StepNone = 0
  StepUpEnabled = 1
  StepDownEnabled = 2
} // endof enum StepEnabledFlag


/*


*/
//type QAbstractSpinBox.ButtonSymbols = int
pub enum QAbstractSpinBoxButtonSymbols {
  UpDownArrows = 0
  PlusMinus = 1
  NoButtons = 2
} // endof enum ButtonSymbols


/*


*/
//type QAbstractSpinBox.CorrectionMode = int
pub enum QAbstractSpinBoxCorrectionMode {
  CorrectToPreviousValue = 0
  CorrectToNearestValue = 1
} // endof enum CorrectionMode


/*


*/
//type QAbstractSpinBox.StepType = int
pub enum QAbstractSpinBoxStepType {
  DefaultStepType = 0
  AdaptiveDecimalStepType = 1
} // endof enum StepType

//  body block end

//  keep block begin


fn init_unused_10183() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
