

module qtwidgets
// /usr/include/qt/QtWidgets/qaction.h
// #include <qaction.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QAction {
pub:
  qtcore.QObject
}

pub interface QActionITF {
//    qtcore.QObjectITF
    get_cthis() voidptr
    toQAction() QAction
}
fn hotfix_QAction_itf_name_table(this QActionITF) {
  that := QAction{}
  hotfix_QAction_itf_name_table(that)
}
pub fn (ptr QAction) toQAction() QAction { return ptr }

pub fn (this QAction) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQActionFromptr(cthis voidptr) QAction {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    return QAction{bcthis0}
}
pub fn (dummy QAction) newFromptr(cthis voidptr) QAction {
    return newQActionFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qaction.h:95
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAction(QObject *)
type T_ZN7QActionC2EP7QObject = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAction) new_for_inherit_(parent qtcore.QObjectITF/*777 QObject **/) QAction {
  //return newQAction(parent)
  return QAction{}
}
pub fn newQAction(parent qtcore.QObjectITF/*777 QObject **/) QAction {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QObject_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN7QActionC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(218008137, "_ZN7QActionC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQActionFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAction)
    // qtrt.connect_destroyed(gothis, "QAction")
  return vthis
}
// /usr/include/qt/QtWidgets/qaction.h:95
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAction(QObject *)

/*

*/
pub fn (dummy QAction) new_for_inherit_p() QAction {
  //return newQActionp()
  return QAction{}
}
pub fn newQActionp() QAction {
    // arg: 0, QObject *=Pointer, QObject=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN7QActionC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(218008137, "_ZN7QActionC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQActionFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAction)
    // qtrt.connect_destroyed(gothis, "QAction")
    return vthis
}
// /usr/include/qt/QtWidgets/qaction.h:96
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAction(const QString &, QObject *)
type T_ZN7QActionC2ERK7QStringP7QObject = fn(cthis voidptr, text voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAction) new_for_inherit_1(text string, parent qtcore.QObjectITF/*777 QObject **/) QAction {
  //return newQAction1(text, parent)
  return QAction{}
}
pub fn newQAction1(text string, parent qtcore.QObjectITF/*777 QObject **/) QAction {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QObject_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN7QActionC2ERK7QStringP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(237064025, "_ZN7QActionC2ERK7QStringP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQActionFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAction)
    // qtrt.connect_destroyed(gothis, "QAction")
  return vthis
}
// /usr/include/qt/QtWidgets/qaction.h:96
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAction(const QString &, QObject *)

/*

*/
pub fn (dummy QAction) new_for_inherit_1p(text string) QAction {
  //return newQAction1p(text)
  return QAction{}
}
pub fn newQAction1p(text string) QAction {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QObject *=Pointer, QObject=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN7QActionC2ERK7QStringP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(237064025, "_ZN7QActionC2ERK7QStringP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQActionFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAction)
    // qtrt.connect_destroyed(gothis, "QAction")
    return vthis
}
// /usr/include/qt/QtWidgets/qaction.h:106
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setText(const QString &)
type T_ZN7QAction7setTextERK7QString = fn(cthis voidptr, text voidptr) /*void*/

/*

*/
pub fn (this QAction) setText(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QAction7setTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3016118847, "_ZN7QAction7setTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:107
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString text() const
type T_ZNK7QAction4textEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QAction) text() string {
    mut fnobj := T_ZNK7QAction4textEv(0)
    fnobj = qtrt.sym_qtfunc6(2317398157, "_ZNK7QAction4textEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qaction.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setIconText(const QString &)
type T_ZN7QAction11setIconTextERK7QString = fn(cthis voidptr, text voidptr) /*void*/

/*

*/
pub fn (this QAction) setIconText(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QAction11setIconTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(4066431650, "_ZN7QAction11setIconTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:110
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString iconText() const
type T_ZNK7QAction8iconTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QAction) iconText() string {
    mut fnobj := T_ZNK7QAction8iconTextEv(0)
    fnobj = qtrt.sym_qtfunc6(1536229387, "_ZNK7QAction8iconTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qaction.h:112
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setToolTip(const QString &)
type T_ZN7QAction10setToolTipERK7QString = fn(cthis voidptr, tip voidptr) /*void*/

/*

*/
pub fn (this QAction) setToolTip(tip string)  {
    mut tmp_arg0 := qtcore.newQString5(tip)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QAction10setToolTipERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1685067186, "_ZN7QAction10setToolTipERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:113
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString toolTip() const
type T_ZNK7QAction7toolTipEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QAction) toolTip() string {
    mut fnobj := T_ZNK7QAction7toolTipEv(0)
    fnobj = qtrt.sym_qtfunc6(3413091492, "_ZNK7QAction7toolTipEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qaction.h:115
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStatusTip(const QString &)
type T_ZN7QAction12setStatusTipERK7QString = fn(cthis voidptr, statusTip voidptr) /*void*/

/*

*/
pub fn (this QAction) setStatusTip(statusTip string)  {
    mut tmp_arg0 := qtcore.newQString5(statusTip)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QAction12setStatusTipERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(4184243817, "_ZN7QAction12setStatusTipERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:116
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString statusTip() const
type T_ZNK7QAction9statusTipEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QAction) statusTip() string {
    mut fnobj := T_ZNK7QAction9statusTipEv(0)
    fnobj = qtrt.sym_qtfunc6(118887278, "_ZNK7QAction9statusTipEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qaction.h:150
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCheckable(bool)
type T_ZN7QAction12setCheckableEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAction) setCheckable(arg0 bool)  {
    mut fnobj := T_ZN7QAction12setCheckableEb(0)
    fnobj = qtrt.sym_qtfunc6(1026958800, "_ZN7QAction12setCheckableEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:151
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isCheckable() const
type T_ZNK7QAction11isCheckableEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAction) isCheckable() bool {
    mut fnobj := T_ZNK7QAction11isCheckableEv(0)
    fnobj = qtrt.sym_qtfunc6(3197261506, "_ZNK7QAction11isCheckableEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qaction.h:156
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isChecked() const
type T_ZNK7QAction9isCheckedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAction) isChecked() bool {
    mut fnobj := T_ZNK7QAction9isCheckedEv(0)
    fnobj = qtrt.sym_qtfunc6(939532047, "_ZNK7QAction9isCheckedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qaction.h:158
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isEnabled() const
type T_ZNK7QAction9isEnabledEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAction) isEnabled() bool {
    mut fnobj := T_ZNK7QAction9isEnabledEv(0)
    fnobj = qtrt.sym_qtfunc6(4605092, "_ZNK7QAction9isEnabledEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qaction.h:160
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isVisible() const
type T_ZNK7QAction9isVisibleEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAction) isVisible() bool {
    mut fnobj := T_ZNK7QAction9isVisibleEv(0)
    fnobj = qtrt.sym_qtfunc6(3333608945, "_ZNK7QAction9isVisibleEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qaction.h:187
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void trigger()
type T_ZN7QAction7triggerEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAction) trigger()  {
    mut fnobj := T_ZN7QAction7triggerEv(0)
    fnobj = qtrt.sym_qtfunc6(3248071529, "_ZN7QAction7triggerEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qaction.h:188
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void hover()
type T_ZN7QAction5hoverEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAction) hover()  {
    mut fnobj := T_ZN7QAction5hoverEv(0)
    fnobj = qtrt.sym_qtfunc6(606202509, "_ZN7QAction5hoverEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qaction.h:189
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setChecked(bool)
type T_ZN7QAction10setCheckedEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAction) setChecked(arg0 bool)  {
    mut fnobj := T_ZN7QAction10setCheckedEb(0)
    fnobj = qtrt.sym_qtfunc6(3927186894, "_ZN7QAction10setCheckedEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:190
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void toggle()
type T_ZN7QAction6toggleEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAction) toggle()  {
    mut fnobj := T_ZN7QAction6toggleEv(0)
    fnobj = qtrt.sym_qtfunc6(612007024, "_ZN7QAction6toggleEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qaction.h:191
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setEnabled(bool)
type T_ZN7QAction10setEnabledEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAction) setEnabled(arg0 bool)  {
    mut fnobj := T_ZN7QAction10setEnabledEb(0)
    fnobj = qtrt.sym_qtfunc6(3528607333, "_ZN7QAction10setEnabledEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:192
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setDisabled(bool)
type T_ZN7QAction11setDisabledEb = fn(cthis voidptr, b bool) /*void*/

/*

*/
pub fn (this QAction) setDisabled(b bool)  {
    mut fnobj := T_ZN7QAction11setDisabledEb(0)
    fnobj = qtrt.sym_qtfunc6(964827636, "_ZN7QAction11setDisabledEb")
    fnobj(this.get_cthis(), b)
}
// /usr/include/qt/QtWidgets/qaction.h:193
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setVisible(bool)
type T_ZN7QAction10setVisibleEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAction) setVisible(arg0 bool)  {
    mut fnobj := T_ZN7QAction10setVisibleEb(0)
    fnobj = qtrt.sym_qtfunc6(346478384, "_ZN7QAction10setVisibleEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qaction.h:196
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void changed()
type T_ZN7QAction7changedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAction) changed()  {
    mut fnobj := T_ZN7QAction7changedEv(0)
    fnobj = qtrt.sym_qtfunc6(3892863327, "_ZN7QAction7changedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qaction.h:197
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void triggered(bool)
type T_ZN7QAction9triggeredEb = fn(cthis voidptr, checked bool) /*void*/

/*

*/
pub fn (this QAction) triggered(checked bool)  {
    mut fnobj := T_ZN7QAction9triggeredEb(0)
    fnobj = qtrt.sym_qtfunc6(3257549264, "_ZN7QAction9triggeredEb")
    fnobj(this.get_cthis(), checked)
}
// /usr/include/qt/QtWidgets/qaction.h:197
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void triggered(bool)

/*

*/
pub fn (this QAction) triggeredp()  {
    // arg: 0, bool=Bool, =Invalid, , Invalid
    checked := false
    mut fnobj := T_ZN7QAction9triggeredEb(0)
    fnobj = qtrt.sym_qtfunc6(3257549264, "_ZN7QAction9triggeredEb")
    fnobj(this.get_cthis(), checked)
}
// /usr/include/qt/QtWidgets/qaction.h:198
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void hovered()
type T_ZN7QAction7hoveredEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAction) hovered()  {
    mut fnobj := T_ZN7QAction7hoveredEv(0)
    fnobj = qtrt.sym_qtfunc6(2538730, "_ZN7QAction7hoveredEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qaction.h:199
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void toggled(bool)
type T_ZN7QAction7toggledEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAction) toggled(arg0 bool)  {
    mut fnobj := T_ZN7QAction7toggledEb(0)
    fnobj = qtrt.sym_qtfunc6(62506592, "_ZN7QAction7toggledEb")
    fnobj(this.get_cthis(), arg0)
}

[no_inline]
pub fn deleteQAction(this &QAction) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2632439783, "_ZN7QActionD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAction) freecpp() { deleteQAction(&this) }

fn (this QAction) free() {

  /*deleteQAction(&this)*/

  cthis := this.get_cthis()
  //println("QAction freeing ${cthis} 0 bytes")

}


/*


*/
//type QAction.MenuRole = int
pub enum QActionMenuRole {
  NoRole = 0
  TextHeuristicRole = 1
  ApplicationSpecificRole = 2
  AboutQtRole = 3
  AboutRole = 4
  PreferencesRole = 5
  QuitRole = 6
} // endof enum MenuRole


/*


*/
//type QAction.Priority = int
pub enum QActionPriority {
  LowPriority = 0
  NormalPriority = 128
  HighPriority = 256
} // endof enum Priority


/*


*/
//type QAction.ActionEvent = int
pub enum QActionActionEvent {
  Trigger = 0
  Hover = 1
} // endof enum ActionEvent

//  body block end

//  keep block begin


fn init_unused_10195() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
