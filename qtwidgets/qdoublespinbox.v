

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qspinbox.h
// #include <qspinbox.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 10
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QDoubleSpinBox {
pub:
  QAbstractSpinBox
}

pub interface QDoubleSpinBoxITF {
//    QAbstractSpinBoxITF
    get_cthis() voidptr
    toQDoubleSpinBox() QDoubleSpinBox
}
fn hotfix_QDoubleSpinBox_itf_name_table(this QDoubleSpinBoxITF) {
  that := QDoubleSpinBox{}
  hotfix_QDoubleSpinBox_itf_name_table(that)
}
pub fn (ptr QDoubleSpinBox) toQDoubleSpinBox() QDoubleSpinBox { return ptr }

pub fn (this QDoubleSpinBox) get_cthis() voidptr {
    return this.QAbstractSpinBox.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQDoubleSpinBoxFromptr(cthis voidptr) QDoubleSpinBox {
    bcthis0 := newQAbstractSpinBoxFromptr(cthis)
    return QDoubleSpinBox{bcthis0}
}
pub fn (dummy QDoubleSpinBox) newFromptr(cthis voidptr) QDoubleSpinBox {
    return newQDoubleSpinBoxFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qspinbox.h:135
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QDoubleSpinBox(QWidget *)
type T_ZN14QDoubleSpinBoxC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QDoubleSpinBox) new_for_inherit_(parent  QWidget/*777 QWidget **/) QDoubleSpinBox {
  //return newQDoubleSpinBox(parent)
  return QDoubleSpinBox{}
}
pub fn newQDoubleSpinBox(parent  QWidget/*777 QWidget **/) QDoubleSpinBox {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN14QDoubleSpinBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(698183486, "_ZN14QDoubleSpinBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQDoubleSpinBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQDoubleSpinBox)
    // qtrt.connect_destroyed(gothis, "QDoubleSpinBox")
  return vthis
}
// /usr/include/qt/QtWidgets/qspinbox.h:135
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QDoubleSpinBox(QWidget *)

/*

*/
pub fn (dummy QDoubleSpinBox) new_for_inherit_p() QDoubleSpinBox {
  //return newQDoubleSpinBoxp()
  return QDoubleSpinBox{}
}
pub fn newQDoubleSpinBoxp() QDoubleSpinBox {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN14QDoubleSpinBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(698183486, "_ZN14QDoubleSpinBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQDoubleSpinBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQDoubleSpinBox)
    // qtrt.connect_destroyed(gothis, "QDoubleSpinBox")
    return vthis
}
// /usr/include/qt/QtWidgets/qspinbox.h:138
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] double value() const
type T_ZNK14QDoubleSpinBox5valueEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QDoubleSpinBox) value() f64 {
    mut fnobj := T_ZNK14QDoubleSpinBox5valueEv(0)
    fnobj = qtrt.sym_qtfunc6(4042644504, "_ZNK14QDoubleSpinBox5valueEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) //.(f64) // 1111
   return f64(rv)
}
// /usr/include/qt/QtWidgets/qspinbox.h:146
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString cleanText() const
type T_ZNK14QDoubleSpinBox9cleanTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QDoubleSpinBox) cleanText() string {
    mut fnobj := T_ZNK14QDoubleSpinBox9cleanTextEv(0)
    fnobj = qtrt.sym_qtfunc6(3151881166, "_ZNK14QDoubleSpinBox9cleanTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qspinbox.h:149
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setSingleStep(double)
type T_ZN14QDoubleSpinBox13setSingleStepEd = fn(cthis voidptr, val f64) /*void*/

/*

*/
pub fn (this QDoubleSpinBox) setSingleStep(val f64)  {
    mut fnobj := T_ZN14QDoubleSpinBox13setSingleStepEd(0)
    fnobj = qtrt.sym_qtfunc6(1279985875, "_ZN14QDoubleSpinBox13setSingleStepEd")
    fnobj(this.get_cthis(), val)
}
// /usr/include/qt/QtWidgets/qspinbox.h:152
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMinimum(double)
type T_ZN14QDoubleSpinBox10setMinimumEd = fn(cthis voidptr, min f64) /*void*/

/*

*/
pub fn (this QDoubleSpinBox) setMinimum(min f64)  {
    mut fnobj := T_ZN14QDoubleSpinBox10setMinimumEd(0)
    fnobj = qtrt.sym_qtfunc6(216875734, "_ZN14QDoubleSpinBox10setMinimumEd")
    fnobj(this.get_cthis(), min)
}
// /usr/include/qt/QtWidgets/qspinbox.h:155
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaximum(double)
type T_ZN14QDoubleSpinBox10setMaximumEd = fn(cthis voidptr, max f64) /*void*/

/*

*/
pub fn (this QDoubleSpinBox) setMaximum(max f64)  {
    mut fnobj := T_ZN14QDoubleSpinBox10setMaximumEd(0)
    fnobj = qtrt.sym_qtfunc6(3674665111, "_ZN14QDoubleSpinBox10setMaximumEd")
    fnobj(this.get_cthis(), max)
}
// /usr/include/qt/QtWidgets/qspinbox.h:157
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setRange(double, double)
type T_ZN14QDoubleSpinBox8setRangeEdd = fn(cthis voidptr, min f64, max f64) /*void*/

/*

*/
pub fn (this QDoubleSpinBox) setRange(min f64, max f64)  {
    mut fnobj := T_ZN14QDoubleSpinBox8setRangeEdd(0)
    fnobj = qtrt.sym_qtfunc6(3493293011, "_ZN14QDoubleSpinBox8setRangeEdd")
    fnobj(this.get_cthis(), min, max)
}
// /usr/include/qt/QtWidgets/qspinbox.h:171
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setValue(double)
type T_ZN14QDoubleSpinBox8setValueEd = fn(cthis voidptr, val f64) /*void*/

/*

*/
pub fn (this QDoubleSpinBox) setValue(val f64)  {
    mut fnobj := T_ZN14QDoubleSpinBox8setValueEd(0)
    fnobj = qtrt.sym_qtfunc6(747158720, "_ZN14QDoubleSpinBox8setValueEd")
    fnobj(this.get_cthis(), val)
}
// /usr/include/qt/QtWidgets/qspinbox.h:174
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void valueChanged(double)
type T_ZN14QDoubleSpinBox12valueChangedEd = fn(cthis voidptr, arg0 f64) /*void*/

/*

*/
pub fn (this QDoubleSpinBox) valueChanged(arg0 f64)  {
    mut fnobj := T_ZN14QDoubleSpinBox12valueChangedEd(0)
    fnobj = qtrt.sym_qtfunc6(1552051280, "_ZN14QDoubleSpinBox12valueChangedEd")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qspinbox.h:175
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void textChanged(const QString &)
type T_ZN14QDoubleSpinBox11textChangedERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QDoubleSpinBox) textChanged(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN14QDoubleSpinBox11textChangedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(135358936, "_ZN14QDoubleSpinBox11textChangedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQDoubleSpinBox(this &QDoubleSpinBox) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3371637413, "_ZN14QDoubleSpinBoxD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDoubleSpinBox) freecpp() { deleteQDoubleSpinBox(&this) }

fn (this QDoubleSpinBox) free() {

  /*deleteQDoubleSpinBox(&this)*/

  cthis := this.get_cthis()
  //println("QDoubleSpinBox freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10249() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
