

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qbuttongroup.h
// #include <qbuttongroup.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 2
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QButtonGroup {
pub:
  qtcore.QObject
}

pub interface QButtonGroupITF {
//    qtcore.QObjectITF
    get_cthis() voidptr
    toQButtonGroup() QButtonGroup
}
fn hotfix_QButtonGroup_itf_name_table(this QButtonGroupITF) {
  that := QButtonGroup{}
  hotfix_QButtonGroup_itf_name_table(that)
}
pub fn (ptr QButtonGroup) toQButtonGroup() QButtonGroup { return ptr }

pub fn (this QButtonGroup) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQButtonGroupFromptr(cthis voidptr) QButtonGroup {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    return QButtonGroup{bcthis0}
}
pub fn (dummy QButtonGroup) newFromptr(cthis voidptr) QButtonGroup {
    return newQButtonGroupFromptr(cthis)
}

[no_inline]
pub fn deleteQButtonGroup(this &QButtonGroup) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(788836440, "_ZN12QButtonGroupD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QButtonGroup) freecpp() { deleteQButtonGroup(&this) }

fn (this QButtonGroup) free() {

  /*deleteQButtonGroup(&this)*/

  cthis := this.get_cthis()
  //println("QButtonGroup freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10215() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
