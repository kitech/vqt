

module qtwidgets
// /usr/include/qt/QtWidgets/qlayoutitem.h
// #include <qlayoutitem.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 5
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QLayoutItem {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QLayoutItemITF {
    get_cthis() voidptr
    toQLayoutItem() QLayoutItem
}
fn hotfix_QLayoutItem_itf_name_table(this QLayoutItemITF) {
  that := QLayoutItem{}
  hotfix_QLayoutItem_itf_name_table(that)
}
pub fn (ptr QLayoutItem) toQLayoutItem() QLayoutItem { return ptr }

pub fn (this QLayoutItem) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQLayoutItemFromptr(cthis voidptr) QLayoutItem {
    return QLayoutItem{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QLayoutItem) newFromptr(cthis voidptr) QLayoutItem {
    return newQLayoutItemFromptr(cthis)
}

[no_inline]
pub fn deleteQLayoutItem(this &QLayoutItem) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2677748963, "_ZN11QLayoutItemD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QLayoutItem) freecpp() { deleteQLayoutItem(&this) }

fn (this QLayoutItem) free() {

  /*deleteQLayoutItem(&this)*/

  cthis := this.get_cthis()
  //println("QLayoutItem freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10199() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
