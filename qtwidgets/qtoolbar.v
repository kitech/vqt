

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qtoolbar.h
// #include <qtoolbar.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 17
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QToolBar {
pub:
  QWidget
}

pub interface QToolBarITF {
//    QWidgetITF
    get_cthis() voidptr
    toQToolBar() QToolBar
}
fn hotfix_QToolBar_itf_name_table(this QToolBarITF) {
  that := QToolBar{}
  hotfix_QToolBar_itf_name_table(that)
}
pub fn (ptr QToolBar) toQToolBar() QToolBar { return ptr }

pub fn (this QToolBar) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQToolBarFromptr(cthis voidptr) QToolBar {
    bcthis0 := newQWidgetFromptr(cthis)
    return QToolBar{bcthis0}
}
pub fn (dummy QToolBar) newFromptr(cthis voidptr) QToolBar {
    return newQToolBarFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qtoolbar.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QToolBar(const QString &, QWidget *)
type T_ZN8QToolBarC2ERK7QStringP7QWidget = fn(cthis voidptr, title voidptr, parent voidptr) 

/*

*/
pub fn (dummy QToolBar) new_for_inherit_(title string, parent  QWidget/*777 QWidget **/) QToolBar {
  //return newQToolBar(title, parent)
  return QToolBar{}
}
pub fn newQToolBar(title string, parent  QWidget/*777 QWidget **/) QToolBar {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN8QToolBarC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(115476381, "_ZN8QToolBarC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQToolBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQToolBar)
    // qtrt.connect_destroyed(gothis, "QToolBar")
  return vthis
}
// /usr/include/qt/QtWidgets/qtoolbar.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QToolBar(const QString &, QWidget *)

/*

*/
pub fn (dummy QToolBar) new_for_inherit_p(title string) QToolBar {
  //return newQToolBarp(title)
  return QToolBar{}
}
pub fn newQToolBarp(title string) QToolBar {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN8QToolBarC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(115476381, "_ZN8QToolBarC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQToolBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQToolBar)
    // qtrt.connect_destroyed(gothis, "QToolBar")
    return vthis
}
// /usr/include/qt/QtWidgets/qtoolbar.h:73
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QToolBar(QWidget *)
type T_ZN8QToolBarC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QToolBar) new_for_inherit_1(parent  QWidget/*777 QWidget **/) QToolBar {
  //return newQToolBar1(parent)
  return QToolBar{}
}
pub fn newQToolBar1(parent  QWidget/*777 QWidget **/) QToolBar {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN8QToolBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2552095711, "_ZN8QToolBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQToolBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQToolBar)
    // qtrt.connect_destroyed(gothis, "QToolBar")
  return vthis
}
// /usr/include/qt/QtWidgets/qtoolbar.h:73
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QToolBar(QWidget *)

/*

*/
pub fn (dummy QToolBar) new_for_inherit_1p() QToolBar {
  //return newQToolBar1p()
  return QToolBar{}
}
pub fn newQToolBar1p() QToolBar {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN8QToolBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2552095711, "_ZN8QToolBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQToolBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQToolBar)
    // qtrt.connect_destroyed(gothis, "QToolBar")
    return vthis
}
// /usr/include/qt/QtWidgets/qtoolbar.h:76
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMovable(bool)
type T_ZN8QToolBar10setMovableEb = fn(cthis voidptr, movable bool) /*void*/

/*

*/
pub fn (this QToolBar) setMovable(movable bool)  {
    mut fnobj := T_ZN8QToolBar10setMovableEb(0)
    fnobj = qtrt.sym_qtfunc6(2579160314, "_ZN8QToolBar10setMovableEb")
    fnobj(this.get_cthis(), movable)
}
// /usr/include/qt/QtWidgets/qtoolbar.h:77
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isMovable() const
type T_ZNK8QToolBar9isMovableEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QToolBar) isMovable() bool {
    mut fnobj := T_ZNK8QToolBar9isMovableEv(0)
    fnobj = qtrt.sym_qtfunc6(1223047420, "_ZNK8QToolBar9isMovableEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}

[no_inline]
pub fn deleteQToolBar(this &QToolBar) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1272080248, "_ZN8QToolBarD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QToolBar) freecpp() { deleteQToolBar(&this) }

fn (this QToolBar) free() {

  /*deleteQToolBar(&this)*/

  cthis := this.get_cthis()
  //println("QToolBar freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10257() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
