

module qtwidgets
// /usr/include/qt/QtWidgets/qboxlayout.h
// #include <qboxlayout.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 8
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QHBoxLayout {
pub:
  QBoxLayout
}

pub interface QHBoxLayoutITF {
//    QBoxLayoutITF
    get_cthis() voidptr
    toQHBoxLayout() QHBoxLayout
}
fn hotfix_QHBoxLayout_itf_name_table(this QHBoxLayoutITF) {
  that := QHBoxLayout{}
  hotfix_QHBoxLayout_itf_name_table(that)
}
pub fn (ptr QHBoxLayout) toQHBoxLayout() QHBoxLayout { return ptr }

pub fn (this QHBoxLayout) get_cthis() voidptr {
    return this.QBoxLayout.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQHBoxLayoutFromptr(cthis voidptr) QHBoxLayout {
    bcthis0 := newQBoxLayoutFromptr(cthis)
    return QHBoxLayout{bcthis0}
}
pub fn (dummy QHBoxLayout) newFromptr(cthis voidptr) QHBoxLayout {
    return newQHBoxLayoutFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:117
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QHBoxLayout()
type T_ZN11QHBoxLayoutC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QHBoxLayout) new_for_inherit_() QHBoxLayout {
  //return newQHBoxLayout()
  return QHBoxLayout{}
}
pub fn newQHBoxLayout() QHBoxLayout {
    mut fnobj := T_ZN11QHBoxLayoutC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(2369781699, "_ZN11QHBoxLayoutC2Ev")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis)
    rv := cthis
    vthis := newQHBoxLayoutFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQHBoxLayout)
    // qtrt.connect_destroyed(gothis, "QHBoxLayout")
  return vthis
}
// /usr/include/qt/QtWidgets/qboxlayout.h:118
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QHBoxLayout(QWidget *)
type T_ZN11QHBoxLayoutC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QHBoxLayout) new_for_inherit_1(parent  QWidget/*777 QWidget **/) QHBoxLayout {
  //return newQHBoxLayout1(parent)
  return QHBoxLayout{}
}
pub fn newQHBoxLayout1(parent  QWidget/*777 QWidget **/) QHBoxLayout {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN11QHBoxLayoutC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2008149469, "_ZN11QHBoxLayoutC2EP7QWidget")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQHBoxLayoutFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQHBoxLayout)
    // qtrt.connect_destroyed(gothis, "QHBoxLayout")
  return vthis
}

[no_inline]
pub fn deleteQHBoxLayout(this &QHBoxLayout) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(283690874, "_ZN11QHBoxLayoutD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QHBoxLayout) freecpp() { deleteQHBoxLayout(&this) }

fn (this QHBoxLayout) free() {

  /*deleteQHBoxLayout(&this)*/

  cthis := this.get_cthis()
  //println("QHBoxLayout freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10211() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
