

module qtwidgets
// /usr/include/qt/QtWidgets/qboxlayout.h
// #include <qboxlayout.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QBoxLayout {
pub:
  QLayout
}

pub interface QBoxLayoutITF {
//    QLayoutITF
    get_cthis() voidptr
    toQBoxLayout() QBoxLayout
}
fn hotfix_QBoxLayout_itf_name_table(this QBoxLayoutITF) {
  that := QBoxLayout{}
  hotfix_QBoxLayout_itf_name_table(that)
}
pub fn (ptr QBoxLayout) toQBoxLayout() QBoxLayout { return ptr }

pub fn (this QBoxLayout) get_cthis() voidptr {
    return this.QLayout.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQBoxLayoutFromptr(cthis voidptr) QBoxLayout {
    bcthis0 := newQLayoutFromptr(cthis)
    return QBoxLayout{bcthis0}
}
pub fn (dummy QBoxLayout) newFromptr(cthis voidptr) QBoxLayout {
    return newQBoxLayoutFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QBoxLayout(QBoxLayout::Direction, QWidget *)
type T_ZN10QBoxLayoutC2ENS_9DirectionEP7QWidget = fn(cthis voidptr, arg0 int, parent voidptr) 

/*

*/
pub fn (dummy QBoxLayout) new_for_inherit_(arg0 int, parent  QWidget/*777 QWidget **/) QBoxLayout {
  //return newQBoxLayout(arg0, parent)
  return QBoxLayout{}
}
pub fn newQBoxLayout(arg0 int, parent  QWidget/*777 QWidget **/) QBoxLayout {
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN10QBoxLayoutC2ENS_9DirectionEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3198258420, "_ZN10QBoxLayoutC2ENS_9DirectionEP7QWidget")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis, arg0, conv_arg1)
    rv := cthis
    vthis := newQBoxLayoutFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQBoxLayout)
    // qtrt.connect_destroyed(gothis, "QBoxLayout")
  return vthis
}
// /usr/include/qt/QtWidgets/qboxlayout.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QBoxLayout(QBoxLayout::Direction, QWidget *)

/*

*/
pub fn (dummy QBoxLayout) new_for_inherit_p(arg0 int) QBoxLayout {
  //return newQBoxLayoutp(arg0)
  return QBoxLayout{}
}
pub fn newQBoxLayoutp(arg0 int) QBoxLayout {
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN10QBoxLayoutC2ENS_9DirectionEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3198258420, "_ZN10QBoxLayoutC2ENS_9DirectionEP7QWidget")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis, arg0, conv_arg1)
    rv := cthis
    vthis := newQBoxLayoutFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQBoxLayout)
    // qtrt.connect_destroyed(gothis, "QBoxLayout")
    return vthis
}
// /usr/include/qt/QtWidgets/qboxlayout.h:68
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QBoxLayout::Direction direction() const
type T_ZNK10QBoxLayout9directionEv = fn(cthis voidptr) int

/*

*/
pub fn (this QBoxLayout) direction() int {
    mut fnobj := T_ZNK10QBoxLayout9directionEv(0)
    fnobj = qtrt.sym_qtfunc6(2927993936, "_ZNK10QBoxLayout9directionEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setDirection(QBoxLayout::Direction)
type T_ZN10QBoxLayout12setDirectionENS_9DirectionE = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QBoxLayout) setDirection(arg0 int)  {
    mut fnobj := T_ZN10QBoxLayout12setDirectionENS_9DirectionE(0)
    fnobj = qtrt.sym_qtfunc6(1715852557, "_ZN10QBoxLayout12setDirectionENS_9DirectionE")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addSpacing(int)
type T_ZN10QBoxLayout10addSpacingEi = fn(cthis voidptr, size int) /*void*/

/*

*/
pub fn (this QBoxLayout) addSpacing(size int)  {
    mut fnobj := T_ZN10QBoxLayout10addSpacingEi(0)
    fnobj = qtrt.sym_qtfunc6(955949330, "_ZN10QBoxLayout10addSpacingEi")
    fnobj(this.get_cthis(), size)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addStretch(int)
type T_ZN10QBoxLayout10addStretchEi = fn(cthis voidptr, stretch int) /*void*/

/*

*/
pub fn (this QBoxLayout) addStretch(stretch int)  {
    mut fnobj := T_ZN10QBoxLayout10addStretchEi(0)
    fnobj = qtrt.sym_qtfunc6(1570309749, "_ZN10QBoxLayout10addStretchEi")
    fnobj(this.get_cthis(), stretch)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addStretch(int)

/*

*/
pub fn (this QBoxLayout) addStretchp()  {
    // arg: 0, int=Int, =Invalid, , Invalid
    stretch := int(0)
    mut fnobj := T_ZN10QBoxLayout10addStretchEi(0)
    fnobj = qtrt.sym_qtfunc6(1570309749, "_ZN10QBoxLayout10addStretchEi")
    fnobj(this.get_cthis(), stretch)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void addWidget(QWidget *, int, Qt::Alignment)
type T_ZN10QBoxLayout9addWidgetEP7QWidgeti6QFlagsIN2Qt13AlignmentFlagEE = fn(cthis voidptr, arg0 voidptr, stretch int, alignment int) /*void*/

/*

*/
pub fn (this QBoxLayout) addWidget(arg0  QWidget/*777 QWidget **/, stretch int, alignment int)  {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QWidget_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZN10QBoxLayout9addWidgetEP7QWidgeti6QFlagsIN2Qt13AlignmentFlagEE(0)
    fnobj = qtrt.sym_qtfunc6(821085071, "_ZN10QBoxLayout9addWidgetEP7QWidgeti6QFlagsIN2Qt13AlignmentFlagEE")
    fnobj(this.get_cthis(), conv_arg0, stretch, alignment)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void addWidget(QWidget *, int, Qt::Alignment)

/*

*/
pub fn (this QBoxLayout) addWidgetp(arg0  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QWidget_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    // arg: 1, int=Int, =Invalid, , Invalid
    stretch := int(0)
    // arg: 2, Qt::Alignment=Elaborated, Qt::Alignment=Typedef, QFlags<Qt::AlignmentFlag>, Unexposed
    alignment := 0
    mut fnobj := T_ZN10QBoxLayout9addWidgetEP7QWidgeti6QFlagsIN2Qt13AlignmentFlagEE(0)
    fnobj = qtrt.sym_qtfunc6(821085071, "_ZN10QBoxLayout9addWidgetEP7QWidgeti6QFlagsIN2Qt13AlignmentFlagEE")
    fnobj(this.get_cthis(), conv_arg0, stretch, alignment)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void addWidget(QWidget *, int, Qt::Alignment)

/*

*/
pub fn (this QBoxLayout) addWidgetp1(arg0  QWidget/*777 QWidget **/, stretch int)  {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QWidget_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    // arg: 2, Qt::Alignment=Elaborated, Qt::Alignment=Typedef, QFlags<Qt::AlignmentFlag>, Unexposed
    alignment := 0
    mut fnobj := T_ZN10QBoxLayout9addWidgetEP7QWidgeti6QFlagsIN2Qt13AlignmentFlagEE(0)
    fnobj = qtrt.sym_qtfunc6(821085071, "_ZN10QBoxLayout9addWidgetEP7QWidgeti6QFlagsIN2Qt13AlignmentFlagEE")
    fnobj(this.get_cthis(), conv_arg0, stretch, alignment)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addLayout(QLayout *, int)
type T_ZN10QBoxLayout9addLayoutEP7QLayouti = fn(cthis voidptr, layout voidptr, stretch int) /*void*/

/*

*/
pub fn (this QBoxLayout) addLayout(layout  QLayout/*777 QLayout **/, stretch int)  {
    mut conv_arg0 := voidptr(0)
    /*if layout != voidptr(0) && layout.QLayout_ptr() != voidptr(0) */ {
        // conv_arg0 = layout.QLayout_ptr().get_cthis()
      conv_arg0 = layout.get_cthis()
    }
    mut fnobj := T_ZN10QBoxLayout9addLayoutEP7QLayouti(0)
    fnobj = qtrt.sym_qtfunc6(4220267810, "_ZN10QBoxLayout9addLayoutEP7QLayouti")
    fnobj(this.get_cthis(), conv_arg0, stretch)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addLayout(QLayout *, int)

/*

*/
pub fn (this QBoxLayout) addLayoutp(layout  QLayout/*777 QLayout **/)  {
    mut conv_arg0 := voidptr(0)
    /*if layout != voidptr(0) && layout.QLayout_ptr() != voidptr(0) */ {
        // conv_arg0 = layout.QLayout_ptr().get_cthis()
      conv_arg0 = layout.get_cthis()
    }
    // arg: 1, int=Int, =Invalid, , Invalid
    stretch := int(0)
    mut fnobj := T_ZN10QBoxLayout9addLayoutEP7QLayouti(0)
    fnobj = qtrt.sym_qtfunc6(4220267810, "_ZN10QBoxLayout9addLayoutEP7QLayouti")
    fnobj(this.get_cthis(), conv_arg0, stretch)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:76
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void addStrut(int)
type T_ZN10QBoxLayout8addStrutEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QBoxLayout) addStrut(arg0 int)  {
    mut fnobj := T_ZN10QBoxLayout8addStrutEi(0)
    fnobj = qtrt.sym_qtfunc6(4240054649, "_ZN10QBoxLayout8addStrutEi")
    fnobj(this.get_cthis(), arg0)
}

[no_inline]
pub fn deleteQBoxLayout(this &QBoxLayout) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3858909359, "_ZN10QBoxLayoutD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QBoxLayout) freecpp() { deleteQBoxLayout(&this) }

fn (this QBoxLayout) free() {

  /*deleteQBoxLayout(&this)*/

  cthis := this.get_cthis()
  //println("QBoxLayout freeing ${cthis} 0 bytes")

}


/*


*/
//type QBoxLayout.Direction = int
pub enum QBoxLayoutDirection {
  LeftToRight = 0
  RightToLeft = 1
  TopToBottom = 2
  BottomToTop = 3
} // endof enum Direction

//  body block end

//  keep block begin


fn init_unused_10209() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
