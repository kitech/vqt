

module qtwidgets
// /usr/include/qt/QtWidgets/qwidget.h
// #include <qwidget.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QWidget {
pub:
  qtcore.QObject
  qtgui.QPaintDevice
}

pub interface QWidgetITF {
//    qtcore.QObjectITF
//    qtgui.QPaintDeviceITF
    get_cthis() voidptr
    toQWidget() QWidget
}
fn hotfix_QWidget_itf_name_table(this QWidgetITF) {
  that := QWidget{}
  hotfix_QWidget_itf_name_table(that)
}
pub fn (ptr QWidget) toQWidget() QWidget { return ptr }

pub fn (this QWidget) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
pub fn (this QWidget) set_cthis(cthis voidptr) {
    // this.QObject = qtcore.newQObjectFromptr(cthis)
    // this.QPaintDevice = qtgui.newQPaintDeviceFromptr(cthis)
}
[no_inline]
pub fn newQWidgetFromptr(cthis voidptr) QWidget {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    bcthis1 := qtgui.newQPaintDeviceFromptr(cthis)
    return QWidget{bcthis0, bcthis1}
}
pub fn (dummy QWidget) newFromptr(cthis voidptr) QWidget {
    return newQWidgetFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qwidget.h:215
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QWidget(QWidget *, Qt::WindowFlags)
type T_ZN7QWidgetC2EPS_6QFlagsIN2Qt10WindowTypeEE = fn(cthis voidptr, parent voidptr, f int) 

/*

*/
pub fn (dummy QWidget) new_for_inherit_(parent  QWidget/*777 QWidget **/, f int) QWidget {
  //return newQWidget(parent, f)
  return QWidget{}
}
pub fn newQWidget(parent  QWidget/*777 QWidget **/, f int) QWidget {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN7QWidgetC2EPS_6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(606334740, "_ZN7QWidgetC2EPS_6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQWidgetFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQWidget)
    // qtrt.connect_destroyed(gothis, "QWidget")
  return vthis
}
// /usr/include/qt/QtWidgets/qwidget.h:215
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QWidget(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QWidget) new_for_inherit_p() QWidget {
  //return newQWidgetp()
  return QWidget{}
}
pub fn newQWidgetp() QWidget {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN7QWidgetC2EPS_6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(606334740, "_ZN7QWidgetC2EPS_6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQWidgetFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQWidget)
    // qtrt.connect_destroyed(gothis, "QWidget")
    return vthis
}
// /usr/include/qt/QtWidgets/qwidget.h:215
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QWidget(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QWidget) new_for_inherit_p1(parent  QWidget/*777 QWidget **/) QWidget {
  //return newQWidgetp1(parent)
  return QWidget{}
}
pub fn newQWidgetp1(parent  QWidget/*777 QWidget **/) QWidget {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN7QWidgetC2EPS_6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(606334740, "_ZN7QWidgetC2EPS_6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQWidgetFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQWidget)
    // qtrt.connect_destroyed(gothis, "QWidget")
    return vthis
}
// /usr/include/qt/QtWidgets/qwidget.h:218
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [4] int devType() const
type T_ZNK7QWidget7devTypeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) devType() int {
    mut fnobj := T_ZNK7QWidget7devTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(170809957, "_ZNK7QWidget7devTypeEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:220
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] WId winId() const
type T_ZNK7QWidget5winIdEv = fn(cthis voidptr) u64

/*

*/
pub fn (this QWidget) winId() u64 {
    mut fnobj := T_ZNK7QWidget5winIdEv(0)
    fnobj = qtrt.sym_qtfunc6(2263857736, "_ZNK7QWidget5winIdEv")
    rv :=
    fnobj(this.get_cthis())
    return u64(rv) // 222
}
// /usr/include/qt/QtWidgets/qwidget.h:222
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] WId internalWinId() const
type T_ZNK7QWidget13internalWinIdEv = fn(cthis voidptr) u64

/*

*/
pub fn (this QWidget) internalWinId() u64 {
    mut fnobj := T_ZNK7QWidget13internalWinIdEv(0)
    fnobj = qtrt.sym_qtfunc6(546760316, "_ZNK7QWidget13internalWinIdEv")
    rv :=
    fnobj(this.get_cthis())
    return u64(rv) // 222
}
// /usr/include/qt/QtWidgets/qwidget.h:223
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] WId effectiveWinId() const
type T_ZNK7QWidget14effectiveWinIdEv = fn(cthis voidptr) u64

/*

*/
pub fn (this QWidget) effectiveWinId() u64 {
    mut fnobj := T_ZNK7QWidget14effectiveWinIdEv(0)
    fnobj = qtrt.sym_qtfunc6(3257125328, "_ZNK7QWidget14effectiveWinIdEv")
    rv :=
    fnobj(this.get_cthis())
    return u64(rv) // 222
}
// /usr/include/qt/QtWidgets/qwidget.h:230
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isTopLevel() const
type T_ZNK7QWidget10isTopLevelEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isTopLevel() bool {
    mut fnobj := T_ZNK7QWidget10isTopLevelEv(0)
    fnobj = qtrt.sym_qtfunc6(434601045, "_ZNK7QWidget10isTopLevelEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:231
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isWindow() const
type T_ZNK7QWidget8isWindowEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isWindow() bool {
    mut fnobj := T_ZNK7QWidget8isWindowEv(0)
    fnobj = qtrt.sym_qtfunc6(612947221, "_ZNK7QWidget8isWindowEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:233
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isModal() const
type T_ZNK7QWidget7isModalEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isModal() bool {
    mut fnobj := T_ZNK7QWidget7isModalEv(0)
    fnobj = qtrt.sym_qtfunc6(4201175838, "_ZNK7QWidget7isModalEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:234
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] Qt::WindowModality windowModality() const
type T_ZNK7QWidget14windowModalityEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) windowModality() int {
    mut fnobj := T_ZNK7QWidget14windowModalityEv(0)
    fnobj = qtrt.sym_qtfunc6(815097609, "_ZNK7QWidget14windowModalityEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:235
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWindowModality(Qt::WindowModality)
type T_ZN7QWidget17setWindowModalityEN2Qt14WindowModalityE = fn(cthis voidptr, windowModality int) /*void*/

/*

*/
pub fn (this QWidget) setWindowModality(windowModality int)  {
    mut fnobj := T_ZN7QWidget17setWindowModalityEN2Qt14WindowModalityE(0)
    fnobj = qtrt.sym_qtfunc6(2668760869, "_ZN7QWidget17setWindowModalityEN2Qt14WindowModalityE")
    fnobj(this.get_cthis(), windowModality)
}
// /usr/include/qt/QtWidgets/qwidget.h:237
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isEnabled() const
type T_ZNK7QWidget9isEnabledEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isEnabled() bool {
    mut fnobj := T_ZNK7QWidget9isEnabledEv(0)
    fnobj = qtrt.sym_qtfunc6(1166952428, "_ZNK7QWidget9isEnabledEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:238
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isEnabledTo(const QWidget *) const
type T_ZNK7QWidget11isEnabledToEPKS_ = fn(cthis voidptr, arg0 voidptr) bool

/*

*/
pub fn (this QWidget) isEnabledTo(arg0  QWidget/*777 const QWidget **/) bool {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QWidget_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZNK7QWidget11isEnabledToEPKS_(0)
    fnobj = qtrt.sym_qtfunc6(3032138607, "_ZNK7QWidget11isEnabledToEPKS_")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:245
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setEnabled(bool)
type T_ZN7QWidget10setEnabledEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QWidget) setEnabled(arg0 bool)  {
    mut fnobj := T_ZN7QWidget10setEnabledEb(0)
    fnobj = qtrt.sym_qtfunc6(1945868058, "_ZN7QWidget10setEnabledEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:246
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setDisabled(bool)
type T_ZN7QWidget11setDisabledEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QWidget) setDisabled(arg0 bool)  {
    mut fnobj := T_ZN7QWidget11setDisabledEb(0)
    fnobj = qtrt.sym_qtfunc6(4187610248, "_ZN7QWidget11setDisabledEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:247
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWindowModified(bool)
type T_ZN7QWidget17setWindowModifiedEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QWidget) setWindowModified(arg0 bool)  {
    mut fnobj := T_ZN7QWidget17setWindowModifiedEb(0)
    fnobj = qtrt.sym_qtfunc6(800400522, "_ZN7QWidget17setWindowModifiedEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:256
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int x() const
type T_ZNK7QWidget1xEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) x() int {
    mut fnobj := T_ZNK7QWidget1xEv(0)
    fnobj = qtrt.sym_qtfunc6(2310490974, "_ZNK7QWidget1xEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:257
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int y() const
type T_ZNK7QWidget1yEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) y() int {
    mut fnobj := T_ZNK7QWidget1yEv(0)
    fnobj = qtrt.sym_qtfunc6(2289377641, "_ZNK7QWidget1yEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:261
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int width() const
type T_ZNK7QWidget5widthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) width() int {
    mut fnobj := T_ZNK7QWidget5widthEv(0)
    fnobj = qtrt.sym_qtfunc6(3338959857, "_ZNK7QWidget5widthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:262
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int height() const
type T_ZNK7QWidget6heightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) height() int {
    mut fnobj := T_ZNK7QWidget6heightEv(0)
    fnobj = qtrt.sym_qtfunc6(2964560984, "_ZNK7QWidget6heightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:269
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int minimumWidth() const
type T_ZNK7QWidget12minimumWidthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) minimumWidth() int {
    mut fnobj := T_ZNK7QWidget12minimumWidthEv(0)
    fnobj = qtrt.sym_qtfunc6(828888399, "_ZNK7QWidget12minimumWidthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:270
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int minimumHeight() const
type T_ZNK7QWidget13minimumHeightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) minimumHeight() int {
    mut fnobj := T_ZNK7QWidget13minimumHeightEv(0)
    fnobj = qtrt.sym_qtfunc6(192531497, "_ZNK7QWidget13minimumHeightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:271
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int maximumWidth() const
type T_ZNK7QWidget12maximumWidthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) maximumWidth() int {
    mut fnobj := T_ZNK7QWidget12maximumWidthEv(0)
    fnobj = qtrt.sym_qtfunc6(2285981246, "_ZNK7QWidget12maximumWidthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:272
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int maximumHeight() const
type T_ZNK7QWidget13maximumHeightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) maximumHeight() int {
    mut fnobj := T_ZNK7QWidget13maximumHeightEv(0)
    fnobj = qtrt.sym_qtfunc6(750955288, "_ZNK7QWidget13maximumHeightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:274
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMinimumSize(int, int)
type T_ZN7QWidget14setMinimumSizeEii = fn(cthis voidptr, minw int, minh int) /*void*/

/*

*/
pub fn (this QWidget) setMinimumSize(minw int, minh int)  {
    mut fnobj := T_ZN7QWidget14setMinimumSizeEii(0)
    fnobj = qtrt.sym_qtfunc6(508498511, "_ZN7QWidget14setMinimumSizeEii")
    fnobj(this.get_cthis(), minw, minh)
}
// /usr/include/qt/QtWidgets/qwidget.h:276
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaximumSize(int, int)
type T_ZN7QWidget14setMaximumSizeEii = fn(cthis voidptr, maxw int, maxh int) /*void*/

/*

*/
pub fn (this QWidget) setMaximumSize(maxw int, maxh int)  {
    mut fnobj := T_ZN7QWidget14setMaximumSizeEii(0)
    fnobj = qtrt.sym_qtfunc6(2808711486, "_ZN7QWidget14setMaximumSizeEii")
    fnobj(this.get_cthis(), maxw, maxh)
}
// /usr/include/qt/QtWidgets/qwidget.h:277
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMinimumWidth(int)
type T_ZN7QWidget15setMinimumWidthEi = fn(cthis voidptr, minw int) /*void*/

/*

*/
pub fn (this QWidget) setMinimumWidth(minw int)  {
    mut fnobj := T_ZN7QWidget15setMinimumWidthEi(0)
    fnobj = qtrt.sym_qtfunc6(998451129, "_ZN7QWidget15setMinimumWidthEi")
    fnobj(this.get_cthis(), minw)
}
// /usr/include/qt/QtWidgets/qwidget.h:278
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMinimumHeight(int)
type T_ZN7QWidget16setMinimumHeightEi = fn(cthis voidptr, minh int) /*void*/

/*

*/
pub fn (this QWidget) setMinimumHeight(minh int)  {
    mut fnobj := T_ZN7QWidget16setMinimumHeightEi(0)
    fnobj = qtrt.sym_qtfunc6(314665889, "_ZN7QWidget16setMinimumHeightEi")
    fnobj(this.get_cthis(), minh)
}
// /usr/include/qt/QtWidgets/qwidget.h:279
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaximumWidth(int)
type T_ZN7QWidget15setMaximumWidthEi = fn(cthis voidptr, maxw int) /*void*/

/*

*/
pub fn (this QWidget) setMaximumWidth(maxw int)  {
    mut fnobj := T_ZN7QWidget15setMaximumWidthEi(0)
    fnobj = qtrt.sym_qtfunc6(2191899848, "_ZN7QWidget15setMaximumWidthEi")
    fnobj(this.get_cthis(), maxw)
}
// /usr/include/qt/QtWidgets/qwidget.h:280
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaximumHeight(int)
type T_ZN7QWidget16setMaximumHeightEi = fn(cthis voidptr, maxh int) /*void*/

/*

*/
pub fn (this QWidget) setMaximumHeight(maxh int)  {
    mut fnobj := T_ZN7QWidget16setMaximumHeightEi(0)
    fnobj = qtrt.sym_qtfunc6(897191056, "_ZN7QWidget16setMaximumHeightEi")
    fnobj(this.get_cthis(), maxh)
}
// /usr/include/qt/QtWidgets/qwidget.h:294
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFixedSize(int, int)
type T_ZN7QWidget12setFixedSizeEii = fn(cthis voidptr, w int, h int) /*void*/

/*

*/
pub fn (this QWidget) setFixedSize(w int, h int)  {
    mut fnobj := T_ZN7QWidget12setFixedSizeEii(0)
    fnobj = qtrt.sym_qtfunc6(1882072225, "_ZN7QWidget12setFixedSizeEii")
    fnobj(this.get_cthis(), w, h)
}
// /usr/include/qt/QtWidgets/qwidget.h:295
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFixedWidth(int)
type T_ZN7QWidget13setFixedWidthEi = fn(cthis voidptr, w int) /*void*/

/*

*/
pub fn (this QWidget) setFixedWidth(w int)  {
    mut fnobj := T_ZN7QWidget13setFixedWidthEi(0)
    fnobj = qtrt.sym_qtfunc6(349713959, "_ZN7QWidget13setFixedWidthEi")
    fnobj(this.get_cthis(), w)
}
// /usr/include/qt/QtWidgets/qwidget.h:296
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFixedHeight(int)
type T_ZN7QWidget14setFixedHeightEi = fn(cthis voidptr, h int) /*void*/

/*

*/
pub fn (this QWidget) setFixedHeight(h int)  {
    mut fnobj := T_ZN7QWidget14setFixedHeightEi(0)
    fnobj = qtrt.sym_qtfunc6(2258465809, "_ZN7QWidget14setFixedHeightEi")
    fnobj(this.get_cthis(), h)
}
// /usr/include/qt/QtWidgets/qwidget.h:332
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMouseTracking(bool)
type T_ZN7QWidget16setMouseTrackingEb = fn(cthis voidptr, enable bool) /*void*/

/*

*/
pub fn (this QWidget) setMouseTracking(enable bool)  {
    mut fnobj := T_ZN7QWidget16setMouseTrackingEb(0)
    fnobj = qtrt.sym_qtfunc6(3977244840, "_ZN7QWidget16setMouseTrackingEb")
    fnobj(this.get_cthis(), enable)
}
// /usr/include/qt/QtWidgets/qwidget.h:334
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool underMouse() const
type T_ZNK7QWidget10underMouseEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) underMouse() bool {
    mut fnobj := T_ZNK7QWidget10underMouseEv(0)
    fnobj = qtrt.sym_qtfunc6(4034935793, "_ZNK7QWidget10underMouseEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:365
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWindowTitle(const QString &)
type T_ZN7QWidget14setWindowTitleERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QWidget) setWindowTitle(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget14setWindowTitleERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3223847508, "_ZN7QWidget14setWindowTitleERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:367
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStyleSheet(const QString &)
type T_ZN7QWidget13setStyleSheetERK7QString = fn(cthis voidptr, styleSheet voidptr) /*void*/

/*

*/
pub fn (this QWidget) setStyleSheet(styleSheet string)  {
    mut tmp_arg0 := qtcore.newQString5(styleSheet)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget13setStyleSheetERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(95364179, "_ZN7QWidget13setStyleSheetERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:371
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString styleSheet() const
type T_ZNK7QWidget10styleSheetEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) styleSheet() string {
    mut fnobj := T_ZNK7QWidget10styleSheetEv(0)
    fnobj = qtrt.sym_qtfunc6(2046494658, "_ZNK7QWidget10styleSheetEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:373
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString windowTitle() const
type T_ZNK7QWidget11windowTitleEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) windowTitle() string {
    mut fnobj := T_ZNK7QWidget11windowTitleEv(0)
    fnobj = qtrt.sym_qtfunc6(2977210028, "_ZNK7QWidget11windowTitleEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:376
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWindowIconText(const QString &)
type T_ZN7QWidget17setWindowIconTextERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QWidget) setWindowIconText(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget17setWindowIconTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(4138903100, "_ZN7QWidget17setWindowIconTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:377
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString windowIconText() const
type T_ZNK7QWidget14windowIconTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) windowIconText() string {
    mut fnobj := T_ZNK7QWidget14windowIconTextEv(0)
    fnobj = qtrt.sym_qtfunc6(465509618, "_ZNK7QWidget14windowIconTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:378
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWindowRole(const QString &)
type T_ZN7QWidget13setWindowRoleERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QWidget) setWindowRole(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget13setWindowRoleERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2541344425, "_ZN7QWidget13setWindowRoleERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:379
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString windowRole() const
type T_ZNK7QWidget10windowRoleEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) windowRole() string {
    mut fnobj := T_ZNK7QWidget10windowRoleEv(0)
    fnobj = qtrt.sym_qtfunc6(1153054232, "_ZNK7QWidget10windowRoleEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:380
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWindowFilePath(const QString &)
type T_ZN7QWidget17setWindowFilePathERK7QString = fn(cthis voidptr, filePath voidptr) /*void*/

/*

*/
pub fn (this QWidget) setWindowFilePath(filePath string)  {
    mut tmp_arg0 := qtcore.newQString5(filePath)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget17setWindowFilePathERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2928768698, "_ZN7QWidget17setWindowFilePathERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:381
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString windowFilePath() const
type T_ZNK7QWidget14windowFilePathEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) windowFilePath() string {
    mut fnobj := T_ZNK7QWidget14windowFilePathEv(0)
    fnobj = qtrt.sym_qtfunc6(3270090626, "_ZNK7QWidget14windowFilePathEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:383
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWindowOpacity(qreal)
type T_ZN7QWidget16setWindowOpacityEd = fn(cthis voidptr, level f64) /*void*/

/*

*/
pub fn (this QWidget) setWindowOpacity(level f64)  {
    mut fnobj := T_ZN7QWidget16setWindowOpacityEd(0)
    fnobj = qtrt.sym_qtfunc6(2415377882, "_ZN7QWidget16setWindowOpacityEd")
    fnobj(this.get_cthis(), level)
}
// /usr/include/qt/QtWidgets/qwidget.h:384
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qreal windowOpacity() const
type T_ZNK7QWidget13windowOpacityEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QWidget) windowOpacity() f64 {
    mut fnobj := T_ZNK7QWidget13windowOpacityEv(0)
    fnobj = qtrt.sym_qtfunc6(3908986607, "_ZNK7QWidget13windowOpacityEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtWidgets/qwidget.h:386
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isWindowModified() const
type T_ZNK7QWidget16isWindowModifiedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isWindowModified() bool {
    mut fnobj := T_ZNK7QWidget16isWindowModifiedEv(0)
    fnobj = qtrt.sym_qtfunc6(2356378208, "_ZNK7QWidget16isWindowModifiedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:388
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setToolTip(const QString &)
type T_ZN7QWidget10setToolTipERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QWidget) setToolTip(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget10setToolTipERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2175679426, "_ZN7QWidget10setToolTipERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:389
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString toolTip() const
type T_ZNK7QWidget7toolTipEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) toolTip() string {
    mut fnobj := T_ZNK7QWidget7toolTipEv(0)
    fnobj = qtrt.sym_qtfunc6(602720826, "_ZNK7QWidget7toolTipEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:390
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setToolTipDuration(int)
type T_ZN7QWidget18setToolTipDurationEi = fn(cthis voidptr, msec int) /*void*/

/*

*/
pub fn (this QWidget) setToolTipDuration(msec int)  {
    mut fnobj := T_ZN7QWidget18setToolTipDurationEi(0)
    fnobj = qtrt.sym_qtfunc6(1625368137, "_ZN7QWidget18setToolTipDurationEi")
    fnobj(this.get_cthis(), msec)
}
// /usr/include/qt/QtWidgets/qwidget.h:391
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int toolTipDuration() const
type T_ZNK7QWidget15toolTipDurationEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidget) toolTipDuration() int {
    mut fnobj := T_ZNK7QWidget15toolTipDurationEv(0)
    fnobj = qtrt.sym_qtfunc6(2630369908, "_ZNK7QWidget15toolTipDurationEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qwidget.h:394
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStatusTip(const QString &)
type T_ZN7QWidget12setStatusTipERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QWidget) setStatusTip(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget12setStatusTipERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(177205134, "_ZN7QWidget12setStatusTipERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:395
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString statusTip() const
type T_ZNK7QWidget9statusTipEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) statusTip() string {
    mut fnobj := T_ZNK7QWidget9statusTipEv(0)
    fnobj = qtrt.sym_qtfunc6(1121849382, "_ZNK7QWidget9statusTipEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:398
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWhatsThis(const QString &)
type T_ZN7QWidget12setWhatsThisERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QWidget) setWhatsThis(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget12setWhatsThisERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(16871603, "_ZN7QWidget12setWhatsThisERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:399
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString whatsThis() const
type T_ZNK7QWidget9whatsThisEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QWidget) whatsThis() string {
    mut fnobj := T_ZNK7QWidget9whatsThisEv(0)
    fnobj = qtrt.sym_qtfunc6(3497781672, "_ZNK7QWidget9whatsThisEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qwidget.h:420
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setFocus()
type T_ZN7QWidget8setFocusEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) setFocus()  {
    mut fnobj := T_ZN7QWidget8setFocusEv(0)
    fnobj = qtrt.sym_qtfunc6(1428955973, "_ZN7QWidget8setFocusEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:423
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isActiveWindow() const
type T_ZNK7QWidget14isActiveWindowEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isActiveWindow() bool {
    mut fnobj := T_ZNK7QWidget14isActiveWindowEv(0)
    fnobj = qtrt.sym_qtfunc6(3080343722, "_ZNK7QWidget14isActiveWindowEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:424
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void activateWindow()
type T_ZN7QWidget14activateWindowEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) activateWindow()  {
    mut fnobj := T_ZN7QWidget14activateWindowEv(0)
    fnobj = qtrt.sym_qtfunc6(264887046, "_ZN7QWidget14activateWindowEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:425
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clearFocus()
type T_ZN7QWidget10clearFocusEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) clearFocus()  {
    mut fnobj := T_ZN7QWidget10clearFocusEv(0)
    fnobj = qtrt.sym_qtfunc6(1605758068, "_ZN7QWidget10clearFocusEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:463
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void update()
type T_ZN7QWidget6updateEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) update()  {
    mut fnobj := T_ZN7QWidget6updateEv(0)
    fnobj = qtrt.sym_qtfunc6(3733308912, "_ZN7QWidget6updateEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:467
// index:1 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void update(int, int, int, int)
type T_ZN7QWidget6updateEiiii = fn(cthis voidptr, x int, y int, w int, h int) /*void*/

/*

*/
pub fn (this QWidget) update1(x int, y int, w int, h int)  {
    mut fnobj := T_ZN7QWidget6updateEiiii(0)
    fnobj = qtrt.sym_qtfunc6(2935439564, "_ZN7QWidget6updateEiiii")
    fnobj(this.get_cthis(), x, y, w, h)
}
// /usr/include/qt/QtWidgets/qwidget.h:464
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void repaint()
type T_ZN7QWidget7repaintEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) repaint()  {
    mut fnobj := T_ZN7QWidget7repaintEv(0)
    fnobj = qtrt.sym_qtfunc6(1692267421, "_ZN7QWidget7repaintEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:478
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Ignore Visibility=Default Availability=Available
// [-2] void setVisible(bool)
type T_ZN7QWidget10setVisibleEb = fn(cthis voidptr, visible bool) /*void*/

/*

*/
pub fn (this QWidget) setVisible(visible bool)  {
    mut fnobj := T_ZN7QWidget10setVisibleEb(0)
    fnobj = qtrt.sym_qtfunc6(3037660751, "_ZN7QWidget10setVisibleEb")
    fnobj(this.get_cthis(), visible)
}
// /usr/include/qt/QtWidgets/qwidget.h:479
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setHidden(bool)
type T_ZN7QWidget9setHiddenEb = fn(cthis voidptr, hidden bool) /*void*/

/*

*/
pub fn (this QWidget) setHidden(hidden bool)  {
    mut fnobj := T_ZN7QWidget9setHiddenEb(0)
    fnobj = qtrt.sym_qtfunc6(1864783706, "_ZN7QWidget9setHiddenEb")
    fnobj(this.get_cthis(), hidden)
}
// /usr/include/qt/QtWidgets/qwidget.h:480
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void show()
type T_ZN7QWidget4showEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) show()  {
    mut fnobj := T_ZN7QWidget4showEv(0)
    fnobj = qtrt.sym_qtfunc6(3435008533, "_ZN7QWidget4showEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:481
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void hide()
type T_ZN7QWidget4hideEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) hide()  {
    mut fnobj := T_ZN7QWidget4hideEv(0)
    fnobj = qtrt.sym_qtfunc6(1349269986, "_ZN7QWidget4hideEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:483
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMinimized()
type T_ZN7QWidget13showMinimizedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) showMinimized()  {
    mut fnobj := T_ZN7QWidget13showMinimizedEv(0)
    fnobj = qtrt.sym_qtfunc6(661499765, "_ZN7QWidget13showMinimizedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:484
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMaximized()
type T_ZN7QWidget13showMaximizedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) showMaximized()  {
    mut fnobj := T_ZN7QWidget13showMaximizedEv(0)
    fnobj = qtrt.sym_qtfunc6(3948991610, "_ZN7QWidget13showMaximizedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:485
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showFullScreen()
type T_ZN7QWidget14showFullScreenEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) showFullScreen()  {
    mut fnobj := T_ZN7QWidget14showFullScreenEv(0)
    fnobj = qtrt.sym_qtfunc6(1342832539, "_ZN7QWidget14showFullScreenEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:486
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showNormal()
type T_ZN7QWidget10showNormalEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) showNormal()  {
    mut fnobj := T_ZN7QWidget10showNormalEv(0)
    fnobj = qtrt.sym_qtfunc6(1806854372, "_ZN7QWidget10showNormalEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:488
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool close()
type T_ZN7QWidget5closeEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) close() bool {
    mut fnobj := T_ZN7QWidget5closeEv(0)
    fnobj = qtrt.sym_qtfunc6(1976830908, "_ZN7QWidget5closeEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:489
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void raise()
type T_ZN7QWidget5raiseEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) raise()  {
    mut fnobj := T_ZN7QWidget5raiseEv(0)
    fnobj = qtrt.sym_qtfunc6(2275637944, "_ZN7QWidget5raiseEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:490
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void lower()
type T_ZN7QWidget5lowerEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) lower()  {
    mut fnobj := T_ZN7QWidget5lowerEv(0)
    fnobj = qtrt.sym_qtfunc6(964913166, "_ZN7QWidget5lowerEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:496
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void resize(int, int)
type T_ZN7QWidget6resizeEii = fn(cthis voidptr, w int, h int) /*void*/

/*

*/
pub fn (this QWidget) resize(w int, h int)  {
    mut fnobj := T_ZN7QWidget6resizeEii(0)
    fnobj = qtrt.sym_qtfunc6(930448380, "_ZN7QWidget6resizeEii")
    fnobj(this.get_cthis(), w, h)
}
// /usr/include/qt/QtWidgets/qwidget.h:502
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void adjustSize()
type T_ZN7QWidget10adjustSizeEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) adjustSize()  {
    mut fnobj := T_ZN7QWidget10adjustSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(2214362968, "_ZN7QWidget10adjustSizeEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:503
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isVisible() const
type T_ZNK7QWidget9isVisibleEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isVisible() bool {
    mut fnobj := T_ZNK7QWidget9isVisibleEv(0)
    fnobj = qtrt.sym_qtfunc6(2205865657, "_ZNK7QWidget9isVisibleEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:504
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isVisibleTo(const QWidget *) const
type T_ZNK7QWidget11isVisibleToEPKS_ = fn(cthis voidptr, arg0 voidptr) bool

/*

*/
pub fn (this QWidget) isVisibleTo(arg0  QWidget/*777 const QWidget **/) bool {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QWidget_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZNK7QWidget11isVisibleToEPKS_(0)
    fnobj = qtrt.sym_qtfunc6(1859694289, "_ZNK7QWidget11isVisibleToEPKS_")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:505
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isHidden() const
type T_ZNK7QWidget8isHiddenEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isHidden() bool {
    mut fnobj := T_ZNK7QWidget8isHiddenEv(0)
    fnobj = qtrt.sym_qtfunc6(1556262255, "_ZNK7QWidget8isHiddenEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:507
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isMinimized() const
type T_ZNK7QWidget11isMinimizedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isMinimized() bool {
    mut fnobj := T_ZNK7QWidget11isMinimizedEv(0)
    fnobj = qtrt.sym_qtfunc6(1936793402, "_ZNK7QWidget11isMinimizedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:508
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isMaximized() const
type T_ZNK7QWidget11isMaximizedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isMaximized() bool {
    mut fnobj := T_ZNK7QWidget11isMaximizedEv(0)
    fnobj = qtrt.sym_qtfunc6(3212600373, "_ZNK7QWidget11isMaximizedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:509
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isFullScreen() const
type T_ZNK7QWidget12isFullScreenEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidget) isFullScreen() bool {
    mut fnobj := T_ZNK7QWidget12isFullScreenEv(0)
    fnobj = qtrt.sym_qtfunc6(2278664472, "_ZNK7QWidget12isFullScreenEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qwidget.h:537
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QLayout * layout() const
type T_ZNK7QWidget6layoutEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QWidget) layout()  QLayout/*777 QLayout **/ {
    mut fnobj := T_ZNK7QWidget6layoutEv(0)
    fnobj = qtrt.sym_qtfunc6(1365471401, "_ZNK7QWidget6layoutEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQLayoutFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qwidget.h:538
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setLayout(QLayout *)
type T_ZN7QWidget9setLayoutEP7QLayout = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QWidget) setLayout(arg0  QLayout/*777 QLayout **/)  {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QLayout_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QLayout_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZN7QWidget9setLayoutEP7QLayout(0)
    fnobj = qtrt.sym_qtfunc6(1717599370, "_ZN7QWidget9setLayoutEP7QLayout")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qwidget.h:539
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void updateGeometry()
type T_ZN7QWidget14updateGeometryEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QWidget) updateGeometry()  {
    mut fnobj := T_ZN7QWidget14updateGeometryEv(0)
    fnobj = qtrt.sym_qtfunc6(2348469980, "_ZN7QWidget14updateGeometryEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qwidget.h:572
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * parentWidget() const
type T_ZNK7QWidget12parentWidgetEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QWidget) parentWidget()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK7QWidget12parentWidgetEv(0)
    fnobj = qtrt.sym_qtfunc6(2946385333, "_ZNK7QWidget12parentWidgetEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qwidget.h:612
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void windowTitleChanged(const QString &)
type T_ZN7QWidget18windowTitleChangedERK7QString = fn(cthis voidptr, title voidptr) /*void*/

/*

*/
pub fn (this QWidget) windowTitleChanged(title string)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QWidget18windowTitleChangedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3974145108, "_ZN7QWidget18windowTitleChangedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQWidget(this &QWidget) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2218150629, "_ZN7QWidgetD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QWidget) freecpp() { deleteQWidget(&this) }

fn (this QWidget) free() {

  /*deleteQWidget(&this)*/

  cthis := this.get_cthis()
  //println("QWidget freeing ${cthis} 0 bytes")

}


/*


*/
//type QWidget.RenderFlag = int
pub enum QWidgetRenderFlag {
  DrawWindowBackground = 1
  DrawChildren = 2
  IgnoreMask = 4
} // endof enum RenderFlag

//  body block end

//  keep block begin


fn init_unused_10179() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
