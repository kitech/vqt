

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qgraphicsitem.h
// #include <qgraphicsitem.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 9
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QGraphicsItem {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QGraphicsItemITF {
    get_cthis() voidptr
    toQGraphicsItem() QGraphicsItem
}
fn hotfix_QGraphicsItem_itf_name_table(this QGraphicsItemITF) {
  that := QGraphicsItem{}
  hotfix_QGraphicsItem_itf_name_table(that)
}
pub fn (ptr QGraphicsItem) toQGraphicsItem() QGraphicsItem { return ptr }

pub fn (this QGraphicsItem) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQGraphicsItemFromptr(cthis voidptr) QGraphicsItem {
    return QGraphicsItem{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QGraphicsItem) newFromptr(cthis voidptr) QGraphicsItem {
    return newQGraphicsItemFromptr(cthis)
}

[no_inline]
pub fn deleteQGraphicsItem(this &QGraphicsItem) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3272780784, "_ZN13QGraphicsItemD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QGraphicsItem) freecpp() { deleteQGraphicsItem(&this) }

fn (this QGraphicsItem) free() {

  /*deleteQGraphicsItem(&this)*/

  cthis := this.get_cthis()
  //println("QGraphicsItem freeing ${cthis} 0 bytes")

}


/*


*/
//type QGraphicsItem.GraphicsItemFlag = int
pub enum QGraphicsItemGraphicsItemFlag {
  ItemIsMovable = 1
  ItemIsSelectable = 2
  ItemIsFocusable = 4
  ItemClipsToShape = 8
  ItemClipsChildrenToShape = 16
  ItemIgnoresTransformations = 32
  ItemIgnoresParentOpacity = 64
  ItemDoesntPropagateOpacityToChildren = 128
  ItemStacksBehindParent = 256
  ItemUsesExtendedStyleOption = 512
  ItemHasNoContents = 1024
  ItemSendsGeometryChanges = 2048
  ItemAcceptsInputMethod = 4096
  ItemNegativeZStacksBehindParent = 8192
  ItemIsPanel = 16384
  ItemIsFocusScope = 32768
  ItemSendsScenePositionChanges = 65536
  ItemStopsClickFocusPropagation = 131072
  ItemStopsFocusHandling = 262144
  ItemContainsChildrenInShape = 524288
} // endof enum GraphicsItemFlag


/*


*/
//type QGraphicsItem.GraphicsItemChange = int
pub enum QGraphicsItemGraphicsItemChange {
  ItemPositionChange = 0
  ItemMatrixChange = 1
  ItemVisibleChange = 2
  ItemEnabledChange = 3
  ItemSelectedChange = 4
  ItemParentChange = 5
  ItemChildAddedChange = 6
  ItemChildRemovedChange = 7
  ItemTransformChange = 8
  ItemPositionHasChanged = 9
  ItemTransformHasChanged = 10
  ItemSceneChange = 11
  ItemVisibleHasChanged = 12
  ItemEnabledHasChanged = 13
  ItemSelectedHasChanged = 14
  ItemParentHasChanged = 15
  ItemSceneHasChanged = 16
  ItemCursorChange = 17
  ItemCursorHasChanged = 18
  ItemToolTipChange = 19
  ItemToolTipHasChanged = 20
  ItemFlagsChange = 21
  ItemFlagsHaveChanged = 22
  ItemZValueChange = 23
  ItemZValueHasChanged = 24
  ItemOpacityChange = 25
  ItemOpacityHasChanged = 26
  ItemScenePositionHasChanged = 27
  ItemRotationChange = 28
  ItemRotationHasChanged = 29
  ItemScaleChange = 30
  ItemScaleHasChanged = 31
  ItemTransformOriginPointChange = 32
  ItemTransformOriginPointHasChanged = 33
} // endof enum GraphicsItemChange


/*


*/
//type QGraphicsItem.CacheMode = int
pub enum QGraphicsItemCacheMode {
  NoCache = 0
  ItemCoordinateCache = 1
  DeviceCoordinateCache = 2
} // endof enum CacheMode


/*


*/
//type QGraphicsItem.PanelModality = int
pub enum QGraphicsItemPanelModality {
  NonModal = 0
  PanelModal = 1
  SceneModal = 2
} // endof enum PanelModality


/*


*/
//type QGraphicsItem.Extension = int
pub enum QGraphicsItemExtension {
  UserExtension = -2147483648
} // endof enum Extension

//  body block end

//  keep block begin


fn init_unused_10223() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
