

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qtabbar.h
// #include <qtabbar.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QTabBar {
pub:
  QWidget
}

pub interface QTabBarITF {
//    QWidgetITF
    get_cthis() voidptr
    toQTabBar() QTabBar
}
fn hotfix_QTabBar_itf_name_table(this QTabBarITF) {
  that := QTabBar{}
  hotfix_QTabBar_itf_name_table(that)
}
pub fn (ptr QTabBar) toQTabBar() QTabBar { return ptr }

pub fn (this QTabBar) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQTabBarFromptr(cthis voidptr) QTabBar {
    bcthis0 := newQWidgetFromptr(cthis)
    return QTabBar{bcthis0}
}
pub fn (dummy QTabBar) newFromptr(cthis voidptr) QTabBar {
    return newQTabBarFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qtabbar.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QTabBar(QWidget *)
type T_ZN7QTabBarC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QTabBar) new_for_inherit_(parent  QWidget/*777 QWidget **/) QTabBar {
  //return newQTabBar(parent)
  return QTabBar{}
}
pub fn newQTabBar(parent  QWidget/*777 QWidget **/) QTabBar {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN7QTabBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(963516406, "_ZN7QTabBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQTabBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQTabBar)
    // qtrt.connect_destroyed(gothis, "QTabBar")
  return vthis
}
// /usr/include/qt/QtWidgets/qtabbar.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QTabBar(QWidget *)

/*

*/
pub fn (dummy QTabBar) new_for_inherit_p() QTabBar {
  //return newQTabBarp()
  return QTabBar{}
}
pub fn newQTabBarp() QTabBar {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN7QTabBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(963516406, "_ZN7QTabBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQTabBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQTabBar)
    // qtrt.connect_destroyed(gothis, "QTabBar")
    return vthis
}
// /usr/include/qt/QtWidgets/qtabbar.h:96
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int addTab(const QString &)
type T_ZN7QTabBar6addTabERK7QString = fn(cthis voidptr, text voidptr) int

/*

*/
pub fn (this QTabBar) addTab(text string) int {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QTabBar6addTabERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3484311115, "_ZN7QTabBar6addTabERK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabbar.h:99
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int insertTab(int, const QString &)
type T_ZN7QTabBar9insertTabEiRK7QString = fn(cthis voidptr, index int, text voidptr) int

/*

*/
pub fn (this QTabBar) insertTab(index int, text string) int {
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut fnobj := T_ZN7QTabBar9insertTabEiRK7QString(0)
    fnobj = qtrt.sym_qtfunc6(998099698, "_ZN7QTabBar9insertTabEiRK7QString")
    rv :=
    fnobj(this.get_cthis(), index, conv_arg1)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabbar.h:102
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void removeTab(int)
type T_ZN7QTabBar9removeTabEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabBar) removeTab(index int)  {
    mut fnobj := T_ZN7QTabBar9removeTabEi(0)
    fnobj = qtrt.sym_qtfunc6(3929345163, "_ZN7QTabBar9removeTabEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabbar.h:103
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void moveTab(int, int)
type T_ZN7QTabBar7moveTabEii = fn(cthis voidptr, from int, to int) /*void*/

/*

*/
pub fn (this QTabBar) moveTab(from int, to int)  {
    mut fnobj := T_ZN7QTabBar7moveTabEii(0)
    fnobj = qtrt.sym_qtfunc6(4289803665, "_ZN7QTabBar7moveTabEii")
    fnobj(this.get_cthis(), from, to)
}
// /usr/include/qt/QtWidgets/qtabbar.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isTabEnabled(int) const
type T_ZNK7QTabBar12isTabEnabledEi = fn(cthis voidptr, index int) bool

/*

*/
pub fn (this QTabBar) isTabEnabled(index int) bool {
    mut fnobj := T_ZNK7QTabBar12isTabEnabledEi(0)
    fnobj = qtrt.sym_qtfunc6(1651733622, "_ZNK7QTabBar12isTabEnabledEi")
    rv :=
    fnobj(this.get_cthis(), index)
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabbar.h:106
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTabEnabled(int, bool)
type T_ZN7QTabBar13setTabEnabledEib = fn(cthis voidptr, index int, enabled bool) /*void*/

/*

*/
pub fn (this QTabBar) setTabEnabled(index int, enabled bool)  {
    mut fnobj := T_ZN7QTabBar13setTabEnabledEib(0)
    fnobj = qtrt.sym_qtfunc6(3388050708, "_ZN7QTabBar13setTabEnabledEib")
    fnobj(this.get_cthis(), index, enabled)
}
// /usr/include/qt/QtWidgets/qtabbar.h:108
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isTabVisible(int) const
type T_ZNK7QTabBar12isTabVisibleEi = fn(cthis voidptr, index int) bool

/*

*/
pub fn (this QTabBar) isTabVisible(index int) bool {
    mut fnobj := T_ZNK7QTabBar12isTabVisibleEi(0)
    fnobj = qtrt.sym_qtfunc6(2760369443, "_ZNK7QTabBar12isTabVisibleEi")
    rv :=
    fnobj(this.get_cthis(), index)
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabbar.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTabVisible(int, bool)
type T_ZN7QTabBar13setTabVisibleEib = fn(cthis voidptr, index int, visible bool) /*void*/

/*

*/
pub fn (this QTabBar) setTabVisible(index int, visible bool)  {
    mut fnobj := T_ZN7QTabBar13setTabVisibleEib(0)
    fnobj = qtrt.sym_qtfunc6(3526810878, "_ZN7QTabBar13setTabVisibleEib")
    fnobj(this.get_cthis(), index, visible)
}
// /usr/include/qt/QtWidgets/qtabbar.h:111
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString tabText(int) const
type T_ZNK7QTabBar7tabTextEi = fn(sretobj voidptr, cthis voidptr, index int) voidptr

/*

*/
pub fn (this QTabBar) tabText(index int) string {
    mut fnobj := T_ZNK7QTabBar7tabTextEi(0)
    fnobj = qtrt.sym_qtfunc6(438043380, "_ZNK7QTabBar7tabTextEi")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis(), index)
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qtabbar.h:112
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTabText(int, const QString &)
type T_ZN7QTabBar10setTabTextEiRK7QString = fn(cthis voidptr, index int, text voidptr) /*void*/

/*

*/
pub fn (this QTabBar) setTabText(index int, text string)  {
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut fnobj := T_ZN7QTabBar10setTabTextEiRK7QString(0)
    fnobj = qtrt.sym_qtfunc6(794153379, "_ZN7QTabBar10setTabTextEiRK7QString")
    fnobj(this.get_cthis(), index, conv_arg1)
}
// /usr/include/qt/QtWidgets/qtabbar.h:139
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int currentIndex() const
type T_ZNK7QTabBar12currentIndexEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabBar) currentIndex() int {
    mut fnobj := T_ZNK7QTabBar12currentIndexEv(0)
    fnobj = qtrt.sym_qtfunc6(4235078822, "_ZNK7QTabBar12currentIndexEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabbar.h:140
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int count() const
type T_ZNK7QTabBar5countEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabBar) count() int {
    mut fnobj := T_ZNK7QTabBar5countEv(0)
    fnobj = qtrt.sym_qtfunc6(2981223135, "_ZNK7QTabBar5countEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabbar.h:166
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isMovable() const
type T_ZNK7QTabBar9isMovableEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QTabBar) isMovable() bool {
    mut fnobj := T_ZNK7QTabBar9isMovableEv(0)
    fnobj = qtrt.sym_qtfunc6(2296874387, "_ZNK7QTabBar9isMovableEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabbar.h:167
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMovable(bool)
type T_ZN7QTabBar10setMovableEb = fn(cthis voidptr, movable bool) /*void*/

/*

*/
pub fn (this QTabBar) setMovable(movable bool)  {
    mut fnobj := T_ZN7QTabBar10setMovableEb(0)
    fnobj = qtrt.sym_qtfunc6(1730991207, "_ZN7QTabBar10setMovableEb")
    fnobj(this.get_cthis(), movable)
}
// /usr/include/qt/QtWidgets/qtabbar.h:172
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool autoHide() const
type T_ZNK7QTabBar8autoHideEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QTabBar) autoHide() bool {
    mut fnobj := T_ZNK7QTabBar8autoHideEv(0)
    fnobj = qtrt.sym_qtfunc6(3583682459, "_ZNK7QTabBar8autoHideEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabbar.h:173
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setAutoHide(bool)
type T_ZN7QTabBar11setAutoHideEb = fn(cthis voidptr, hide bool) /*void*/

/*

*/
pub fn (this QTabBar) setAutoHide(hide bool)  {
    mut fnobj := T_ZN7QTabBar11setAutoHideEb(0)
    fnobj = qtrt.sym_qtfunc6(975943713, "_ZN7QTabBar11setAutoHideEb")
    fnobj(this.get_cthis(), hide)
}
// /usr/include/qt/QtWidgets/qtabbar.h:184
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCurrentIndex(int)
type T_ZN7QTabBar15setCurrentIndexEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabBar) setCurrentIndex(index int)  {
    mut fnobj := T_ZN7QTabBar15setCurrentIndexEi(0)
    fnobj = qtrt.sym_qtfunc6(2237102313, "_ZN7QTabBar15setCurrentIndexEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabbar.h:187
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void currentChanged(int)
type T_ZN7QTabBar14currentChangedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabBar) currentChanged(index int)  {
    mut fnobj := T_ZN7QTabBar14currentChangedEi(0)
    fnobj = qtrt.sym_qtfunc6(1804836883, "_ZN7QTabBar14currentChangedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabbar.h:188
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void tabCloseRequested(int)
type T_ZN7QTabBar17tabCloseRequestedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabBar) tabCloseRequested(index int)  {
    mut fnobj := T_ZN7QTabBar17tabCloseRequestedEi(0)
    fnobj = qtrt.sym_qtfunc6(1827543677, "_ZN7QTabBar17tabCloseRequestedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabbar.h:189
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void tabMoved(int, int)
type T_ZN7QTabBar8tabMovedEii = fn(cthis voidptr, from int, to int) /*void*/

/*

*/
pub fn (this QTabBar) tabMoved(from int, to int)  {
    mut fnobj := T_ZN7QTabBar8tabMovedEii(0)
    fnobj = qtrt.sym_qtfunc6(747402176, "_ZN7QTabBar8tabMovedEii")
    fnobj(this.get_cthis(), from, to)
}
// /usr/include/qt/QtWidgets/qtabbar.h:190
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void tabBarClicked(int)
type T_ZN7QTabBar13tabBarClickedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabBar) tabBarClicked(index int)  {
    mut fnobj := T_ZN7QTabBar13tabBarClickedEi(0)
    fnobj = qtrt.sym_qtfunc6(3685740000, "_ZN7QTabBar13tabBarClickedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabbar.h:191
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void tabBarDoubleClicked(int)
type T_ZN7QTabBar19tabBarDoubleClickedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabBar) tabBarDoubleClicked(index int)  {
    mut fnobj := T_ZN7QTabBar19tabBarDoubleClickedEi(0)
    fnobj = qtrt.sym_qtfunc6(2502640271, "_ZN7QTabBar19tabBarDoubleClickedEi")
    fnobj(this.get_cthis(), index)
}

[no_inline]
pub fn deleteQTabBar(this &QTabBar) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2615344116, "_ZN7QTabBarD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QTabBar) freecpp() { deleteQTabBar(&this) }

fn (this QTabBar) free() {

  /*deleteQTabBar(&this)*/

  cthis := this.get_cthis()
  //println("QTabBar freeing ${cthis} 0 bytes")

}


/*


*/
//type QTabBar.Shape = int
pub enum QTabBarShape {
  RoundedNorth = 0
  RoundedSouth = 1
  RoundedWest = 2
  RoundedEast = 3
  TriangularNorth = 4
  TriangularSouth = 5
  TriangularWest = 6
  TriangularEast = 7
} // endof enum Shape


/*


*/
//type QTabBar.ButtonPosition = int
pub enum QTabBarButtonPosition {
  LeftSide = 0
  RightSide = 1
} // endof enum ButtonPosition


/*


*/
//type QTabBar.SelectionBehavior = int
pub enum QTabBarSelectionBehavior {
  SelectLeftTab = 0
  SelectRightTab = 1
  SelectPreviousTab = 2
} // endof enum SelectionBehavior

//  body block end

//  keep block begin


fn init_unused_10185() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
