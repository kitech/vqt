

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qpushbutton.h
// #include <qpushbutton.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 28
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QPushButton {
pub:
  QAbstractButton
}

pub interface QPushButtonITF {
//    QAbstractButtonITF
    get_cthis() voidptr
    toQPushButton() QPushButton
}
fn hotfix_QPushButton_itf_name_table(this QPushButtonITF) {
  that := QPushButton{}
  hotfix_QPushButton_itf_name_table(that)
}
pub fn (ptr QPushButton) toQPushButton() QPushButton { return ptr }

pub fn (this QPushButton) get_cthis() voidptr {
    return this.QAbstractButton.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQPushButtonFromptr(cthis voidptr) QPushButton {
    bcthis0 := newQAbstractButtonFromptr(cthis)
    return QPushButton{bcthis0}
}
pub fn (dummy QPushButton) newFromptr(cthis voidptr) QPushButton {
    return newQPushButtonFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qpushbutton.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPushButton(QWidget *)
type T_ZN11QPushButtonC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QPushButton) new_for_inherit_(parent  QWidget/*777 QWidget **/) QPushButton {
  //return newQPushButton(parent)
  return QPushButton{}
}
pub fn newQPushButton(parent  QWidget/*777 QWidget **/) QPushButton {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN11QPushButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(638151916, "_ZN11QPushButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQPushButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPushButton)
    // qtrt.connect_destroyed(gothis, "QPushButton")
  return vthis
}
// /usr/include/qt/QtWidgets/qpushbutton.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPushButton(QWidget *)

/*

*/
pub fn (dummy QPushButton) new_for_inherit_p() QPushButton {
  //return newQPushButtonp()
  return QPushButton{}
}
pub fn newQPushButtonp() QPushButton {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN11QPushButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(638151916, "_ZN11QPushButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQPushButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPushButton)
    // qtrt.connect_destroyed(gothis, "QPushButton")
    return vthis
}
// /usr/include/qt/QtWidgets/qpushbutton.h:65
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPushButton(const QString &, QWidget *)
type T_ZN11QPushButtonC2ERK7QStringP7QWidget = fn(cthis voidptr, text voidptr, parent voidptr) 

/*

*/
pub fn (dummy QPushButton) new_for_inherit_1(text string, parent  QWidget/*777 QWidget **/) QPushButton {
  //return newQPushButton1(text, parent)
  return QPushButton{}
}
pub fn newQPushButton1(text string, parent  QWidget/*777 QWidget **/) QPushButton {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN11QPushButtonC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3027368468, "_ZN11QPushButtonC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQPushButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPushButton)
    // qtrt.connect_destroyed(gothis, "QPushButton")
  return vthis
}
// /usr/include/qt/QtWidgets/qpushbutton.h:65
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPushButton(const QString &, QWidget *)

/*

*/
pub fn (dummy QPushButton) new_for_inherit_1p(text string) QPushButton {
  //return newQPushButton1p(text)
  return QPushButton{}
}
pub fn newQPushButton1p(text string) QPushButton {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN11QPushButtonC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3027368468, "_ZN11QPushButtonC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQPushButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPushButton)
    // qtrt.connect_destroyed(gothis, "QPushButton")
    return vthis
}
// /usr/include/qt/QtWidgets/qpushbutton.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize sizeHint() const
type T_ZNK11QPushButton8sizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QPushButton) sizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QPushButton8sizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(1743812697, "_ZNK11QPushButton8sizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qpushbutton.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize minimumSizeHint() const
type T_ZNK11QPushButton15minimumSizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QPushButton) minimumSizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QPushButton15minimumSizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(2291737008, "_ZNK11QPushButton15minimumSizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qpushbutton.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool autoDefault() const
type T_ZNK11QPushButton11autoDefaultEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QPushButton) autoDefault() bool {
    mut fnobj := T_ZNK11QPushButton11autoDefaultEv(0)
    fnobj = qtrt.sym_qtfunc6(627767004, "_ZNK11QPushButton11autoDefaultEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qpushbutton.h:73
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setAutoDefault(bool)
type T_ZN11QPushButton14setAutoDefaultEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QPushButton) setAutoDefault(arg0 bool)  {
    mut fnobj := T_ZN11QPushButton14setAutoDefaultEb(0)
    fnobj = qtrt.sym_qtfunc6(2921668158, "_ZN11QPushButton14setAutoDefaultEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qpushbutton.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isDefault() const
type T_ZNK11QPushButton9isDefaultEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QPushButton) isDefault() bool {
    mut fnobj := T_ZNK11QPushButton9isDefaultEv(0)
    fnobj = qtrt.sym_qtfunc6(1488522037, "_ZNK11QPushButton9isDefaultEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qpushbutton.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setDefault(bool)
type T_ZN11QPushButton10setDefaultEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QPushButton) setDefault(arg0 bool)  {
    mut fnobj := T_ZN11QPushButton10setDefaultEb(0)
    fnobj = qtrt.sym_qtfunc6(2236558823, "_ZN11QPushButton10setDefaultEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qpushbutton.h:82
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFlat(bool)
type T_ZN11QPushButton7setFlatEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QPushButton) setFlat(arg0 bool)  {
    mut fnobj := T_ZN11QPushButton7setFlatEb(0)
    fnobj = qtrt.sym_qtfunc6(167659761, "_ZN11QPushButton7setFlatEb")
    fnobj(this.get_cthis(), arg0)
}

[no_inline]
pub fn deleteQPushButton(this &QPushButton) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(29964820, "_ZN11QPushButtonD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPushButton) freecpp() { deleteQPushButton(&this) }

fn (this QPushButton) free() {

  /*deleteQPushButton(&this)*/

  cthis := this.get_cthis()
  //println("QPushButton freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10221() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
