

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qplaintextedit.h
// #include <qplaintextedit.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 12
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QPlainTextEdit {
pub:
  QAbstractScrollArea
}

pub interface QPlainTextEditITF {
//    QAbstractScrollAreaITF
    get_cthis() voidptr
    toQPlainTextEdit() QPlainTextEdit
}
fn hotfix_QPlainTextEdit_itf_name_table(this QPlainTextEditITF) {
  that := QPlainTextEdit{}
  hotfix_QPlainTextEdit_itf_name_table(that)
}
pub fn (ptr QPlainTextEdit) toQPlainTextEdit() QPlainTextEdit { return ptr }

pub fn (this QPlainTextEdit) get_cthis() voidptr {
    return this.QAbstractScrollArea.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQPlainTextEditFromptr(cthis voidptr) QPlainTextEdit {
    bcthis0 := newQAbstractScrollAreaFromptr(cthis)
    return QPlainTextEdit{bcthis0}
}
pub fn (dummy QPlainTextEdit) newFromptr(cthis voidptr) QPlainTextEdit {
    return newQPlainTextEditFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:95
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPlainTextEdit(QWidget *)
type T_ZN14QPlainTextEditC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QPlainTextEdit) new_for_inherit_(parent  QWidget/*777 QWidget **/) QPlainTextEdit {
  //return newQPlainTextEdit(parent)
  return QPlainTextEdit{}
}
pub fn newQPlainTextEdit(parent  QWidget/*777 QWidget **/) QPlainTextEdit {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN14QPlainTextEditC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(396516486, "_ZN14QPlainTextEditC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQPlainTextEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPlainTextEdit)
    // qtrt.connect_destroyed(gothis, "QPlainTextEdit")
  return vthis
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:95
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPlainTextEdit(QWidget *)

/*

*/
pub fn (dummy QPlainTextEdit) new_for_inherit_p() QPlainTextEdit {
  //return newQPlainTextEditp()
  return QPlainTextEdit{}
}
pub fn newQPlainTextEditp() QPlainTextEdit {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN14QPlainTextEditC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(396516486, "_ZN14QPlainTextEditC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQPlainTextEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPlainTextEdit)
    // qtrt.connect_destroyed(gothis, "QPlainTextEdit")
    return vthis
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:96
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPlainTextEdit(const QString &, QWidget *)
type T_ZN14QPlainTextEditC2ERK7QStringP7QWidget = fn(cthis voidptr, text voidptr, parent voidptr) 

/*

*/
pub fn (dummy QPlainTextEdit) new_for_inherit_1(text string, parent  QWidget/*777 QWidget **/) QPlainTextEdit {
  //return newQPlainTextEdit1(text, parent)
  return QPlainTextEdit{}
}
pub fn newQPlainTextEdit1(text string, parent  QWidget/*777 QWidget **/) QPlainTextEdit {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN14QPlainTextEditC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2780232515, "_ZN14QPlainTextEditC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQPlainTextEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPlainTextEdit)
    // qtrt.connect_destroyed(gothis, "QPlainTextEdit")
  return vthis
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:96
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPlainTextEdit(const QString &, QWidget *)

/*

*/
pub fn (dummy QPlainTextEdit) new_for_inherit_1p(text string) QPlainTextEdit {
  //return newQPlainTextEdit1p(text)
  return QPlainTextEdit{}
}
pub fn newQPlainTextEdit1p(text string) QPlainTextEdit {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN14QPlainTextEditC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2780232515, "_ZN14QPlainTextEditC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQPlainTextEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPlainTextEdit)
    // qtrt.connect_destroyed(gothis, "QPlainTextEdit")
    return vthis
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:102
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPlaceholderText(const QString &)
type T_ZN14QPlainTextEdit18setPlaceholderTextERK7QString = fn(cthis voidptr, placeholderText voidptr) /*void*/

/*

*/
pub fn (this QPlainTextEdit) setPlaceholderText(placeholderText string)  {
    mut tmp_arg0 := qtcore.newQString5(placeholderText)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN14QPlainTextEdit18setPlaceholderTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3191527676, "_ZN14QPlainTextEdit18setPlaceholderTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:103
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString placeholderText() const
type T_ZNK14QPlainTextEdit15placeholderTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QPlainTextEdit) placeholderText() string {
    mut fnobj := T_ZNK14QPlainTextEdit15placeholderTextEv(0)
    fnobj = qtrt.sym_qtfunc6(388573047, "_ZNK14QPlainTextEdit15placeholderTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setReadOnly(bool)
type T_ZN14QPlainTextEdit11setReadOnlyEb = fn(cthis voidptr, ro bool) /*void*/

/*

*/
pub fn (this QPlainTextEdit) setReadOnly(ro bool)  {
    mut fnobj := T_ZN14QPlainTextEdit11setReadOnlyEb(0)
    fnobj = qtrt.sym_qtfunc6(3324855670, "_ZN14QPlainTextEdit11setReadOnlyEb")
    fnobj(this.get_cthis(), ro)
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:111
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void setTextInteractionFlags(Qt::TextInteractionFlags)
type T_ZN14QPlainTextEdit23setTextInteractionFlagsE6QFlagsIN2Qt19TextInteractionFlagEE = fn(cthis voidptr, flags int) /*void*/

/*

*/
pub fn (this QPlainTextEdit) setTextInteractionFlags(flags int)  {
    mut fnobj := T_ZN14QPlainTextEdit23setTextInteractionFlagsE6QFlagsIN2Qt19TextInteractionFlagEE(0)
    fnobj = qtrt.sym_qtfunc6(400835228, "_ZN14QPlainTextEdit23setTextInteractionFlagsE6QFlagsIN2Qt19TextInteractionFlagEE")
    fnobj(this.get_cthis(), flags)
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:143
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setBackgroundVisible(bool)
type T_ZN14QPlainTextEdit20setBackgroundVisibleEb = fn(cthis voidptr, visible bool) /*void*/

/*

*/
pub fn (this QPlainTextEdit) setBackgroundVisible(visible bool)  {
    mut fnobj := T_ZN14QPlainTextEdit20setBackgroundVisibleEb(0)
    fnobj = qtrt.sym_qtfunc6(957632610, "_ZN14QPlainTextEdit20setBackgroundVisibleEb")
    fnobj(this.get_cthis(), visible)
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:157
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [8] QString toPlainText() const
type T_ZNK14QPlainTextEdit11toPlainTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QPlainTextEdit) toPlainText() string {
    mut fnobj := T_ZNK14QPlainTextEdit11toPlainTextEv(0)
    fnobj = qtrt.sym_qtfunc6(4142553692, "_ZNK14QPlainTextEdit11toPlainTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:160
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void ensureCursorVisible()
type T_ZN14QPlainTextEdit19ensureCursorVisibleEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QPlainTextEdit) ensureCursorVisible()  {
    mut fnobj := T_ZN14QPlainTextEdit19ensureCursorVisibleEv(0)
    fnobj = qtrt.sym_qtfunc6(3070872959, "_ZN14QPlainTextEdit19ensureCursorVisibleEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:203
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPlainText(const QString &)
type T_ZN14QPlainTextEdit12setPlainTextERK7QString = fn(cthis voidptr, text voidptr) /*void*/

/*

*/
pub fn (this QPlainTextEdit) setPlainText(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN14QPlainTextEdit12setPlainTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2634248857, "_ZN14QPlainTextEdit12setPlainTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qplaintextedit.h:228
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void textChanged()
type T_ZN14QPlainTextEdit11textChangedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QPlainTextEdit) textChanged()  {
    mut fnobj := T_ZN14QPlainTextEdit11textChangedEv(0)
    fnobj = qtrt.sym_qtfunc6(4250366701, "_ZN14QPlainTextEdit11textChangedEv")
    fnobj(this.get_cthis())
}

[no_inline]
pub fn deleteQPlainTextEdit(this &QPlainTextEdit) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2127496755, "_ZN14QPlainTextEditD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPlainTextEdit) freecpp() { deleteQPlainTextEdit(&this) }

fn (this QPlainTextEdit) free() {

  /*deleteQPlainTextEdit(&this)*/

  cthis := this.get_cthis()
  //println("QPlainTextEdit freeing ${cthis} 0 bytes")

}


/*


*/
//type QPlainTextEdit.LineWrapMode = int
pub enum QPlainTextEditLineWrapMode {
  NoWrap = 0
  WidgetWidth = 1
} // endof enum LineWrapMode

//  body block end

//  keep block begin


fn init_unused_10241() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
