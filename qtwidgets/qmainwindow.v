

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qmainwindow.h
// #include <qmainwindow.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QMainWindow {
pub:
  QWidget
}

pub interface QMainWindowITF {
//    QWidgetITF
    get_cthis() voidptr
    toQMainWindow() QMainWindow
}
fn hotfix_QMainWindow_itf_name_table(this QMainWindowITF) {
  that := QMainWindow{}
  hotfix_QMainWindow_itf_name_table(that)
}
pub fn (ptr QMainWindow) toQMainWindow() QMainWindow { return ptr }

pub fn (this QMainWindow) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQMainWindowFromptr(cthis voidptr) QMainWindow {
    bcthis0 := newQWidgetFromptr(cthis)
    return QMainWindow{bcthis0}
}
pub fn (dummy QMainWindow) newFromptr(cthis voidptr) QMainWindow {
    return newQMainWindowFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qmainwindow.h:94
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMainWindow(QWidget *, Qt::WindowFlags)
type T_ZN11QMainWindowC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE = fn(cthis voidptr, parent voidptr, flags int) 

/*

*/
pub fn (dummy QMainWindow) new_for_inherit_(parent  QWidget/*777 QWidget **/, flags int) QMainWindow {
  //return newQMainWindow(parent, flags)
  return QMainWindow{}
}
pub fn newQMainWindow(parent  QWidget/*777 QWidget **/, flags int) QMainWindow {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN11QMainWindowC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(1132463102, "_ZN11QMainWindowC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, flags)
    rv := cthis
    vthis := newQMainWindowFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMainWindow)
    // qtrt.connect_destroyed(gothis, "QMainWindow")
  return vthis
}
// /usr/include/qt/QtWidgets/qmainwindow.h:94
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMainWindow(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QMainWindow) new_for_inherit_p() QMainWindow {
  //return newQMainWindowp()
  return QMainWindow{}
}
pub fn newQMainWindowp() QMainWindow {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    flags := 0
    mut fnobj := T_ZN11QMainWindowC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(1132463102, "_ZN11QMainWindowC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, flags)
    rv := cthis
    vthis := newQMainWindowFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMainWindow)
    // qtrt.connect_destroyed(gothis, "QMainWindow")
    return vthis
}
// /usr/include/qt/QtWidgets/qmainwindow.h:94
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMainWindow(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QMainWindow) new_for_inherit_p1(parent  QWidget/*777 QWidget **/) QMainWindow {
  //return newQMainWindowp1(parent)
  return QMainWindow{}
}
pub fn newQMainWindowp1(parent  QWidget/*777 QWidget **/) QMainWindow {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    flags := 0
    mut fnobj := T_ZN11QMainWindowC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(1132463102, "_ZN11QMainWindowC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, flags)
    rv := cthis
    vthis := newQMainWindowFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMainWindow)
    // qtrt.connect_destroyed(gothis, "QMainWindow")
    return vthis
}
// /usr/include/qt/QtWidgets/qmainwindow.h:97
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QSize iconSize() const
type T_ZNK11QMainWindow8iconSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QMainWindow) iconSize()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QMainWindow8iconSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(69313091, "_ZNK11QMainWindow8iconSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qmainwindow.h:98
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setIconSize(const QSize &)
type T_ZN11QMainWindow11setIconSizeERK5QSize = fn(cthis voidptr, iconSize voidptr) /*void*/

/*

*/
pub fn (this QMainWindow) setIconSize(iconSize  qtcore.QSizeITF)  {
    mut conv_arg0 := voidptr(0)
    /*if iconSize != voidptr(0) && iconSize.QSize_ptr() != voidptr(0) */ {
        // conv_arg0 = iconSize.QSize_ptr().get_cthis()
      conv_arg0 = iconSize.get_cthis()
    }
    mut fnobj := T_ZN11QMainWindow11setIconSizeERK5QSize(0)
    fnobj = qtrt.sym_qtfunc6(4268406733, "_ZN11QMainWindow11setIconSizeERK5QSize")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmainwindow.h:126
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QMenuBar * menuBar() const
type T_ZNK11QMainWindow7menuBarEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMainWindow) menuBar()  QMenuBar/*777 QMenuBar **/ {
    mut fnobj := T_ZNK11QMainWindow7menuBarEv(0)
    fnobj = qtrt.sym_qtfunc6(3966983917, "_ZNK11QMainWindow7menuBarEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQMenuBarFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmainwindow.h:127
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMenuBar(QMenuBar *)
type T_ZN11QMainWindow10setMenuBarEP8QMenuBar = fn(cthis voidptr, menubar voidptr) /*void*/

/*

*/
pub fn (this QMainWindow) setMenuBar(menubar  QMenuBar/*777 QMenuBar **/)  {
    mut conv_arg0 := voidptr(0)
    /*if menubar != voidptr(0) && menubar.QMenuBar_ptr() != voidptr(0) */ {
        // conv_arg0 = menubar.QMenuBar_ptr().get_cthis()
      conv_arg0 = menubar.get_cthis()
    }
    mut fnobj := T_ZN11QMainWindow10setMenuBarEP8QMenuBar(0)
    fnobj = qtrt.sym_qtfunc6(2414944809, "_ZN11QMainWindow10setMenuBarEP8QMenuBar")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmainwindow.h:129
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * menuWidget() const
type T_ZNK11QMainWindow10menuWidgetEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMainWindow) menuWidget()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK11QMainWindow10menuWidgetEv(0)
    fnobj = qtrt.sym_qtfunc6(350857657, "_ZNK11QMainWindow10menuWidgetEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmainwindow.h:130
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMenuWidget(QWidget *)
type T_ZN11QMainWindow13setMenuWidgetEP7QWidget = fn(cthis voidptr, menubar voidptr) /*void*/

/*

*/
pub fn (this QMainWindow) setMenuWidget(menubar  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if menubar != voidptr(0) && menubar.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = menubar.QWidget_ptr().get_cthis()
      conv_arg0 = menubar.get_cthis()
    }
    mut fnobj := T_ZN11QMainWindow13setMenuWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1658462860, "_ZN11QMainWindow13setMenuWidgetEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmainwindow.h:134
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QStatusBar * statusBar() const
type T_ZNK11QMainWindow9statusBarEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMainWindow) statusBar()  QStatusBar/*777 QStatusBar **/ {
    mut fnobj := T_ZNK11QMainWindow9statusBarEv(0)
    fnobj = qtrt.sym_qtfunc6(1994287522, "_ZNK11QMainWindow9statusBarEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQStatusBarFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmainwindow.h:135
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStatusBar(QStatusBar *)
type T_ZN11QMainWindow12setStatusBarEP10QStatusBar = fn(cthis voidptr, statusbar voidptr) /*void*/

/*

*/
pub fn (this QMainWindow) setStatusBar(statusbar  QStatusBar/*777 QStatusBar **/)  {
    mut conv_arg0 := voidptr(0)
    /*if statusbar != voidptr(0) && statusbar.QStatusBar_ptr() != voidptr(0) */ {
        // conv_arg0 = statusbar.QStatusBar_ptr().get_cthis()
      conv_arg0 = statusbar.get_cthis()
    }
    mut fnobj := T_ZN11QMainWindow12setStatusBarEP10QStatusBar(0)
    fnobj = qtrt.sym_qtfunc6(2354640415, "_ZN11QMainWindow12setStatusBarEP10QStatusBar")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmainwindow.h:138
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * centralWidget() const
type T_ZNK11QMainWindow13centralWidgetEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMainWindow) centralWidget()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK11QMainWindow13centralWidgetEv(0)
    fnobj = qtrt.sym_qtfunc6(1283551286, "_ZNK11QMainWindow13centralWidgetEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmainwindow.h:139
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCentralWidget(QWidget *)
type T_ZN11QMainWindow16setCentralWidgetEP7QWidget = fn(cthis voidptr, widget voidptr) /*void*/

/*

*/
pub fn (this QMainWindow) setCentralWidget(widget  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN11QMainWindow16setCentralWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(422271864, "_ZN11QMainWindow16setCentralWidgetEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmainwindow.h:154
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QToolBar * addToolBar(const QString &)
type T_ZN11QMainWindow10addToolBarERK7QString = fn(cthis voidptr, title voidptr) voidptr/*666*/

/*

*/
pub fn (this QMainWindow) addToolBar(title string)  QToolBar/*777 QToolBar **/ {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN11QMainWindow10addToolBarERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(4007182669, "_ZN11QMainWindow10addToolBarERK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQToolBarFromptr(voidptr(rv)) // 444
}

[no_inline]
pub fn deleteQMainWindow(this &QMainWindow) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3330525862, "_ZN11QMainWindowD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMainWindow) freecpp() { deleteQMainWindow(&this) }

fn (this QMainWindow) free() {

  /*deleteQMainWindow(&this)*/

  cthis := this.get_cthis()
  //println("QMainWindow freeing ${cthis} 0 bytes")

}


/*


*/
//type QMainWindow.DockOption = int
pub enum QMainWindowDockOption {
  AnimatedDocks = 1
  AllowNestedDocks = 2
  AllowTabbedDocks = 4
  ForceTabbedDocks = 8
  VerticalTabs = 16
  GroupedDragging = 32
} // endof enum DockOption

//  body block end

//  keep block begin


fn init_unused_10235() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
