

module qtwidgets
// /usr/include/qt/QtWidgets/qboxlayout.h
// #include <qboxlayout.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 2
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QVBoxLayout {
pub:
  QBoxLayout
}

pub interface QVBoxLayoutITF {
//    QBoxLayoutITF
    get_cthis() voidptr
    toQVBoxLayout() QVBoxLayout
}
fn hotfix_QVBoxLayout_itf_name_table(this QVBoxLayoutITF) {
  that := QVBoxLayout{}
  hotfix_QVBoxLayout_itf_name_table(that)
}
pub fn (ptr QVBoxLayout) toQVBoxLayout() QVBoxLayout { return ptr }

pub fn (this QVBoxLayout) get_cthis() voidptr {
    return this.QBoxLayout.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQVBoxLayoutFromptr(cthis voidptr) QVBoxLayout {
    bcthis0 := newQBoxLayoutFromptr(cthis)
    return QVBoxLayout{bcthis0}
}
pub fn (dummy QVBoxLayout) newFromptr(cthis voidptr) QVBoxLayout {
    return newQVBoxLayoutFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qboxlayout.h:130
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVBoxLayout()
type T_ZN11QVBoxLayoutC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QVBoxLayout) new_for_inherit_() QVBoxLayout {
  //return newQVBoxLayout()
  return QVBoxLayout{}
}
pub fn newQVBoxLayout() QVBoxLayout {
    mut fnobj := T_ZN11QVBoxLayoutC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(538351151, "_ZN11QVBoxLayoutC2Ev")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis)
    rv := cthis
    vthis := newQVBoxLayoutFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVBoxLayout)
    // qtrt.connect_destroyed(gothis, "QVBoxLayout")
  return vthis
}
// /usr/include/qt/QtWidgets/qboxlayout.h:131
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVBoxLayout(QWidget *)
type T_ZN11QVBoxLayoutC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QVBoxLayout) new_for_inherit_1(parent  QWidget/*777 QWidget **/) QVBoxLayout {
  //return newQVBoxLayout1(parent)
  return QVBoxLayout{}
}
pub fn newQVBoxLayout1(parent  QWidget/*777 QWidget **/) QVBoxLayout {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN11QVBoxLayoutC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(368368412, "_ZN11QVBoxLayoutC2EP7QWidget")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQVBoxLayoutFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVBoxLayout)
    // qtrt.connect_destroyed(gothis, "QVBoxLayout")
  return vthis
}

[no_inline]
pub fn deleteQVBoxLayout(this &QVBoxLayout) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3183586966, "_ZN11QVBoxLayoutD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QVBoxLayout) freecpp() { deleteQVBoxLayout(&this) }

fn (this QVBoxLayout) free() {

  /*deleteQVBoxLayout(&this)*/

  cthis := this.get_cthis()
  //println("QVBoxLayout freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10213() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
