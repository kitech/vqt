

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qtreeview.h
// #include <qtreeview.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 3
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QTreeView {
pub:
  QAbstractItemView
}

pub interface QTreeViewITF {
//    QAbstractItemViewITF
    get_cthis() voidptr
    toQTreeView() QTreeView
}
fn hotfix_QTreeView_itf_name_table(this QTreeViewITF) {
  that := QTreeView{}
  hotfix_QTreeView_itf_name_table(that)
}
pub fn (ptr QTreeView) toQTreeView() QTreeView { return ptr }

pub fn (this QTreeView) get_cthis() voidptr {
    return this.QAbstractItemView.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQTreeViewFromptr(cthis voidptr) QTreeView {
    bcthis0 := newQAbstractItemViewFromptr(cthis)
    return QTreeView{bcthis0}
}
pub fn (dummy QTreeView) newFromptr(cthis voidptr) QTreeView {
    return newQTreeViewFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qtreeview.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QTreeView(QWidget *)
type T_ZN9QTreeViewC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QTreeView) new_for_inherit_(parent  QWidget/*777 QWidget **/) QTreeView {
  //return newQTreeView(parent)
  return QTreeView{}
}
pub fn newQTreeView(parent  QWidget/*777 QWidget **/) QTreeView {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QTreeViewC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2330190834, "_ZN9QTreeViewC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQTreeViewFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQTreeView)
    // qtrt.connect_destroyed(gothis, "QTreeView")
  return vthis
}
// /usr/include/qt/QtWidgets/qtreeview.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QTreeView(QWidget *)

/*

*/
pub fn (dummy QTreeView) new_for_inherit_p() QTreeView {
  //return newQTreeViewp()
  return QTreeView{}
}
pub fn newQTreeViewp() QTreeView {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN9QTreeViewC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2330190834, "_ZN9QTreeViewC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQTreeViewFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQTreeView)
    // qtrt.connect_destroyed(gothis, "QTreeView")
    return vthis
}
// /usr/include/qt/QtWidgets/qtreeview.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Ignore Visibility=Default Availability=Available
// [-2] void setModel(QAbstractItemModel *)
type T_ZN9QTreeView8setModelEP18QAbstractItemModel = fn(cthis voidptr, model voidptr) /*void*/

/*

*/
pub fn (this QTreeView) setModel(model qtcore.QAbstractItemModelITF/*777 QAbstractItemModel **/)  {
    mut conv_arg0 := voidptr(0)
    /*if model != voidptr(0) && model.QAbstractItemModel_ptr() != voidptr(0) */ {
        // conv_arg0 = model.QAbstractItemModel_ptr().get_cthis()
      conv_arg0 = model.get_cthis()
    }
    mut fnobj := T_ZN9QTreeView8setModelEP18QAbstractItemModel(0)
    fnobj = qtrt.sym_qtfunc6(2108085925, "_ZN9QTreeView8setModelEP18QAbstractItemModel")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qtreeview.h:155
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void hideColumn(int)
type T_ZN9QTreeView10hideColumnEi = fn(cthis voidptr, column int) /*void*/

/*

*/
pub fn (this QTreeView) hideColumn(column int)  {
    mut fnobj := T_ZN9QTreeView10hideColumnEi(0)
    fnobj = qtrt.sym_qtfunc6(3246213577, "_ZN9QTreeView10hideColumnEi")
    fnobj(this.get_cthis(), column)
}
// /usr/include/qt/QtWidgets/qtreeview.h:156
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showColumn(int)
type T_ZN9QTreeView10showColumnEi = fn(cthis voidptr, column int) /*void*/

/*

*/
pub fn (this QTreeView) showColumn(column int)  {
    mut fnobj := T_ZN9QTreeView10showColumnEi(0)
    fnobj = qtrt.sym_qtfunc6(128929531, "_ZN9QTreeView10showColumnEi")
    fnobj(this.get_cthis(), column)
}
// /usr/include/qt/QtWidgets/qtreeview.h:157
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void expand(const QModelIndex &)
type T_ZN9QTreeView6expandERK11QModelIndex = fn(cthis voidptr, index voidptr) /*void*/

/*

*/
pub fn (this QTreeView) expand(index  qtcore.QModelIndexITF)  {
    mut conv_arg0 := voidptr(0)
    /*if index != voidptr(0) && index.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = index.QModelIndex_ptr().get_cthis()
      conv_arg0 = index.get_cthis()
    }
    mut fnobj := T_ZN9QTreeView6expandERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(2194613163, "_ZN9QTreeView6expandERK11QModelIndex")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qtreeview.h:158
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void collapse(const QModelIndex &)
type T_ZN9QTreeView8collapseERK11QModelIndex = fn(cthis voidptr, index voidptr) /*void*/

/*

*/
pub fn (this QTreeView) collapse(index  qtcore.QModelIndexITF)  {
    mut conv_arg0 := voidptr(0)
    /*if index != voidptr(0) && index.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = index.QModelIndex_ptr().get_cthis()
      conv_arg0 = index.get_cthis()
    }
    mut fnobj := T_ZN9QTreeView8collapseERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1002132025, "_ZN9QTreeView8collapseERK11QModelIndex")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qtreeview.h:159
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void resizeColumnToContents(int)
type T_ZN9QTreeView22resizeColumnToContentsEi = fn(cthis voidptr, column int) /*void*/

/*

*/
pub fn (this QTreeView) resizeColumnToContents(column int)  {
    mut fnobj := T_ZN9QTreeView22resizeColumnToContentsEi(0)
    fnobj = qtrt.sym_qtfunc6(3203152354, "_ZN9QTreeView22resizeColumnToContentsEi")
    fnobj(this.get_cthis(), column)
}

[no_inline]
pub fn deleteQTreeView(this &QTreeView) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2932882339, "_ZN9QTreeViewD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QTreeView) freecpp() { deleteQTreeView(&this) }

fn (this QTreeView) free() {

  /*deleteQTreeView(&this)*/

  cthis := this.get_cthis()
  //println("QTreeView freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10261() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
