

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qlabel.h
// #include <qlabel.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 22
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QLabel {
pub:
  QFrame
}

pub interface QLabelITF {
//    QFrameITF
    get_cthis() voidptr
    toQLabel() QLabel
}
fn hotfix_QLabel_itf_name_table(this QLabelITF) {
  that := QLabel{}
  hotfix_QLabel_itf_name_table(that)
}
pub fn (ptr QLabel) toQLabel() QLabel { return ptr }

pub fn (this QLabel) get_cthis() voidptr {
    return this.QFrame.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQLabelFromptr(cthis voidptr) QLabel {
    bcthis0 := newQFrameFromptr(cthis)
    return QLabel{bcthis0}
}
pub fn (dummy QLabel) newFromptr(cthis voidptr) QLabel {
    return newQLabelFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qlabel.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLabel(QWidget *, Qt::WindowFlags)
type T_ZN6QLabelC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE = fn(cthis voidptr, parent voidptr, f int) 

/*

*/
pub fn (dummy QLabel) new_for_inherit_(parent  QWidget/*777 QWidget **/, f int) QLabel {
  //return newQLabel(parent, f)
  return QLabel{}
}
pub fn newQLabel(parent  QWidget/*777 QWidget **/, f int) QLabel {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN6QLabelC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(3619358992, "_ZN6QLabelC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQLabelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLabel)
    // qtrt.connect_destroyed(gothis, "QLabel")
  return vthis
}
// /usr/include/qt/QtWidgets/qlabel.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLabel(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QLabel) new_for_inherit_p() QLabel {
  //return newQLabelp()
  return QLabel{}
}
pub fn newQLabelp() QLabel {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN6QLabelC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(3619358992, "_ZN6QLabelC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQLabelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLabel)
    // qtrt.connect_destroyed(gothis, "QLabel")
    return vthis
}
// /usr/include/qt/QtWidgets/qlabel.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLabel(QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QLabel) new_for_inherit_p1(parent  QWidget/*777 QWidget **/) QLabel {
  //return newQLabelp1(parent)
  return QLabel{}
}
pub fn newQLabelp1(parent  QWidget/*777 QWidget **/) QLabel {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    // arg: 1, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN6QLabelC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(3619358992, "_ZN6QLabelC2EP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, f)
    rv := cthis
    vthis := newQLabelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLabel)
    // qtrt.connect_destroyed(gothis, "QLabel")
    return vthis
}
// /usr/include/qt/QtWidgets/qlabel.h:71
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLabel(const QString &, QWidget *, Qt::WindowFlags)
type T_ZN6QLabelC2ERK7QStringP7QWidget6QFlagsIN2Qt10WindowTypeEE = fn(cthis voidptr, text voidptr, parent voidptr, f int) 

/*

*/
pub fn (dummy QLabel) new_for_inherit_1(text string, parent  QWidget/*777 QWidget **/, f int) QLabel {
  //return newQLabel1(text, parent, f)
  return QLabel{}
}
pub fn newQLabel1(text string, parent  QWidget/*777 QWidget **/, f int) QLabel {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN6QLabelC2ERK7QStringP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(199160535, "_ZN6QLabelC2ERK7QStringP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1, f)
    rv := cthis
    vthis := newQLabelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLabel)
    // qtrt.connect_destroyed(gothis, "QLabel")
  return vthis
}
// /usr/include/qt/QtWidgets/qlabel.h:71
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLabel(const QString &, QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QLabel) new_for_inherit_1p(text string) QLabel {
  //return newQLabel1p(text)
  return QLabel{}
}
pub fn newQLabel1p(text string) QLabel {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    // arg: 2, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN6QLabelC2ERK7QStringP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(199160535, "_ZN6QLabelC2ERK7QStringP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1, f)
    rv := cthis
    vthis := newQLabelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLabel)
    // qtrt.connect_destroyed(gothis, "QLabel")
    return vthis
}
// /usr/include/qt/QtWidgets/qlabel.h:71
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLabel(const QString &, QWidget *, Qt::WindowFlags)

/*

*/
pub fn (dummy QLabel) new_for_inherit_1p1(text string, parent  QWidget/*777 QWidget **/) QLabel {
  //return newQLabel1p1(text, parent)
  return QLabel{}
}
pub fn newQLabel1p1(text string, parent  QWidget/*777 QWidget **/) QLabel {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    // arg: 2, Qt::WindowFlags=Elaborated, Qt::WindowFlags=Typedef, QFlags<Qt::WindowType>, Unexposed
    f := 0
    mut fnobj := T_ZN6QLabelC2ERK7QStringP7QWidget6QFlagsIN2Qt10WindowTypeEE(0)
    fnobj = qtrt.sym_qtfunc6(199160535, "_ZN6QLabelC2ERK7QStringP7QWidget6QFlagsIN2Qt10WindowTypeEE")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1, f)
    rv := cthis
    vthis := newQLabelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLabel)
    // qtrt.connect_destroyed(gothis, "QLabel")
    return vthis
}
// /usr/include/qt/QtWidgets/qlabel.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString text() const
type T_ZNK6QLabel4textEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QLabel) text() string {
    mut fnobj := T_ZNK6QLabel4textEv(0)
    fnobj = qtrt.sym_qtfunc6(1414356847, "_ZNK6QLabel4textEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qlabel.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWordWrap(bool)
type T_ZN6QLabel11setWordWrapEb = fn(cthis voidptr, on bool) /*void*/

/*

*/
pub fn (this QLabel) setWordWrap(on bool)  {
    mut fnobj := T_ZN6QLabel11setWordWrapEb(0)
    fnobj = qtrt.sym_qtfunc6(405278513, "_ZN6QLabel11setWordWrapEb")
    fnobj(this.get_cthis(), on)
}
// /usr/include/qt/QtWidgets/qlabel.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setIndent(int)
type T_ZN6QLabel9setIndentEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QLabel) setIndent(arg0 int)  {
    mut fnobj := T_ZN6QLabel9setIndentEi(0)
    fnobj = qtrt.sym_qtfunc6(3589004144, "_ZN6QLabel9setIndentEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qlabel.h:125
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setOpenExternalLinks(bool)
type T_ZN6QLabel20setOpenExternalLinksEb = fn(cthis voidptr, open bool) /*void*/

/*

*/
pub fn (this QLabel) setOpenExternalLinks(open bool)  {
    mut fnobj := T_ZN6QLabel20setOpenExternalLinksEb(0)
    fnobj = qtrt.sym_qtfunc6(496473961, "_ZN6QLabel20setOpenExternalLinksEb")
    fnobj(this.get_cthis(), open)
}
// /usr/include/qt/QtWidgets/qlabel.h:127
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void setTextInteractionFlags(Qt::TextInteractionFlags)
type T_ZN6QLabel23setTextInteractionFlagsE6QFlagsIN2Qt19TextInteractionFlagEE = fn(cthis voidptr, flags int) /*void*/

/*

*/
pub fn (this QLabel) setTextInteractionFlags(flags int)  {
    mut fnobj := T_ZN6QLabel23setTextInteractionFlagsE6QFlagsIN2Qt19TextInteractionFlagEE(0)
    fnobj = qtrt.sym_qtfunc6(844288563, "_ZN6QLabel23setTextInteractionFlagsE6QFlagsIN2Qt19TextInteractionFlagEE")
    fnobj(this.get_cthis(), flags)
}
// /usr/include/qt/QtWidgets/qlabel.h:136
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setText(const QString &)
type T_ZN6QLabel7setTextERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QLabel) setText(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN6QLabel7setTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1605932811, "_ZN6QLabel7setTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qlabel.h:137
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPixmap(const QPixmap &)
type T_ZN6QLabel9setPixmapERK7QPixmap = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QLabel) setPixmap(arg0  qtgui.QPixmapITF)  {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QPixmap_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QPixmap_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZN6QLabel9setPixmapERK7QPixmap(0)
    fnobj = qtrt.sym_qtfunc6(261774113, "_ZN6QLabel9setPixmapERK7QPixmap")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qlabel.h:144
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setNum(int)
type T_ZN6QLabel6setNumEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QLabel) setNum(arg0 int)  {
    mut fnobj := T_ZN6QLabel6setNumEi(0)
    fnobj = qtrt.sym_qtfunc6(711141672, "_ZN6QLabel6setNumEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qlabel.h:145
// index:1 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setNum(double)
type T_ZN6QLabel6setNumEd = fn(cthis voidptr, arg0 f64) /*void*/

/*

*/
pub fn (this QLabel) setNum1(arg0 f64)  {
    mut fnobj := T_ZN6QLabel6setNumEd(0)
    fnobj = qtrt.sym_qtfunc6(1423070613, "_ZN6QLabel6setNumEd")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qlabel.h:146
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clear()
type T_ZN6QLabel5clearEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QLabel) clear()  {
    mut fnobj := T_ZN6QLabel5clearEv(0)
    fnobj = qtrt.sym_qtfunc6(2401623602, "_ZN6QLabel5clearEv")
    fnobj(this.get_cthis())
}

[no_inline]
pub fn deleteQLabel(this &QLabel) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3882796990, "_ZN6QLabelD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QLabel) freecpp() { deleteQLabel(&this) }

fn (this QLabel) free() {

  /*deleteQLabel(&this)*/

  cthis := this.get_cthis()
  //println("QLabel freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10231() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
