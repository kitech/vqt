

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qspinbox.h
// #include <qspinbox.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 4
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QSpinBox {
pub:
  QAbstractSpinBox
}

pub interface QSpinBoxITF {
//    QAbstractSpinBoxITF
    get_cthis() voidptr
    toQSpinBox() QSpinBox
}
fn hotfix_QSpinBox_itf_name_table(this QSpinBoxITF) {
  that := QSpinBox{}
  hotfix_QSpinBox_itf_name_table(that)
}
pub fn (ptr QSpinBox) toQSpinBox() QSpinBox { return ptr }

pub fn (this QSpinBox) get_cthis() voidptr {
    return this.QAbstractSpinBox.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQSpinBoxFromptr(cthis voidptr) QSpinBox {
    bcthis0 := newQAbstractSpinBoxFromptr(cthis)
    return QSpinBox{bcthis0}
}
pub fn (dummy QSpinBox) newFromptr(cthis voidptr) QSpinBox {
    return newQSpinBoxFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qspinbox.h:66
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QSpinBox(QWidget *)
type T_ZN8QSpinBoxC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QSpinBox) new_for_inherit_(parent  QWidget/*777 QWidget **/) QSpinBox {
  //return newQSpinBox(parent)
  return QSpinBox{}
}
pub fn newQSpinBox(parent  QWidget/*777 QWidget **/) QSpinBox {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN8QSpinBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3685691689, "_ZN8QSpinBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQSpinBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSpinBox)
    // qtrt.connect_destroyed(gothis, "QSpinBox")
  return vthis
}
// /usr/include/qt/QtWidgets/qspinbox.h:66
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QSpinBox(QWidget *)

/*

*/
pub fn (dummy QSpinBox) new_for_inherit_p() QSpinBox {
  //return newQSpinBoxp()
  return QSpinBox{}
}
pub fn newQSpinBoxp() QSpinBox {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN8QSpinBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3685691689, "_ZN8QSpinBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQSpinBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSpinBox)
    // qtrt.connect_destroyed(gothis, "QSpinBox")
    return vthis
}
// /usr/include/qt/QtWidgets/qspinbox.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int value() const
type T_ZNK8QSpinBox5valueEv = fn(cthis voidptr) int

/*

*/
pub fn (this QSpinBox) value() int {
    mut fnobj := T_ZNK8QSpinBox5valueEv(0)
    fnobj = qtrt.sym_qtfunc6(882179790, "_ZNK8QSpinBox5valueEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qspinbox.h:77
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString cleanText() const
type T_ZNK8QSpinBox9cleanTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QSpinBox) cleanText() string {
    mut fnobj := T_ZNK8QSpinBox9cleanTextEv(0)
    fnobj = qtrt.sym_qtfunc6(1673841482, "_ZNK8QSpinBox9cleanTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qspinbox.h:80
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setSingleStep(int)
type T_ZN8QSpinBox13setSingleStepEi = fn(cthis voidptr, val int) /*void*/

/*

*/
pub fn (this QSpinBox) setSingleStep(val int)  {
    mut fnobj := T_ZN8QSpinBox13setSingleStepEi(0)
    fnobj = qtrt.sym_qtfunc6(3398301100, "_ZN8QSpinBox13setSingleStepEi")
    fnobj(this.get_cthis(), val)
}
// /usr/include/qt/QtWidgets/qspinbox.h:83
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMinimum(int)
type T_ZN8QSpinBox10setMinimumEi = fn(cthis voidptr, min int) /*void*/

/*

*/
pub fn (this QSpinBox) setMinimum(min int)  {
    mut fnobj := T_ZN8QSpinBox10setMinimumEi(0)
    fnobj = qtrt.sym_qtfunc6(4211881020, "_ZN8QSpinBox10setMinimumEi")
    fnobj(this.get_cthis(), min)
}
// /usr/include/qt/QtWidgets/qspinbox.h:86
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaximum(int)
type T_ZN8QSpinBox10setMaximumEi = fn(cthis voidptr, max int) /*void*/

/*

*/
pub fn (this QSpinBox) setMaximum(max int)  {
    mut fnobj := T_ZN8QSpinBox10setMaximumEi(0)
    fnobj = qtrt.sym_qtfunc6(753370749, "_ZN8QSpinBox10setMaximumEi")
    fnobj(this.get_cthis(), max)
}
// /usr/include/qt/QtWidgets/qspinbox.h:88
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setRange(int, int)
type T_ZN8QSpinBox8setRangeEii = fn(cthis voidptr, min int, max int) /*void*/

/*

*/
pub fn (this QSpinBox) setRange(min int, max int)  {
    mut fnobj := T_ZN8QSpinBox8setRangeEii(0)
    fnobj = qtrt.sym_qtfunc6(3910809396, "_ZN8QSpinBox8setRangeEii")
    fnobj(this.get_cthis(), min, max)
}
// /usr/include/qt/QtWidgets/qspinbox.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setValue(int)
type T_ZN8QSpinBox8setValueEi = fn(cthis voidptr, val int) /*void*/

/*

*/
pub fn (this QSpinBox) setValue(val int)  {
    mut fnobj := T_ZN8QSpinBox8setValueEi(0)
    fnobj = qtrt.sym_qtfunc6(40343854, "_ZN8QSpinBox8setValueEi")
    fnobj(this.get_cthis(), val)
}
// /usr/include/qt/QtWidgets/qspinbox.h:108
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void valueChanged(int)
type T_ZN8QSpinBox12valueChangedEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QSpinBox) valueChanged(arg0 int)  {
    mut fnobj := T_ZN8QSpinBox12valueChangedEi(0)
    fnobj = qtrt.sym_qtfunc6(924629808, "_ZN8QSpinBox12valueChangedEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qspinbox.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void textChanged(const QString &)
type T_ZN8QSpinBox11textChangedERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QSpinBox) textChanged(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN8QSpinBox11textChangedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2388599440, "_ZN8QSpinBox11textChangedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQSpinBox(this &QSpinBox) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4123635640, "_ZN8QSpinBoxD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QSpinBox) freecpp() { deleteQSpinBox(&this) }

fn (this QSpinBox) free() {

  /*deleteQSpinBox(&this)*/

  cthis := this.get_cthis()
  //println("QSpinBox freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10247() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
