

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qprogressbar.h
// #include <qprogressbar.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 11
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QProgressBar {
pub:
  QWidget
}

pub interface QProgressBarITF {
//    QWidgetITF
    get_cthis() voidptr
    toQProgressBar() QProgressBar
}
fn hotfix_QProgressBar_itf_name_table(this QProgressBarITF) {
  that := QProgressBar{}
  hotfix_QProgressBar_itf_name_table(that)
}
pub fn (ptr QProgressBar) toQProgressBar() QProgressBar { return ptr }

pub fn (this QProgressBar) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQProgressBarFromptr(cthis voidptr) QProgressBar {
    bcthis0 := newQWidgetFromptr(cthis)
    return QProgressBar{bcthis0}
}
pub fn (dummy QProgressBar) newFromptr(cthis voidptr) QProgressBar {
    return newQProgressBarFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QProgressBar(QWidget *)
type T_ZN12QProgressBarC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QProgressBar) new_for_inherit_(parent  QWidget/*777 QWidget **/) QProgressBar {
  //return newQProgressBar(parent)
  return QProgressBar{}
}
pub fn newQProgressBar(parent  QWidget/*777 QWidget **/) QProgressBar {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN12QProgressBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(572852642, "_ZN12QProgressBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQProgressBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQProgressBar)
    // qtrt.connect_destroyed(gothis, "QProgressBar")
  return vthis
}
// /usr/include/qt/QtWidgets/qprogressbar.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QProgressBar(QWidget *)

/*

*/
pub fn (dummy QProgressBar) new_for_inherit_p() QProgressBar {
  //return newQProgressBarp()
  return QProgressBar{}
}
pub fn newQProgressBarp() QProgressBar {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN12QProgressBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(572852642, "_ZN12QProgressBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQProgressBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQProgressBar)
    // qtrt.connect_destroyed(gothis, "QProgressBar")
    return vthis
}
// /usr/include/qt/QtWidgets/qprogressbar.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int minimum() const
type T_ZNK12QProgressBar7minimumEv = fn(cthis voidptr) int

/*

*/
pub fn (this QProgressBar) minimum() int {
    mut fnobj := T_ZNK12QProgressBar7minimumEv(0)
    fnobj = qtrt.sym_qtfunc6(4052484470, "_ZNK12QProgressBar7minimumEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int maximum() const
type T_ZNK12QProgressBar7maximumEv = fn(cthis voidptr) int

/*

*/
pub fn (this QProgressBar) maximum() int {
    mut fnobj := T_ZNK12QProgressBar7maximumEv(0)
    fnobj = qtrt.sym_qtfunc6(644330295, "_ZNK12QProgressBar7maximumEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:77
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int value() const
type T_ZNK12QProgressBar5valueEv = fn(cthis voidptr) int

/*

*/
pub fn (this QProgressBar) value() int {
    mut fnobj := T_ZNK12QProgressBar5valueEv(0)
    fnobj = qtrt.sym_qtfunc6(504073427, "_ZNK12QProgressBar5valueEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:79
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Indirect Visibility=Default Availability=Available
// [8] QString text() const
type T_ZNK12QProgressBar4textEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QProgressBar) text() string {
    mut fnobj := T_ZNK12QProgressBar4textEv(0)
    fnobj = qtrt.sym_qtfunc6(4022741238, "_ZNK12QProgressBar4textEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qprogressbar.h:80
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTextVisible(bool)
type T_ZN12QProgressBar14setTextVisibleEb = fn(cthis voidptr, visible bool) /*void*/

/*

*/
pub fn (this QProgressBar) setTextVisible(visible bool)  {
    mut fnobj := T_ZN12QProgressBar14setTextVisibleEb(0)
    fnobj = qtrt.sym_qtfunc6(2485447105, "_ZN12QProgressBar14setTextVisibleEb")
    fnobj(this.get_cthis(), visible)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:81
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isTextVisible() const
type T_ZNK12QProgressBar13isTextVisibleEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QProgressBar) isTextVisible() bool {
    mut fnobj := T_ZNK12QProgressBar13isTextVisibleEv(0)
    fnobj = qtrt.sym_qtfunc6(1900994530, "_ZNK12QProgressBar13isTextVisibleEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qprogressbar.h:101
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void reset()
type T_ZN12QProgressBar5resetEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QProgressBar) reset()  {
    mut fnobj := T_ZN12QProgressBar5resetEv(0)
    fnobj = qtrt.sym_qtfunc6(2829967089, "_ZN12QProgressBar5resetEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qprogressbar.h:102
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setRange(int, int)
type T_ZN12QProgressBar8setRangeEii = fn(cthis voidptr, minimum int, maximum int) /*void*/

/*

*/
pub fn (this QProgressBar) setRange(minimum int, maximum int)  {
    mut fnobj := T_ZN12QProgressBar8setRangeEii(0)
    fnobj = qtrt.sym_qtfunc6(277876671, "_ZN12QProgressBar8setRangeEii")
    fnobj(this.get_cthis(), minimum, maximum)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:103
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMinimum(int)
type T_ZN12QProgressBar10setMinimumEi = fn(cthis voidptr, minimum int) /*void*/

/*

*/
pub fn (this QProgressBar) setMinimum(minimum int)  {
    mut fnobj := T_ZN12QProgressBar10setMinimumEi(0)
    fnobj = qtrt.sym_qtfunc6(192492712, "_ZN12QProgressBar10setMinimumEi")
    fnobj(this.get_cthis(), minimum)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:104
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaximum(int)
type T_ZN12QProgressBar10setMaximumEi = fn(cthis voidptr, maximum int) /*void*/

/*

*/
pub fn (this QProgressBar) setMaximum(maximum int)  {
    mut fnobj := T_ZN12QProgressBar10setMaximumEi(0)
    fnobj = qtrt.sym_qtfunc6(3700590313, "_ZN12QProgressBar10setMaximumEi")
    fnobj(this.get_cthis(), maximum)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setValue(int)
type T_ZN12QProgressBar8setValueEi = fn(cthis voidptr, value int) /*void*/

/*

*/
pub fn (this QProgressBar) setValue(value int)  {
    mut fnobj := T_ZN12QProgressBar8setValueEi(0)
    fnobj = qtrt.sym_qtfunc6(830504370, "_ZN12QProgressBar8setValueEi")
    fnobj(this.get_cthis(), value)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:106
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setOrientation(Qt::Orientation)
type T_ZN12QProgressBar14setOrientationEN2Qt11OrientationE = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QProgressBar) setOrientation(arg0 int)  {
    mut fnobj := T_ZN12QProgressBar14setOrientationEN2Qt11OrientationE(0)
    fnobj = qtrt.sym_qtfunc6(2370329577, "_ZN12QProgressBar14setOrientationEN2Qt11OrientationE")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qprogressbar.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void valueChanged(int)
type T_ZN12QProgressBar12valueChangedEi = fn(cthis voidptr, value int) /*void*/

/*

*/
pub fn (this QProgressBar) valueChanged(value int)  {
    mut fnobj := T_ZN12QProgressBar12valueChangedEi(0)
    fnobj = qtrt.sym_qtfunc6(573637723, "_ZN12QProgressBar12valueChangedEi")
    fnobj(this.get_cthis(), value)
}

[no_inline]
pub fn deleteQProgressBar(this &QProgressBar) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3766336249, "_ZN12QProgressBarD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QProgressBar) freecpp() { deleteQProgressBar(&this) }

fn (this QProgressBar) free() {

  /*deleteQProgressBar(&this)*/

  cthis := this.get_cthis()
  //println("QProgressBar freeing ${cthis} 0 bytes")

}


/*


*/
//type QProgressBar.Direction = int
pub enum QProgressBarDirection {
  TopToBottom = 0
  BottomToTop = 1
} // endof enum Direction

//  body block end

//  keep block begin


fn init_unused_10243() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
