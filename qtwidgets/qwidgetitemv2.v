

module qtwidgets
// /usr/include/qt/QtWidgets/qlayoutitem.h
// #include <qlayoutitem.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 8
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QWidgetItemV2 {
pub:
  QWidgetItem
}

pub interface QWidgetItemV2ITF {
//    QWidgetItemITF
    get_cthis() voidptr
    toQWidgetItemV2() QWidgetItemV2
}
fn hotfix_QWidgetItemV2_itf_name_table(this QWidgetItemV2ITF) {
  that := QWidgetItemV2{}
  hotfix_QWidgetItemV2_itf_name_table(that)
}
pub fn (ptr QWidgetItemV2) toQWidgetItemV2() QWidgetItemV2 { return ptr }

pub fn (this QWidgetItemV2) get_cthis() voidptr {
    return this.QWidgetItem.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQWidgetItemV2Fromptr(cthis voidptr) QWidgetItemV2 {
    bcthis0 := newQWidgetItemFromptr(cthis)
    return QWidgetItemV2{bcthis0}
}
pub fn (dummy QWidgetItemV2) newFromptr(cthis voidptr) QWidgetItemV2 {
    return newQWidgetItemV2Fromptr(cthis)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:156
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QWidgetItemV2(QWidget *)
type T_ZN13QWidgetItemV2C2EP7QWidget = fn(cthis voidptr, widget voidptr) 

/*

*/
pub fn (dummy QWidgetItemV2) new_for_inherit_(widget  QWidget/*777 QWidget **/) QWidgetItemV2 {
  //return newQWidgetItemV2(widget)
  return QWidgetItemV2{}
}
pub fn newQWidgetItemV2(widget  QWidget/*777 QWidget **/) QWidgetItemV2 {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN13QWidgetItemV2C2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1692828260, "_ZN13QWidgetItemV2C2EP7QWidget")
    mut cthis := qtrt.mallocraw(88)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQWidgetItemV2Fromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQWidgetItemV2)
  return vthis
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:159
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize sizeHint() const
type T_ZNK13QWidgetItemV28sizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWidgetItemV2) sizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK13QWidgetItemV28sizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(3325180898, "_ZNK13QWidgetItemV28sizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:160
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize minimumSize() const
type T_ZNK13QWidgetItemV211minimumSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWidgetItemV2) minimumSize()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK13QWidgetItemV211minimumSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(3287127018, "_ZNK13QWidgetItemV211minimumSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:161
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize maximumSize() const
type T_ZNK13QWidgetItemV211maximumSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWidgetItemV2) maximumSize()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK13QWidgetItemV211maximumSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(2208492444, "_ZNK13QWidgetItemV211maximumSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:162
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [4] int heightForWidth(int) const
type T_ZNK13QWidgetItemV214heightForWidthEi = fn(cthis voidptr, width int) int

/*

*/
pub fn (this QWidgetItemV2) heightForWidth(width int) int {
    mut fnobj := T_ZNK13QWidgetItemV214heightForWidthEi(0)
    fnobj = qtrt.sym_qtfunc6(4230616517, "_ZNK13QWidgetItemV214heightForWidthEi")
    rv :=
    fnobj(this.get_cthis(), width)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}

[no_inline]
pub fn deleteQWidgetItemV2(this &QWidgetItemV2) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1368758239, "_ZN13QWidgetItemV2D2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QWidgetItemV2) freecpp() { deleteQWidgetItemV2(&this) }

fn (this QWidgetItemV2) free() {

  /*deleteQWidgetItemV2(&this)*/

  cthis := this.get_cthis()
  //println("QWidgetItemV2 freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10205() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
