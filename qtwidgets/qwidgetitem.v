

module qtwidgets
// /usr/include/qt/QtWidgets/qlayoutitem.h
// #include <qlayoutitem.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 7
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QWidgetItem {
pub:
  QLayoutItem
}

pub interface QWidgetItemITF {
//    QLayoutItemITF
    get_cthis() voidptr
    toQWidgetItem() QWidgetItem
}
fn hotfix_QWidgetItem_itf_name_table(this QWidgetItemITF) {
  that := QWidgetItem{}
  hotfix_QWidgetItem_itf_name_table(that)
}
pub fn (ptr QWidgetItem) toQWidgetItem() QWidgetItem { return ptr }

pub fn (this QWidgetItem) get_cthis() voidptr {
    return this.QLayoutItem.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQWidgetItemFromptr(cthis voidptr) QWidgetItem {
    bcthis0 := newQLayoutItemFromptr(cthis)
    return QWidgetItem{bcthis0}
}
pub fn (dummy QWidgetItem) newFromptr(cthis voidptr) QWidgetItem {
    return newQWidgetItemFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:130
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QWidgetItem(QWidget *)
type T_ZN11QWidgetItemC2EP7QWidget = fn(cthis voidptr, w voidptr) 

/*

*/
pub fn (dummy QWidgetItem) new_for_inherit_(w  QWidget/*777 QWidget **/) QWidgetItem {
  //return newQWidgetItem(w)
  return QWidgetItem{}
}
pub fn newQWidgetItem(w  QWidget/*777 QWidget **/) QWidgetItem {
    mut conv_arg0 := voidptr(0)
    /*if w != voidptr(0) && w.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = w.QWidget_ptr().get_cthis()
      conv_arg0 = w.get_cthis()
    }
    mut fnobj := T_ZN11QWidgetItemC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3510836749, "_ZN11QWidgetItemC2EP7QWidget")
    mut cthis := qtrt.mallocraw(24)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQWidgetItemFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQWidgetItem)
  return vthis
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:133
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize sizeHint() const
type T_ZNK11QWidgetItem8sizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWidgetItem) sizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QWidgetItem8sizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(1339409613, "_ZNK11QWidgetItem8sizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:134
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize minimumSize() const
type T_ZNK11QWidgetItem11minimumSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWidgetItem) minimumSize()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QWidgetItem11minimumSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(8341184, "_ZNK11QWidgetItem11minimumSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:135
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize maximumSize() const
type T_ZNK11QWidgetItem11maximumSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWidgetItem) maximumSize()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QWidgetItem11maximumSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(1076903606, "_ZNK11QWidgetItem11maximumSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:136
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Visibility=Default Availability=Available
// [4] Qt::Orientations expandingDirections() const
type T_ZNK11QWidgetItem19expandingDirectionsEv = fn(cthis voidptr) int

/*

*/
pub fn (this QWidgetItem) expandingDirections() int {
    mut fnobj := T_ZNK11QWidgetItem19expandingDirectionsEv(0)
    fnobj = qtrt.sym_qtfunc6(531281421, "_ZNK11QWidgetItem19expandingDirectionsEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:137
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool isEmpty() const
type T_ZNK11QWidgetItem7isEmptyEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWidgetItem) isEmpty() bool {
    mut fnobj := T_ZNK11QWidgetItem7isEmptyEv(0)
    fnobj = qtrt.sym_qtfunc6(3246172656, "_ZNK11QWidgetItem7isEmptyEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:141
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QWidget * widget()
type T_ZN11QWidgetItem6widgetEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QWidgetItem) widget()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZN11QWidgetItem6widgetEv(0)
    fnobj = qtrt.sym_qtfunc6(4104848494, "_ZN11QWidgetItem6widgetEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:147
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [4] int heightForWidth(int) const
type T_ZNK11QWidgetItem14heightForWidthEi = fn(cthis voidptr, arg0 int) int

/*

*/
pub fn (this QWidgetItem) heightForWidth(arg0 int) int {
    mut fnobj := T_ZNK11QWidgetItem14heightForWidthEi(0)
    fnobj = qtrt.sym_qtfunc6(733877506, "_ZNK11QWidgetItem14heightForWidthEi")
    rv :=
    fnobj(this.get_cthis(), arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}

[no_inline]
pub fn deleteQWidgetItem(this &QWidgetItem) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3821563056, "_ZN11QWidgetItemD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QWidgetItem) freecpp() { deleteQWidgetItem(&this) }

fn (this QWidgetItem) free() {

  /*deleteQWidgetItem(&this)*/

  cthis := this.get_cthis()
  //println("QWidgetItem freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10203() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
