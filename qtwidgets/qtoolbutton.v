

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qtoolbutton.h
// #include <qtoolbutton.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 4
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QToolButton {
pub:
  QAbstractButton
}

pub interface QToolButtonITF {
//    QAbstractButtonITF
    get_cthis() voidptr
    toQToolButton() QToolButton
}
fn hotfix_QToolButton_itf_name_table(this QToolButtonITF) {
  that := QToolButton{}
  hotfix_QToolButton_itf_name_table(that)
}
pub fn (ptr QToolButton) toQToolButton() QToolButton { return ptr }

pub fn (this QToolButton) get_cthis() voidptr {
    return this.QAbstractButton.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQToolButtonFromptr(cthis voidptr) QToolButton {
    bcthis0 := newQAbstractButtonFromptr(cthis)
    return QToolButton{bcthis0}
}
pub fn (dummy QToolButton) newFromptr(cthis voidptr) QToolButton {
    return newQToolButtonFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qtoolbutton.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QToolButton(QWidget *)
type T_ZN11QToolButtonC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QToolButton) new_for_inherit_(parent  QWidget/*777 QWidget **/) QToolButton {
  //return newQToolButton(parent)
  return QToolButton{}
}
pub fn newQToolButton(parent  QWidget/*777 QWidget **/) QToolButton {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN11QToolButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(719486503, "_ZN11QToolButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQToolButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQToolButton)
    // qtrt.connect_destroyed(gothis, "QToolButton")
  return vthis
}
// /usr/include/qt/QtWidgets/qtoolbutton.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QToolButton(QWidget *)

/*

*/
pub fn (dummy QToolButton) new_for_inherit_p() QToolButton {
  //return newQToolButtonp()
  return QToolButton{}
}
pub fn newQToolButtonp() QToolButton {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN11QToolButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(719486503, "_ZN11QToolButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQToolButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQToolButton)
    // qtrt.connect_destroyed(gothis, "QToolButton")
    return vthis
}
// /usr/include/qt/QtWidgets/qtoolbutton.h:95
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setAutoRaise(bool)
type T_ZN11QToolButton12setAutoRaiseEb = fn(cthis voidptr, enable bool) /*void*/

/*

*/
pub fn (this QToolButton) setAutoRaise(enable bool)  {
    mut fnobj := T_ZN11QToolButton12setAutoRaiseEb(0)
    fnobj = qtrt.sym_qtfunc6(2791407414, "_ZN11QToolButton12setAutoRaiseEb")
    fnobj(this.get_cthis(), enable)
}
// /usr/include/qt/QtWidgets/qtoolbutton.h:106
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void triggered(QAction *)
type T_ZN11QToolButton9triggeredEP7QAction = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QToolButton) triggered(arg0  QAction/*777 QAction **/)  {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QAction_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QAction_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZN11QToolButton9triggeredEP7QAction(0)
    fnobj = qtrt.sym_qtfunc6(784990465, "_ZN11QToolButton9triggeredEP7QAction")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQToolButton(this &QToolButton) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(804693516, "_ZN11QToolButtonD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QToolButton) freecpp() { deleteQToolButton(&this) }

fn (this QToolButton) free() {

  /*deleteQToolButton(&this)*/

  cthis := this.get_cthis()
  //println("QToolButton freeing ${cthis} 0 bytes")

}


/*


*/
//type QToolButton.ToolButtonPopupMode = int
pub enum QToolButtonToolButtonPopupMode {
  DelayedPopup = 0
  MenuButtonPopup = 1
  InstantPopup = 2
} // endof enum ToolButtonPopupMode

//  body block end

//  keep block begin


fn init_unused_10259() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
