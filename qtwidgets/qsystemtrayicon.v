

module qtwidgets
// /usr/include/qt/QtWidgets/qsystemtrayicon.h
// #include <qsystemtrayicon.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 12
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QSystemTrayIcon {
pub:
  qtcore.QObject
}

pub interface QSystemTrayIconITF {
//    qtcore.QObjectITF
    get_cthis() voidptr
    toQSystemTrayIcon() QSystemTrayIcon
}
fn hotfix_QSystemTrayIcon_itf_name_table(this QSystemTrayIconITF) {
  that := QSystemTrayIcon{}
  hotfix_QSystemTrayIcon_itf_name_table(that)
}
pub fn (ptr QSystemTrayIcon) toQSystemTrayIcon() QSystemTrayIcon { return ptr }

pub fn (this QSystemTrayIcon) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQSystemTrayIconFromptr(cthis voidptr) QSystemTrayIcon {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    return QSystemTrayIcon{bcthis0}
}
pub fn (dummy QSystemTrayIcon) newFromptr(cthis voidptr) QSystemTrayIcon {
    return newQSystemTrayIconFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QSystemTrayIcon(QObject *)
type T_ZN15QSystemTrayIconC2EP7QObject = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QSystemTrayIcon) new_for_inherit_(parent qtcore.QObjectITF/*777 QObject **/) QSystemTrayIcon {
  //return newQSystemTrayIcon(parent)
  return QSystemTrayIcon{}
}
pub fn newQSystemTrayIcon(parent qtcore.QObjectITF/*777 QObject **/) QSystemTrayIcon {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QObject_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN15QSystemTrayIconC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(3284962045, "_ZN15QSystemTrayIconC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQSystemTrayIconFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSystemTrayIcon)
    // qtrt.connect_destroyed(gothis, "QSystemTrayIcon")
  return vthis
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QSystemTrayIcon(QObject *)

/*

*/
pub fn (dummy QSystemTrayIcon) new_for_inherit_p() QSystemTrayIcon {
  //return newQSystemTrayIconp()
  return QSystemTrayIcon{}
}
pub fn newQSystemTrayIconp() QSystemTrayIcon {
    // arg: 0, QObject *=Pointer, QObject=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN15QSystemTrayIconC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(3284962045, "_ZN15QSystemTrayIconC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQSystemTrayIconFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSystemTrayIcon)
    // qtrt.connect_destroyed(gothis, "QSystemTrayIcon")
    return vthis
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:82
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setContextMenu(QMenu *)
type T_ZN15QSystemTrayIcon14setContextMenuEP5QMenu = fn(cthis voidptr, menu voidptr) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) setContextMenu(menu  QMenu/*777 QMenu **/)  {
    mut conv_arg0 := voidptr(0)
    /*if menu != voidptr(0) && menu.QMenu_ptr() != voidptr(0) */ {
        // conv_arg0 = menu.QMenu_ptr().get_cthis()
      conv_arg0 = menu.get_cthis()
    }
    mut fnobj := T_ZN15QSystemTrayIcon14setContextMenuEP5QMenu(0)
    fnobj = qtrt.sym_qtfunc6(2313850013, "_ZN15QSystemTrayIcon14setContextMenuEP5QMenu")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:83
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QMenu * contextMenu() const
type T_ZNK15QSystemTrayIcon11contextMenuEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QSystemTrayIcon) contextMenu()  QMenu/*777 QMenu **/ {
    mut fnobj := T_ZNK15QSystemTrayIcon11contextMenuEv(0)
    fnobj = qtrt.sym_qtfunc6(1821247260, "_ZNK15QSystemTrayIcon11contextMenuEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQMenuFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:86
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QIcon icon() const
type T_ZNK15QSystemTrayIcon4iconEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QSystemTrayIcon) icon()  qtgui.QIcon/*123*/ {
    mut fnobj := T_ZNK15QSystemTrayIcon4iconEv(0)
    fnobj = qtrt.sym_qtfunc6(2914842153, "_ZNK15QSystemTrayIcon4iconEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtgui.newQIconFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtgui.deleteQIcon)
    return rv2
    //return qtgui.QIcon{rv}
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:87
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setIcon(const QIcon &)
type T_ZN15QSystemTrayIcon7setIconERK5QIcon = fn(cthis voidptr, icon voidptr) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) setIcon(icon  qtgui.QIconITF)  {
    mut conv_arg0 := voidptr(0)
    /*if icon != voidptr(0) && icon.QIcon_ptr() != voidptr(0) */ {
        // conv_arg0 = icon.QIcon_ptr().get_cthis()
      conv_arg0 = icon.get_cthis()
    }
    mut fnobj := T_ZN15QSystemTrayIcon7setIconERK5QIcon(0)
    fnobj = qtrt.sym_qtfunc6(1753229533, "_ZN15QSystemTrayIcon7setIconERK5QIcon")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:89
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString toolTip() const
type T_ZNK15QSystemTrayIcon7toolTipEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QSystemTrayIcon) toolTip() string {
    mut fnobj := T_ZNK15QSystemTrayIcon7toolTipEv(0)
    fnobj = qtrt.sym_qtfunc6(3511699919, "_ZNK15QSystemTrayIcon7toolTipEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:90
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setToolTip(const QString &)
type T_ZN15QSystemTrayIcon10setToolTipERK7QString = fn(cthis voidptr, tip voidptr) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) setToolTip(tip string)  {
    mut tmp_arg0 := qtcore.newQString5(tip)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN15QSystemTrayIcon10setToolTipERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(677619068, "_ZN15QSystemTrayIcon10setToolTipERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:92
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Extend Visibility=Default Availability=Available
// [1] bool isSystemTrayAvailable()
type T_ZN15QSystemTrayIcon21isSystemTrayAvailableEv = fn() bool

/*

*/
pub fn (this QSystemTrayIcon) isSystemTrayAvailable() bool {
    mut fnobj := T_ZN15QSystemTrayIcon21isSystemTrayAvailableEv(0)
    fnobj = qtrt.sym_qtfunc6(2178589267, "_ZN15QSystemTrayIcon21isSystemTrayAvailableEv")
    rv :=
    fnobj()
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:93
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Extend Visibility=Default Availability=Available
// [1] bool supportsMessages()
type T_ZN15QSystemTrayIcon16supportsMessagesEv = fn() bool

/*

*/
pub fn (this QSystemTrayIcon) supportsMessages() bool {
    mut fnobj := T_ZN15QSystemTrayIcon16supportsMessagesEv(0)
    fnobj = qtrt.sym_qtfunc6(85488849, "_ZN15QSystemTrayIcon16supportsMessagesEv")
    rv :=
    fnobj()
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:98
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isVisible() const
type T_ZNK15QSystemTrayIcon9isVisibleEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QSystemTrayIcon) isVisible() bool {
    mut fnobj := T_ZNK15QSystemTrayIcon9isVisibleEv(0)
    fnobj = qtrt.sym_qtfunc6(2650858343, "_ZNK15QSystemTrayIcon9isVisibleEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:101
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setVisible(bool)
type T_ZN15QSystemTrayIcon10setVisibleEb = fn(cthis voidptr, visible bool) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) setVisible(visible bool)  {
    mut fnobj := T_ZN15QSystemTrayIcon10setVisibleEb(0)
    fnobj = qtrt.sym_qtfunc6(2484040638, "_ZN15QSystemTrayIcon10setVisibleEb")
    fnobj(this.get_cthis(), visible)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:102
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void show()
type T_ZN15QSystemTrayIcon4showEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) show()  {
    mut fnobj := T_ZN15QSystemTrayIcon4showEv(0)
    fnobj = qtrt.sym_qtfunc6(1600631068, "_ZN15QSystemTrayIcon4showEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:103
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void hide()
type T_ZN15QSystemTrayIcon4hideEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) hide()  {
    mut fnobj := T_ZN15QSystemTrayIcon4hideEv(0)
    fnobj = qtrt.sym_qtfunc6(3283458795, "_ZN15QSystemTrayIcon4hideEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:104
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMessage(const QString &, const QString &, const QIcon &, int)
type T_ZN15QSystemTrayIcon11showMessageERK7QStringS2_RK5QIconi = fn(cthis voidptr, title voidptr, msg voidptr, icon voidptr, msecs int) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) showMessage(title string, msg string, icon  qtgui.QIconITF, msecs int)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut tmp_arg1 := qtcore.newQString5(msg)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut conv_arg2 := voidptr(0)
    /*if icon != voidptr(0) && icon.QIcon_ptr() != voidptr(0) */ {
        // conv_arg2 = icon.QIcon_ptr().get_cthis()
      conv_arg2 = icon.get_cthis()
    }
    mut fnobj := T_ZN15QSystemTrayIcon11showMessageERK7QStringS2_RK5QIconi(0)
    fnobj = qtrt.sym_qtfunc6(1080679435, "_ZN15QSystemTrayIcon11showMessageERK7QStringS2_RK5QIconi")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, conv_arg2, msecs)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:104
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMessage(const QString &, const QString &, const QIcon &, int)

/*

*/
pub fn (this QSystemTrayIcon) showMessagep(title string, msg string, icon  qtgui.QIconITF)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut tmp_arg1 := qtcore.newQString5(msg)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut conv_arg2 := voidptr(0)
    /*if icon != voidptr(0) && icon.QIcon_ptr() != voidptr(0) */ {
        // conv_arg2 = icon.QIcon_ptr().get_cthis()
      conv_arg2 = icon.get_cthis()
    }
    // arg: 3, int=Int, =Invalid, , Invalid
    msecs := int(10000)
    mut fnobj := T_ZN15QSystemTrayIcon11showMessageERK7QStringS2_RK5QIconi(0)
    fnobj = qtrt.sym_qtfunc6(1080679435, "_ZN15QSystemTrayIcon11showMessageERK7QStringS2_RK5QIconi")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, conv_arg2, msecs)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:105
// index:1 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMessage(const QString &, const QString &, QSystemTrayIcon::MessageIcon, int)
type T_ZN15QSystemTrayIcon11showMessageERK7QStringS2_NS_11MessageIconEi = fn(cthis voidptr, title voidptr, msg voidptr, icon int, msecs int) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) showMessage1(title string, msg string, icon int, msecs int)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut tmp_arg1 := qtcore.newQString5(msg)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut fnobj := T_ZN15QSystemTrayIcon11showMessageERK7QStringS2_NS_11MessageIconEi(0)
    fnobj = qtrt.sym_qtfunc6(1630116740, "_ZN15QSystemTrayIcon11showMessageERK7QStringS2_NS_11MessageIconEi")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, icon, msecs)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:105
// index:1 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMessage(const QString &, const QString &, QSystemTrayIcon::MessageIcon, int)

/*

*/
pub fn (this QSystemTrayIcon) showMessage1p(title string, msg string)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut tmp_arg1 := qtcore.newQString5(msg)
    mut conv_arg1 := tmp_arg1.get_cthis()
    // arg: 2, QSystemTrayIcon::MessageIcon=Elaborated, QSystemTrayIcon::MessageIcon=Enum, , Invalid
    icon := 0
    // arg: 3, int=Int, =Invalid, , Invalid
    msecs := int(10000)
    mut fnobj := T_ZN15QSystemTrayIcon11showMessageERK7QStringS2_NS_11MessageIconEi(0)
    fnobj = qtrt.sym_qtfunc6(1630116740, "_ZN15QSystemTrayIcon11showMessageERK7QStringS2_NS_11MessageIconEi")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, icon, msecs)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:105
// index:1 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void showMessage(const QString &, const QString &, QSystemTrayIcon::MessageIcon, int)

/*

*/
pub fn (this QSystemTrayIcon) showMessage1p1(title string, msg string, icon int)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut tmp_arg1 := qtcore.newQString5(msg)
    mut conv_arg1 := tmp_arg1.get_cthis()
    // arg: 3, int=Int, =Invalid, , Invalid
    msecs := int(10000)
    mut fnobj := T_ZN15QSystemTrayIcon11showMessageERK7QStringS2_NS_11MessageIconEi(0)
    fnobj = qtrt.sym_qtfunc6(1630116740, "_ZN15QSystemTrayIcon11showMessageERK7QStringS2_NS_11MessageIconEi")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, icon, msecs)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void activated(QSystemTrayIcon::ActivationReason)
type T_ZN15QSystemTrayIcon9activatedENS_16ActivationReasonE = fn(cthis voidptr, reason int) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) activated(reason int)  {
    mut fnobj := T_ZN15QSystemTrayIcon9activatedENS_16ActivationReasonE(0)
    fnobj = qtrt.sym_qtfunc6(4062607574, "_ZN15QSystemTrayIcon9activatedENS_16ActivationReasonE")
    fnobj(this.get_cthis(), reason)
}
// /usr/include/qt/QtWidgets/qsystemtrayicon.h:110
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void messageClicked()
type T_ZN15QSystemTrayIcon14messageClickedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QSystemTrayIcon) messageClicked()  {
    mut fnobj := T_ZN15QSystemTrayIcon14messageClickedEv(0)
    fnobj = qtrt.sym_qtfunc6(2131465345, "_ZN15QSystemTrayIcon14messageClickedEv")
    fnobj(this.get_cthis())
}

[no_inline]
pub fn deleteQSystemTrayIcon(this &QSystemTrayIcon) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(94928577, "_ZN15QSystemTrayIconD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QSystemTrayIcon) freecpp() { deleteQSystemTrayIcon(&this) }

fn (this QSystemTrayIcon) free() {

  /*deleteQSystemTrayIcon(&this)*/

  cthis := this.get_cthis()
  //println("QSystemTrayIcon freeing ${cthis} 0 bytes")

}


/*


*/
//type QSystemTrayIcon.ActivationReason = int
pub enum QSystemTrayIconActivationReason {
  Unknown = 0
  Context = 1
  DoubleClick = 2
  Trigger = 3
  MiddleClick = 4
} // endof enum ActivationReason


/*


*/
//type QSystemTrayIcon.MessageIcon = int
pub enum QSystemTrayIconMessageIcon {
  NoIcon = 0
  Information = 1
  Warning = 2
  Critical = 3
} // endof enum MessageIcon

//  body block end

//  keep block begin


fn init_unused_10255() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
