

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qmenu.h
// #include <qmenu.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 12
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QMenu {
pub:
  QWidget
}

pub interface QMenuITF {
//    QWidgetITF
    get_cthis() voidptr
    toQMenu() QMenu
}
fn hotfix_QMenu_itf_name_table(this QMenuITF) {
  that := QMenu{}
  hotfix_QMenu_itf_name_table(that)
}
pub fn (ptr QMenu) toQMenu() QMenu { return ptr }

pub fn (this QMenu) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQMenuFromptr(cthis voidptr) QMenu {
    bcthis0 := newQWidgetFromptr(cthis)
    return QMenu{bcthis0}
}
pub fn (dummy QMenu) newFromptr(cthis voidptr) QMenu {
    return newQMenuFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qmenu.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMenu(QWidget *)
type T_ZN5QMenuC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QMenu) new_for_inherit_(parent  QWidget/*777 QWidget **/) QMenu {
  //return newQMenu(parent)
  return QMenu{}
}
pub fn newQMenu(parent  QWidget/*777 QWidget **/) QMenu {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN5QMenuC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(32748694, "_ZN5QMenuC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQMenuFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMenu)
    // qtrt.connect_destroyed(gothis, "QMenu")
  return vthis
}
// /usr/include/qt/QtWidgets/qmenu.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMenu(QWidget *)

/*

*/
pub fn (dummy QMenu) new_for_inherit_p() QMenu {
  //return newQMenup()
  return QMenu{}
}
pub fn newQMenup() QMenu {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN5QMenuC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(32748694, "_ZN5QMenuC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQMenuFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMenu)
    // qtrt.connect_destroyed(gothis, "QMenu")
    return vthis
}
// /usr/include/qt/QtWidgets/qmenu.h:75
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMenu(const QString &, QWidget *)
type T_ZN5QMenuC2ERK7QStringP7QWidget = fn(cthis voidptr, title voidptr, parent voidptr) 

/*

*/
pub fn (dummy QMenu) new_for_inherit_1(title string, parent  QWidget/*777 QWidget **/) QMenu {
  //return newQMenu1(title, parent)
  return QMenu{}
}
pub fn newQMenu1(title string, parent  QWidget/*777 QWidget **/) QMenu {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN5QMenuC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2658403316, "_ZN5QMenuC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQMenuFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMenu)
    // qtrt.connect_destroyed(gothis, "QMenu")
  return vthis
}
// /usr/include/qt/QtWidgets/qmenu.h:75
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMenu(const QString &, QWidget *)

/*

*/
pub fn (dummy QMenu) new_for_inherit_1p(title string) QMenu {
  //return newQMenu1p(title)
  return QMenu{}
}
pub fn newQMenu1p(title string) QMenu {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN5QMenuC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2658403316, "_ZN5QMenuC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQMenuFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMenu)
    // qtrt.connect_destroyed(gothis, "QMenu")
    return vthis
}
// /usr/include/qt/QtWidgets/qmenu.h:79
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * addAction(const QString &)
type T_ZN5QMenu9addActionERK7QString = fn(cthis voidptr, text voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenu) addAction(text string)  QAction/*777 QAction **/ {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN5QMenu9addActionERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1698034204, "_ZN5QMenu9addActionERK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenu.h:152
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * addMenu(QMenu *)
type T_ZN5QMenu7addMenuEPS_ = fn(cthis voidptr, menu voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenu) addMenu(menu  QMenu/*777 QMenu **/)  QAction/*777 QAction **/ {
    mut conv_arg0 := voidptr(0)
    /*if menu != voidptr(0) && menu.QMenu_ptr() != voidptr(0) */ {
        // conv_arg0 = menu.QMenu_ptr().get_cthis()
      conv_arg0 = menu.get_cthis()
    }
    mut fnobj := T_ZN5QMenu7addMenuEPS_(0)
    fnobj = qtrt.sym_qtfunc6(4017241996, "_ZN5QMenu7addMenuEPS_")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenu.h:153
// index:1 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QMenu * addMenu(const QString &)
type T_ZN5QMenu7addMenuERK7QString = fn(cthis voidptr, title voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenu) addMenu1(title string)  QMenu/*777 QMenu **/ {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN5QMenu7addMenuERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2540271396, "_ZN5QMenu7addMenuERK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQMenuFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenu.h:156
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * addSeparator()
type T_ZN5QMenu12addSeparatorEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenu) addSeparator()  QAction/*777 QAction **/ {
    mut fnobj := T_ZN5QMenu12addSeparatorEv(0)
    fnobj = qtrt.sym_qtfunc6(3779268459, "_ZN5QMenu12addSeparatorEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenu.h:166
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isEmpty() const
type T_ZNK5QMenu7isEmptyEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QMenu) isEmpty() bool {
    mut fnobj := T_ZNK5QMenu7isEmptyEv(0)
    fnobj = qtrt.sym_qtfunc6(1363387044, "_ZNK5QMenu7isEmptyEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qmenu.h:167
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clear()
type T_ZN5QMenu5clearEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QMenu) clear()  {
    mut fnobj := T_ZN5QMenu5clearEv(0)
    fnobj = qtrt.sym_qtfunc6(3862211293, "_ZN5QMenu5clearEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qmenu.h:200
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString title() const
type T_ZNK5QMenu5titleEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QMenu) title() string {
    mut fnobj := T_ZNK5QMenu5titleEv(0)
    fnobj = qtrt.sym_qtfunc6(2610552010, "_ZNK5QMenu5titleEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qmenu.h:201
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTitle(const QString &)
type T_ZN5QMenu8setTitleERK7QString = fn(cthis voidptr, title voidptr) /*void*/

/*

*/
pub fn (this QMenu) setTitle(title string)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN5QMenu8setTitleERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(841200333, "_ZN5QMenu8setTitleERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmenu.h:222
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void aboutToShow()
type T_ZN5QMenu11aboutToShowEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QMenu) aboutToShow()  {
    mut fnobj := T_ZN5QMenu11aboutToShowEv(0)
    fnobj = qtrt.sym_qtfunc6(1566663078, "_ZN5QMenu11aboutToShowEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qmenu.h:223
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void aboutToHide()
type T_ZN5QMenu11aboutToHideEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QMenu) aboutToHide()  {
    mut fnobj := T_ZN5QMenu11aboutToHideEv(0)
    fnobj = qtrt.sym_qtfunc6(3249752657, "_ZN5QMenu11aboutToHideEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qmenu.h:224
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void triggered(QAction *)
type T_ZN5QMenu9triggeredEP7QAction = fn(cthis voidptr, action voidptr) /*void*/

/*

*/
pub fn (this QMenu) triggered(action  QAction/*777 QAction **/)  {
    mut conv_arg0 := voidptr(0)
    /*if action != voidptr(0) && action.QAction_ptr() != voidptr(0) */ {
        // conv_arg0 = action.QAction_ptr().get_cthis()
      conv_arg0 = action.get_cthis()
    }
    mut fnobj := T_ZN5QMenu9triggeredEP7QAction(0)
    fnobj = qtrt.sym_qtfunc6(576772310, "_ZN5QMenu9triggeredEP7QAction")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmenu.h:225
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void hovered(QAction *)
type T_ZN5QMenu7hoveredEP7QAction = fn(cthis voidptr, action voidptr) /*void*/

/*

*/
pub fn (this QMenu) hovered(action  QAction/*777 QAction **/)  {
    mut conv_arg0 := voidptr(0)
    /*if action != voidptr(0) && action.QAction_ptr() != voidptr(0) */ {
        // conv_arg0 = action.QAction_ptr().get_cthis()
      conv_arg0 = action.get_cthis()
    }
    mut fnobj := T_ZN5QMenu7hoveredEP7QAction(0)
    fnobj = qtrt.sym_qtfunc6(2472517649, "_ZN5QMenu7hoveredEP7QAction")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQMenu(this &QMenu) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4146338877, "_ZN5QMenuD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMenu) freecpp() { deleteQMenu(&this) }

fn (this QMenu) free() {

  /*deleteQMenu(&this)*/

  cthis := this.get_cthis()
  //println("QMenu freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10237() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
