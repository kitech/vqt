

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qgraphicseffect.h
// #include <qgraphicseffect.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QGraphicsEffect {
pub:
  qtcore.QObject
}

pub interface QGraphicsEffectITF {
//    qtcore.QObjectITF
    get_cthis() voidptr
    toQGraphicsEffect() QGraphicsEffect
}
fn hotfix_QGraphicsEffect_itf_name_table(this QGraphicsEffectITF) {
  that := QGraphicsEffect{}
  hotfix_QGraphicsEffect_itf_name_table(that)
}
pub fn (ptr QGraphicsEffect) toQGraphicsEffect() QGraphicsEffect { return ptr }

pub fn (this QGraphicsEffect) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQGraphicsEffectFromptr(cthis voidptr) QGraphicsEffect {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    return QGraphicsEffect{bcthis0}
}
pub fn (dummy QGraphicsEffect) newFromptr(cthis voidptr) QGraphicsEffect {
    return newQGraphicsEffectFromptr(cthis)
}

[no_inline]
pub fn deleteQGraphicsEffect(this &QGraphicsEffect) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2205524303, "_ZN15QGraphicsEffectD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QGraphicsEffect) freecpp() { deleteQGraphicsEffect(&this) }

fn (this QGraphicsEffect) free() {

  /*deleteQGraphicsEffect(&this)*/

  cthis := this.get_cthis()
  //println("QGraphicsEffect freeing ${cthis} 0 bytes")

}


/*


*/
//type QGraphicsEffect.ChangeFlag = int
pub enum QGraphicsEffectChangeFlag {
  SourceAttached = 1
  SourceDetached = 2
  SourceBoundingRectChanged = 4
  SourceInvalidated = 8
} // endof enum ChangeFlag


/*


*/
//type QGraphicsEffect.PixmapPadMode = int
pub enum QGraphicsEffectPixmapPadMode {
  NoPad = 0
  PadToTransparentBorder = 1
  PadToEffectiveBoundingRect = 2
} // endof enum PixmapPadMode

//  body block end

//  keep block begin


fn init_unused_10225() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
