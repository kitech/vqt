

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qgroupbox.h
// #include <qgroupbox.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QGroupBox {
pub:
  QWidget
}

pub interface QGroupBoxITF {
//    QWidgetITF
    get_cthis() voidptr
    toQGroupBox() QGroupBox
}
fn hotfix_QGroupBox_itf_name_table(this QGroupBoxITF) {
  that := QGroupBox{}
  hotfix_QGroupBox_itf_name_table(that)
}
pub fn (ptr QGroupBox) toQGroupBox() QGroupBox { return ptr }

pub fn (this QGroupBox) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQGroupBoxFromptr(cthis voidptr) QGroupBox {
    bcthis0 := newQWidgetFromptr(cthis)
    return QGroupBox{bcthis0}
}
pub fn (dummy QGroupBox) newFromptr(cthis voidptr) QGroupBox {
    return newQGroupBoxFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qgroupbox.h:62
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QGroupBox(QWidget *)
type T_ZN9QGroupBoxC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QGroupBox) new_for_inherit_(parent  QWidget/*777 QWidget **/) QGroupBox {
  //return newQGroupBox(parent)
  return QGroupBox{}
}
pub fn newQGroupBox(parent  QWidget/*777 QWidget **/) QGroupBox {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QGroupBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(662756536, "_ZN9QGroupBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQGroupBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQGroupBox)
    // qtrt.connect_destroyed(gothis, "QGroupBox")
  return vthis
}
// /usr/include/qt/QtWidgets/qgroupbox.h:62
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QGroupBox(QWidget *)

/*

*/
pub fn (dummy QGroupBox) new_for_inherit_p() QGroupBox {
  //return newQGroupBoxp()
  return QGroupBox{}
}
pub fn newQGroupBoxp() QGroupBox {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN9QGroupBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(662756536, "_ZN9QGroupBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQGroupBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQGroupBox)
    // qtrt.connect_destroyed(gothis, "QGroupBox")
    return vthis
}
// /usr/include/qt/QtWidgets/qgroupbox.h:63
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QGroupBox(const QString &, QWidget *)
type T_ZN9QGroupBoxC2ERK7QStringP7QWidget = fn(cthis voidptr, title voidptr, parent voidptr) 

/*

*/
pub fn (dummy QGroupBox) new_for_inherit_1(title string, parent  QWidget/*777 QWidget **/) QGroupBox {
  //return newQGroupBox1(title, parent)
  return QGroupBox{}
}
pub fn newQGroupBox1(title string, parent  QWidget/*777 QWidget **/) QGroupBox {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QGroupBoxC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2800323070, "_ZN9QGroupBoxC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQGroupBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQGroupBox)
    // qtrt.connect_destroyed(gothis, "QGroupBox")
  return vthis
}
// /usr/include/qt/QtWidgets/qgroupbox.h:63
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QGroupBox(const QString &, QWidget *)

/*

*/
pub fn (dummy QGroupBox) new_for_inherit_1p(title string) QGroupBox {
  //return newQGroupBox1p(title)
  return QGroupBox{}
}
pub fn newQGroupBox1p(title string) QGroupBox {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN9QGroupBoxC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2800323070, "_ZN9QGroupBoxC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQGroupBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQGroupBox)
    // qtrt.connect_destroyed(gothis, "QGroupBox")
    return vthis
}
// /usr/include/qt/QtWidgets/qgroupbox.h:66
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString title() const
type T_ZNK9QGroupBox5titleEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QGroupBox) title() string {
    mut fnobj := T_ZNK9QGroupBox5titleEv(0)
    fnobj = qtrt.sym_qtfunc6(3632320991, "_ZNK9QGroupBox5titleEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qgroupbox.h:67
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTitle(const QString &)
type T_ZN9QGroupBox8setTitleERK7QString = fn(cthis voidptr, title voidptr) /*void*/

/*

*/
pub fn (this QGroupBox) setTitle(title string)  {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QGroupBox8setTitleERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2779866592, "_ZN9QGroupBox8setTitleERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qgroupbox.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isFlat() const
type T_ZNK9QGroupBox6isFlatEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QGroupBox) isFlat() bool {
    mut fnobj := T_ZNK9QGroupBox6isFlatEv(0)
    fnobj = qtrt.sym_qtfunc6(2002744521, "_ZNK9QGroupBox6isFlatEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qgroupbox.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFlat(bool)
type T_ZN9QGroupBox7setFlatEb = fn(cthis voidptr, flat bool) /*void*/

/*

*/
pub fn (this QGroupBox) setFlat(flat bool)  {
    mut fnobj := T_ZN9QGroupBox7setFlatEb(0)
    fnobj = qtrt.sym_qtfunc6(2689482562, "_ZN9QGroupBox7setFlatEb")
    fnobj(this.get_cthis(), flat)
}
// /usr/include/qt/QtWidgets/qgroupbox.h:76
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isCheckable() const
type T_ZNK9QGroupBox11isCheckableEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QGroupBox) isCheckable() bool {
    mut fnobj := T_ZNK9QGroupBox11isCheckableEv(0)
    fnobj = qtrt.sym_qtfunc6(2216875125, "_ZNK9QGroupBox11isCheckableEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qgroupbox.h:77
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCheckable(bool)
type T_ZN9QGroupBox12setCheckableEb = fn(cthis voidptr, checkable bool) /*void*/

/*

*/
pub fn (this QGroupBox) setCheckable(checkable bool)  {
    mut fnobj := T_ZN9QGroupBox12setCheckableEb(0)
    fnobj = qtrt.sym_qtfunc6(2495419631, "_ZN9QGroupBox12setCheckableEb")
    fnobj(this.get_cthis(), checkable)
}
// /usr/include/qt/QtWidgets/qgroupbox.h:78
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isChecked() const
type T_ZNK9QGroupBox9isCheckedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QGroupBox) isChecked() bool {
    mut fnobj := T_ZNK9QGroupBox9isCheckedEv(0)
    fnobj = qtrt.sym_qtfunc6(3230016360, "_ZNK9QGroupBox9isCheckedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qgroupbox.h:81
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setChecked(bool)
type T_ZN9QGroupBox10setCheckedEb = fn(cthis voidptr, checked bool) /*void*/

/*

*/
pub fn (this QGroupBox) setChecked(checked bool)  {
    mut fnobj := T_ZN9QGroupBox10setCheckedEb(0)
    fnobj = qtrt.sym_qtfunc6(2906849573, "_ZN9QGroupBox10setCheckedEb")
    fnobj(this.get_cthis(), checked)
}
// /usr/include/qt/QtWidgets/qgroupbox.h:84
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clicked(bool)
type T_ZN9QGroupBox7clickedEb = fn(cthis voidptr, checked bool) /*void*/

/*

*/
pub fn (this QGroupBox) clicked(checked bool)  {
    mut fnobj := T_ZN9QGroupBox7clickedEb(0)
    fnobj = qtrt.sym_qtfunc6(1674438564, "_ZN9QGroupBox7clickedEb")
    fnobj(this.get_cthis(), checked)
}
// /usr/include/qt/QtWidgets/qgroupbox.h:84
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clicked(bool)

/*

*/
pub fn (this QGroupBox) clickedp()  {
    // arg: 0, bool=Bool, =Invalid, , Invalid
    checked := false
    mut fnobj := T_ZN9QGroupBox7clickedEb(0)
    fnobj = qtrt.sym_qtfunc6(1674438564, "_ZN9QGroupBox7clickedEb")
    fnobj(this.get_cthis(), checked)
}
// /usr/include/qt/QtWidgets/qgroupbox.h:85
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void toggled(bool)
type T_ZN9QGroupBox7toggledEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QGroupBox) toggled(arg0 bool)  {
    mut fnobj := T_ZN9QGroupBox7toggledEb(0)
    fnobj = qtrt.sym_qtfunc6(1550658212, "_ZN9QGroupBox7toggledEb")
    fnobj(this.get_cthis(), arg0)
}

[no_inline]
pub fn deleteQGroupBox(this &QGroupBox) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3218891799, "_ZN9QGroupBoxD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QGroupBox) freecpp() { deleteQGroupBox(&this) }

fn (this QGroupBox) free() {

  /*deleteQGroupBox(&this)*/

  cthis := this.get_cthis()
  //println("QGroupBox freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10227() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
