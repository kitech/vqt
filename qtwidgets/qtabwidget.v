

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qtabwidget.h
// #include <qtabwidget.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 23
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QTabWidget {
pub:
  QWidget
}

pub interface QTabWidgetITF {
//    QWidgetITF
    get_cthis() voidptr
    toQTabWidget() QTabWidget
}
fn hotfix_QTabWidget_itf_name_table(this QTabWidgetITF) {
  that := QTabWidget{}
  hotfix_QTabWidget_itf_name_table(that)
}
pub fn (ptr QTabWidget) toQTabWidget() QTabWidget { return ptr }

pub fn (this QTabWidget) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQTabWidgetFromptr(cthis voidptr) QTabWidget {
    bcthis0 := newQWidgetFromptr(cthis)
    return QTabWidget{bcthis0}
}
pub fn (dummy QTabWidget) newFromptr(cthis voidptr) QTabWidget {
    return newQTabWidgetFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QTabWidget(QWidget *)
type T_ZN10QTabWidgetC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QTabWidget) new_for_inherit_(parent  QWidget/*777 QWidget **/) QTabWidget {
  //return newQTabWidget(parent)
  return QTabWidget{}
}
pub fn newQTabWidget(parent  QWidget/*777 QWidget **/) QTabWidget {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN10QTabWidgetC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2398945872, "_ZN10QTabWidgetC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQTabWidgetFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQTabWidget)
    // qtrt.connect_destroyed(gothis, "QTabWidget")
  return vthis
}
// /usr/include/qt/QtWidgets/qtabwidget.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QTabWidget(QWidget *)

/*

*/
pub fn (dummy QTabWidget) new_for_inherit_p() QTabWidget {
  //return newQTabWidgetp()
  return QTabWidget{}
}
pub fn newQTabWidgetp() QTabWidget {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN10QTabWidgetC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2398945872, "_ZN10QTabWidgetC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQTabWidgetFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQTabWidget)
    // qtrt.connect_destroyed(gothis, "QTabWidget")
    return vthis
}
// /usr/include/qt/QtWidgets/qtabwidget.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int addTab(QWidget *, const QString &)
type T_ZN10QTabWidget6addTabEP7QWidgetRK7QString = fn(cthis voidptr, widget voidptr, arg1 voidptr) int

/*

*/
pub fn (this QTabWidget) addTab(widget  QWidget/*777 QWidget **/, arg1 string) int {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut tmp_arg1 := qtcore.newQString5(arg1)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut fnobj := T_ZN10QTabWidget6addTabEP7QWidgetRK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1498629996, "_ZN10QTabWidget6addTabEP7QWidgetRK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0, conv_arg1)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:77
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int insertTab(int, QWidget *, const QString &)
type T_ZN10QTabWidget9insertTabEiP7QWidgetRK7QString = fn(cthis voidptr, index int, widget voidptr, arg2 voidptr) int

/*

*/
pub fn (this QTabWidget) insertTab(index int, widget  QWidget/*777 QWidget **/, arg2 string) int {
    mut conv_arg1 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = widget.QWidget_ptr().get_cthis()
      conv_arg1 = widget.get_cthis()
    }
    mut tmp_arg2 := qtcore.newQString5(arg2)
    mut conv_arg2 := tmp_arg2.get_cthis()
    mut fnobj := T_ZN10QTabWidget9insertTabEiP7QWidgetRK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3896844051, "_ZN10QTabWidget9insertTabEiP7QWidgetRK7QString")
    rv :=
    fnobj(this.get_cthis(), index, conv_arg1, conv_arg2)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:80
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void removeTab(int)
type T_ZN10QTabWidget9removeTabEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabWidget) removeTab(index int)  {
    mut fnobj := T_ZN10QTabWidget9removeTabEi(0)
    fnobj = qtrt.sym_qtfunc6(1571166509, "_ZN10QTabWidget9removeTabEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:82
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isTabEnabled(int) const
type T_ZNK10QTabWidget12isTabEnabledEi = fn(cthis voidptr, index int) bool

/*

*/
pub fn (this QTabWidget) isTabEnabled(index int) bool {
    mut fnobj := T_ZNK10QTabWidget12isTabEnabledEi(0)
    fnobj = qtrt.sym_qtfunc6(3901106487, "_ZNK10QTabWidget12isTabEnabledEi")
    rv :=
    fnobj(this.get_cthis(), index)
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabwidget.h:83
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTabEnabled(int, bool)
type T_ZN10QTabWidget13setTabEnabledEib = fn(cthis voidptr, index int, enabled bool) /*void*/

/*

*/
pub fn (this QTabWidget) setTabEnabled(index int, enabled bool)  {
    mut fnobj := T_ZN10QTabWidget13setTabEnabledEib(0)
    fnobj = qtrt.sym_qtfunc6(3243295178, "_ZN10QTabWidget13setTabEnabledEib")
    fnobj(this.get_cthis(), index, enabled)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:85
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isTabVisible(int) const
type T_ZNK10QTabWidget12isTabVisibleEi = fn(cthis voidptr, index int) bool

/*

*/
pub fn (this QTabWidget) isTabVisible(index int) bool {
    mut fnobj := T_ZNK10QTabWidget12isTabVisibleEi(0)
    fnobj = qtrt.sym_qtfunc6(779270242, "_ZNK10QTabWidget12isTabVisibleEi")
    rv :=
    fnobj(this.get_cthis(), index)
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabwidget.h:86
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTabVisible(int, bool)
type T_ZN10QTabWidget13setTabVisibleEib = fn(cthis voidptr, index int, visible bool) /*void*/

/*

*/
pub fn (this QTabWidget) setTabVisible(index int, visible bool)  {
    mut fnobj := T_ZN10QTabWidget13setTabVisibleEib(0)
    fnobj = qtrt.sym_qtfunc6(3667365920, "_ZN10QTabWidget13setTabVisibleEib")
    fnobj(this.get_cthis(), index, visible)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:88
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString tabText(int) const
type T_ZNK10QTabWidget7tabTextEi = fn(sretobj voidptr, cthis voidptr, index int) voidptr

/*

*/
pub fn (this QTabWidget) tabText(index int) string {
    mut fnobj := T_ZNK10QTabWidget7tabTextEi(0)
    fnobj = qtrt.sym_qtfunc6(3660912747, "_ZNK10QTabWidget7tabTextEi")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis(), index)
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qtabwidget.h:89
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTabText(int, const QString &)
type T_ZN10QTabWidget10setTabTextEiRK7QString = fn(cthis voidptr, index int, text voidptr) /*void*/

/*

*/
pub fn (this QTabWidget) setTabText(index int, text string)  {
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut fnobj := T_ZN10QTabWidget10setTabTextEiRK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1638282860, "_ZN10QTabWidget10setTabTextEiRK7QString")
    fnobj(this.get_cthis(), index, conv_arg1)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:104
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int currentIndex() const
type T_ZNK10QTabWidget12currentIndexEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabWidget) currentIndex() int {
    mut fnobj := T_ZNK10QTabWidget12currentIndexEv(0)
    fnobj = qtrt.sym_qtfunc6(1989898727, "_ZNK10QTabWidget12currentIndexEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * currentWidget() const
type T_ZNK10QTabWidget13currentWidgetEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QTabWidget) currentWidget()  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK10QTabWidget13currentWidgetEv(0)
    fnobj = qtrt.sym_qtfunc6(1593011613, "_ZNK10QTabWidget13currentWidgetEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qtabwidget.h:106
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QWidget * widget(int) const
type T_ZNK10QTabWidget6widgetEi = fn(cthis voidptr, index int) voidptr/*666*/

/*

*/
pub fn (this QTabWidget) widget(index int)  QWidget/*777 QWidget **/ {
    mut fnobj := T_ZNK10QTabWidget6widgetEi(0)
    fnobj = qtrt.sym_qtfunc6(1033250205, "_ZNK10QTabWidget6widgetEi")
    rv :=
    fnobj(this.get_cthis(), index)
    return /*==*/newQWidgetFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qtabwidget.h:107
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOf(QWidget *) const
type T_ZNK10QTabWidget7indexOfEP7QWidget = fn(cthis voidptr, widget voidptr) int

/*

*/
pub fn (this QTabWidget) indexOf(widget  QWidget/*777 QWidget **/) int {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZNK10QTabWidget7indexOfEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1793686560, "_ZNK10QTabWidget7indexOfEP7QWidget")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:108
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int count() const
type T_ZNK10QTabWidget5countEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabWidget) count() int {
    mut fnobj := T_ZNK10QTabWidget5countEv(0)
    fnobj = qtrt.sym_qtfunc6(3464247378, "_ZNK10QTabWidget5countEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:115
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool tabsClosable() const
type T_ZNK10QTabWidget12tabsClosableEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QTabWidget) tabsClosable() bool {
    mut fnobj := T_ZNK10QTabWidget12tabsClosableEv(0)
    fnobj = qtrt.sym_qtfunc6(2589699479, "_ZNK10QTabWidget12tabsClosableEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabwidget.h:116
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTabsClosable(bool)
type T_ZN10QTabWidget15setTabsClosableEb = fn(cthis voidptr, closeable bool) /*void*/

/*

*/
pub fn (this QTabWidget) setTabsClosable(closeable bool)  {
    mut fnobj := T_ZN10QTabWidget15setTabsClosableEb(0)
    fnobj = qtrt.sym_qtfunc6(2670076814, "_ZN10QTabWidget15setTabsClosableEb")
    fnobj(this.get_cthis(), closeable)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:118
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isMovable() const
type T_ZNK10QTabWidget9isMovableEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QTabWidget) isMovable() bool {
    mut fnobj := T_ZNK10QTabWidget9isMovableEv(0)
    fnobj = qtrt.sym_qtfunc6(2203855381, "_ZNK10QTabWidget9isMovableEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qtabwidget.h:119
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMovable(bool)
type T_ZN10QTabWidget10setMovableEb = fn(cthis voidptr, movable bool) /*void*/

/*

*/
pub fn (this QTabWidget) setMovable(movable bool)  {
    mut fnobj := T_ZN10QTabWidget10setMovableEb(0)
    fnobj = qtrt.sym_qtfunc6(4071398512, "_ZN10QTabWidget10setMovableEb")
    fnobj(this.get_cthis(), movable)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:154
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCurrentIndex(int)
type T_ZN10QTabWidget15setCurrentIndexEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabWidget) setCurrentIndex(index int)  {
    mut fnobj := T_ZN10QTabWidget15setCurrentIndexEi(0)
    fnobj = qtrt.sym_qtfunc6(3828622966, "_ZN10QTabWidget15setCurrentIndexEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:155
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCurrentWidget(QWidget *)
type T_ZN10QTabWidget16setCurrentWidgetEP7QWidget = fn(cthis voidptr, widget voidptr) /*void*/

/*

*/
pub fn (this QTabWidget) setCurrentWidget(widget  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if widget != voidptr(0) && widget.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = widget.QWidget_ptr().get_cthis()
      conv_arg0 = widget.get_cthis()
    }
    mut fnobj := T_ZN10QTabWidget16setCurrentWidgetEP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1617009302, "_ZN10QTabWidget16setCurrentWidgetEP7QWidget")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:158
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void currentChanged(int)
type T_ZN10QTabWidget14currentChangedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabWidget) currentChanged(index int)  {
    mut fnobj := T_ZN10QTabWidget14currentChangedEi(0)
    fnobj = qtrt.sym_qtfunc6(1664281805, "_ZN10QTabWidget14currentChangedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:159
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void tabCloseRequested(int)
type T_ZN10QTabWidget17tabCloseRequestedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabWidget) tabCloseRequested(index int)  {
    mut fnobj := T_ZN10QTabWidget17tabCloseRequestedEi(0)
    fnobj = qtrt.sym_qtfunc6(384078136, "_ZN10QTabWidget17tabCloseRequestedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:160
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void tabBarClicked(int)
type T_ZN10QTabWidget13tabBarClickedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabWidget) tabBarClicked(index int)  {
    mut fnobj := T_ZN10QTabWidget13tabBarClickedEi(0)
    fnobj = qtrt.sym_qtfunc6(400432813, "_ZN10QTabWidget13tabBarClickedEi")
    fnobj(this.get_cthis(), index)
}
// /usr/include/qt/QtWidgets/qtabwidget.h:161
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void tabBarDoubleClicked(int)
type T_ZN10QTabWidget19tabBarDoubleClickedEi = fn(cthis voidptr, index int) /*void*/

/*

*/
pub fn (this QTabWidget) tabBarDoubleClicked(index int)  {
    mut fnobj := T_ZN10QTabWidget19tabBarDoubleClickedEi(0)
    fnobj = qtrt.sym_qtfunc6(1649400173, "_ZN10QTabWidget19tabBarDoubleClickedEi")
    fnobj(this.get_cthis(), index)
}

[no_inline]
pub fn deleteQTabWidget(this &QTabWidget) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3835285240, "_ZN10QTabWidgetD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QTabWidget) freecpp() { deleteQTabWidget(&this) }

fn (this QTabWidget) free() {

  /*deleteQTabWidget(&this)*/

  cthis := this.get_cthis()
  //println("QTabWidget freeing ${cthis} 0 bytes")

}


/*


*/
//type QTabWidget.TabPosition = int
pub enum QTabWidgetTabPosition {
  North = 0
  South = 1
  West = 2
  East = 3
} // endof enum TabPosition


/*


*/
//type QTabWidget.TabShape = int
pub enum QTabWidgetTabShape {
  Rounded = 0
  Triangular = 1
} // endof enum TabShape

//  body block end

//  keep block begin


fn init_unused_10187() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
