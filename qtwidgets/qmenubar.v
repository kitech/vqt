

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qmenubar.h
// #include <qmenubar.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 14
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QMenuBar {
pub:
  QWidget
}

pub interface QMenuBarITF {
//    QWidgetITF
    get_cthis() voidptr
    toQMenuBar() QMenuBar
}
fn hotfix_QMenuBar_itf_name_table(this QMenuBarITF) {
  that := QMenuBar{}
  hotfix_QMenuBar_itf_name_table(that)
}
pub fn (ptr QMenuBar) toQMenuBar() QMenuBar { return ptr }

pub fn (this QMenuBar) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQMenuBarFromptr(cthis voidptr) QMenuBar {
    bcthis0 := newQWidgetFromptr(cthis)
    return QMenuBar{bcthis0}
}
pub fn (dummy QMenuBar) newFromptr(cthis voidptr) QMenuBar {
    return newQMenuBarFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qmenubar.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMenuBar(QWidget *)
type T_ZN8QMenuBarC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QMenuBar) new_for_inherit_(parent  QWidget/*777 QWidget **/) QMenuBar {
  //return newQMenuBar(parent)
  return QMenuBar{}
}
pub fn newQMenuBar(parent  QWidget/*777 QWidget **/) QMenuBar {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN8QMenuBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(4217600900, "_ZN8QMenuBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQMenuBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMenuBar)
    // qtrt.connect_destroyed(gothis, "QMenuBar")
  return vthis
}
// /usr/include/qt/QtWidgets/qmenubar.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QMenuBar(QWidget *)

/*

*/
pub fn (dummy QMenuBar) new_for_inherit_p() QMenuBar {
  //return newQMenuBarp()
  return QMenuBar{}
}
pub fn newQMenuBarp() QMenuBar {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN8QMenuBarC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(4217600900, "_ZN8QMenuBarC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQMenuBarFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQMenuBar)
    // qtrt.connect_destroyed(gothis, "QMenuBar")
    return vthis
}
// /usr/include/qt/QtWidgets/qmenubar.h:67
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * addAction(const QString &)
type T_ZN8QMenuBar9addActionERK7QString = fn(cthis voidptr, text voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenuBar) addAction(text string)  QAction/*777 QAction **/ {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN8QMenuBar9addActionERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(524583219, "_ZN8QMenuBar9addActionERK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenubar.h:96
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * addMenu(QMenu *)
type T_ZN8QMenuBar7addMenuEP5QMenu = fn(cthis voidptr, menu voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenuBar) addMenu(menu  QMenu/*777 QMenu **/)  QAction/*777 QAction **/ {
    mut conv_arg0 := voidptr(0)
    /*if menu != voidptr(0) && menu.QMenu_ptr() != voidptr(0) */ {
        // conv_arg0 = menu.QMenu_ptr().get_cthis()
      conv_arg0 = menu.get_cthis()
    }
    mut fnobj := T_ZN8QMenuBar7addMenuEP5QMenu(0)
    fnobj = qtrt.sym_qtfunc6(1432639305, "_ZN8QMenuBar7addMenuEP5QMenu")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenubar.h:97
// index:1 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QMenu * addMenu(const QString &)
type T_ZN8QMenuBar7addMenuERK7QString = fn(cthis voidptr, title voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenuBar) addMenu1(title string)  QMenu/*777 QMenu **/ {
    mut tmp_arg0 := qtcore.newQString5(title)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN8QMenuBar7addMenuERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3209398129, "_ZN8QMenuBar7addMenuERK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQMenuFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenubar.h:98
// index:2 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QMenu * addMenu(const QIcon &, const QString &)
type T_ZN8QMenuBar7addMenuERK5QIconRK7QString = fn(cthis voidptr, icon voidptr, title voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenuBar) addMenu2(icon  qtgui.QIconITF, title string)  QMenu/*777 QMenu **/ {
    mut conv_arg0 := voidptr(0)
    /*if icon != voidptr(0) && icon.QIcon_ptr() != voidptr(0) */ {
        // conv_arg0 = icon.QIcon_ptr().get_cthis()
      conv_arg0 = icon.get_cthis()
    }
    mut tmp_arg1 := qtcore.newQString5(title)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut fnobj := T_ZN8QMenuBar7addMenuERK5QIconRK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3402422444, "_ZN8QMenuBar7addMenuERK5QIconRK7QString")
    rv :=
    fnobj(this.get_cthis(), conv_arg0, conv_arg1)
    return /*==*/newQMenuFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenubar.h:101
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * addSeparator()
type T_ZN8QMenuBar12addSeparatorEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenuBar) addSeparator()  QAction/*777 QAction **/ {
    mut fnobj := T_ZN8QMenuBar12addSeparatorEv(0)
    fnobj = qtrt.sym_qtfunc6(2911490074, "_ZN8QMenuBar12addSeparatorEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenubar.h:102
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * insertSeparator(QAction *)
type T_ZN8QMenuBar15insertSeparatorEP7QAction = fn(cthis voidptr, before voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenuBar) insertSeparator(before  QAction/*777 QAction **/)  QAction/*777 QAction **/ {
    mut conv_arg0 := voidptr(0)
    /*if before != voidptr(0) && before.QAction_ptr() != voidptr(0) */ {
        // conv_arg0 = before.QAction_ptr().get_cthis()
      conv_arg0 = before.get_cthis()
    }
    mut fnobj := T_ZN8QMenuBar15insertSeparatorEP7QAction(0)
    fnobj = qtrt.sym_qtfunc6(136532056, "_ZN8QMenuBar15insertSeparatorEP7QAction")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenubar.h:104
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QAction * insertMenu(QAction *, QMenu *)
type T_ZN8QMenuBar10insertMenuEP7QActionP5QMenu = fn(cthis voidptr, before voidptr, menu voidptr) voidptr/*666*/

/*

*/
pub fn (this QMenuBar) insertMenu(before  QAction/*777 QAction **/, menu  QMenu/*777 QMenu **/)  QAction/*777 QAction **/ {
    mut conv_arg0 := voidptr(0)
    /*if before != voidptr(0) && before.QAction_ptr() != voidptr(0) */ {
        // conv_arg0 = before.QAction_ptr().get_cthis()
      conv_arg0 = before.get_cthis()
    }
    mut conv_arg1 := voidptr(0)
    /*if menu != voidptr(0) && menu.QMenu_ptr() != voidptr(0) */ {
        // conv_arg1 = menu.QMenu_ptr().get_cthis()
      conv_arg1 = menu.get_cthis()
    }
    mut fnobj := T_ZN8QMenuBar10insertMenuEP7QActionP5QMenu(0)
    fnobj = qtrt.sym_qtfunc6(2471833135, "_ZN8QMenuBar10insertMenuEP7QActionP5QMenu")
    rv :=
    fnobj(this.get_cthis(), conv_arg0, conv_arg1)
    return /*==*/newQActionFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtWidgets/qmenubar.h:106
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clear()
type T_ZN8QMenuBar5clearEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QMenuBar) clear()  {
    mut fnobj := T_ZN8QMenuBar5clearEv(0)
    fnobj = qtrt.sym_qtfunc6(1047224492, "_ZN8QMenuBar5clearEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qmenubar.h:132
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Ignore Visibility=Default Availability=Available
// [-2] void setVisible(bool)
type T_ZN8QMenuBar10setVisibleEb = fn(cthis voidptr, visible bool) /*void*/

/*

*/
pub fn (this QMenuBar) setVisible(visible bool)  {
    mut fnobj := T_ZN8QMenuBar10setVisibleEb(0)
    fnobj = qtrt.sym_qtfunc6(1756387468, "_ZN8QMenuBar10setVisibleEb")
    fnobj(this.get_cthis(), visible)
}
// /usr/include/qt/QtWidgets/qmenubar.h:135
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void triggered(QAction *)
type T_ZN8QMenuBar9triggeredEP7QAction = fn(cthis voidptr, action voidptr) /*void*/

/*

*/
pub fn (this QMenuBar) triggered(action  QAction/*777 QAction **/)  {
    mut conv_arg0 := voidptr(0)
    /*if action != voidptr(0) && action.QAction_ptr() != voidptr(0) */ {
        // conv_arg0 = action.QAction_ptr().get_cthis()
      conv_arg0 = action.get_cthis()
    }
    mut fnobj := T_ZN8QMenuBar9triggeredEP7QAction(0)
    fnobj = qtrt.sym_qtfunc6(961109853, "_ZN8QMenuBar9triggeredEP7QAction")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qmenubar.h:136
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void hovered(QAction *)
type T_ZN8QMenuBar7hoveredEP7QAction = fn(cthis voidptr, action voidptr) /*void*/

/*

*/
pub fn (this QMenuBar) hovered(action  QAction/*777 QAction **/)  {
    mut conv_arg0 := voidptr(0)
    /*if action != voidptr(0) && action.QAction_ptr() != voidptr(0) */ {
        // conv_arg0 = action.QAction_ptr().get_cthis()
      conv_arg0 = action.get_cthis()
    }
    mut fnobj := T_ZN8QMenuBar7hoveredEP7QAction(0)
    fnobj = qtrt.sym_qtfunc6(3004656425, "_ZN8QMenuBar7hoveredEP7QAction")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQMenuBar(this &QMenuBar) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4257493948, "_ZN8QMenuBarD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMenuBar) freecpp() { deleteQMenuBar(&this) }

fn (this QMenuBar) free() {

  /*deleteQMenuBar(&this)*/

  cthis := this.get_cthis()
  //println("QMenuBar freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10239() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
