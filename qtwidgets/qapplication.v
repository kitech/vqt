

module qtwidgets
// /usr/include/qt/QtWidgets/qapplication.h
// #include <qapplication.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 26
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QApplication {
pub:
  qtgui.QGuiApplication
}

pub interface QApplicationITF {
//    qtgui.QGuiApplicationITF
    get_cthis() voidptr
    toQApplication() QApplication
}
fn hotfix_QApplication_itf_name_table(this QApplicationITF) {
  that := QApplication{}
  hotfix_QApplication_itf_name_table(that)
}
pub fn (ptr QApplication) toQApplication() QApplication { return ptr }

pub fn (this QApplication) get_cthis() voidptr {
    return this.QGuiApplication.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQApplicationFromptr(cthis voidptr) QApplication {
    bcthis0 := qtgui.newQGuiApplicationFromptr(cthis)
    return QApplication{bcthis0}
}
pub fn (dummy QApplication) newFromptr(cthis voidptr) QApplication {
    return newQApplicationFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qapplication.h:147
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Ignore Visibility=Default Availability=Available
// [-2] void beep()
type T_ZN12QApplication4beepEv = fn() /*void*/

/*

*/
pub fn (this QApplication) beep()  {
    mut fnobj := T_ZN12QApplication4beepEv(0)
    fnobj = qtrt.sym_qtfunc6(3241209511, "_ZN12QApplication4beepEv")
    fnobj()
}
// /usr/include/qt/QtWidgets/qapplication.h:198
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void focusChanged(QWidget *, QWidget *)
type T_ZN12QApplication12focusChangedEP7QWidgetS1_ = fn(cthis voidptr, old voidptr, now voidptr) /*void*/

/*

*/
pub fn (this QApplication) focusChanged(old  QWidget/*777 QWidget **/, now  QWidget/*777 QWidget **/)  {
    mut conv_arg0 := voidptr(0)
    /*if old != voidptr(0) && old.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = old.QWidget_ptr().get_cthis()
      conv_arg0 = old.get_cthis()
    }
    mut conv_arg1 := voidptr(0)
    /*if now != voidptr(0) && now.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = now.QWidget_ptr().get_cthis()
      conv_arg1 = now.get_cthis()
    }
    mut fnobj := T_ZN12QApplication12focusChangedEP7QWidgetS1_(0)
    fnobj = qtrt.sym_qtfunc6(299776149, "_ZN12QApplication12focusChangedEP7QWidgetS1_")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1)
}
// /usr/include/qt/QtWidgets/qapplication.h:201
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString styleSheet() const
type T_ZNK12QApplication10styleSheetEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QApplication) styleSheet() string {
    mut fnobj := T_ZNK12QApplication10styleSheetEv(0)
    fnobj = qtrt.sym_qtfunc6(306547508, "_ZNK12QApplication10styleSheetEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qapplication.h:204
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStyleSheet(const QString &)
type T_ZN12QApplication13setStyleSheetERK7QString = fn(cthis voidptr, sheet voidptr) /*void*/

/*

*/
pub fn (this QApplication) setStyleSheet(sheet string)  {
    mut tmp_arg0 := qtcore.newQString5(sheet)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN12QApplication13setStyleSheetERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(412095265, "_ZN12QApplication13setStyleSheetERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qapplication.h:209
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Ignore Visibility=Default Availability=Available
// [-2] void aboutQt()
type T_ZN12QApplication7aboutQtEv = fn() /*void*/

/*

*/
pub fn (this QApplication) aboutQt()  {
    mut fnobj := T_ZN12QApplication7aboutQtEv(0)
    fnobj = qtrt.sym_qtfunc6(799315374, "_ZN12QApplication7aboutQtEv")
    fnobj()
}

[no_inline]
pub fn deleteQApplication(this &QApplication) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1156959137, "_ZN12QApplicationD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QApplication) freecpp() { deleteQApplication(&this) }

fn (this QApplication) free() {

  /*deleteQApplication(&this)*/

  cthis := this.get_cthis()
  //println("QApplication freeing ${cthis} 0 bytes")

}


/*


*/
//type QApplication.ColorSpec = int
pub enum QApplicationColorSpec {
  NormalColor = 0
  CustomColor = 1
  ManyColor = 2
} // endof enum ColorSpec

//  body block end

//  keep block begin


fn init_unused_10197() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
