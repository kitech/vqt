

module qtwidgets
// /usr/include/qt/QtWidgets/qlayoutitem.h
// #include <qlayoutitem.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QSpacerItem {
pub:
  QLayoutItem
}

pub interface QSpacerItemITF {
//    QLayoutItemITF
    get_cthis() voidptr
    toQSpacerItem() QSpacerItem
}
fn hotfix_QSpacerItem_itf_name_table(this QSpacerItemITF) {
  that := QSpacerItem{}
  hotfix_QSpacerItem_itf_name_table(that)
}
pub fn (ptr QSpacerItem) toQSpacerItem() QSpacerItem { return ptr }

pub fn (this QSpacerItem) get_cthis() voidptr {
    return this.QLayoutItem.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQSpacerItemFromptr(cthis voidptr) QSpacerItem {
    bcthis0 := newQLayoutItemFromptr(cthis)
    return QSpacerItem{bcthis0}
}
pub fn (dummy QSpacerItem) newFromptr(cthis voidptr) QSpacerItem {
    return newQSpacerItemFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:99
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QSpacerItem(int, int, QSizePolicy::Policy, QSizePolicy::Policy)
type T_ZN11QSpacerItemC2EiiN11QSizePolicy6PolicyES1_ = fn(cthis voidptr, w int, h int, hData int, vData int) 

/*

*/
pub fn (dummy QSpacerItem) new_for_inherit_(w int, h int, hData int, vData int) QSpacerItem {
  //return newQSpacerItem(w, h, hData, vData)
  return QSpacerItem{}
}
pub fn newQSpacerItem(w int, h int, hData int, vData int) QSpacerItem {
    mut fnobj := T_ZN11QSpacerItemC2EiiN11QSizePolicy6PolicyES1_(0)
    fnobj = qtrt.sym_qtfunc6(886124047, "_ZN11QSpacerItemC2EiiN11QSizePolicy6PolicyES1_")
    mut cthis := qtrt.mallocraw(40)
    fnobj(cthis, w, h, hData, vData)
    rv := cthis
    vthis := newQSpacerItemFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSpacerItem)
  return vthis
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:99
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QSpacerItem(int, int, QSizePolicy::Policy, QSizePolicy::Policy)

/*

*/
pub fn (dummy QSpacerItem) new_for_inherit_p(w int, h int) QSpacerItem {
  //return newQSpacerItemp(w, h)
  return QSpacerItem{}
}
pub fn newQSpacerItemp(w int, h int) QSpacerItem {
    // arg: 2, QSizePolicy::Policy=Elaborated, QSizePolicy::Policy=Enum, , Invalid
    hData := 0
    // arg: 3, QSizePolicy::Policy=Elaborated, QSizePolicy::Policy=Enum, , Invalid
    vData := 0
    mut fnobj := T_ZN11QSpacerItemC2EiiN11QSizePolicy6PolicyES1_(0)
    fnobj = qtrt.sym_qtfunc6(886124047, "_ZN11QSpacerItemC2EiiN11QSizePolicy6PolicyES1_")
    mut cthis := qtrt.mallocraw(40)
    fnobj(cthis, w, h, hData, vData)
    rv := cthis
    vthis := newQSpacerItemFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSpacerItem)
    return vthis
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:99
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QSpacerItem(int, int, QSizePolicy::Policy, QSizePolicy::Policy)

/*

*/
pub fn (dummy QSpacerItem) new_for_inherit_p1(w int, h int, hData int) QSpacerItem {
  //return newQSpacerItemp1(w, h, hData)
  return QSpacerItem{}
}
pub fn newQSpacerItemp1(w int, h int, hData int) QSpacerItem {
    // arg: 3, QSizePolicy::Policy=Elaborated, QSizePolicy::Policy=Enum, , Invalid
    vData := 0
    mut fnobj := T_ZN11QSpacerItemC2EiiN11QSizePolicy6PolicyES1_(0)
    fnobj = qtrt.sym_qtfunc6(886124047, "_ZN11QSpacerItemC2EiiN11QSizePolicy6PolicyES1_")
    mut cthis := qtrt.mallocraw(40)
    fnobj(cthis, w, h, hData, vData)
    rv := cthis
    vthis := newQSpacerItemFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSpacerItem)
    return vthis
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void changeSize(int, int, QSizePolicy::Policy, QSizePolicy::Policy)
type T_ZN11QSpacerItem10changeSizeEiiN11QSizePolicy6PolicyES1_ = fn(cthis voidptr, w int, h int, hData int, vData int) /*void*/

/*

*/
pub fn (this QSpacerItem) changeSize(w int, h int, hData int, vData int)  {
    mut fnobj := T_ZN11QSpacerItem10changeSizeEiiN11QSizePolicy6PolicyES1_(0)
    fnobj = qtrt.sym_qtfunc6(2168887114, "_ZN11QSpacerItem10changeSizeEiiN11QSizePolicy6PolicyES1_")
    fnobj(this.get_cthis(), w, h, hData, vData)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void changeSize(int, int, QSizePolicy::Policy, QSizePolicy::Policy)

/*

*/
pub fn (this QSpacerItem) changeSizep(w int, h int)  {
    // arg: 2, QSizePolicy::Policy=Elaborated, QSizePolicy::Policy=Enum, , Invalid
    hData := 0
    // arg: 3, QSizePolicy::Policy=Elaborated, QSizePolicy::Policy=Enum, , Invalid
    vData := 0
    mut fnobj := T_ZN11QSpacerItem10changeSizeEiiN11QSizePolicy6PolicyES1_(0)
    fnobj = qtrt.sym_qtfunc6(2168887114, "_ZN11QSpacerItem10changeSizeEiiN11QSizePolicy6PolicyES1_")
    fnobj(this.get_cthis(), w, h, hData, vData)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void changeSize(int, int, QSizePolicy::Policy, QSizePolicy::Policy)

/*

*/
pub fn (this QSpacerItem) changeSizep1(w int, h int, hData int)  {
    // arg: 3, QSizePolicy::Policy=Elaborated, QSizePolicy::Policy=Enum, , Invalid
    vData := 0
    mut fnobj := T_ZN11QSpacerItem10changeSizeEiiN11QSizePolicy6PolicyES1_(0)
    fnobj = qtrt.sym_qtfunc6(2168887114, "_ZN11QSpacerItem10changeSizeEiiN11QSizePolicy6PolicyES1_")
    fnobj(this.get_cthis(), w, h, hData, vData)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:108
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize sizeHint() const
type T_ZNK11QSpacerItem8sizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QSpacerItem) sizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QSpacerItem8sizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(602256895, "_ZNK11QSpacerItem8sizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize minimumSize() const
type T_ZNK11QSpacerItem11minimumSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QSpacerItem) minimumSize()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QSpacerItem11minimumSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(2485939981, "_ZNK11QSpacerItem11minimumSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:110
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize maximumSize() const
type T_ZNK11QSpacerItem11maximumSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QSpacerItem) maximumSize()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK11QSpacerItem11maximumSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(3563263867, "_ZNK11QSpacerItem11maximumSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:111
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Visibility=Default Availability=Available
// [4] Qt::Orientations expandingDirections() const
type T_ZNK11QSpacerItem19expandingDirectionsEv = fn(cthis voidptr) int

/*

*/
pub fn (this QSpacerItem) expandingDirections() int {
    mut fnobj := T_ZNK11QSpacerItem19expandingDirectionsEv(0)
    fnobj = qtrt.sym_qtfunc6(179331885, "_ZNK11QSpacerItem19expandingDirectionsEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtWidgets/qlayoutitem.h:112
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool isEmpty() const
type T_ZNK11QSpacerItem7isEmptyEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QSpacerItem) isEmpty() bool {
    mut fnobj := T_ZNK11QSpacerItem7isEmptyEv(0)
    fnobj = qtrt.sym_qtfunc6(4153964196, "_ZNK11QSpacerItem7isEmptyEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}

[no_inline]
pub fn deleteQSpacerItem(this &QSpacerItem) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2459316865, "_ZN11QSpacerItemD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QSpacerItem) freecpp() { deleteQSpacerItem(&this) }

fn (this QSpacerItem) free() {

  /*deleteQSpacerItem(&this)*/

  cthis := this.get_cthis()
  //println("QSpacerItem freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10201() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
