

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qradiobutton.h
// #include <qradiobutton.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 14
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QRadioButton {
pub:
  QAbstractButton
}

pub interface QRadioButtonITF {
//    QAbstractButtonITF
    get_cthis() voidptr
    toQRadioButton() QRadioButton
}
fn hotfix_QRadioButton_itf_name_table(this QRadioButtonITF) {
  that := QRadioButton{}
  hotfix_QRadioButton_itf_name_table(that)
}
pub fn (ptr QRadioButton) toQRadioButton() QRadioButton { return ptr }

pub fn (this QRadioButton) get_cthis() voidptr {
    return this.QAbstractButton.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQRadioButtonFromptr(cthis voidptr) QRadioButton {
    bcthis0 := newQAbstractButtonFromptr(cthis)
    return QRadioButton{bcthis0}
}
pub fn (dummy QRadioButton) newFromptr(cthis voidptr) QRadioButton {
    return newQRadioButtonFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qradiobutton.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QRadioButton(QWidget *)
type T_ZN12QRadioButtonC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QRadioButton) new_for_inherit_(parent  QWidget/*777 QWidget **/) QRadioButton {
  //return newQRadioButton(parent)
  return QRadioButton{}
}
pub fn newQRadioButton(parent  QWidget/*777 QWidget **/) QRadioButton {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN12QRadioButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3516896294, "_ZN12QRadioButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQRadioButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQRadioButton)
    // qtrt.connect_destroyed(gothis, "QRadioButton")
  return vthis
}
// /usr/include/qt/QtWidgets/qradiobutton.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QRadioButton(QWidget *)

/*

*/
pub fn (dummy QRadioButton) new_for_inherit_p() QRadioButton {
  //return newQRadioButtonp()
  return QRadioButton{}
}
pub fn newQRadioButtonp() QRadioButton {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN12QRadioButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3516896294, "_ZN12QRadioButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQRadioButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQRadioButton)
    // qtrt.connect_destroyed(gothis, "QRadioButton")
    return vthis
}
// /usr/include/qt/QtWidgets/qradiobutton.h:60
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QRadioButton(const QString &, QWidget *)
type T_ZN12QRadioButtonC2ERK7QStringP7QWidget = fn(cthis voidptr, text voidptr, parent voidptr) 

/*

*/
pub fn (dummy QRadioButton) new_for_inherit_1(text string, parent  QWidget/*777 QWidget **/) QRadioButton {
  //return newQRadioButton1(text, parent)
  return QRadioButton{}
}
pub fn newQRadioButton1(text string, parent  QWidget/*777 QWidget **/) QRadioButton {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN12QRadioButtonC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3203710175, "_ZN12QRadioButtonC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQRadioButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQRadioButton)
    // qtrt.connect_destroyed(gothis, "QRadioButton")
  return vthis
}
// /usr/include/qt/QtWidgets/qradiobutton.h:60
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QRadioButton(const QString &, QWidget *)

/*

*/
pub fn (dummy QRadioButton) new_for_inherit_1p(text string) QRadioButton {
  //return newQRadioButton1p(text)
  return QRadioButton{}
}
pub fn newQRadioButton1p(text string) QRadioButton {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN12QRadioButtonC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(3203710175, "_ZN12QRadioButtonC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQRadioButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQRadioButton)
    // qtrt.connect_destroyed(gothis, "QRadioButton")
    return vthis
}
// /usr/include/qt/QtWidgets/qradiobutton.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize sizeHint() const
type T_ZNK12QRadioButton8sizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QRadioButton) sizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK12QRadioButton8sizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(1977029849, "_ZNK12QRadioButton8sizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qradiobutton.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize minimumSizeHint() const
type T_ZNK12QRadioButton15minimumSizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QRadioButton) minimumSizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK12QRadioButton15minimumSizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(2537386903, "_ZNK12QRadioButton15minimumSizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}

[no_inline]
pub fn deleteQRadioButton(this &QRadioButton) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3108312079, "_ZN12QRadioButtonD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QRadioButton) freecpp() { deleteQRadioButton(&this) }

fn (this QRadioButton) free() {

  /*deleteQRadioButton(&this)*/

  cthis := this.get_cthis()
  //println("QRadioButton freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10245() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
