

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qcheckbox.h
// #include <qcheckbox.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QCheckBox {
pub:
  QAbstractButton
}

pub interface QCheckBoxITF {
//    QAbstractButtonITF
    get_cthis() voidptr
    toQCheckBox() QCheckBox
}
fn hotfix_QCheckBox_itf_name_table(this QCheckBoxITF) {
  that := QCheckBox{}
  hotfix_QCheckBox_itf_name_table(that)
}
pub fn (ptr QCheckBox) toQCheckBox() QCheckBox { return ptr }

pub fn (this QCheckBox) get_cthis() voidptr {
    return this.QAbstractButton.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQCheckBoxFromptr(cthis voidptr) QCheckBox {
    bcthis0 := newQAbstractButtonFromptr(cthis)
    return QCheckBox{bcthis0}
}
pub fn (dummy QCheckBox) newFromptr(cthis voidptr) QCheckBox {
    return newQCheckBoxFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qcheckbox.h:61
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QCheckBox(QWidget *)
type T_ZN9QCheckBoxC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QCheckBox) new_for_inherit_(parent  QWidget/*777 QWidget **/) QCheckBox {
  //return newQCheckBox(parent)
  return QCheckBox{}
}
pub fn newQCheckBox(parent  QWidget/*777 QWidget **/) QCheckBox {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QCheckBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(421980486, "_ZN9QCheckBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQCheckBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQCheckBox)
    // qtrt.connect_destroyed(gothis, "QCheckBox")
  return vthis
}
// /usr/include/qt/QtWidgets/qcheckbox.h:61
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QCheckBox(QWidget *)

/*

*/
pub fn (dummy QCheckBox) new_for_inherit_p() QCheckBox {
  //return newQCheckBoxp()
  return QCheckBox{}
}
pub fn newQCheckBoxp() QCheckBox {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN9QCheckBoxC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(421980486, "_ZN9QCheckBoxC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQCheckBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQCheckBox)
    // qtrt.connect_destroyed(gothis, "QCheckBox")
    return vthis
}
// /usr/include/qt/QtWidgets/qcheckbox.h:62
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QCheckBox(const QString &, QWidget *)
type T_ZN9QCheckBoxC2ERK7QStringP7QWidget = fn(cthis voidptr, text voidptr, parent voidptr) 

/*

*/
pub fn (dummy QCheckBox) new_for_inherit_1(text string, parent  QWidget/*777 QWidget **/) QCheckBox {
  //return newQCheckBox1(text, parent)
  return QCheckBox{}
}
pub fn newQCheckBox1(text string, parent  QWidget/*777 QWidget **/) QCheckBox {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QCheckBoxC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1488224333, "_ZN9QCheckBoxC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQCheckBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQCheckBox)
    // qtrt.connect_destroyed(gothis, "QCheckBox")
  return vthis
}
// /usr/include/qt/QtWidgets/qcheckbox.h:62
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QCheckBox(const QString &, QWidget *)

/*

*/
pub fn (dummy QCheckBox) new_for_inherit_1p(text string) QCheckBox {
  //return newQCheckBox1p(text)
  return QCheckBox{}
}
pub fn newQCheckBox1p(text string) QCheckBox {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN9QCheckBoxC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1488224333, "_ZN9QCheckBoxC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQCheckBoxFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQCheckBox)
    // qtrt.connect_destroyed(gothis, "QCheckBox")
    return vthis
}
// /usr/include/qt/QtWidgets/qcheckbox.h:65
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize sizeHint() const
type T_ZNK9QCheckBox8sizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QCheckBox) sizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK9QCheckBox8sizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(539622553, "_ZNK9QCheckBox8sizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qcheckbox.h:66
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [8] QSize minimumSizeHint() const
type T_ZNK9QCheckBox15minimumSizeHintEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QCheckBox) minimumSizeHint()  qtcore.QSize/*123*/ {
    mut fnobj := T_ZNK9QCheckBox15minimumSizeHintEv(0)
    fnobj = qtrt.sym_qtfunc6(1367630296, "_ZNK9QCheckBox15minimumSizeHintEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQSize)
    return rv2
    //return qtcore.QSize{rv}
}
// /usr/include/qt/QtWidgets/qcheckbox.h:68
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTristate(bool)
type T_ZN9QCheckBox11setTristateEb = fn(cthis voidptr, y bool) /*void*/

/*

*/
pub fn (this QCheckBox) setTristate(y bool)  {
    mut fnobj := T_ZN9QCheckBox11setTristateEb(0)
    fnobj = qtrt.sym_qtfunc6(4240076077, "_ZN9QCheckBox11setTristateEb")
    fnobj(this.get_cthis(), y)
}
// /usr/include/qt/QtWidgets/qcheckbox.h:68
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setTristate(bool)

/*

*/
pub fn (this QCheckBox) setTristatep()  {
    // arg: 0, bool=Bool, =Invalid, , Invalid
    y := true
    mut fnobj := T_ZN9QCheckBox11setTristateEb(0)
    fnobj = qtrt.sym_qtfunc6(4240076077, "_ZN9QCheckBox11setTristateEb")
    fnobj(this.get_cthis(), y)
}
// /usr/include/qt/QtWidgets/qcheckbox.h:69
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isTristate() const
type T_ZNK9QCheckBox10isTristateEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QCheckBox) isTristate() bool {
    mut fnobj := T_ZNK9QCheckBox10isTristateEv(0)
    fnobj = qtrt.sym_qtfunc6(4236192185, "_ZNK9QCheckBox10isTristateEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qcheckbox.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] Qt::CheckState checkState() const
type T_ZNK9QCheckBox10checkStateEv = fn(cthis voidptr) int

/*

*/
pub fn (this QCheckBox) checkState() int {
    mut fnobj := T_ZNK9QCheckBox10checkStateEv(0)
    fnobj = qtrt.sym_qtfunc6(713884546, "_ZNK9QCheckBox10checkStateEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtWidgets/qcheckbox.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCheckState(Qt::CheckState)
type T_ZN9QCheckBox13setCheckStateEN2Qt10CheckStateE = fn(cthis voidptr, state int) /*void*/

/*

*/
pub fn (this QCheckBox) setCheckState(state int)  {
    mut fnobj := T_ZN9QCheckBox13setCheckStateEN2Qt10CheckStateE(0)
    fnobj = qtrt.sym_qtfunc6(847293468, "_ZN9QCheckBox13setCheckStateEN2Qt10CheckStateE")
    fnobj(this.get_cthis(), state)
}
// /usr/include/qt/QtWidgets/qcheckbox.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void stateChanged(int)
type T_ZN9QCheckBox12stateChangedEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QCheckBox) stateChanged(arg0 int)  {
    mut fnobj := T_ZN9QCheckBox12stateChangedEi(0)
    fnobj = qtrt.sym_qtfunc6(977411577, "_ZN9QCheckBox12stateChangedEi")
    fnobj(this.get_cthis(), arg0)
}

[no_inline]
pub fn deleteQCheckBox(this &QCheckBox) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(430589288, "_ZN9QCheckBoxD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QCheckBox) freecpp() { deleteQCheckBox(&this) }

fn (this QCheckBox) free() {

  /*deleteQCheckBox(&this)*/

  cthis := this.get_cthis()
  //println("QCheckBox freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10217() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
