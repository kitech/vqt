

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qabstractbutton.h
// #include <qabstractbutton.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 86
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QAbstractButton {
pub:
  QWidget
}

pub interface QAbstractButtonITF {
//    QWidgetITF
    get_cthis() voidptr
    toQAbstractButton() QAbstractButton
}
fn hotfix_QAbstractButton_itf_name_table(this QAbstractButtonITF) {
  that := QAbstractButton{}
  hotfix_QAbstractButton_itf_name_table(that)
}
pub fn (ptr QAbstractButton) toQAbstractButton() QAbstractButton { return ptr }

pub fn (this QAbstractButton) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQAbstractButtonFromptr(cthis voidptr) QAbstractButton {
    bcthis0 := newQWidgetFromptr(cthis)
    return QAbstractButton{bcthis0}
}
pub fn (dummy QAbstractButton) newFromptr(cthis voidptr) QAbstractButton {
    return newQAbstractButtonFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractButton(QWidget *)
type T_ZN15QAbstractButtonC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAbstractButton) new_for_inherit_(parent  QWidget/*777 QWidget **/) QAbstractButton {
  //return newQAbstractButton(parent)
  return QAbstractButton{}
}
pub fn newQAbstractButton(parent  QWidget/*777 QWidget **/) QAbstractButton {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN15QAbstractButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1746901202, "_ZN15QAbstractButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractButton)
    // qtrt.connect_destroyed(gothis, "QAbstractButton")
  return vthis
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractButton(QWidget *)

/*

*/
pub fn (dummy QAbstractButton) new_for_inherit_p() QAbstractButton {
  //return newQAbstractButtonp()
  return QAbstractButton{}
}
pub fn newQAbstractButtonp() QAbstractButton {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN15QAbstractButtonC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(1746901202, "_ZN15QAbstractButtonC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractButtonFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractButton)
    // qtrt.connect_destroyed(gothis, "QAbstractButton")
    return vthis
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:78
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setText(const QString &)
type T_ZN15QAbstractButton7setTextERK7QString = fn(cthis voidptr, text voidptr) /*void*/

/*

*/
pub fn (this QAbstractButton) setText(text string)  {
    mut tmp_arg0 := qtcore.newQString5(text)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN15QAbstractButton7setTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(199757402, "_ZN15QAbstractButton7setTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:79
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString text() const
type T_ZNK15QAbstractButton4textEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QAbstractButton) text() string {
    mut fnobj := T_ZNK15QAbstractButton4textEv(0)
    fnobj = qtrt.sym_qtfunc6(786980147, "_ZNK15QAbstractButton4textEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:91
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCheckable(bool)
type T_ZN15QAbstractButton12setCheckableEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAbstractButton) setCheckable(arg0 bool)  {
    mut fnobj := T_ZN15QAbstractButton12setCheckableEb(0)
    fnobj = qtrt.sym_qtfunc6(2802838592, "_ZN15QAbstractButton12setCheckableEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:92
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isCheckable() const
type T_ZNK15QAbstractButton11isCheckableEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAbstractButton) isCheckable() bool {
    mut fnobj := T_ZNK15QAbstractButton11isCheckableEv(0)
    fnobj = qtrt.sym_qtfunc6(1736830991, "_ZNK15QAbstractButton11isCheckableEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:94
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isChecked() const
type T_ZNK15QAbstractButton9isCheckedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAbstractButton) isChecked() bool {
    mut fnobj := T_ZNK15QAbstractButton9isCheckedEv(0)
    fnobj = qtrt.sym_qtfunc6(3862269578, "_ZNK15QAbstractButton9isCheckedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:96
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setDown(bool)
type T_ZN15QAbstractButton7setDownEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAbstractButton) setDown(arg0 bool)  {
    mut fnobj := T_ZN15QAbstractButton7setDownEb(0)
    fnobj = qtrt.sym_qtfunc6(2815823766, "_ZN15QAbstractButton7setDownEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:97
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isDown() const
type T_ZNK15QAbstractButton6isDownEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAbstractButton) isDown() bool {
    mut fnobj := T_ZNK15QAbstractButton6isDownEv(0)
    fnobj = qtrt.sym_qtfunc6(1977163016, "_ZNK15QAbstractButton6isDownEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:99
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setAutoRepeat(bool)
type T_ZN15QAbstractButton13setAutoRepeatEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAbstractButton) setAutoRepeat(arg0 bool)  {
    mut fnobj := T_ZN15QAbstractButton13setAutoRepeatEb(0)
    fnobj = qtrt.sym_qtfunc6(1398880692, "_ZN15QAbstractButton13setAutoRepeatEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:100
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool autoRepeat() const
type T_ZNK15QAbstractButton10autoRepeatEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAbstractButton) autoRepeat() bool {
    mut fnobj := T_ZNK15QAbstractButton10autoRepeatEv(0)
    fnobj = qtrt.sym_qtfunc6(3492544815, "_ZNK15QAbstractButton10autoRepeatEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:102
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setAutoRepeatDelay(int)
type T_ZN15QAbstractButton18setAutoRepeatDelayEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QAbstractButton) setAutoRepeatDelay(arg0 int)  {
    mut fnobj := T_ZN15QAbstractButton18setAutoRepeatDelayEi(0)
    fnobj = qtrt.sym_qtfunc6(2923678386, "_ZN15QAbstractButton18setAutoRepeatDelayEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:103
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int autoRepeatDelay() const
type T_ZNK15QAbstractButton15autoRepeatDelayEv = fn(cthis voidptr) int

/*

*/
pub fn (this QAbstractButton) autoRepeatDelay() int {
    mut fnobj := T_ZNK15QAbstractButton15autoRepeatDelayEv(0)
    fnobj = qtrt.sym_qtfunc6(4206853868, "_ZNK15QAbstractButton15autoRepeatDelayEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setAutoRepeatInterval(int)
type T_ZN15QAbstractButton21setAutoRepeatIntervalEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QAbstractButton) setAutoRepeatInterval(arg0 int)  {
    mut fnobj := T_ZN15QAbstractButton21setAutoRepeatIntervalEi(0)
    fnobj = qtrt.sym_qtfunc6(477848196, "_ZN15QAbstractButton21setAutoRepeatIntervalEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:106
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int autoRepeatInterval() const
type T_ZNK15QAbstractButton18autoRepeatIntervalEv = fn(cthis voidptr) int

/*

*/
pub fn (this QAbstractButton) autoRepeatInterval() int {
    mut fnobj := T_ZNK15QAbstractButton18autoRepeatIntervalEv(0)
    fnobj = qtrt.sym_qtfunc6(94216004, "_ZNK15QAbstractButton18autoRepeatIntervalEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:108
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setAutoExclusive(bool)
type T_ZN15QAbstractButton16setAutoExclusiveEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAbstractButton) setAutoExclusive(arg0 bool)  {
    mut fnobj := T_ZN15QAbstractButton16setAutoExclusiveEb(0)
    fnobj = qtrt.sym_qtfunc6(3712112795, "_ZN15QAbstractButton16setAutoExclusiveEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:109
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool autoExclusive() const
type T_ZNK15QAbstractButton13autoExclusiveEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAbstractButton) autoExclusive() bool {
    mut fnobj := T_ZNK15QAbstractButton13autoExclusiveEv(0)
    fnobj = qtrt.sym_qtfunc6(3776291618, "_ZNK15QAbstractButton13autoExclusiveEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:119
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void toggle()
type T_ZN15QAbstractButton6toggleEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAbstractButton) toggle()  {
    mut fnobj := T_ZN15QAbstractButton6toggleEv(0)
    fnobj = qtrt.sym_qtfunc6(3222906074, "_ZN15QAbstractButton6toggleEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:120
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setChecked(bool)
type T_ZN15QAbstractButton10setCheckedEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QAbstractButton) setChecked(arg0 bool)  {
    mut fnobj := T_ZN15QAbstractButton10setCheckedEb(0)
    fnobj = qtrt.sym_qtfunc6(1138771998, "_ZN15QAbstractButton10setCheckedEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:123
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void pressed()
type T_ZN15QAbstractButton7pressedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAbstractButton) pressed()  {
    mut fnobj := T_ZN15QAbstractButton7pressedEv(0)
    fnobj = qtrt.sym_qtfunc6(2025725746, "_ZN15QAbstractButton7pressedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:124
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void released()
type T_ZN15QAbstractButton8releasedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAbstractButton) released()  {
    mut fnobj := T_ZN15QAbstractButton8releasedEv(0)
    fnobj = qtrt.sym_qtfunc6(868254391, "_ZN15QAbstractButton8releasedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:125
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clicked(bool)
type T_ZN15QAbstractButton7clickedEb = fn(cthis voidptr, checked bool) /*void*/

/*

*/
pub fn (this QAbstractButton) clicked(checked bool)  {
    mut fnobj := T_ZN15QAbstractButton7clickedEb(0)
    fnobj = qtrt.sym_qtfunc6(184427086, "_ZN15QAbstractButton7clickedEb")
    fnobj(this.get_cthis(), checked)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:125
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clicked(bool)

/*

*/
pub fn (this QAbstractButton) clickedp()  {
    // arg: 0, bool=Bool, =Invalid, , Invalid
    checked := false
    mut fnobj := T_ZN15QAbstractButton7clickedEb(0)
    fnobj = qtrt.sym_qtfunc6(184427086, "_ZN15QAbstractButton7clickedEb")
    fnobj(this.get_cthis(), checked)
}
// /usr/include/qt/QtWidgets/qabstractbutton.h:126
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void toggled(bool)
type T_ZN15QAbstractButton7toggledEb = fn(cthis voidptr, checked bool) /*void*/

/*

*/
pub fn (this QAbstractButton) toggled(checked bool)  {
    mut fnobj := T_ZN15QAbstractButton7toggledEb(0)
    fnobj = qtrt.sym_qtfunc6(895414094, "_ZN15QAbstractButton7toggledEb")
    fnobj(this.get_cthis(), checked)
}

[no_inline]
pub fn deleteQAbstractButton(this &QAbstractButton) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1663717769, "_ZN15QAbstractButtonD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAbstractButton) freecpp() { deleteQAbstractButton(&this) }

fn (this QAbstractButton) free() {

  /*deleteQAbstractButton(&this)*/

  cthis := this.get_cthis()
  //println("QAbstractButton freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10181() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
