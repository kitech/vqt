

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qlistview.h
// #include <qlistview.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 12
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QListView {
pub:
  QAbstractItemView
}

pub interface QListViewITF {
//    QAbstractItemViewITF
    get_cthis() voidptr
    toQListView() QListView
}
fn hotfix_QListView_itf_name_table(this QListViewITF) {
  that := QListView{}
  hotfix_QListView_itf_name_table(that)
}
pub fn (ptr QListView) toQListView() QListView { return ptr }

pub fn (this QListView) get_cthis() voidptr {
    return this.QAbstractItemView.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQListViewFromptr(cthis voidptr) QListView {
    bcthis0 := newQAbstractItemViewFromptr(cthis)
    return QListView{bcthis0}
}
pub fn (dummy QListView) newFromptr(cthis voidptr) QListView {
    return newQListViewFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qlistview.h:82
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QListView(QWidget *)
type T_ZN9QListViewC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QListView) new_for_inherit_(parent  QWidget/*777 QWidget **/) QListView {
  //return newQListView(parent)
  return QListView{}
}
pub fn newQListView(parent  QWidget/*777 QWidget **/) QListView {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QListViewC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(4267936992, "_ZN9QListViewC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQListViewFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQListView)
    // qtrt.connect_destroyed(gothis, "QListView")
  return vthis
}
// /usr/include/qt/QtWidgets/qlistview.h:82
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QListView(QWidget *)

/*

*/
pub fn (dummy QListView) new_for_inherit_p() QListView {
  //return newQListViewp()
  return QListView{}
}
pub fn newQListViewp() QListView {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN9QListViewC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(4267936992, "_ZN9QListViewC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQListViewFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQListView)
    // qtrt.connect_destroyed(gothis, "QListView")
    return vthis
}

[no_inline]
pub fn deleteQListView(this &QListView) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3088598195, "_ZN9QListViewD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QListView) freecpp() { deleteQListView(&this) }

fn (this QListView) free() {

  /*deleteQListView(&this)*/

  cthis := this.get_cthis()
  //println("QListView freeing ${cthis} 0 bytes")

}


/*


*/
//type QListView.Movement = int
pub enum QListViewMovement {
  Static = 0
  Free = 1
  Snap = 2
} // endof enum Movement


/*


*/
//type QListView.Flow = int
pub enum QListViewFlow {
  LeftToRight = 0
  TopToBottom = 1
} // endof enum Flow


/*


*/
//type QListView.ResizeMode = int
pub enum QListViewResizeMode {
  Fixed = 0
  Adjust = 1
} // endof enum ResizeMode


/*


*/
//type QListView.LayoutMode = int
pub enum QListViewLayoutMode {
  SinglePass = 0
  Batched = 1
} // endof enum LayoutMode


/*


*/
//type QListView.ViewMode = int
pub enum QListViewViewMode {
  ListMode = 0
  IconMode = 1
} // endof enum ViewMode

//  body block end

//  keep block begin


fn init_unused_10233() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
