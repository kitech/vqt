

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qabstractitemview.h
// #include <qabstractitemview.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 5
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QAbstractItemView {
pub:
  QAbstractScrollArea
}

pub interface QAbstractItemViewITF {
//    QAbstractScrollAreaITF
    get_cthis() voidptr
    toQAbstractItemView() QAbstractItemView
}
fn hotfix_QAbstractItemView_itf_name_table(this QAbstractItemViewITF) {
  that := QAbstractItemView{}
  hotfix_QAbstractItemView_itf_name_table(that)
}
pub fn (ptr QAbstractItemView) toQAbstractItemView() QAbstractItemView { return ptr }

pub fn (this QAbstractItemView) get_cthis() voidptr {
    return this.QAbstractScrollArea.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQAbstractItemViewFromptr(cthis voidptr) QAbstractItemView {
    bcthis0 := newQAbstractScrollAreaFromptr(cthis)
    return QAbstractItemView{bcthis0}
}
pub fn (dummy QAbstractItemView) newFromptr(cthis voidptr) QAbstractItemView {
    return newQAbstractItemViewFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qabstractitemview.h:127
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractItemView(QWidget *)
type T_ZN17QAbstractItemViewC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAbstractItemView) new_for_inherit_(parent  QWidget/*777 QWidget **/) QAbstractItemView {
  //return newQAbstractItemView(parent)
  return QAbstractItemView{}
}
pub fn newQAbstractItemView(parent  QWidget/*777 QWidget **/) QAbstractItemView {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN17QAbstractItemViewC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2582094347, "_ZN17QAbstractItemViewC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractItemViewFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractItemView)
    // qtrt.connect_destroyed(gothis, "QAbstractItemView")
  return vthis
}
// /usr/include/qt/QtWidgets/qabstractitemview.h:127
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractItemView(QWidget *)

/*

*/
pub fn (dummy QAbstractItemView) new_for_inherit_p() QAbstractItemView {
  //return newQAbstractItemViewp()
  return QAbstractItemView{}
}
pub fn newQAbstractItemViewp() QAbstractItemView {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN17QAbstractItemViewC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2582094347, "_ZN17QAbstractItemViewC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractItemViewFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractItemView)
    // qtrt.connect_destroyed(gothis, "QAbstractItemView")
    return vthis
}

[no_inline]
pub fn deleteQAbstractItemView(this &QAbstractItemView) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(423019845, "_ZN17QAbstractItemViewD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAbstractItemView) freecpp() { deleteQAbstractItemView(&this) }

fn (this QAbstractItemView) free() {

  /*deleteQAbstractItemView(&this)*/

  cthis := this.get_cthis()
  //println("QAbstractItemView freeing ${cthis} 0 bytes")

}


/*


*/
//type QAbstractItemView.SelectionMode = int
pub enum QAbstractItemViewSelectionMode {
  NoSelection = 0
  SingleSelection = 1
  MultiSelection = 2
  ExtendedSelection = 3
  ContiguousSelection = 4
} // endof enum SelectionMode


/*


*/
//type QAbstractItemView.SelectionBehavior = int
pub enum QAbstractItemViewSelectionBehavior {
  SelectItems = 0
  SelectRows = 1
  SelectColumns = 2
} // endof enum SelectionBehavior


/*


*/
//type QAbstractItemView.ScrollHint = int
pub enum QAbstractItemViewScrollHint {
  EnsureVisible = 0
  PositionAtTop = 1
  PositionAtBottom = 2
  PositionAtCenter = 3
} // endof enum ScrollHint


/*


*/
//type QAbstractItemView.EditTrigger = int
pub enum QAbstractItemViewEditTrigger {
  NoEditTriggers = 0
  CurrentChanged = 1
  DoubleClicked = 2
  SelectedClicked = 4
  EditKeyPressed = 8
  AnyKeyPressed = 16
  AllEditTriggers = 31
} // endof enum EditTrigger


/*


*/
//type QAbstractItemView.ScrollMode = int
pub enum QAbstractItemViewScrollMode {
  ScrollPerItem = 0
  ScrollPerPixel = 1
} // endof enum ScrollMode


/*


*/
//type QAbstractItemView.DragDropMode = int
pub enum QAbstractItemViewDragDropMode {
  NoDragDrop = 0
  DragOnly = 1
  DropOnly = 2
  DragDrop = 3
  InternalMove = 4
} // endof enum DragDropMode


/*


*/
//type QAbstractItemView.CursorAction = int
pub enum QAbstractItemViewCursorAction {
  MoveUp = 0
  MoveDown = 1
  MoveLeft = 2
  MoveRight = 3
  MoveHome = 4
  MoveEnd = 5
  MovePageUp = 6
  MovePageDown = 7
  MoveNext = 8
  MovePrevious = 9
} // endof enum CursorAction


/*


*/
//type QAbstractItemView.State = int
pub enum QAbstractItemViewState {
  NoState = 0
  DraggingState = 1
  DragSelectingState = 2
  EditingState = 3
  ExpandingState = 4
  CollapsingState = 5
  AnimatingState = 6
} // endof enum State


/*


*/
//type QAbstractItemView.DropIndicatorPosition = int
pub enum QAbstractItemViewDropIndicatorPosition {
  OnItem = 0
  AboveItem = 1
  BelowItem = 2
  OnViewport = 3
} // endof enum DropIndicatorPosition

//  body block end

//  keep block begin


fn init_unused_10193() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
