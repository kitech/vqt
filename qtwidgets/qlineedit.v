

// +build !minimal

module qtwidgets
// /usr/include/qt/QtWidgets/qlineedit.h
// #include <qlineedit.h>
// #include <QtWidgets>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 12
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
// import github.com/kitech/qt.go/qtgui
import vqt.qtgui
//  ext block end

//  body block begin



/*

*/
pub struct QLineEdit {
pub:
  QWidget
}

pub interface QLineEditITF {
//    QWidgetITF
    get_cthis() voidptr
    toQLineEdit() QLineEdit
}
fn hotfix_QLineEdit_itf_name_table(this QLineEditITF) {
  that := QLineEdit{}
  hotfix_QLineEdit_itf_name_table(that)
}
pub fn (ptr QLineEdit) toQLineEdit() QLineEdit { return ptr }

pub fn (this QLineEdit) get_cthis() voidptr {
    return this.QWidget.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQLineEditFromptr(cthis voidptr) QLineEdit {
    bcthis0 := newQWidgetFromptr(cthis)
    return QLineEdit{bcthis0}
}
pub fn (dummy QLineEdit) newFromptr(cthis voidptr) QLineEdit {
    return newQLineEditFromptr(cthis)
}
// /usr/include/qt/QtWidgets/qlineedit.h:93
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLineEdit(QWidget *)
type T_ZN9QLineEditC2EP7QWidget = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QLineEdit) new_for_inherit_(parent  QWidget/*777 QWidget **/) QLineEdit {
  //return newQLineEdit(parent)
  return QLineEdit{}
}
pub fn newQLineEdit(parent  QWidget/*777 QWidget **/) QLineEdit {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QWidget_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QLineEditC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(390219950, "_ZN9QLineEditC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQLineEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLineEdit)
    // qtrt.connect_destroyed(gothis, "QLineEdit")
  return vthis
}
// /usr/include/qt/QtWidgets/qlineedit.h:93
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLineEdit(QWidget *)

/*

*/
pub fn (dummy QLineEdit) new_for_inherit_p() QLineEdit {
  //return newQLineEditp()
  return QLineEdit{}
}
pub fn newQLineEditp() QLineEdit {
    // arg: 0, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN9QLineEditC2EP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(390219950, "_ZN9QLineEditC2EP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQLineEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLineEdit)
    // qtrt.connect_destroyed(gothis, "QLineEdit")
    return vthis
}
// /usr/include/qt/QtWidgets/qlineedit.h:94
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLineEdit(const QString &, QWidget *)
type T_ZN9QLineEditC2ERK7QStringP7QWidget = fn(cthis voidptr, arg0 voidptr, parent voidptr) 

/*

*/
pub fn (dummy QLineEdit) new_for_inherit_1(arg0 string, parent  QWidget/*777 QWidget **/) QLineEdit {
  //return newQLineEdit1(arg0, parent)
  return QLineEdit{}
}
pub fn newQLineEdit1(arg0 string, parent  QWidget/*777 QWidget **/) QLineEdit {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QWidget_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QWidget_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN9QLineEditC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2344652411, "_ZN9QLineEditC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQLineEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLineEdit)
    // qtrt.connect_destroyed(gothis, "QLineEdit")
  return vthis
}
// /usr/include/qt/QtWidgets/qlineedit.h:94
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QLineEdit(const QString &, QWidget *)

/*

*/
pub fn (dummy QLineEdit) new_for_inherit_1p(arg0 string) QLineEdit {
  //return newQLineEdit1p(arg0)
  return QLineEdit{}
}
pub fn newQLineEdit1p(arg0 string) QLineEdit {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QWidget *=Pointer, QWidget=Record, , Invalid
    mut conv_arg1 := voidptr(0)
    mut fnobj := T_ZN9QLineEditC2ERK7QStringP7QWidget(0)
    fnobj = qtrt.sym_qtfunc6(2344652411, "_ZN9QLineEditC2ERK7QStringP7QWidget")
    mut cthis := qtrt.mallocraw(48)
    fnobj(cthis, conv_arg0, conv_arg1)
    rv := cthis
    vthis := newQLineEditFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQLineEdit)
    // qtrt.connect_destroyed(gothis, "QLineEdit")
    return vthis
}
// /usr/include/qt/QtWidgets/qlineedit.h:97
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString text() const
type T_ZNK9QLineEdit4textEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QLineEdit) text() string {
    mut fnobj := T_ZNK9QLineEdit4textEv(0)
    fnobj = qtrt.sym_qtfunc6(2325476995, "_ZNK9QLineEdit4textEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qlineedit.h:99
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString displayText() const
type T_ZNK9QLineEdit11displayTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QLineEdit) displayText() string {
    mut fnobj := T_ZNK9QLineEdit11displayTextEv(0)
    fnobj = qtrt.sym_qtfunc6(2064742310, "_ZNK9QLineEdit11displayTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qlineedit.h:101
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString placeholderText() const
type T_ZNK9QLineEdit15placeholderTextEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QLineEdit) placeholderText() string {
    mut fnobj := T_ZNK9QLineEdit15placeholderTextEv(0)
    fnobj = qtrt.sym_qtfunc6(4260629787, "_ZNK9QLineEdit15placeholderTextEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtWidgets/qlineedit.h:102
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPlaceholderText(const QString &)
type T_ZN9QLineEdit18setPlaceholderTextERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) setPlaceholderText(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QLineEdit18setPlaceholderTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2105386133, "_ZN9QLineEdit18setPlaceholderTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qlineedit.h:104
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int maxLength() const
type T_ZNK9QLineEdit9maxLengthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QLineEdit) maxLength() int {
    mut fnobj := T_ZNK9QLineEdit9maxLengthEv(0)
    fnobj = qtrt.sym_qtfunc6(709857483, "_ZNK9QLineEdit9maxLengthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtWidgets/qlineedit.h:105
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setMaxLength(int)
type T_ZN9QLineEdit12setMaxLengthEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QLineEdit) setMaxLength(arg0 int)  {
    mut fnobj := T_ZN9QLineEdit12setMaxLengthEi(0)
    fnobj = qtrt.sym_qtfunc6(1951674275, "_ZN9QLineEdit12setMaxLengthEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qlineedit.h:107
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFrame(bool)
type T_ZN9QLineEdit8setFrameEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QLineEdit) setFrame(arg0 bool)  {
    mut fnobj := T_ZN9QLineEdit8setFrameEb(0)
    fnobj = qtrt.sym_qtfunc6(4212786472, "_ZN9QLineEdit8setFrameEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qlineedit.h:108
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool hasFrame() const
type T_ZNK9QLineEdit8hasFrameEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QLineEdit) hasFrame() bool {
    mut fnobj := T_ZNK9QLineEdit8hasFrameEv(0)
    fnobj = qtrt.sym_qtfunc6(2152517407, "_ZNK9QLineEdit8hasFrameEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qlineedit.h:110
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setClearButtonEnabled(bool)
type T_ZN9QLineEdit21setClearButtonEnabledEb = fn(cthis voidptr, enable bool) /*void*/

/*

*/
pub fn (this QLineEdit) setClearButtonEnabled(enable bool)  {
    mut fnobj := T_ZN9QLineEdit21setClearButtonEnabledEb(0)
    fnobj = qtrt.sym_qtfunc6(678036293, "_ZN9QLineEdit21setClearButtonEnabledEb")
    fnobj(this.get_cthis(), enable)
}
// /usr/include/qt/QtWidgets/qlineedit.h:111
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isClearButtonEnabled() const
type T_ZNK9QLineEdit20isClearButtonEnabledEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QLineEdit) isClearButtonEnabled() bool {
    mut fnobj := T_ZNK9QLineEdit20isClearButtonEnabledEv(0)
    fnobj = qtrt.sym_qtfunc6(3216843421, "_ZNK9QLineEdit20isClearButtonEnabledEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtWidgets/qlineedit.h:119
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setReadOnly(bool)
type T_ZN9QLineEdit11setReadOnlyEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QLineEdit) setReadOnly(arg0 bool)  {
    mut fnobj := T_ZN9QLineEdit11setReadOnlyEb(0)
    fnobj = qtrt.sym_qtfunc6(2128045827, "_ZN9QLineEdit11setReadOnlyEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtWidgets/qlineedit.h:163
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setDragEnabled(bool)
type T_ZN9QLineEdit14setDragEnabledEb = fn(cthis voidptr, b bool) /*void*/

/*

*/
pub fn (this QLineEdit) setDragEnabled(b bool)  {
    mut fnobj := T_ZN9QLineEdit14setDragEnabledEb(0)
    fnobj = qtrt.sym_qtfunc6(292897755, "_ZN9QLineEdit14setDragEnabledEb")
    fnobj(this.get_cthis(), b)
}
// /usr/include/qt/QtWidgets/qlineedit.h:188
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setText(const QString &)
type T_ZN9QLineEdit7setTextERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) setText(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QLineEdit7setTextERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3250656154, "_ZN9QLineEdit7setTextERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qlineedit.h:207
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void textChanged(const QString &)
type T_ZN9QLineEdit11textChangedERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) textChanged(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QLineEdit11textChangedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2206252564, "_ZN9QLineEdit11textChangedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qlineedit.h:208
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void textEdited(const QString &)
type T_ZN9QLineEdit10textEditedERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) textEdited(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN9QLineEdit10textEditedERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1739381919, "_ZN9QLineEdit10textEditedERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtWidgets/qlineedit.h:209
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void cursorPositionChanged(int, int)
type T_ZN9QLineEdit21cursorPositionChangedEii = fn(cthis voidptr, arg0 int, arg1 int) /*void*/

/*

*/
pub fn (this QLineEdit) cursorPositionChanged(arg0 int, arg1 int)  {
    mut fnobj := T_ZN9QLineEdit21cursorPositionChangedEii(0)
    fnobj = qtrt.sym_qtfunc6(3269494345, "_ZN9QLineEdit21cursorPositionChangedEii")
    fnobj(this.get_cthis(), arg0, arg1)
}
// /usr/include/qt/QtWidgets/qlineedit.h:210
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void returnPressed()
type T_ZN9QLineEdit13returnPressedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) returnPressed()  {
    mut fnobj := T_ZN9QLineEdit13returnPressedEv(0)
    fnobj = qtrt.sym_qtfunc6(3672316555, "_ZN9QLineEdit13returnPressedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qlineedit.h:211
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void editingFinished()
type T_ZN9QLineEdit15editingFinishedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) editingFinished()  {
    mut fnobj := T_ZN9QLineEdit15editingFinishedEv(0)
    fnobj = qtrt.sym_qtfunc6(3160178389, "_ZN9QLineEdit15editingFinishedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qlineedit.h:212
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void selectionChanged()
type T_ZN9QLineEdit16selectionChangedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) selectionChanged()  {
    mut fnobj := T_ZN9QLineEdit16selectionChangedEv(0)
    fnobj = qtrt.sym_qtfunc6(575234119, "_ZN9QLineEdit16selectionChangedEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtWidgets/qlineedit.h:213
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void inputRejected()
type T_ZN9QLineEdit13inputRejectedEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QLineEdit) inputRejected()  {
    mut fnobj := T_ZN9QLineEdit13inputRejectedEv(0)
    fnobj = qtrt.sym_qtfunc6(796961596, "_ZN9QLineEdit13inputRejectedEv")
    fnobj(this.get_cthis())
}

[no_inline]
pub fn deleteQLineEdit(this &QLineEdit) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1920136570, "_ZN9QLineEditD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QLineEdit) freecpp() { deleteQLineEdit(&this) }

fn (this QLineEdit) free() {

  /*deleteQLineEdit(&this)*/

  cthis := this.get_cthis()
  //println("QLineEdit freeing ${cthis} 0 bytes")

}


/*


*/
//type QLineEdit.ActionPosition = int
pub enum QLineEditActionPosition {
  LeadingPosition = 0
  TrailingPosition = 1
} // endof enum ActionPosition


/*


*/
//type QLineEdit.EchoMode = int
pub enum QLineEditEchoMode {
  Normal = 0
  NoEcho = 1
  Password = 2
  PasswordEchoOnEdit = 3
} // endof enum EchoMode

//  body block end

//  keep block begin


fn init_unused_10229() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
if false {qtgui.keepme()}
}
//  keep block end
