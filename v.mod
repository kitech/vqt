Module {
	name: 'qt'
	description: 'Qt binding library.'
	version: '5.x'
	license: 'MIT'
	repo_url: 'qt.io'
	dependencies: []
}
