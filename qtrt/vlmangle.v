module qtrt

pub fn modof(submod string) string {
    return "vqt__qt${submod}__"
}

fn sym_typefunc(typ string, fun string) voidptr {
    for i := qtmods.len-1; i>=0;i--{
        mod := qtmods[i]
        fullmod := modof(mod) + typ +"_"+ fun
        ptr := sym_mefunc6(0,fullmod)
        if ptr != vnil {
            return ptr
        }
    }
    return vnil
}

fn sym_freefunc(fun string) voidptr {
    for i := qtmods.len-1; i>=0;i--{
        mod := qtmods[i]
        fullmod := modof(mod) + fun
        ptr := sym_mefunc6(0,fullmod)
        if ptr != vnil {
            infoln(@FILE, @LINE, fullmod, ptr)
            return ptr
        }
    }
    return vnil
}

fn qtsym_fromptr(cls string) voidptr {
    return sym_freefunc("new${cls}Fromptr")
}
fn qtsym_delete(cls string) voidptr {
    return sym_freefunc("delete${cls}")
}
fn qtsym_get_cthis(cls string) voidptr {
    return sym_typefunc(cls, "get_cthis")
}

// not support overload
fn qtsym_method(cls string, mth string) voidptr {
    return sym_typefunc(cls, mth)
}


