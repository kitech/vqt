module qtrt

fn test_getVtableEntry() {
    syms := getVtableEntry("QLabel")
    // println(syms)
    // println(syms.len)
    assert syms.len == 64
}

fn get_test_symbol() string {
    return "_ZN6QLabel12focusInEventEP11QFocusEvent"
}
fn get_test_symbol2() string {
    return "_ZN15QAbstractButton16staticMetaObjectE"
}

fn test_demangle() {
    line, _ := cxa_demangle(get_test_symbol())
    assert line.len == 34
    assert line == "QLabel::focusInEvent(QFocusEvent*)"
    // println(line)
}

fn test_cppfilt() {
    cmo := cppfilt(get_test_symbol())
    assert cmo.cls == "QLabel"
    assert cmo.mth == "focusInEvent"
    assert cmo.types.len == 1
}

fn test_get_vtm_index() {
    mut idx := virtab.getVTMIndex("QLabel", "focusInEvent")
    assert idx != -1
    idx = virtab.getVTMIndex("QLabel", "focusInEvent123")
    assert idx == -1
}


