#ifndef QTRT_VTHOOK_H
#define QTRT_VTHOOK_H

#include <stdint.h>

extern void dump_vtable_entry(void*);
extern int fillin_vtable_entry(void*, void**);

void* vtablehook_hook(void* instance, void* hook, int offset);

extern /*ffi_closure*/void*
make_cppvm_ffi_closure(/*ffi_cif*/void* cif, void** closfnaddr, void* capdata,
                       void* retype, int argc, uintptr_t* argtys);
extern /*ffi_closure*/void*
make_cppvm_ffi_closure2(/*ffi_cif*/void* cif, void** closfnaddr, void* capdata,
                       void* retype, int argc, uintptr_t* argtys);
extern void cppvm_ffi_closure_callback(void* cthis, void** argvals);

#endif /* QTRT_VTHOOK_H */

