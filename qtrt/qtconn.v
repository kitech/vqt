module qtrt

struct Qtconn {
    mut:
    signal string
    fnptr voidptr
    cobj voidptr
    vobj voidptr
}

fn qtconn_exist(sender voidptr, signal string) bool {
    exist := qtconn_exist2(sender, signal)
    println("qtconn_exist: ${sender}, ${signal} ${exist}")
    return exist
}
fn qtconn_exist2(sender voidptr, signal string) bool {
    rtv := rtg
    if sender in rtv.conns {
        if signal in rtv.conns[sender] {
            return true
        }
    }
    return false
}

fn qtconn_get(sender voidptr, signal string) &Qtconn {
    println("qtconn_exist: ${sender}, ${signal}")
    rtv := rtg
    if sender in rtv.conns {
        if signal in rtv.conns[sender] {
            return rtv.conns[sender][signal]
        }
    }
    return vnil
}

fn qtconn_put(sender voidptr, signal string, fnptr voidptr) &Qtconn {
    println("qtconn_put: ${sender}, ${signal}")
    mut rtv := rtg
    if sender in rtv.conns {
        if signal in rtv.conns[sender] {
            println("already exists ${signal}")
        }else{
            mut con := &Qtconn{}
            con.signal = signal
            con.fnptr = fnptr
            rtv.conns[sender][signal] = con
            return con
        }
    }else{
        mut con := &Qtconn{}
        con.signal = signal
        con.fnptr = fnptr
        rtv.conns[sender][signal] = con
        return con
    }
    assert qtconn_exist(sender, signal) == true
    return vnil
}

fn qtconn_delete(sender voidptr, signal string) {
    mut rtv := rtg
    if sender in rtv.conns {
        if signal in rtv.conns[sender] {
            rtv.conns[sender].delete(signal)
        }else{
            println("qtconn not exist signal")
        }
    }else{
        println("qtconn not exist sender")
    }
}

fn qtconn_delete_all(sender voidptr) {

}
