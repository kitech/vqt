module qtrt

/*
usage:
    ifelse(true, 1, 2)
    ifelse(true, "1", "2")
    ifelse(true, voidptr(0), voidptr(1))
    ifelse(true, f32(0), f32(1))
*/
pub fn ifelse<T>(cond bool, a T, b T) T {
    if cond { return a}
    return b
}



///////////
