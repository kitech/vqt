module qtrt

import ffi

/*
pub fn setAllInheritCallback2(cbobj CObjectITF, name string, f interface{}) {
	if cbobj.GetCthis() != nil {
	} else { // take as global function call, just no this field
		if debugDynSlot {
			log.Println("obj is nil, try as global event", name)
		}
	}

	clsname := getClassNameByObject(cbobj)
	log.Println(clsname, reflect.TypeOf(cbobj).String(), cbobj.GetCthis())

	// check hookable
	vtidx := virtab.getVTMIndex(clsname, name)
	if vtidx == -1 {
		log.Println("Cannot inherit for not found", clsname, name)
		return
	}

	if signal := qt.LendSignal(cbobj.GetCthis(), name); signal != nil {
		log.Println("already exists:", name, reflect.TypeOf(cbobj))
		qt.ConnectSignal(cbobj.GetCthis(), name, func(args ...uint64) {
			signal.(func(...uint64))(args...)
			callbackInheritInvokeGo(f, args...)
		})
	} else {
		qt.ConnectSignal(cbobj.GetCthis(), name, f)
	}

	// check hook
	virtab.hookit(clsname, name, cbobj.GetCthis(), f)
}


// TODO unhookit
pub fn unsetAllInheritCallback2(cbobj CObjectITF, name string) {
	if signal := qt.LendSignal(cbobj.GetCthis(), name); signal != nil {
		qt.DisconnectSignal(cbobj.GetCthis(), name)
	} else {
		log.Println("not exists:", cbobj, name)
	}
}
*/

type T123 = fn(voidptr) int

const sretmemsz = 16*5
// called by cppvm_ffi_closure_callback
fn callbackHookInherits2(vmclos &CppvmClosure, argvals &voidptr) {
    infoln(@FILE, @LINE, vmclos.clsname, argvals)
    cbobj := voidptr(argvals[0]) // useless???

    argc := vmclos.argc - 1
    infoln(@FILE, @LINE, "argc", argc, vmclos.cpparg_types)

    mut types := []voidptr{len:argc+8}
    mut values := []voidptr{len:argc+8}
    // save tmp value
    mut tmpvals := []voidptr{len:argc+8}
    // save tmp value address
    mut tmpadrs := []voidptr{len:argc+8}

    // reflect.call newQClassFromptr if needed
    for idx, cppty in vmclos.cpparg_types {
        if cppty.starts_with("Q") && cppty.contains_any("*&") {
            types[0] = ffi.to_pointer
            types[1] = ffi.to_pointer

            clsname := cppty.trim_right("*&")
            infoln(@FILE, @LINE, idx, clsname)
            fnptr := qtsym_fromptr(clsname) // ("QKeyEvent")
            infoln(@FILE, @LINE, idx, clsname, fnptr)
            assert fnptr != vnil

            // force sret
            // ret = newQClassFromptr(cthis) => newQClassFromptr(&sret, cthis)
            retval := mallocgc(sretmemsz)
            values[0] = &retval
            values[1] = argvals[1+idx]
            ffi.call4(fnptr, 2, types.data, values.data)
            infoln(@FILE, @LINE, retval)
            tmpvals[idx] = retval
            unsafe {
                tmpadrs[idx] = &tmpvals[idx]
            }
        }else if cppty.starts_with("Q") { // maybe QClass record
            infoln(@FILE, @LINE, "unimpl", cppty)
        }else{
            tmpadrs[idx] = argvals[1+idx]
        }
    }

    infoln(@FILE, @LINE, vmclos.argtys[1], ffi.to_pointer)
    // call user suply callback
    ffi.call4(vmclos.upcbfn, argc, &vmclos.argtys[1], tmpadrs.data)

}

