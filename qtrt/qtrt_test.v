module qtrt

import time

fn test_dlhme() {
    ptr := sym_mefunc6(0, "main__start_testing")
    ptr2 := sym_mefunc6(0, "main__main")

    println("main__start_testing $ptr, main__main $ptr2")
    assert ptr != vnil || ptr2 != vnil
}

