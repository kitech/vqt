module qtrt

import ffi

// see qobjectdefs.h:263
pub fn qmethod(name string) string { return "0${name}"}
pub fn qslot(name string) string { return "1${name}"}
pub fn qsignal(name string) string { return "2${name}"}
pub fn unqsignal(name string) string { return name[1..] }
pub fn unqslot(name string) string { return name[1..] }
pub fn unqmethod(name string) string { return name[1..] }


pub fn callback_all_qdynslot_object(obj voidptr, callty int, callid int, argvals &voidptr,
                                    name byteptr, argc int, argtys &int, cbptr voidptr) {
    name_ := tos2(name)
    callback_all_qdynslot_object_v(obj, callty, callid, argvals, name_, argc, argtys, cbptr)
}

type T_voidfn = fn()
pub fn callback_all_qdynslot_object_v(obj voidptr, callty int, callid int, argvals &voidptr,
                                      name string, argc int, argtys &int, cbptr voidptr) {
    sobj := cbptr
    exist := qtconn_exist(sobj, name)
    // println("commmmmmmmm ${obj}, ${name}, ${argc}, ${argtys}, ${cbptr} exist: ${exist}")
    con := qtconn_get(sobj, name)
    if con == vnil {
        println("wtf")
    }
    if con != vnil {}
    assert con != vnil
    assert con.fnptr != vnil
    fnptr := con.fnptr
    // println("${name}, fnptr ${fnptr}")

    // ffi call
    mut atypes := []int{}
    mut avalues := []voidptr{}
    start := 1 // _a[0] special, is return value (sret)
    for i in 0..argc {
        i1 := i + start
        argval := argvals[i1]
        argty := argtys[i]
        atypes << argty
        if argty == ffi.TYPE_POINTER {
            avalues << &argvals[i1] // 修正了 Qt与FFI传递指针的不同
        }else{
            avalues << argval
        }
        // println("argc ${argc} ${i} ${argval} ty ${argty}")
    }
    rv := ffi.call3(fnptr, atypes, avalues)
    // println("rv=${rv} ${argvals} ${argc} ${argtys}")
}

