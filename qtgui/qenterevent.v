

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 2
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QEnterEvent {
pub:
  qtcore.QEvent
}

pub interface QEnterEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQEnterEvent() QEnterEvent
}
fn hotfix_QEnterEvent_itf_name_table(this QEnterEventITF) {
  that := QEnterEvent{}
  hotfix_QEnterEvent_itf_name_table(that)
}
pub fn (ptr QEnterEvent) toQEnterEvent() QEnterEvent { return ptr }

pub fn (this QEnterEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQEnterEventFromptr(cthis voidptr) QEnterEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QEnterEvent{bcthis0}
}
pub fn (dummy QEnterEvent) newFromptr(cthis voidptr) QEnterEvent {
    return newQEnterEventFromptr(cthis)
}

[no_inline]
pub fn deleteQEnterEvent(this &QEnterEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(727210895, "_ZN11QEnterEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QEnterEvent) freecpp() { deleteQEnterEvent(&this) }

fn (this QEnterEvent) free() {

  /*deleteQEnterEvent(&this)*/

  cthis := this.get_cthis()
  //println("QEnterEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10069() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
