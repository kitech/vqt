

module qtgui
// /usr/include/qt/QtGui/qcolor.h
// #include <qcolor.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QColor {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QColorITF {
    get_cthis() voidptr
    toQColor() QColor
}
fn hotfix_QColor_itf_name_table(this QColorITF) {
  that := QColor{}
  hotfix_QColor_itf_name_table(that)
}
pub fn (ptr QColor) toQColor() QColor { return ptr }

pub fn (this QColor) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQColorFromptr(cthis voidptr) QColor {
    return QColor{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QColor) newFromptr(cthis voidptr) QColor {
    return newQColorFromptr(cthis)
}
// /usr/include/qt/QtGui/qcolor.h:70
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QColor()
type T_ZN6QColorC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QColor) new_for_inherit_() QColor {
  //return newQColor()
  return QColor{}
}
pub fn newQColor() QColor {
    mut fnobj := T_ZN6QColorC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(2667467580, "_ZN6QColorC2Ev")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis)
    rv := cthis
    vthis := newQColorFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQColor)
  return vthis
}
// /usr/include/qt/QtGui/qcolor.h:73
// index:1 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QColor(int, int, int, int)
type T_ZN6QColorC2Eiiii = fn(cthis voidptr, r int, g int, b int, a int) 

/*

*/
pub fn (dummy QColor) new_for_inherit_1(r int, g int, b int, a int) QColor {
  //return newQColor1(r, g, b, a)
  return QColor{}
}
pub fn newQColor1(r int, g int, b int, a int) QColor {
    mut fnobj := T_ZN6QColorC2Eiiii(0)
    fnobj = qtrt.sym_qtfunc6(3599673984, "_ZN6QColorC2Eiiii")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, r, g, b, a)
    rv := cthis
    vthis := newQColorFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQColor)
  return vthis
}
// /usr/include/qt/QtGui/qcolor.h:73
// index:1 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QColor(int, int, int, int)

/*

*/
pub fn (dummy QColor) new_for_inherit_1p(r int, g int, b int) QColor {
  //return newQColor1p(r, g, b)
  return QColor{}
}
pub fn newQColor1p(r int, g int, b int) QColor {
    // arg: 3, int=Int, =Invalid, , Invalid
    a := int(255)
    mut fnobj := T_ZN6QColorC2Eiiii(0)
    fnobj = qtrt.sym_qtfunc6(3599673984, "_ZN6QColorC2Eiiii")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, r, g, b, a)
    rv := cthis
    vthis := newQColorFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQColor)
    return vthis
}

[no_inline]
pub fn deleteQColor(this &QColor) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(53046149, "_ZN6QColorD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QColor) freecpp() { deleteQColor(&this) }

fn (this QColor) free() {

  /*deleteQColor(&this)*/

  cthis := this.get_cthis()
  //println("QColor freeing ${cthis} 0 bytes")

}


/*


*/
//type QColor.Spec = int
pub enum QColorSpec {
  Invalid = 0
  Rgb = 1
  Hsv = 2
  Cmyk = 3
  Hsl = 4
  ExtendedRgb = 5
} // endof enum Spec


/*


*/
//type QColor.NameFormat = int
pub enum QColorNameFormat {
  HexRgb = 0
  HexArgb = 1
} // endof enum NameFormat

//  body block end

//  keep block begin


fn init_unused_10065() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
