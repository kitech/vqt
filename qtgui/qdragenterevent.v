

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QDragEnterEvent {
pub:
  QDragMoveEvent
}

pub interface QDragEnterEventITF {
//    QDragMoveEventITF
    get_cthis() voidptr
    toQDragEnterEvent() QDragEnterEvent
}
fn hotfix_QDragEnterEvent_itf_name_table(this QDragEnterEventITF) {
  that := QDragEnterEvent{}
  hotfix_QDragEnterEvent_itf_name_table(that)
}
pub fn (ptr QDragEnterEvent) toQDragEnterEvent() QDragEnterEvent { return ptr }

pub fn (this QDragEnterEvent) get_cthis() voidptr {
    return this.QDragMoveEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQDragEnterEventFromptr(cthis voidptr) QDragEnterEvent {
    bcthis0 := newQDragMoveEventFromptr(cthis)
    return QDragEnterEvent{bcthis0}
}
pub fn (dummy QDragEnterEvent) newFromptr(cthis voidptr) QDragEnterEvent {
    return newQDragEnterEventFromptr(cthis)
}

[no_inline]
pub fn deleteQDragEnterEvent(this &QDragEnterEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2127097226, "_ZN15QDragEnterEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDragEnterEvent) freecpp() { deleteQDragEnterEvent(&this) }

fn (this QDragEnterEvent) free() {

  /*deleteQDragEnterEvent(&this)*/

  cthis := this.get_cthis()
  //println("QDragEnterEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10113() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
