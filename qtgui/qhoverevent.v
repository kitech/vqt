

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 9
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QHoverEvent {
pub:
  QInputEvent
}

pub interface QHoverEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQHoverEvent() QHoverEvent
}
fn hotfix_QHoverEvent_itf_name_table(this QHoverEventITF) {
  that := QHoverEvent{}
  hotfix_QHoverEvent_itf_name_table(that)
}
pub fn (ptr QHoverEvent) toQHoverEvent() QHoverEvent { return ptr }

pub fn (this QHoverEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQHoverEventFromptr(cthis voidptr) QHoverEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QHoverEvent{bcthis0}
}
pub fn (dummy QHoverEvent) newFromptr(cthis voidptr) QHoverEvent {
    return newQHoverEventFromptr(cthis)
}

[no_inline]
pub fn deleteQHoverEvent(this &QHoverEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2903469262, "_ZN11QHoverEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QHoverEvent) freecpp() { deleteQHoverEvent(&this) }

fn (this QHoverEvent) free() {

  /*deleteQHoverEvent(&this)*/

  cthis := this.get_cthis()
  //println("QHoverEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10073() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
