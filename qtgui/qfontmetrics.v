

module qtgui
// /usr/include/qt/QtGui/qfontmetrics.h
// #include <qfontmetrics.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QFontMetrics {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QFontMetricsITF {
    get_cthis() voidptr
    toQFontMetrics() QFontMetrics
}
fn hotfix_QFontMetrics_itf_name_table(this QFontMetricsITF) {
  that := QFontMetrics{}
  hotfix_QFontMetrics_itf_name_table(that)
}
pub fn (ptr QFontMetrics) toQFontMetrics() QFontMetrics { return ptr }

pub fn (this QFontMetrics) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQFontMetricsFromptr(cthis voidptr) QFontMetrics {
    return QFontMetrics{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QFontMetrics) newFromptr(cthis voidptr) QFontMetrics {
    return newQFontMetricsFromptr(cthis)
}

[no_inline]
pub fn deleteQFontMetrics(this &QFontMetrics) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1609985194, "_ZN12QFontMetricsD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QFontMetrics) freecpp() { deleteQFontMetrics(&this) }

fn (this QFontMetrics) free() {

  /*deleteQFontMetrics(&this)*/

  cthis := this.get_cthis()
  //println("QFontMetrics freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10169() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
