

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QApplicationStateChangeEvent {
pub:
  qtcore.QEvent
}

pub interface QApplicationStateChangeEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQApplicationStateChangeEvent() QApplicationStateChangeEvent
}
fn hotfix_QApplicationStateChangeEvent_itf_name_table(this QApplicationStateChangeEventITF) {
  that := QApplicationStateChangeEvent{}
  hotfix_QApplicationStateChangeEvent_itf_name_table(that)
}
pub fn (ptr QApplicationStateChangeEvent) toQApplicationStateChangeEvent() QApplicationStateChangeEvent { return ptr }

pub fn (this QApplicationStateChangeEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQApplicationStateChangeEventFromptr(cthis voidptr) QApplicationStateChangeEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QApplicationStateChangeEvent{bcthis0}
}
pub fn (dummy QApplicationStateChangeEvent) newFromptr(cthis voidptr) QApplicationStateChangeEvent {
    return newQApplicationStateChangeEventFromptr(cthis)
}

[no_inline]
pub fn deleteQApplicationStateChangeEvent(this &QApplicationStateChangeEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2579810998, "_ZN28QApplicationStateChangeEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QApplicationStateChangeEvent) freecpp() { deleteQApplicationStateChangeEvent(&this) }

fn (this QApplicationStateChangeEvent) free() {

  /*deleteQApplicationStateChangeEvent(&this)*/

  cthis := this.get_cthis()
  //println("QApplicationStateChangeEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10141() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
