

module qtgui
// /usr/include/qt/QtGui/qimage.h
// #include <qimage.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 16
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QImage {
pub:
  QPaintDevice
}

pub interface QImageITF {
//    QPaintDeviceITF
    get_cthis() voidptr
    toQImage() QImage
}
fn hotfix_QImage_itf_name_table(this QImageITF) {
  that := QImage{}
  hotfix_QImage_itf_name_table(that)
}
pub fn (ptr QImage) toQImage() QImage { return ptr }

pub fn (this QImage) get_cthis() voidptr {
    return this.QPaintDevice.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQImageFromptr(cthis voidptr) QImage {
    bcthis0 := newQPaintDeviceFromptr(cthis)
    return QImage{bcthis0}
}
pub fn (dummy QImage) newFromptr(cthis voidptr) QImage {
    return newQImageFromptr(cthis)
}
// /usr/include/qt/QtGui/qimage.h:141
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QImage()
type T_ZN6QImageC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QImage) new_for_inherit_() QImage {
  //return newQImage()
  return QImage{}
}
pub fn newQImage() QImage {
    mut fnobj := T_ZN6QImageC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(380729056, "_ZN6QImageC2Ev")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis)
    rv := cthis
    vthis := newQImageFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQImage)
  return vthis
}
// /usr/include/qt/QtGui/qimage.h:142
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QImage(const QSize &, QImage::Format)
type T_ZN6QImageC2ERK5QSizeNS_6FormatE = fn(cthis voidptr, size voidptr, format int) 

/*

*/
pub fn (dummy QImage) new_for_inherit_1(size  qtcore.QSizeITF, format int) QImage {
  //return newQImage1(size, format)
  return QImage{}
}
pub fn newQImage1(size  qtcore.QSizeITF, format int) QImage {
    mut conv_arg0 := voidptr(0)
    /*if size != voidptr(0) && size.QSize_ptr() != voidptr(0) */ {
        // conv_arg0 = size.QSize_ptr().get_cthis()
      conv_arg0 = size.get_cthis()
    }
    mut fnobj := T_ZN6QImageC2ERK5QSizeNS_6FormatE(0)
    fnobj = qtrt.sym_qtfunc6(2386780666, "_ZN6QImageC2ERK5QSizeNS_6FormatE")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis, conv_arg0, format)
    rv := cthis
    vthis := newQImageFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQImage)
  return vthis
}
// /usr/include/qt/QtGui/qimage.h:143
// index:2 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QImage(int, int, QImage::Format)
type T_ZN6QImageC2EiiNS_6FormatE = fn(cthis voidptr, width int, height int, format int) 

/*

*/
pub fn (dummy QImage) new_for_inherit_2(width int, height int, format int) QImage {
  //return newQImage2(width, height, format)
  return QImage{}
}
pub fn newQImage2(width int, height int, format int) QImage {
    mut fnobj := T_ZN6QImageC2EiiNS_6FormatE(0)
    fnobj = qtrt.sym_qtfunc6(355905797, "_ZN6QImageC2EiiNS_6FormatE")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis, width, height, format)
    rv := cthis
    vthis := newQImageFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQImage)
  return vthis
}

[no_inline]
pub fn deleteQImage(this &QImage) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2338737753, "_ZN6QImageD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QImage) freecpp() { deleteQImage(&this) }

fn (this QImage) free() {

  /*deleteQImage(&this)*/

  cthis := this.get_cthis()
  //println("QImage freeing ${cthis} 0 bytes")

}


/*


*/
//type QImage.InvertMode = int
pub enum QImageInvertMode {
  InvertRgb = 0
  InvertRgba = 1
} // endof enum InvertMode


/*


*/
//type QImage.Format = int
pub enum QImageFormat {
  Format_Invalid = 0
  Format_Mono = 1
  Format_MonoLSB = 2
  Format_Indexed8 = 3
  Format_RGB32 = 4
  Format_ARGB32 = 5
  Format_ARGB32_Premultiplied = 6
  Format_RGB16 = 7
  Format_ARGB8565_Premultiplied = 8
  Format_RGB666 = 9
  Format_ARGB6666_Premultiplied = 10
  Format_RGB555 = 11
  Format_ARGB8555_Premultiplied = 12
  Format_RGB888 = 13
  Format_RGB444 = 14
  Format_ARGB4444_Premultiplied = 15
  Format_RGBX8888 = 16
  Format_RGBA8888 = 17
  Format_RGBA8888_Premultiplied = 18
  Format_BGR30 = 19
  Format_A2BGR30_Premultiplied = 20
  Format_RGB30 = 21
  Format_A2RGB30_Premultiplied = 22
  Format_Alpha8 = 23
  Format_Grayscale8 = 24
  Format_RGBX64 = 25
  Format_RGBA64 = 26
  Format_RGBA64_Premultiplied = 27
  Format_Grayscale16 = 28
  Format_BGR888 = 29
  NImageFormats = 30
} // endof enum Format

//  body block end

//  keep block begin


fn init_unused_10147() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
