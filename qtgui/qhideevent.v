

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QHideEvent {
pub:
  qtcore.QEvent
}

pub interface QHideEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQHideEvent() QHideEvent
}
fn hotfix_QHideEvent_itf_name_table(this QHideEventITF) {
  that := QHideEvent{}
  hotfix_QHideEvent_itf_name_table(that)
}
pub fn (ptr QHideEvent) toQHideEvent() QHideEvent { return ptr }

pub fn (this QHideEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQHideEventFromptr(cthis voidptr) QHideEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QHideEvent{bcthis0}
}
pub fn (dummy QHideEvent) newFromptr(cthis voidptr) QHideEvent {
    return newQHideEventFromptr(cthis)
}

[no_inline]
pub fn deleteQHideEvent(this &QHideEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4019755370, "_ZN10QHideEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QHideEvent) freecpp() { deleteQHideEvent(&this) }

fn (this QHideEvent) free() {

  /*deleteQHideEvent(&this)*/

  cthis := this.get_cthis()
  //println("QHideEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10101() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
