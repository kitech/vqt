

module qtgui
// /usr/include/qt/QtGui/qfontinfo.h
// #include <qfontinfo.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 3
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QFontInfo {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QFontInfoITF {
    get_cthis() voidptr
    toQFontInfo() QFontInfo
}
fn hotfix_QFontInfo_itf_name_table(this QFontInfoITF) {
  that := QFontInfo{}
  hotfix_QFontInfo_itf_name_table(that)
}
pub fn (ptr QFontInfo) toQFontInfo() QFontInfo { return ptr }

pub fn (this QFontInfo) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQFontInfoFromptr(cthis voidptr) QFontInfo {
    return QFontInfo{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QFontInfo) newFromptr(cthis voidptr) QFontInfo {
    return newQFontInfoFromptr(cthis)
}

[no_inline]
pub fn deleteQFontInfo(this &QFontInfo) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3740458797, "_ZN9QFontInfoD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QFontInfo) freecpp() { deleteQFontInfo(&this) }

fn (this QFontInfo) free() {

  /*deleteQFontInfo(&this)*/

  cthis := this.get_cthis()
  //println("QFontInfo freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10167() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
