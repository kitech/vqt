

module qtgui
// /usr/include/qt/QtGui/qtextoption.h
// #include <qtextoption.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QTextOption {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QTextOptionITF {
    get_cthis() voidptr
    toQTextOption() QTextOption
}
fn hotfix_QTextOption_itf_name_table(this QTextOptionITF) {
  that := QTextOption{}
  hotfix_QTextOption_itf_name_table(that)
}
pub fn (ptr QTextOption) toQTextOption() QTextOption { return ptr }

pub fn (this QTextOption) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQTextOptionFromptr(cthis voidptr) QTextOption {
    return QTextOption{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QTextOption) newFromptr(cthis voidptr) QTextOption {
    return newQTextOptionFromptr(cthis)
}
// /usr/include/qt/QtGui/qtextoption.h:85
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QTextOption()
type T_ZN11QTextOptionC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QTextOption) new_for_inherit_() QTextOption {
  //return newQTextOption()
  return QTextOption{}
}
pub fn newQTextOption() QTextOption {
    mut fnobj := T_ZN11QTextOptionC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(1571547807, "_ZN11QTextOptionC2Ev")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis)
    rv := cthis
    vthis := newQTextOptionFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQTextOption)
  return vthis
}

[no_inline]
pub fn deleteQTextOption(this &QTextOption) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3229405734, "_ZN11QTextOptionD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QTextOption) freecpp() { deleteQTextOption(&this) }

fn (this QTextOption) free() {

  /*deleteQTextOption(&this)*/

  cthis := this.get_cthis()
  //println("QTextOption freeing ${cthis} 0 bytes")

}


/*


*/
//type QTextOption.TabType = int
pub enum QTextOptionTabType {
  LeftTab = 0
  RightTab = 1
  CenterTab = 2
  DelimiterTab = 3
} // endof enum TabType


/*


*/
//type QTextOption.WrapMode = int
pub enum QTextOptionWrapMode {
  NoWrap = 0
  WordWrap = 1
  ManualWrap = 2
  WrapAnywhere = 3
  WrapAtWordBoundaryOrAnywhere = 4
} // endof enum WrapMode


/*


*/
//type QTextOption.Flag = int
pub enum QTextOptionFlag {
  ShowTabsAndSpaces = 1
  ShowLineAndParagraphSeparators = 2
  AddSpaceForLineAndParagraphSeparators = 4
  SuppressColors = 8
  ShowDocumentTerminator = 16
  IncludeTrailingSpaces = -2147483648
} // endof enum Flag

//  body block end

//  keep block begin


fn init_unused_10155() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
