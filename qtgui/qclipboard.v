

module qtgui
// /usr/include/qt/QtGui/qclipboard.h
// #include <qclipboard.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QClipboard {
pub:
  qtcore.QObject
}

pub interface QClipboardITF {
//    qtcore.QObjectITF
    get_cthis() voidptr
    toQClipboard() QClipboard
}
fn hotfix_QClipboard_itf_name_table(this QClipboardITF) {
  that := QClipboard{}
  hotfix_QClipboard_itf_name_table(that)
}
pub fn (ptr QClipboard) toQClipboard() QClipboard { return ptr }

pub fn (this QClipboard) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQClipboardFromptr(cthis voidptr) QClipboard {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    return QClipboard{bcthis0}
}
pub fn (dummy QClipboard) newFromptr(cthis voidptr) QClipboard {
    return newQClipboardFromptr(cthis)
}
// /usr/include/qt/QtGui/qclipboard.h:65
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clear(QClipboard::Mode)
type T_ZN10QClipboard5clearENS_4ModeE = fn(cthis voidptr, mode int) /*void*/

/*

*/
pub fn (this QClipboard) clear(mode int)  {
    mut fnobj := T_ZN10QClipboard5clearENS_4ModeE(0)
    fnobj = qtrt.sym_qtfunc6(3673203142, "_ZN10QClipboard5clearENS_4ModeE")
    fnobj(this.get_cthis(), mode)
}
// /usr/include/qt/QtGui/qclipboard.h:65
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clear(QClipboard::Mode)

/*

*/
pub fn (this QClipboard) clearp()  {
    // arg: 0, QClipboard::Mode=Enum, QClipboard::Mode=Enum, , Invalid
    mode := 0
    mut fnobj := T_ZN10QClipboard5clearENS_4ModeE(0)
    fnobj = qtrt.sym_qtfunc6(3673203142, "_ZN10QClipboard5clearENS_4ModeE")
    fnobj(this.get_cthis(), mode)
}
// /usr/include/qt/QtGui/qclipboard.h:67
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool supportsSelection() const
type T_ZNK10QClipboard17supportsSelectionEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QClipboard) supportsSelection() bool {
    mut fnobj := T_ZNK10QClipboard17supportsSelectionEv(0)
    fnobj = qtrt.sym_qtfunc6(1880697911, "_ZNK10QClipboard17supportsSelectionEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qclipboard.h:68
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool supportsFindBuffer() const
type T_ZNK10QClipboard18supportsFindBufferEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QClipboard) supportsFindBuffer() bool {
    mut fnobj := T_ZNK10QClipboard18supportsFindBufferEv(0)
    fnobj = qtrt.sym_qtfunc6(2133879840, "_ZNK10QClipboard18supportsFindBufferEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qclipboard.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool ownsSelection() const
type T_ZNK10QClipboard13ownsSelectionEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QClipboard) ownsSelection() bool {
    mut fnobj := T_ZNK10QClipboard13ownsSelectionEv(0)
    fnobj = qtrt.sym_qtfunc6(317563336, "_ZNK10QClipboard13ownsSelectionEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qclipboard.h:71
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool ownsClipboard() const
type T_ZNK10QClipboard13ownsClipboardEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QClipboard) ownsClipboard() bool {
    mut fnobj := T_ZNK10QClipboard13ownsClipboardEv(0)
    fnobj = qtrt.sym_qtfunc6(1848675440, "_ZNK10QClipboard13ownsClipboardEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qclipboard.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool ownsFindBuffer() const
type T_ZNK10QClipboard14ownsFindBufferEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QClipboard) ownsFindBuffer() bool {
    mut fnobj := T_ZNK10QClipboard14ownsFindBufferEv(0)
    fnobj = qtrt.sym_qtfunc6(1054311493, "_ZNK10QClipboard14ownsFindBufferEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}

[no_inline]
pub fn deleteQClipboard(this &QClipboard) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4282311273, "_ZN10QClipboardD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QClipboard) freecpp() { deleteQClipboard(&this) }

fn (this QClipboard) free() {

  /*deleteQClipboard(&this)*/

  cthis := this.get_cthis()
  //println("QClipboard freeing ${cthis} 0 bytes")

}


/*


*/
//type QClipboard.Mode = int
pub enum QClipboardMode {
  Clipboard = 0
  Selection = 1
  FindBuffer = 2
} // endof enum Mode

//  body block end

//  keep block begin


fn init_unused_10163() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
