

module qtgui
// /usr/include/qt/QtGui/qguiapplication.h
// #include <qguiapplication.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QGuiApplication {
pub:
  qtcore.QCoreApplication
}

pub interface QGuiApplicationITF {
//    qtcore.QCoreApplicationITF
    get_cthis() voidptr
    toQGuiApplication() QGuiApplication
}
fn hotfix_QGuiApplication_itf_name_table(this QGuiApplicationITF) {
  that := QGuiApplication{}
  hotfix_QGuiApplication_itf_name_table(that)
}
pub fn (ptr QGuiApplication) toQGuiApplication() QGuiApplication { return ptr }

pub fn (this QGuiApplication) get_cthis() voidptr {
    return this.QCoreApplication.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQGuiApplicationFromptr(cthis voidptr) QGuiApplication {
    bcthis0 := qtcore.newQCoreApplicationFromptr(cthis)
    return QGuiApplication{bcthis0}
}
pub fn (dummy QGuiApplication) newFromptr(cthis voidptr) QGuiApplication {
    return newQGuiApplicationFromptr(cthis)
}

[no_inline]
pub fn deleteQGuiApplication(this &QGuiApplication) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2140321792, "_ZN15QGuiApplicationD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QGuiApplication) freecpp() { deleteQGuiApplication(&this) }

fn (this QGuiApplication) free() {

  /*deleteQGuiApplication(&this)*/

  cthis := this.get_cthis()
  //println("QGuiApplication freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10171() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
