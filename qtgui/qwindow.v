

module qtgui
// /usr/include/qt/QtGui/qwindow.h
// #include <qwindow.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 3
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QWindow {
pub:
  qtcore.QObject
  QSurface
}

pub interface QWindowITF {
//    qtcore.QObjectITF
//    QSurfaceITF
    get_cthis() voidptr
    toQWindow() QWindow
}
fn hotfix_QWindow_itf_name_table(this QWindowITF) {
  that := QWindow{}
  hotfix_QWindow_itf_name_table(that)
}
pub fn (ptr QWindow) toQWindow() QWindow { return ptr }

pub fn (this QWindow) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
pub fn (this QWindow) set_cthis(cthis voidptr) {
    // this.QObject = qtcore.newQObjectFromptr(cthis)
    // this.QSurface = newQSurfaceFromptr(cthis)
}
[no_inline]
pub fn newQWindowFromptr(cthis voidptr) QWindow {
    bcthis0 := qtcore.newQObjectFromptr(cthis)
    bcthis1 := newQSurfaceFromptr(cthis)
    return QWindow{bcthis0, bcthis1}
}
pub fn (dummy QWindow) newFromptr(cthis voidptr) QWindow {
    return newQWindowFromptr(cthis)
}

[no_inline]
pub fn deleteQWindow(this &QWindow) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3257210118, "_ZN7QWindowD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QWindow) freecpp() { deleteQWindow(&this) }

fn (this QWindow) free() {

  /*deleteQWindow(&this)*/

  cthis := this.get_cthis()
  //println("QWindow freeing ${cthis} 0 bytes")

}


/*


*/
//type QWindow.Visibility = int
pub enum QWindowVisibility {
  Hidden = 0
  AutomaticVisibility = 1
  Windowed = 2
  Minimized = 3
  Maximized = 4
  FullScreen = 5
} // endof enum Visibility


/*


*/
//type QWindow.AncestorMode = int
pub enum QWindowAncestorMode {
  ExcludeTransients = 0
  IncludeTransients = 1
} // endof enum AncestorMode

//  body block end

//  keep block begin


fn init_unused_10161() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
