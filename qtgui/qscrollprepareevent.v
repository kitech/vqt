

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QScrollPrepareEvent {
pub:
  qtcore.QEvent
}

pub interface QScrollPrepareEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQScrollPrepareEvent() QScrollPrepareEvent
}
fn hotfix_QScrollPrepareEvent_itf_name_table(this QScrollPrepareEventITF) {
  that := QScrollPrepareEvent{}
  hotfix_QScrollPrepareEvent_itf_name_table(that)
}
pub fn (ptr QScrollPrepareEvent) toQScrollPrepareEvent() QScrollPrepareEvent { return ptr }

pub fn (this QScrollPrepareEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQScrollPrepareEventFromptr(cthis voidptr) QScrollPrepareEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QScrollPrepareEvent{bcthis0}
}
pub fn (dummy QScrollPrepareEvent) newFromptr(cthis voidptr) QScrollPrepareEvent {
    return newQScrollPrepareEventFromptr(cthis)
}

[no_inline]
pub fn deleteQScrollPrepareEvent(this &QScrollPrepareEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(684418620, "_ZN19QScrollPrepareEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QScrollPrepareEvent) freecpp() { deleteQScrollPrepareEvent(&this) }

fn (this QScrollPrepareEvent) free() {

  /*deleteQScrollPrepareEvent(&this)*/

  cthis := this.get_cthis()
  //println("QScrollPrepareEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10135() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
