

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QContextMenuEvent {
pub:
  QInputEvent
}

pub interface QContextMenuEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQContextMenuEvent() QContextMenuEvent
}
fn hotfix_QContextMenuEvent_itf_name_table(this QContextMenuEventITF) {
  that := QContextMenuEvent{}
  hotfix_QContextMenuEvent_itf_name_table(that)
}
pub fn (ptr QContextMenuEvent) toQContextMenuEvent() QContextMenuEvent { return ptr }

pub fn (this QContextMenuEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQContextMenuEventFromptr(cthis voidptr) QContextMenuEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QContextMenuEvent{bcthis0}
}
pub fn (dummy QContextMenuEvent) newFromptr(cthis voidptr) QContextMenuEvent {
    return newQContextMenuEventFromptr(cthis)
}

[no_inline]
pub fn deleteQContextMenuEvent(this &QContextMenuEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3486689009, "_ZN17QContextMenuEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QContextMenuEvent) freecpp() { deleteQContextMenuEvent(&this) }

fn (this QContextMenuEvent) free() {

  /*deleteQContextMenuEvent(&this)*/

  cthis := this.get_cthis()
  //println("QContextMenuEvent freeing ${cthis} 0 bytes")

}


/*


*/
//type QContextMenuEvent.Reason = int
pub enum QContextMenuEventReason {
  Mouse = 0
  Keyboard = 1
  Other = 2
} // endof enum Reason

//  body block end

//  keep block begin


fn init_unused_10103() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
