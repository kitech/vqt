

module qtgui
// /usr/include/qt/QtGui/qpaintengine.h
// #include <qpaintengine.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 8
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QPaintEngine {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QPaintEngineITF {
    get_cthis() voidptr
    toQPaintEngine() QPaintEngine
}
fn hotfix_QPaintEngine_itf_name_table(this QPaintEngineITF) {
  that := QPaintEngine{}
  hotfix_QPaintEngine_itf_name_table(that)
}
pub fn (ptr QPaintEngine) toQPaintEngine() QPaintEngine { return ptr }

pub fn (this QPaintEngine) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQPaintEngineFromptr(cthis voidptr) QPaintEngine {
    return QPaintEngine{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QPaintEngine) newFromptr(cthis voidptr) QPaintEngine {
    return newQPaintEngineFromptr(cthis)
}

[no_inline]
pub fn deleteQPaintEngine(this &QPaintEngine) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(727220551, "_ZN12QPaintEngineD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPaintEngine) freecpp() { deleteQPaintEngine(&this) }

fn (this QPaintEngine) free() {

  /*deleteQPaintEngine(&this)*/

  cthis := this.get_cthis()
  //println("QPaintEngine freeing ${cthis} 0 bytes")

}


/*


*/
//type QPaintEngine.PaintEngineFeature = int
pub enum QPaintEnginePaintEngineFeature {
  PrimitiveTransform = 1
  PatternTransform = 2
  PixmapTransform = 4
  PatternBrush = 8
  LinearGradientFill = 16
  RadialGradientFill = 32
  ConicalGradientFill = 64
  AlphaBlend = 128
  PorterDuff = 256
  PainterPaths = 512
  Antialiasing = 1024
  BrushStroke = 2048
  ConstantOpacity = 4096
  MaskedBrush = 8192
  PerspectiveTransform = 16384
  BlendModes = 32768
  ObjectBoundingModeGradients = 65536
  RasterOpModes = 131072
  PaintOutsidePaintEvent = 536870912
  AllFeatures = -1
} // endof enum PaintEngineFeature


/*


*/
//type QPaintEngine.DirtyFlag = int
pub enum QPaintEngineDirtyFlag {
  DirtyPen = 1
  DirtyBrush = 2
  DirtyBrushOrigin = 4
  DirtyFont = 8
  DirtyBackground = 16
  DirtyBackgroundMode = 32
  DirtyTransform = 64
  DirtyClipRegion = 128
  DirtyClipPath = 256
  DirtyHints = 512
  DirtyCompositionMode = 1024
  DirtyClipEnabled = 2048
  DirtyOpacity = 4096
  AllDirty = 65535
} // endof enum DirtyFlag


/*


*/
//type QPaintEngine.PolygonDrawMode = int
pub enum QPaintEnginePolygonDrawMode {
  OddEvenMode = 0
  WindingMode = 1
  ConvexMode = 2
  PolylineMode = 3
} // endof enum PolygonDrawMode


/*


*/
//type QPaintEngine.Type = int
pub enum QPaintEngineType {
  X11 = 0
  Windows = 1
  QuickDraw = 2
  CoreGraphics = 3
  MacPrinter = 4
  QWindowSystem = 5
  PostScript = 6
  OpenGL = 7
  Picture = 8
  SVG = 9
  Raster = 10
  Direct3D = 11
  Pdf = 12
  OpenVG = 13
  OpenGL2 = 14
  PaintBuffer = 15
  Blitter = 16
  Direct2D = 17
  User = 50
  MaxUser = 100
} // endof enum Type

//  body block end

//  keep block begin


fn init_unused_10175() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
