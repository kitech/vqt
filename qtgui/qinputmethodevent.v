

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QInputMethodEvent {
pub:
  qtcore.QEvent
}

pub interface QInputMethodEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQInputMethodEvent() QInputMethodEvent
}
fn hotfix_QInputMethodEvent_itf_name_table(this QInputMethodEventITF) {
  that := QInputMethodEvent{}
  hotfix_QInputMethodEvent_itf_name_table(that)
}
pub fn (ptr QInputMethodEvent) toQInputMethodEvent() QInputMethodEvent { return ptr }

pub fn (this QInputMethodEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQInputMethodEventFromptr(cthis voidptr) QInputMethodEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QInputMethodEvent{bcthis0}
}
pub fn (dummy QInputMethodEvent) newFromptr(cthis voidptr) QInputMethodEvent {
    return newQInputMethodEventFromptr(cthis)
}

[no_inline]
pub fn deleteQInputMethodEvent(this &QInputMethodEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2759769397, "_ZN17QInputMethodEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QInputMethodEvent) freecpp() { deleteQInputMethodEvent(&this) }

fn (this QInputMethodEvent) free() {

  /*deleteQInputMethodEvent(&this)*/

  cthis := this.get_cthis()
  //println("QInputMethodEvent freeing ${cthis} 0 bytes")

}


/*


*/
//type QInputMethodEvent.AttributeType = int
pub enum QInputMethodEventAttributeType {
  TextFormat = 0
  Cursor = 1
  Language = 2
  Ruby = 3
  Selection = 4
} // endof enum AttributeType

//  body block end

//  keep block begin


fn init_unused_10105() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
