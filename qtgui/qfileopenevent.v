

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QFileOpenEvent {
pub:
  qtcore.QEvent
}

pub interface QFileOpenEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQFileOpenEvent() QFileOpenEvent
}
fn hotfix_QFileOpenEvent_itf_name_table(this QFileOpenEventITF) {
  that := QFileOpenEvent{}
  hotfix_QFileOpenEvent_itf_name_table(that)
}
pub fn (ptr QFileOpenEvent) toQFileOpenEvent() QFileOpenEvent { return ptr }

pub fn (this QFileOpenEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQFileOpenEventFromptr(cthis voidptr) QFileOpenEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QFileOpenEvent{bcthis0}
}
pub fn (dummy QFileOpenEvent) newFromptr(cthis voidptr) QFileOpenEvent {
    return newQFileOpenEventFromptr(cthis)
}
// /usr/include/qt/QtGui/qevent.h:784
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [8] QString file() const
type T_ZNK14QFileOpenEvent4fileEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QFileOpenEvent) file() string {
    mut fnobj := T_ZNK14QFileOpenEvent4fileEv(0)
    fnobj = qtrt.sym_qtfunc6(1192897722, "_ZNK14QFileOpenEvent4fileEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}

[no_inline]
pub fn deleteQFileOpenEvent(this &QFileOpenEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3021811242, "_ZN14QFileOpenEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QFileOpenEvent) freecpp() { deleteQFileOpenEvent(&this) }

fn (this QFileOpenEvent) free() {

  /*deleteQFileOpenEvent(&this)*/

  cthis := this.get_cthis()
  //println("QFileOpenEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10125() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
