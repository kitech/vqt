

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QScreenOrientationChangeEvent {
pub:
  qtcore.QEvent
}

pub interface QScreenOrientationChangeEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQScreenOrientationChangeEvent() QScreenOrientationChangeEvent
}
fn hotfix_QScreenOrientationChangeEvent_itf_name_table(this QScreenOrientationChangeEventITF) {
  that := QScreenOrientationChangeEvent{}
  hotfix_QScreenOrientationChangeEvent_itf_name_table(that)
}
pub fn (ptr QScreenOrientationChangeEvent) toQScreenOrientationChangeEvent() QScreenOrientationChangeEvent { return ptr }

pub fn (this QScreenOrientationChangeEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQScreenOrientationChangeEventFromptr(cthis voidptr) QScreenOrientationChangeEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QScreenOrientationChangeEvent{bcthis0}
}
pub fn (dummy QScreenOrientationChangeEvent) newFromptr(cthis voidptr) QScreenOrientationChangeEvent {
    return newQScreenOrientationChangeEventFromptr(cthis)
}

[no_inline]
pub fn deleteQScreenOrientationChangeEvent(this &QScreenOrientationChangeEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(375093733, "_ZN29QScreenOrientationChangeEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QScreenOrientationChangeEvent) freecpp() { deleteQScreenOrientationChangeEvent(&this) }

fn (this QScreenOrientationChangeEvent) free() {

  /*deleteQScreenOrientationChangeEvent(&this)*/

  cthis := this.get_cthis()
  //println("QScreenOrientationChangeEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10139() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
