

module qtgui
// /usr/include/qt/QtGui/qicon.h
// #include <qicon.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QIcon {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QIconITF {
    get_cthis() voidptr
    toQIcon() QIcon
}
fn hotfix_QIcon_itf_name_table(this QIconITF) {
  that := QIcon{}
  hotfix_QIcon_itf_name_table(that)
}
pub fn (ptr QIcon) toQIcon() QIcon { return ptr }

pub fn (this QIcon) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQIconFromptr(cthis voidptr) QIcon {
    return QIcon{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QIcon) newFromptr(cthis voidptr) QIcon {
    return newQIconFromptr(cthis)
}
// /usr/include/qt/QtGui/qicon.h:61
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QIcon()
type T_ZN5QIconC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QIcon) new_for_inherit_() QIcon {
  //return newQIcon()
  return QIcon{}
}
pub fn newQIcon() QIcon {
    mut fnobj := T_ZN5QIconC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(1359309426, "_ZN5QIconC2Ev")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis)
    rv := cthis
    vthis := newQIconFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQIcon)
  return vthis
}
// /usr/include/qt/QtGui/qicon.h:62
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QIcon(const QPixmap &)
type T_ZN5QIconC2ERK7QPixmap = fn(cthis voidptr, pixmap voidptr) 

/*

*/
pub fn (dummy QIcon) new_for_inherit_1(pixmap  QPixmap) QIcon {
  //return newQIcon1(pixmap)
  return QIcon{}
}
pub fn newQIcon1(pixmap  QPixmap) QIcon {
    mut conv_arg0 := voidptr(0)
    /*if pixmap != voidptr(0) && pixmap.QPixmap_ptr() != voidptr(0) */ {
        // conv_arg0 = pixmap.QPixmap_ptr().get_cthis()
      conv_arg0 = pixmap.get_cthis()
    }
    mut fnobj := T_ZN5QIconC2ERK7QPixmap(0)
    fnobj = qtrt.sym_qtfunc6(2794313597, "_ZN5QIconC2ERK7QPixmap")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQIconFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQIcon)
  return vthis
}
// /usr/include/qt/QtGui/qicon.h:67
// index:2 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QIcon(const QString &)
type T_ZN5QIconC2ERK7QString = fn(cthis voidptr, fileName voidptr) 

/*

*/
pub fn (dummy QIcon) new_for_inherit_2(fileName string) QIcon {
  //return newQIcon2(fileName)
  return QIcon{}
}
pub fn newQIcon2(fileName string) QIcon {
    mut tmp_arg0 := qtcore.newQString5(fileName)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN5QIconC2ERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3566610256, "_ZN5QIconC2ERK7QString")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQIconFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQIcon)
  return vthis
}

[no_inline]
pub fn deleteQIcon(this &QIcon) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3436335819, "_ZN5QIconD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QIcon) freecpp() { deleteQIcon(&this) }

fn (this QIcon) free() {

  /*deleteQIcon(&this)*/

  cthis := this.get_cthis()
  //println("QIcon freeing ${cthis} 0 bytes")

}


/*


*/
//type QIcon.Mode = int
pub enum QIconMode {
  Normal = 0
  Disabled = 1
  Active = 2
  Selected = 3
} // endof enum Mode


/*


*/
//type QIcon.State = int
pub enum QIconState {
  On = 0
  Off = 1
} // endof enum State

//  body block end

//  keep block begin


fn init_unused_10159() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
