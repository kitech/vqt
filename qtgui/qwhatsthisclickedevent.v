

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QWhatsThisClickedEvent {
pub:
  qtcore.QEvent
}

pub interface QWhatsThisClickedEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQWhatsThisClickedEvent() QWhatsThisClickedEvent
}
fn hotfix_QWhatsThisClickedEvent_itf_name_table(this QWhatsThisClickedEventITF) {
  that := QWhatsThisClickedEvent{}
  hotfix_QWhatsThisClickedEvent_itf_name_table(that)
}
pub fn (ptr QWhatsThisClickedEvent) toQWhatsThisClickedEvent() QWhatsThisClickedEvent { return ptr }

pub fn (this QWhatsThisClickedEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQWhatsThisClickedEventFromptr(cthis voidptr) QWhatsThisClickedEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QWhatsThisClickedEvent{bcthis0}
}
pub fn (dummy QWhatsThisClickedEvent) newFromptr(cthis voidptr) QWhatsThisClickedEvent {
    return newQWhatsThisClickedEventFromptr(cthis)
}

[no_inline]
pub fn deleteQWhatsThisClickedEvent(this &QWhatsThisClickedEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2415798845, "_ZN22QWhatsThisClickedEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QWhatsThisClickedEvent) freecpp() { deleteQWhatsThisClickedEvent(&this) }

fn (this QWhatsThisClickedEvent) free() {

  /*deleteQWhatsThisClickedEvent(&this)*/

  cthis := this.get_cthis()
  //println("QWhatsThisClickedEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10121() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
