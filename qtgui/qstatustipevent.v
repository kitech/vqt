

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QStatusTipEvent {
pub:
  qtcore.QEvent
}

pub interface QStatusTipEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQStatusTipEvent() QStatusTipEvent
}
fn hotfix_QStatusTipEvent_itf_name_table(this QStatusTipEventITF) {
  that := QStatusTipEvent{}
  hotfix_QStatusTipEvent_itf_name_table(that)
}
pub fn (ptr QStatusTipEvent) toQStatusTipEvent() QStatusTipEvent { return ptr }

pub fn (this QStatusTipEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQStatusTipEventFromptr(cthis voidptr) QStatusTipEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QStatusTipEvent{bcthis0}
}
pub fn (dummy QStatusTipEvent) newFromptr(cthis voidptr) QStatusTipEvent {
    return newQStatusTipEventFromptr(cthis)
}

[no_inline]
pub fn deleteQStatusTipEvent(this &QStatusTipEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(223208154, "_ZN15QStatusTipEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QStatusTipEvent) freecpp() { deleteQStatusTipEvent(&this) }

fn (this QStatusTipEvent) free() {

  /*deleteQStatusTipEvent(&this)*/

  cthis := this.get_cthis()
  //println("QStatusTipEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10119() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
