

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QShortcutEvent {
pub:
  qtcore.QEvent
}

pub interface QShortcutEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQShortcutEvent() QShortcutEvent
}
fn hotfix_QShortcutEvent_itf_name_table(this QShortcutEventITF) {
  that := QShortcutEvent{}
  hotfix_QShortcutEvent_itf_name_table(that)
}
pub fn (ptr QShortcutEvent) toQShortcutEvent() QShortcutEvent { return ptr }

pub fn (this QShortcutEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQShortcutEventFromptr(cthis voidptr) QShortcutEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QShortcutEvent{bcthis0}
}
pub fn (dummy QShortcutEvent) newFromptr(cthis voidptr) QShortcutEvent {
    return newQShortcutEventFromptr(cthis)
}

[no_inline]
pub fn deleteQShortcutEvent(this &QShortcutEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4005859061, "_ZN14QShortcutEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QShortcutEvent) freecpp() { deleteQShortcutEvent(&this) }

fn (this QShortcutEvent) free() {

  /*deleteQShortcutEvent(&this)*/

  cthis := this.get_cthis()
  //println("QShortcutEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10129() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
