

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QTouchEvent {
pub:
  QInputEvent
}

pub interface QTouchEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQTouchEvent() QTouchEvent
}
fn hotfix_QTouchEvent_itf_name_table(this QTouchEventITF) {
  that := QTouchEvent{}
  hotfix_QTouchEvent_itf_name_table(that)
}
pub fn (ptr QTouchEvent) toQTouchEvent() QTouchEvent { return ptr }

pub fn (this QTouchEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQTouchEventFromptr(cthis voidptr) QTouchEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QTouchEvent{bcthis0}
}
pub fn (dummy QTouchEvent) newFromptr(cthis voidptr) QTouchEvent {
    return newQTouchEventFromptr(cthis)
}

[no_inline]
pub fn deleteQTouchEvent(this &QTouchEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1324337181, "_ZN11QTouchEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QTouchEvent) freecpp() { deleteQTouchEvent(&this) }

fn (this QTouchEvent) free() {

  /*deleteQTouchEvent(&this)*/

  cthis := this.get_cthis()
  //println("QTouchEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10133() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
