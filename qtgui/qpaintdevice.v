

module qtgui
// /usr/include/qt/QtGui/qpaintdevice.h
// #include <qpaintdevice.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 45
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QPaintDevice {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QPaintDeviceITF {
    get_cthis() voidptr
    toQPaintDevice() QPaintDevice
}
fn hotfix_QPaintDevice_itf_name_table(this QPaintDeviceITF) {
  that := QPaintDevice{}
  hotfix_QPaintDevice_itf_name_table(that)
}
pub fn (ptr QPaintDevice) toQPaintDevice() QPaintDevice { return ptr }

pub fn (this QPaintDevice) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQPaintDeviceFromptr(cthis voidptr) QPaintDevice {
    return QPaintDevice{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QPaintDevice) newFromptr(cthis voidptr) QPaintDevice {
    return newQPaintDeviceFromptr(cthis)
}
// /usr/include/qt/QtGui/qpaintdevice.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Direct Visibility=Default Availability=Available
// [4] int devType() const
type T_ZNK12QPaintDevice7devTypeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) devType() int {
    mut fnobj := T_ZNK12QPaintDevice7devTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(2740044500, "_ZNK12QPaintDevice7devTypeEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool paintingActive() const
type T_ZNK12QPaintDevice14paintingActiveEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QPaintDevice) paintingActive() bool {
    mut fnobj := T_ZNK12QPaintDevice14paintingActiveEv(0)
    fnobj = qtrt.sym_qtfunc6(4098809866, "_ZNK12QPaintDevice14paintingActiveEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qpaintdevice.h:76
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Direct Visibility=Default Availability=Available
// [8] QPaintEngine * paintEngine() const
type T_ZNK12QPaintDevice11paintEngineEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QPaintDevice) paintEngine()  QPaintEngine/*777 QPaintEngine **/ {
    mut fnobj := T_ZNK12QPaintDevice11paintEngineEv(0)
    fnobj = qtrt.sym_qtfunc6(1815356186, "_ZNK12QPaintDevice11paintEngineEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQPaintEngineFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtGui/qpaintdevice.h:78
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int width() const
type T_ZNK12QPaintDevice5widthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) width() int {
    mut fnobj := T_ZNK12QPaintDevice5widthEv(0)
    fnobj = qtrt.sym_qtfunc6(4251465228, "_ZNK12QPaintDevice5widthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:79
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int height() const
type T_ZNK12QPaintDevice6heightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) height() int {
    mut fnobj := T_ZNK12QPaintDevice6heightEv(0)
    fnobj = qtrt.sym_qtfunc6(1938128564, "_ZNK12QPaintDevice6heightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:80
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int widthMM() const
type T_ZNK12QPaintDevice7widthMMEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) widthMM() int {
    mut fnobj := T_ZNK12QPaintDevice7widthMMEv(0)
    fnobj = qtrt.sym_qtfunc6(4290881768, "_ZNK12QPaintDevice7widthMMEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:81
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int heightMM() const
type T_ZNK12QPaintDevice8heightMMEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) heightMM() int {
    mut fnobj := T_ZNK12QPaintDevice8heightMMEv(0)
    fnobj = qtrt.sym_qtfunc6(4250480684, "_ZNK12QPaintDevice8heightMMEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:82
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int logicalDpiX() const
type T_ZNK12QPaintDevice11logicalDpiXEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) logicalDpiX() int {
    mut fnobj := T_ZNK12QPaintDevice11logicalDpiXEv(0)
    fnobj = qtrt.sym_qtfunc6(1690059126, "_ZNK12QPaintDevice11logicalDpiXEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:83
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int logicalDpiY() const
type T_ZNK12QPaintDevice11logicalDpiYEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) logicalDpiY() int {
    mut fnobj := T_ZNK12QPaintDevice11logicalDpiYEv(0)
    fnobj = qtrt.sym_qtfunc6(1702767425, "_ZNK12QPaintDevice11logicalDpiYEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:84
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int physicalDpiX() const
type T_ZNK12QPaintDevice12physicalDpiXEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) physicalDpiX() int {
    mut fnobj := T_ZNK12QPaintDevice12physicalDpiXEv(0)
    fnobj = qtrt.sym_qtfunc6(1606783227, "_ZNK12QPaintDevice12physicalDpiXEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:85
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int physicalDpiY() const
type T_ZNK12QPaintDevice12physicalDpiYEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) physicalDpiY() int {
    mut fnobj := T_ZNK12QPaintDevice12physicalDpiYEv(0)
    fnobj = qtrt.sym_qtfunc6(1577581260, "_ZNK12QPaintDevice12physicalDpiYEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:86
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int devicePixelRatio() const
type T_ZNK12QPaintDevice16devicePixelRatioEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) devicePixelRatio() int {
    mut fnobj := T_ZNK12QPaintDevice16devicePixelRatioEv(0)
    fnobj = qtrt.sym_qtfunc6(2416087545, "_ZNK12QPaintDevice16devicePixelRatioEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:87
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] qreal devicePixelRatioF() const
type T_ZNK12QPaintDevice17devicePixelRatioFEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QPaintDevice) devicePixelRatioF() f64 {
    mut fnobj := T_ZNK12QPaintDevice17devicePixelRatioFEv(0)
    fnobj = qtrt.sym_qtfunc6(1021285250, "_ZNK12QPaintDevice17devicePixelRatioFEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtGui/qpaintdevice.h:88
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int colorCount() const
type T_ZNK12QPaintDevice10colorCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) colorCount() int {
    mut fnobj := T_ZNK12QPaintDevice10colorCountEv(0)
    fnobj = qtrt.sym_qtfunc6(3927565789, "_ZNK12QPaintDevice10colorCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:89
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int depth() const
type T_ZNK12QPaintDevice5depthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPaintDevice) depth() int {
    mut fnobj := T_ZNK12QPaintDevice5depthEv(0)
    fnobj = qtrt.sym_qtfunc6(1241268582, "_ZNK12QPaintDevice5depthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpaintdevice.h:91
// index:0 inlined:true externc:Language=CPlusPlus
// Public static inline Direct Visibility=Default Availability=Available
// [8] qreal devicePixelRatioFScale()
type T_ZN12QPaintDevice22devicePixelRatioFScaleEv = fn() f64

/*

*/
pub fn (this QPaintDevice) devicePixelRatioFScale() f64 {
    mut fnobj := T_ZN12QPaintDevice22devicePixelRatioFScaleEv(0)
    fnobj = qtrt.sym_qtfunc6(3846552345, "_ZN12QPaintDevice22devicePixelRatioFScaleEv")
    rv :=
    fnobj()
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}

[no_inline]
pub fn deleteQPaintDevice(this &QPaintDevice) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1245730511, "_ZN12QPaintDeviceD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPaintDevice) freecpp() { deleteQPaintDevice(&this) }

fn (this QPaintDevice) free() {

  /*deleteQPaintDevice(&this)*/

  cthis := this.get_cthis()
  //println("QPaintDevice freeing ${cthis} 0 bytes")

}


/*


*/
//type QPaintDevice.PaintDeviceMetric = int
pub enum QPaintDevicePaintDeviceMetric {
  PdmWidth = 1
  PdmHeight = 2
  PdmWidthMM = 3
  PdmHeightMM = 4
  PdmNumColors = 5
  PdmDepth = 6
  PdmDpiX = 7
  PdmDpiY = 8
  PdmPhysicalDpiX = 9
  PdmPhysicalDpiY = 10
  PdmDevicePixelRatio = 11
  PdmDevicePixelRatioScaled = 12
} // endof enum PaintDeviceMetric

//  body block end

//  keep block begin


fn init_unused_10145() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
