

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QActionEvent {
pub:
  qtcore.QEvent
}

pub interface QActionEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQActionEvent() QActionEvent
}
fn hotfix_QActionEvent_itf_name_table(this QActionEventITF) {
  that := QActionEvent{}
  hotfix_QActionEvent_itf_name_table(that)
}
pub fn (ptr QActionEvent) toQActionEvent() QActionEvent { return ptr }

pub fn (this QActionEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQActionEventFromptr(cthis voidptr) QActionEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QActionEvent{bcthis0}
}
pub fn (dummy QActionEvent) newFromptr(cthis voidptr) QActionEvent {
    return newQActionEventFromptr(cthis)
}

[no_inline]
pub fn deleteQActionEvent(this &QActionEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2699258947, "_ZN12QActionEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QActionEvent) freecpp() { deleteQActionEvent(&this) }

fn (this QActionEvent) free() {

  /*deleteQActionEvent(&this)*/

  cthis := this.get_cthis()
  //println("QActionEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10123() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
