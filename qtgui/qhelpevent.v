

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QHelpEvent {
pub:
  qtcore.QEvent
}

pub interface QHelpEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQHelpEvent() QHelpEvent
}
fn hotfix_QHelpEvent_itf_name_table(this QHelpEventITF) {
  that := QHelpEvent{}
  hotfix_QHelpEvent_itf_name_table(that)
}
pub fn (ptr QHelpEvent) toQHelpEvent() QHelpEvent { return ptr }

pub fn (this QHelpEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQHelpEventFromptr(cthis voidptr) QHelpEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QHelpEvent{bcthis0}
}
pub fn (dummy QHelpEvent) newFromptr(cthis voidptr) QHelpEvent {
    return newQHelpEventFromptr(cthis)
}

[no_inline]
pub fn deleteQHelpEvent(this &QHelpEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2446753538, "_ZN10QHelpEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QHelpEvent) freecpp() { deleteQHelpEvent(&this) }

fn (this QHelpEvent) free() {

  /*deleteQHelpEvent(&this)*/

  cthis := this.get_cthis()
  //println("QHelpEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10117() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
