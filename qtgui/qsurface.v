

module qtgui
// /usr/include/qt/QtGui/qsurface.h
// #include <qsurface.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QSurface {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QSurfaceITF {
    get_cthis() voidptr
    toQSurface() QSurface
}
fn hotfix_QSurface_itf_name_table(this QSurfaceITF) {
  that := QSurface{}
  hotfix_QSurface_itf_name_table(that)
}
pub fn (ptr QSurface) toQSurface() QSurface { return ptr }

pub fn (this QSurface) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQSurfaceFromptr(cthis voidptr) QSurface {
    return QSurface{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QSurface) newFromptr(cthis voidptr) QSurface {
    return newQSurfaceFromptr(cthis)
}

[no_inline]
pub fn deleteQSurface(this &QSurface) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4285993900, "_ZN8QSurfaceD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QSurface) freecpp() { deleteQSurface(&this) }

fn (this QSurface) free() {

  /*deleteQSurface(&this)*/

  cthis := this.get_cthis()
  //println("QSurface freeing ${cthis} 0 bytes")

}


/*


*/
//type QSurface.SurfaceClass = int
pub enum QSurfaceSurfaceClass {
  Window = 0
  Offscreen = 1
} // endof enum SurfaceClass


/*


*/
//type QSurface.SurfaceType = int
pub enum QSurfaceSurfaceType {
  RasterSurface = 0
  OpenGLSurface = 1
  RasterGLSurface = 2
  OpenVGSurface = 3
  VulkanSurface = 4
  MetalSurface = 5
} // endof enum SurfaceType

//  body block end

//  keep block begin


fn init_unused_10157() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
