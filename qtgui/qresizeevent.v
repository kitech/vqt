

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QResizeEvent {
pub:
  qtcore.QEvent
}

pub interface QResizeEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQResizeEvent() QResizeEvent
}
fn hotfix_QResizeEvent_itf_name_table(this QResizeEventITF) {
  that := QResizeEvent{}
  hotfix_QResizeEvent_itf_name_table(that)
}
pub fn (ptr QResizeEvent) toQResizeEvent() QResizeEvent { return ptr }

pub fn (this QResizeEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQResizeEventFromptr(cthis voidptr) QResizeEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QResizeEvent{bcthis0}
}
pub fn (dummy QResizeEvent) newFromptr(cthis voidptr) QResizeEvent {
    return newQResizeEventFromptr(cthis)
}

[no_inline]
pub fn deleteQResizeEvent(this &QResizeEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1460422629, "_ZN12QResizeEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QResizeEvent) freecpp() { deleteQResizeEvent(&this) }

fn (this QResizeEvent) free() {

  /*deleteQResizeEvent(&this)*/

  cthis := this.get_cthis()
  //println("QResizeEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10093() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
