

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QExposeEvent {
pub:
  qtcore.QEvent
}

pub interface QExposeEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQExposeEvent() QExposeEvent
}
fn hotfix_QExposeEvent_itf_name_table(this QExposeEventITF) {
  that := QExposeEvent{}
  hotfix_QExposeEvent_itf_name_table(that)
}
pub fn (ptr QExposeEvent) toQExposeEvent() QExposeEvent { return ptr }

pub fn (this QExposeEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQExposeEventFromptr(cthis voidptr) QExposeEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QExposeEvent{bcthis0}
}
pub fn (dummy QExposeEvent) newFromptr(cthis voidptr) QExposeEvent {
    return newQExposeEventFromptr(cthis)
}

[no_inline]
pub fn deleteQExposeEvent(this &QExposeEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1889365036, "_ZN12QExposeEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QExposeEvent) freecpp() { deleteQExposeEvent(&this) }

fn (this QExposeEvent) free() {

  /*deleteQExposeEvent(&this)*/

  cthis := this.get_cthis()
  //println("QExposeEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10089() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
