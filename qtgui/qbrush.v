

module qtgui
// /usr/include/qt/QtGui/qbrush.h
// #include <qbrush.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 6
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QBrush {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QBrushITF {
    get_cthis() voidptr
    toQBrush() QBrush
}
fn hotfix_QBrush_itf_name_table(this QBrushITF) {
  that := QBrush{}
  hotfix_QBrush_itf_name_table(that)
}
pub fn (ptr QBrush) toQBrush() QBrush { return ptr }

pub fn (this QBrush) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQBrushFromptr(cthis voidptr) QBrush {
    return QBrush{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QBrush) newFromptr(cthis voidptr) QBrush {
    return newQBrushFromptr(cthis)
}
// /usr/include/qt/QtGui/qbrush.h:66
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QBrush()
type T_ZN6QBrushC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QBrush) new_for_inherit_() QBrush {
  //return newQBrush()
  return QBrush{}
}
pub fn newQBrush() QBrush {
    mut fnobj := T_ZN6QBrushC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(3258578779, "_ZN6QBrushC2Ev")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis)
    rv := cthis
    vthis := newQBrushFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQBrush)
  return vthis
}

[no_inline]
pub fn deleteQBrush(this &QBrush) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1609486306, "_ZN6QBrushD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QBrush) freecpp() { deleteQBrush(&this) }

fn (this QBrush) free() {

  /*deleteQBrush(&this)*/

  cthis := this.get_cthis()
  //println("QBrush freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10151() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
