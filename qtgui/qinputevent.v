

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 2
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QInputEvent {
pub:
  qtcore.QEvent
}

pub interface QInputEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQInputEvent() QInputEvent
}
fn hotfix_QInputEvent_itf_name_table(this QInputEventITF) {
  that := QInputEvent{}
  hotfix_QInputEvent_itf_name_table(that)
}
pub fn (ptr QInputEvent) toQInputEvent() QInputEvent { return ptr }

pub fn (this QInputEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQInputEventFromptr(cthis voidptr) QInputEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QInputEvent{bcthis0}
}
pub fn (dummy QInputEvent) newFromptr(cthis voidptr) QInputEvent {
    return newQInputEventFromptr(cthis)
}
// /usr/include/qt/QtGui/qevent.h:75
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] ulong timestamp() const
type T_ZNK11QInputEvent9timestampEv = fn(cthis voidptr) u64

/*

*/
pub fn (this QInputEvent) timestamp() u64 {
    mut fnobj := T_ZNK11QInputEvent9timestampEv(0)
    fnobj = qtrt.sym_qtfunc6(3682385401, "_ZNK11QInputEvent9timestampEv")
    rv :=
    fnobj(this.get_cthis())
    return u64(rv) // 222
}
// /usr/include/qt/QtGui/qevent.h:76
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setTimestamp(ulong)
type T_ZN11QInputEvent12setTimestampEm = fn(cthis voidptr, atimestamp u64) /*void*/

/*

*/
pub fn (this QInputEvent) setTimestamp(atimestamp u64)  {
    mut fnobj := T_ZN11QInputEvent12setTimestampEm(0)
    fnobj = qtrt.sym_qtfunc6(1066347960, "_ZN11QInputEvent12setTimestampEm")
    fnobj(this.get_cthis(), atimestamp)
}

[no_inline]
pub fn deleteQInputEvent(this &QInputEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2705540433, "_ZN11QInputEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QInputEvent) freecpp() { deleteQInputEvent(&this) }

fn (this QInputEvent) free() {

  /*deleteQInputEvent(&this)*/

  cthis := this.get_cthis()
  //println("QInputEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10067() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
