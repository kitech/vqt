

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QCloseEvent {
pub:
  qtcore.QEvent
}

pub interface QCloseEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQCloseEvent() QCloseEvent
}
fn hotfix_QCloseEvent_itf_name_table(this QCloseEventITF) {
  that := QCloseEvent{}
  hotfix_QCloseEvent_itf_name_table(that)
}
pub fn (ptr QCloseEvent) toQCloseEvent() QCloseEvent { return ptr }

pub fn (this QCloseEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQCloseEventFromptr(cthis voidptr) QCloseEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QCloseEvent{bcthis0}
}
pub fn (dummy QCloseEvent) newFromptr(cthis voidptr) QCloseEvent {
    return newQCloseEventFromptr(cthis)
}

[no_inline]
pub fn deleteQCloseEvent(this &QCloseEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1300068505, "_ZN11QCloseEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QCloseEvent) freecpp() { deleteQCloseEvent(&this) }

fn (this QCloseEvent) free() {

  /*deleteQCloseEvent(&this)*/

  cthis := this.get_cthis()
  //println("QCloseEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10095() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
