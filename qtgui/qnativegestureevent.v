

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 4
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QNativeGestureEvent {
pub:
  QInputEvent
}

pub interface QNativeGestureEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQNativeGestureEvent() QNativeGestureEvent
}
fn hotfix_QNativeGestureEvent_itf_name_table(this QNativeGestureEventITF) {
  that := QNativeGestureEvent{}
  hotfix_QNativeGestureEvent_itf_name_table(that)
}
pub fn (ptr QNativeGestureEvent) toQNativeGestureEvent() QNativeGestureEvent { return ptr }

pub fn (this QNativeGestureEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQNativeGestureEventFromptr(cthis voidptr) QNativeGestureEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QNativeGestureEvent{bcthis0}
}
pub fn (dummy QNativeGestureEvent) newFromptr(cthis voidptr) QNativeGestureEvent {
    return newQNativeGestureEventFromptr(cthis)
}

[no_inline]
pub fn deleteQNativeGestureEvent(this &QNativeGestureEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1178069565, "_ZN19QNativeGestureEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QNativeGestureEvent) freecpp() { deleteQNativeGestureEvent(&this) }

fn (this QNativeGestureEvent) free() {

  /*deleteQNativeGestureEvent(&this)*/

  cthis := this.get_cthis()
  //println("QNativeGestureEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10079() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
