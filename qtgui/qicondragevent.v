

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QIconDragEvent {
pub:
  qtcore.QEvent
}

pub interface QIconDragEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQIconDragEvent() QIconDragEvent
}
fn hotfix_QIconDragEvent_itf_name_table(this QIconDragEventITF) {
  that := QIconDragEvent{}
  hotfix_QIconDragEvent_itf_name_table(that)
}
pub fn (ptr QIconDragEvent) toQIconDragEvent() QIconDragEvent { return ptr }

pub fn (this QIconDragEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQIconDragEventFromptr(cthis voidptr) QIconDragEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QIconDragEvent{bcthis0}
}
pub fn (dummy QIconDragEvent) newFromptr(cthis voidptr) QIconDragEvent {
    return newQIconDragEventFromptr(cthis)
}

[no_inline]
pub fn deleteQIconDragEvent(this &QIconDragEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2673315635, "_ZN14QIconDragEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QIconDragEvent) freecpp() { deleteQIconDragEvent(&this) }

fn (this QIconDragEvent) free() {

  /*deleteQIconDragEvent(&this)*/

  cthis := this.get_cthis()
  //println("QIconDragEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10097() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
