

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QPlatformSurfaceEvent {
pub:
  qtcore.QEvent
}

pub interface QPlatformSurfaceEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQPlatformSurfaceEvent() QPlatformSurfaceEvent
}
fn hotfix_QPlatformSurfaceEvent_itf_name_table(this QPlatformSurfaceEventITF) {
  that := QPlatformSurfaceEvent{}
  hotfix_QPlatformSurfaceEvent_itf_name_table(that)
}
pub fn (ptr QPlatformSurfaceEvent) toQPlatformSurfaceEvent() QPlatformSurfaceEvent { return ptr }

pub fn (this QPlatformSurfaceEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQPlatformSurfaceEventFromptr(cthis voidptr) QPlatformSurfaceEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QPlatformSurfaceEvent{bcthis0}
}
pub fn (dummy QPlatformSurfaceEvent) newFromptr(cthis voidptr) QPlatformSurfaceEvent {
    return newQPlatformSurfaceEventFromptr(cthis)
}

[no_inline]
pub fn deleteQPlatformSurfaceEvent(this &QPlatformSurfaceEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2664255028, "_ZN21QPlatformSurfaceEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPlatformSurfaceEvent) freecpp() { deleteQPlatformSurfaceEvent(&this) }

fn (this QPlatformSurfaceEvent) free() {

  /*deleteQPlatformSurfaceEvent(&this)*/

  cthis := this.get_cthis()
  //println("QPlatformSurfaceEvent freeing ${cthis} 0 bytes")

}


/*


*/
//type QPlatformSurfaceEvent.SurfaceEventType = int
pub enum QPlatformSurfaceEventSurfaceEventType {
  SurfaceCreated = 0
  SurfaceAboutToBeDestroyed = 1
} // endof enum SurfaceEventType

//  body block end

//  keep block begin


fn init_unused_10091() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
