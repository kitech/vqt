

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 5
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QTabletEvent {
pub:
  QInputEvent
}

pub interface QTabletEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQTabletEvent() QTabletEvent
}
fn hotfix_QTabletEvent_itf_name_table(this QTabletEventITF) {
  that := QTabletEvent{}
  hotfix_QTabletEvent_itf_name_table(that)
}
pub fn (ptr QTabletEvent) toQTabletEvent() QTabletEvent { return ptr }

pub fn (this QTabletEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQTabletEventFromptr(cthis voidptr) QTabletEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QTabletEvent{bcthis0}
}
pub fn (dummy QTabletEvent) newFromptr(cthis voidptr) QTabletEvent {
    return newQTabletEventFromptr(cthis)
}
// /usr/include/qt/QtGui/qevent.h:306
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int x() const
type T_ZNK12QTabletEvent1xEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabletEvent) x() int {
    mut fnobj := T_ZNK12QTabletEvent1xEv(0)
    fnobj = qtrt.sym_qtfunc6(40556910, "_ZNK12QTabletEvent1xEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:307
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int y() const
type T_ZNK12QTabletEvent1yEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabletEvent) y() int {
    mut fnobj := T_ZNK12QTabletEvent1yEv(0)
    fnobj = qtrt.sym_qtfunc6(61387609, "_ZNK12QTabletEvent1yEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:308
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int globalX() const
type T_ZNK12QTabletEvent7globalXEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabletEvent) globalX() int {
    mut fnobj := T_ZNK12QTabletEvent7globalXEv(0)
    fnobj = qtrt.sym_qtfunc6(4236764281, "_ZNK12QTabletEvent7globalXEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:309
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int globalY() const
type T_ZNK12QTabletEvent7globalYEv = fn(cthis voidptr) int

/*

*/
pub fn (this QTabletEvent) globalY() int {
    mut fnobj := T_ZNK12QTabletEvent7globalYEv(0)
    fnobj = qtrt.sym_qtfunc6(4249194062, "_ZNK12QTabletEvent7globalYEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}

[no_inline]
pub fn deleteQTabletEvent(this &QTabletEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1745538712, "_ZN12QTabletEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QTabletEvent) freecpp() { deleteQTabletEvent(&this) }

fn (this QTabletEvent) free() {

  /*deleteQTabletEvent(&this)*/

  cthis := this.get_cthis()
  //println("QTabletEvent freeing ${cthis} 0 bytes")

}


/*


*/
//type QTabletEvent.TabletDevice = int
pub enum QTabletEventTabletDevice {
  NoDevice = 0
  Puck = 1
  Stylus = 2
  Airbrush = 3
  FourDMouse = 4
  XFreeEraser = 5
  RotationStylus = 6
} // endof enum TabletDevice


/*


*/
//type QTabletEvent.PointerType = int
pub enum QTabletEventPointerType {
  UnknownPointer = 0
  Pen = 1
  Cursor = 2
  Eraser = 3
} // endof enum PointerType

//  body block end

//  keep block begin


fn init_unused_10077() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
