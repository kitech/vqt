

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QWheelEvent {
pub:
  QInputEvent
}

pub interface QWheelEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQWheelEvent() QWheelEvent
}
fn hotfix_QWheelEvent_itf_name_table(this QWheelEventITF) {
  that := QWheelEvent{}
  hotfix_QWheelEvent_itf_name_table(that)
}
pub fn (ptr QWheelEvent) toQWheelEvent() QWheelEvent { return ptr }

pub fn (this QWheelEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQWheelEventFromptr(cthis voidptr) QWheelEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QWheelEvent{bcthis0}
}
pub fn (dummy QWheelEvent) newFromptr(cthis voidptr) QWheelEvent {
    return newQWheelEventFromptr(cthis)
}
// /usr/include/qt/QtGui/qevent.h:213
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] QPoint pixelDelta() const
type T_ZNK11QWheelEvent10pixelDeltaEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWheelEvent) pixelDelta()  qtcore.QPoint/*123*/ {
    mut fnobj := T_ZNK11QWheelEvent10pixelDeltaEv(0)
    fnobj = qtrt.sym_qtfunc6(1776295009, "_ZNK11QWheelEvent10pixelDeltaEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQPoint)
    return rv2
    //return qtcore.QPoint{rv}
}
// /usr/include/qt/QtGui/qevent.h:214
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] QPoint angleDelta() const
type T_ZNK11QWheelEvent10angleDeltaEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWheelEvent) angleDelta()  qtcore.QPoint/*123*/ {
    mut fnobj := T_ZNK11QWheelEvent10angleDeltaEv(0)
    fnobj = qtrt.sym_qtfunc6(2989481828, "_ZNK11QWheelEvent10angleDeltaEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQPoint)
    return rv2
    //return qtcore.QPoint{rv}
}
// /usr/include/qt/QtGui/qevent.h:243
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [16] QPointF position() const
type T_ZNK11QWheelEvent8positionEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWheelEvent) position()  qtcore.QPointF/*123*/ {
    mut fnobj := T_ZNK11QWheelEvent8positionEv(0)
    fnobj = qtrt.sym_qtfunc6(1239393956, "_ZNK11QWheelEvent8positionEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQPointF)
    return rv2
    //return qtcore.QPointF{rv}
}
// /usr/include/qt/QtGui/qevent.h:244
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [16] QPointF globalPosition() const
type T_ZNK11QWheelEvent14globalPositionEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QWheelEvent) globalPosition()  qtcore.QPointF/*123*/ {
    mut fnobj := T_ZNK11QWheelEvent14globalPositionEv(0)
    fnobj = qtrt.sym_qtfunc6(3973060595, "_ZNK11QWheelEvent14globalPositionEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQPointF)
    return rv2
    //return qtcore.QPointF{rv}
}
// /usr/include/qt/QtGui/qevent.h:249
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool inverted() const
type T_ZNK11QWheelEvent8invertedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QWheelEvent) inverted() bool {
    mut fnobj := T_ZNK11QWheelEvent8invertedEv(0)
    fnobj = qtrt.sym_qtfunc6(643606857, "_ZNK11QWheelEvent8invertedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}

[no_inline]
pub fn deleteQWheelEvent(this &QWheelEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2730191963, "_ZN11QWheelEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QWheelEvent) freecpp() { deleteQWheelEvent(&this) }

fn (this QWheelEvent) free() {

  /*deleteQWheelEvent(&this)*/

  cthis := this.get_cthis()
  //println("QWheelEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10075() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
