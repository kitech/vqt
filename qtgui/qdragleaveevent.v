

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QDragLeaveEvent {
pub:
  qtcore.QEvent
}

pub interface QDragLeaveEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQDragLeaveEvent() QDragLeaveEvent
}
fn hotfix_QDragLeaveEvent_itf_name_table(this QDragLeaveEventITF) {
  that := QDragLeaveEvent{}
  hotfix_QDragLeaveEvent_itf_name_table(that)
}
pub fn (ptr QDragLeaveEvent) toQDragLeaveEvent() QDragLeaveEvent { return ptr }

pub fn (this QDragLeaveEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQDragLeaveEventFromptr(cthis voidptr) QDragLeaveEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QDragLeaveEvent{bcthis0}
}
pub fn (dummy QDragLeaveEvent) newFromptr(cthis voidptr) QDragLeaveEvent {
    return newQDragLeaveEventFromptr(cthis)
}

[no_inline]
pub fn deleteQDragLeaveEvent(this &QDragLeaveEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1368002526, "_ZN15QDragLeaveEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDragLeaveEvent) freecpp() { deleteQDragLeaveEvent(&this) }

fn (this QDragLeaveEvent) free() {

  /*deleteQDragLeaveEvent(&this)*/

  cthis := this.get_cthis()
  //println("QDragLeaveEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10115() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
