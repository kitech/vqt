

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QMouseEvent {
pub:
  QInputEvent
}

pub interface QMouseEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQMouseEvent() QMouseEvent
}
fn hotfix_QMouseEvent_itf_name_table(this QMouseEventITF) {
  that := QMouseEvent{}
  hotfix_QMouseEvent_itf_name_table(that)
}
pub fn (ptr QMouseEvent) toQMouseEvent() QMouseEvent { return ptr }

pub fn (this QMouseEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQMouseEventFromptr(cthis voidptr) QMouseEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QMouseEvent{bcthis0}
}
pub fn (dummy QMouseEvent) newFromptr(cthis voidptr) QMouseEvent {
    return newQMouseEventFromptr(cthis)
}
// /usr/include/qt/QtGui/qevent.h:121
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] QPoint pos() const
type T_ZNK11QMouseEvent3posEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QMouseEvent) pos()  qtcore.QPoint/*123*/ {
    mut fnobj := T_ZNK11QMouseEvent3posEv(0)
    fnobj = qtrt.sym_qtfunc6(1659492746, "_ZNK11QMouseEvent3posEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQPoint)
    return rv2
    //return qtcore.QPoint{rv}
}
// /usr/include/qt/QtGui/qevent.h:122
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] QPoint globalPos() const
type T_ZNK11QMouseEvent9globalPosEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QMouseEvent) globalPos()  qtcore.QPoint/*123*/ {
    mut fnobj := T_ZNK11QMouseEvent9globalPosEv(0)
    fnobj = qtrt.sym_qtfunc6(2976136822, "_ZNK11QMouseEvent9globalPosEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQPoint)
    return rv2
    //return qtcore.QPoint{rv}
}
// /usr/include/qt/QtGui/qevent.h:123
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int x() const
type T_ZNK11QMouseEvent1xEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMouseEvent) x() int {
    mut fnobj := T_ZNK11QMouseEvent1xEv(0)
    fnobj = qtrt.sym_qtfunc6(1953764823, "_ZNK11QMouseEvent1xEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:124
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int y() const
type T_ZNK11QMouseEvent1yEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMouseEvent) y() int {
    mut fnobj := T_ZNK11QMouseEvent1yEv(0)
    fnobj = qtrt.sym_qtfunc6(1974894560, "_ZNK11QMouseEvent1yEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:125
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int globalX() const
type T_ZNK11QMouseEvent7globalXEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMouseEvent) globalX() int {
    mut fnobj := T_ZNK11QMouseEvent7globalXEv(0)
    fnobj = qtrt.sym_qtfunc6(220043088, "_ZNK11QMouseEvent7globalXEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:126
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int globalY() const
type T_ZNK11QMouseEvent7globalYEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMouseEvent) globalY() int {
    mut fnobj := T_ZNK11QMouseEvent7globalYEv(0)
    fnobj = qtrt.sym_qtfunc6(216005991, "_ZNK11QMouseEvent7globalYEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:128
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [16] const QPointF & localPos() const
type T_ZNK11QMouseEvent8localPosEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QMouseEvent) localPos()  qtcore.QPointF {
    mut fnobj := T_ZNK11QMouseEvent8localPosEv(0)
    fnobj = qtrt.sym_qtfunc6(1441673618, "_ZNK11QMouseEvent8localPosEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFFromptr(rv) // 4441
    // qtrt.set_finalizer(rv2, qtcore.deleteQPointF)
    return rv2
    //return voidptr(0)
}
// /usr/include/qt/QtGui/qevent.h:129
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [16] const QPointF & windowPos() const
type T_ZNK11QMouseEvent9windowPosEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QMouseEvent) windowPos()  qtcore.QPointF {
    mut fnobj := T_ZNK11QMouseEvent9windowPosEv(0)
    fnobj = qtrt.sym_qtfunc6(293709112, "_ZNK11QMouseEvent9windowPosEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFFromptr(rv) // 4441
    // qtrt.set_finalizer(rv2, qtcore.deleteQPointF)
    return rv2
    //return voidptr(0)
}
// /usr/include/qt/QtGui/qevent.h:130
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [16] const QPointF & screenPos() const
type T_ZNK11QMouseEvent9screenPosEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QMouseEvent) screenPos()  qtcore.QPointF {
    mut fnobj := T_ZNK11QMouseEvent9screenPosEv(0)
    fnobj = qtrt.sym_qtfunc6(2962094997, "_ZNK11QMouseEvent9screenPosEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQPointFFromptr(rv) // 4441
    // qtrt.set_finalizer(rv2, qtcore.deleteQPointF)
    return rv2
    //return voidptr(0)
}

[no_inline]
pub fn deleteQMouseEvent(this &QMouseEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3707978826, "_ZN11QMouseEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMouseEvent) freecpp() { deleteQMouseEvent(&this) }

fn (this QMouseEvent) free() {

  /*deleteQMouseEvent(&this)*/

  cthis := this.get_cthis()
  //println("QMouseEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10071() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
