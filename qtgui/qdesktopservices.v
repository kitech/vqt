

module qtgui
// /usr/include/qt/QtGui/qdesktopservices.h
// #include <qdesktopservices.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 6
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QDesktopServices {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QDesktopServicesITF {
    get_cthis() voidptr
    toQDesktopServices() QDesktopServices
}
fn hotfix_QDesktopServices_itf_name_table(this QDesktopServicesITF) {
  that := QDesktopServices{}
  hotfix_QDesktopServices_itf_name_table(that)
}
pub fn (ptr QDesktopServices) toQDesktopServices() QDesktopServices { return ptr }

pub fn (this QDesktopServices) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQDesktopServicesFromptr(cthis voidptr) QDesktopServices {
    return QDesktopServices{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QDesktopServices) newFromptr(cthis voidptr) QDesktopServices {
    return newQDesktopServicesFromptr(cthis)
}
// /usr/include/qt/QtGui/qdesktopservices.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Extend Visibility=Default Availability=Available
// [1] bool openUrl(const QUrl &)
type T_ZN16QDesktopServices7openUrlERK4QUrl = fn(url voidptr) bool

/*

*/
pub fn (this QDesktopServices) openUrl(url  qtcore.QUrlITF) bool {
    mut conv_arg0 := voidptr(0)
    /*if url != voidptr(0) && url.QUrl_ptr() != voidptr(0) */ {
        // conv_arg0 = url.QUrl_ptr().get_cthis()
      conv_arg0 = url.get_cthis()
    }
    mut fnobj := T_ZN16QDesktopServices7openUrlERK4QUrl(0)
    fnobj = qtrt.sym_qtfunc6(2434727476, "_ZN16QDesktopServices7openUrlERK4QUrl")
    rv :=
    fnobj(conv_arg0)
    return rv//!=0
}
// /usr/include/qt/QtGui/qdesktopservices.h:60
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Ignore Visibility=Default Availability=Available
// [-2] void setUrlHandler(const QString &, QObject *, const char *)
type T_ZN16QDesktopServices13setUrlHandlerERK7QStringP7QObjectPKc = fn(scheme voidptr, receiver voidptr, method voidptr) /*void*/

/*

*/
pub fn (this QDesktopServices) setUrlHandler(scheme string, receiver qtcore.QObjectITF/*777 QObject **/, method string)  {
    mut tmp_arg0 := qtcore.newQString5(scheme)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut conv_arg1 := voidptr(0)
    /*if receiver != voidptr(0) && receiver.QObject_ptr() != voidptr(0) */ {
        // conv_arg1 = receiver.QObject_ptr().get_cthis()
      conv_arg1 = receiver.get_cthis()
    }
    mut conv_arg2 := qtrt.cstringr(&method)
    mut fnobj := T_ZN16QDesktopServices13setUrlHandlerERK7QStringP7QObjectPKc(0)
    fnobj = qtrt.sym_qtfunc6(254877298, "_ZN16QDesktopServices13setUrlHandlerERK7QStringP7QObjectPKc")
    fnobj(conv_arg0, conv_arg1, conv_arg2)
}
// /usr/include/qt/QtGui/qdesktopservices.h:61
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Ignore Visibility=Default Availability=Available
// [-2] void unsetUrlHandler(const QString &)
type T_ZN16QDesktopServices15unsetUrlHandlerERK7QString = fn(scheme voidptr) /*void*/

/*

*/
pub fn (this QDesktopServices) unsetUrlHandler(scheme string)  {
    mut tmp_arg0 := qtcore.newQString5(scheme)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN16QDesktopServices15unsetUrlHandlerERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(1369796868, "_ZN16QDesktopServices15unsetUrlHandlerERK7QString")
    fnobj(conv_arg0)
}

[no_inline]
pub fn deleteQDesktopServices(this &QDesktopServices) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1858443276, "_ZN16QDesktopServicesD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDesktopServices) freecpp() { deleteQDesktopServices(&this) }

fn (this QDesktopServices) free() {

  /*deleteQDesktopServices(&this)*/

  cthis := this.get_cthis()
  //println("QDesktopServices freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10165() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
