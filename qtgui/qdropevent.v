

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QDropEvent {
pub:
  qtcore.QEvent
}

pub interface QDropEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQDropEvent() QDropEvent
}
fn hotfix_QDropEvent_itf_name_table(this QDropEventITF) {
  that := QDropEvent{}
  hotfix_QDropEvent_itf_name_table(that)
}
pub fn (ptr QDropEvent) toQDropEvent() QDropEvent { return ptr }

pub fn (this QDropEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQDropEventFromptr(cthis voidptr) QDropEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QDropEvent{bcthis0}
}
pub fn (dummy QDropEvent) newFromptr(cthis voidptr) QDropEvent {
    return newQDropEventFromptr(cthis)
}

[no_inline]
pub fn deleteQDropEvent(this &QDropEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3926828171, "_ZN10QDropEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDropEvent) freecpp() { deleteQDropEvent(&this) }

fn (this QDropEvent) free() {

  /*deleteQDropEvent(&this)*/

  cthis := this.get_cthis()
  //println("QDropEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10109() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
