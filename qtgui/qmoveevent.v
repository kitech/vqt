

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QMoveEvent {
pub:
  qtcore.QEvent
}

pub interface QMoveEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQMoveEvent() QMoveEvent
}
fn hotfix_QMoveEvent_itf_name_table(this QMoveEventITF) {
  that := QMoveEvent{}
  hotfix_QMoveEvent_itf_name_table(that)
}
pub fn (ptr QMoveEvent) toQMoveEvent() QMoveEvent { return ptr }

pub fn (this QMoveEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQMoveEventFromptr(cthis voidptr) QMoveEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QMoveEvent{bcthis0}
}
pub fn (dummy QMoveEvent) newFromptr(cthis voidptr) QMoveEvent {
    return newQMoveEventFromptr(cthis)
}

[no_inline]
pub fn deleteQMoveEvent(this &QMoveEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3943756435, "_ZN10QMoveEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMoveEvent) freecpp() { deleteQMoveEvent(&this) }

fn (this QMoveEvent) free() {

  /*deleteQMoveEvent(&this)*/

  cthis := this.get_cthis()
  //println("QMoveEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10087() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
