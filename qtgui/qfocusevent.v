

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 7
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QFocusEvent {
pub:
  qtcore.QEvent
}

pub interface QFocusEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQFocusEvent() QFocusEvent
}
fn hotfix_QFocusEvent_itf_name_table(this QFocusEventITF) {
  that := QFocusEvent{}
  hotfix_QFocusEvent_itf_name_table(that)
}
pub fn (ptr QFocusEvent) toQFocusEvent() QFocusEvent { return ptr }

pub fn (this QFocusEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQFocusEventFromptr(cthis voidptr) QFocusEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QFocusEvent{bcthis0}
}
pub fn (dummy QFocusEvent) newFromptr(cthis voidptr) QFocusEvent {
    return newQFocusEventFromptr(cthis)
}

[no_inline]
pub fn deleteQFocusEvent(this &QFocusEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3004894295, "_ZN11QFocusEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QFocusEvent) freecpp() { deleteQFocusEvent(&this) }

fn (this QFocusEvent) free() {

  /*deleteQFocusEvent(&this)*/

  cthis := this.get_cthis()
  //println("QFocusEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10083() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
