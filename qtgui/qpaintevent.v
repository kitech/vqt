

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QPaintEvent {
pub:
  qtcore.QEvent
}

pub interface QPaintEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQPaintEvent() QPaintEvent
}
fn hotfix_QPaintEvent_itf_name_table(this QPaintEventITF) {
  that := QPaintEvent{}
  hotfix_QPaintEvent_itf_name_table(that)
}
pub fn (ptr QPaintEvent) toQPaintEvent() QPaintEvent { return ptr }

pub fn (this QPaintEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQPaintEventFromptr(cthis voidptr) QPaintEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QPaintEvent{bcthis0}
}
pub fn (dummy QPaintEvent) newFromptr(cthis voidptr) QPaintEvent {
    return newQPaintEventFromptr(cthis)
}

[no_inline]
pub fn deleteQPaintEvent(this &QPaintEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1387382698, "_ZN11QPaintEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPaintEvent) freecpp() { deleteQPaintEvent(&this) }

fn (this QPaintEvent) free() {

  /*deleteQPaintEvent(&this)*/

  cthis := this.get_cthis()
  //println("QPaintEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10085() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
