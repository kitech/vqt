

module qtgui
// /usr/include/qt/QtGui/qpainter.h
// #include <qpainter.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QPainter {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QPainterITF {
    get_cthis() voidptr
    toQPainter() QPainter
}
fn hotfix_QPainter_itf_name_table(this QPainterITF) {
  that := QPainter{}
  hotfix_QPainter_itf_name_table(that)
}
pub fn (ptr QPainter) toQPainter() QPainter { return ptr }

pub fn (this QPainter) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQPainterFromptr(cthis voidptr) QPainter {
    return QPainter{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QPainter) newFromptr(cthis voidptr) QPainter {
    return newQPainterFromptr(cthis)
}
// /usr/include/qt/QtGui/qpainter.h:127
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPainter(QPaintDevice *)
type T_ZN8QPainterC2EP12QPaintDevice = fn(cthis voidptr, arg0 voidptr) 

/*

*/
pub fn (dummy QPainter) new_for_inherit_(arg0  QPaintDevice/*777 QPaintDevice **/) QPainter {
  //return newQPainter(arg0)
  return QPainter{}
}
pub fn newQPainter(arg0  QPaintDevice/*777 QPaintDevice **/) QPainter {
    mut conv_arg0 := voidptr(0)
    /*if arg0 != voidptr(0) && arg0.QPaintDevice_ptr() != voidptr(0) */ {
        // conv_arg0 = arg0.QPaintDevice_ptr().get_cthis()
      conv_arg0 = arg0.get_cthis()
    }
    mut fnobj := T_ZN8QPainterC2EP12QPaintDevice(0)
    fnobj = qtrt.sym_qtfunc6(1768399324, "_ZN8QPainterC2EP12QPaintDevice")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQPainterFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPainter)
  return vthis
}
// /usr/include/qt/QtGui/qpainter.h:130
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QPaintDevice * device() const
type T_ZNK8QPainter6deviceEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QPainter) device()  QPaintDevice/*777 QPaintDevice **/ {
    mut fnobj := T_ZNK8QPainter6deviceEv(0)
    fnobj = qtrt.sym_qtfunc6(3490243267, "_ZNK8QPainter6deviceEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQPaintDeviceFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtGui/qpainter.h:194
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPen(const QColor &)
type T_ZN8QPainter6setPenERK6QColor = fn(cthis voidptr, color voidptr) /*void*/

/*

*/
pub fn (this QPainter) setPen(color  QColor)  {
    mut conv_arg0 := voidptr(0)
    /*if color != voidptr(0) && color.QColor_ptr() != voidptr(0) */ {
        // conv_arg0 = color.QColor_ptr().get_cthis()
      conv_arg0 = color.get_cthis()
    }
    mut fnobj := T_ZN8QPainter6setPenERK6QColor(0)
    fnobj = qtrt.sym_qtfunc6(3639192824, "_ZN8QPainter6setPenERK6QColor")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtGui/qpainter.h:195
// index:1 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPen(const QPen &)
type T_ZN8QPainter6setPenERK4QPen = fn(cthis voidptr, pen voidptr) /*void*/

/*

*/
pub fn (this QPainter) setPen1(pen  QPen)  {
    mut conv_arg0 := voidptr(0)
    /*if pen != voidptr(0) && pen.QPen_ptr() != voidptr(0) */ {
        // conv_arg0 = pen.QPen_ptr().get_cthis()
      conv_arg0 = pen.get_cthis()
    }
    mut fnobj := T_ZN8QPainter6setPenERK4QPen(0)
    fnobj = qtrt.sym_qtfunc6(2622452835, "_ZN8QPainter6setPenERK4QPen")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtGui/qpainter.h:196
// index:2 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPen(Qt::PenStyle)
type T_ZN8QPainter6setPenEN2Qt8PenStyleE = fn(cthis voidptr, style int) /*void*/

/*

*/
pub fn (this QPainter) setPen2(style int)  {
    mut fnobj := T_ZN8QPainter6setPenEN2Qt8PenStyleE(0)
    fnobj = qtrt.sym_qtfunc6(1129551355, "_ZN8QPainter6setPenEN2Qt8PenStyleE")
    fnobj(this.get_cthis(), style)
}
// /usr/include/qt/QtGui/qpainter.h:442
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void drawText(const QRect &, int, const QString &, QRect *)
type T_ZN8QPainter8drawTextERK5QRectiRK7QStringPS0_ = fn(cthis voidptr, r voidptr, flags int, text voidptr, br voidptr) /*void*/

/*

*/
pub fn (this QPainter) drawText(r  qtcore.QRectITF, flags int, text string, br qtcore.QRectITF/*777 QRect **/)  {
    mut conv_arg0 := voidptr(0)
    /*if r != voidptr(0) && r.QRect_ptr() != voidptr(0) */ {
        // conv_arg0 = r.QRect_ptr().get_cthis()
      conv_arg0 = r.get_cthis()
    }
    mut tmp_arg2 := qtcore.newQString5(text)
    mut conv_arg2 := tmp_arg2.get_cthis()
    mut conv_arg3 := voidptr(0)
    /*if br != voidptr(0) && br.QRect_ptr() != voidptr(0) */ {
        // conv_arg3 = br.QRect_ptr().get_cthis()
      conv_arg3 = br.get_cthis()
    }
    mut fnobj := T_ZN8QPainter8drawTextERK5QRectiRK7QStringPS0_(0)
    fnobj = qtrt.sym_qtfunc6(2115100769, "_ZN8QPainter8drawTextERK5QRectiRK7QStringPS0_")
    fnobj(this.get_cthis(), conv_arg0, flags, conv_arg2, conv_arg3)
}
// /usr/include/qt/QtGui/qpainter.h:442
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void drawText(const QRect &, int, const QString &, QRect *)

/*

*/
pub fn (this QPainter) drawTextp(r  qtcore.QRectITF, flags int, text string)  {
    mut conv_arg0 := voidptr(0)
    /*if r != voidptr(0) && r.QRect_ptr() != voidptr(0) */ {
        // conv_arg0 = r.QRect_ptr().get_cthis()
      conv_arg0 = r.get_cthis()
    }
    mut tmp_arg2 := qtcore.newQString5(text)
    mut conv_arg2 := tmp_arg2.get_cthis()
    // arg: 3, QRect *=Pointer, QRect=Record, , Invalid
    mut conv_arg3 := voidptr(0)
    mut fnobj := T_ZN8QPainter8drawTextERK5QRectiRK7QStringPS0_(0)
    fnobj = qtrt.sym_qtfunc6(2115100769, "_ZN8QPainter8drawTextERK5QRectiRK7QStringPS0_")
    fnobj(this.get_cthis(), conv_arg0, flags, conv_arg2, conv_arg3)
}
// /usr/include/qt/QtGui/qpainter.h:443
// index:1 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void drawText(int, int, int, int, int, const QString &, QRect *)
type T_ZN8QPainter8drawTextEiiiiiRK7QStringP5QRect = fn(cthis voidptr, x int, y int, w int, h int, flags int, text voidptr, br voidptr) /*void*/

/*

*/
pub fn (this QPainter) drawText1(x int, y int, w int, h int, flags int, text string, br qtcore.QRectITF/*777 QRect **/)  {
    mut tmp_arg5 := qtcore.newQString5(text)
    mut conv_arg5 := tmp_arg5.get_cthis()
    mut conv_arg6 := voidptr(0)
    /*if br != voidptr(0) && br.QRect_ptr() != voidptr(0) */ {
        // conv_arg6 = br.QRect_ptr().get_cthis()
      conv_arg6 = br.get_cthis()
    }
    mut fnobj := T_ZN8QPainter8drawTextEiiiiiRK7QStringP5QRect(0)
    fnobj = qtrt.sym_qtfunc6(4132837198, "_ZN8QPainter8drawTextEiiiiiRK7QStringP5QRect")
    fnobj(this.get_cthis(), x, y, w, h, flags, conv_arg5, conv_arg6)
}
// /usr/include/qt/QtGui/qpainter.h:443
// index:1 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void drawText(int, int, int, int, int, const QString &, QRect *)

/*

*/
pub fn (this QPainter) drawText1p(x int, y int, w int, h int, flags int, text string)  {
    mut tmp_arg5 := qtcore.newQString5(text)
    mut conv_arg5 := tmp_arg5.get_cthis()
    // arg: 6, QRect *=Pointer, QRect=Record, , Invalid
    mut conv_arg6 := voidptr(0)
    mut fnobj := T_ZN8QPainter8drawTextEiiiiiRK7QStringP5QRect(0)
    fnobj = qtrt.sym_qtfunc6(4132837198, "_ZN8QPainter8drawTextEiiiiiRK7QStringP5QRect")
    fnobj(this.get_cthis(), x, y, w, h, flags, conv_arg5, conv_arg6)
}
// /usr/include/qt/QtGui/qpainter.h:445
// index:2 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void drawText(const QRectF &, const QString &, const QTextOption &)
type T_ZN8QPainter8drawTextERK6QRectFRK7QStringRK11QTextOption = fn(cthis voidptr, r voidptr, text voidptr, o voidptr) /*void*/

/*

*/
pub fn (this QPainter) drawText2(r  qtcore.QRectFITF, text string, o  QTextOption)  {
    mut conv_arg0 := voidptr(0)
    /*if r != voidptr(0) && r.QRectF_ptr() != voidptr(0) */ {
        // conv_arg0 = r.QRectF_ptr().get_cthis()
      conv_arg0 = r.get_cthis()
    }
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    mut conv_arg2 := voidptr(0)
    /*if o != voidptr(0) && o.QTextOption_ptr() != voidptr(0) */ {
        // conv_arg2 = o.QTextOption_ptr().get_cthis()
      conv_arg2 = o.get_cthis()
    }
    mut fnobj := T_ZN8QPainter8drawTextERK6QRectFRK7QStringRK11QTextOption(0)
    fnobj = qtrt.sym_qtfunc6(1906770236, "_ZN8QPainter8drawTextERK6QRectFRK7QStringRK11QTextOption")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, conv_arg2)
}
// /usr/include/qt/QtGui/qpainter.h:445
// index:2 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void drawText(const QRectF &, const QString &, const QTextOption &)

/*

*/
pub fn (this QPainter) drawText2p(r  qtcore.QRectFITF, text string)  {
    mut conv_arg0 := voidptr(0)
    /*if r != voidptr(0) && r.QRectF_ptr() != voidptr(0) */ {
        // conv_arg0 = r.QRectF_ptr().get_cthis()
      conv_arg0 = r.get_cthis()
    }
    mut tmp_arg1 := qtcore.newQString5(text)
    mut conv_arg1 := tmp_arg1.get_cthis()
    // arg: 2, const QTextOption &=LValueReference, QTextOption=Record, , Invalid
    mut conv_arg2 := voidptr(0)
    mut fnobj := T_ZN8QPainter8drawTextERK6QRectFRK7QStringRK11QTextOption(0)
    fnobj = qtrt.sym_qtfunc6(1906770236, "_ZN8QPainter8drawTextERK6QRectFRK7QStringRK11QTextOption")
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, conv_arg2)
}

[no_inline]
pub fn deleteQPainter(this &QPainter) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2229242048, "_ZN8QPainterD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPainter) freecpp() { deleteQPainter(&this) }

fn (this QPainter) free() {

  /*deleteQPainter(&this)*/

  cthis := this.get_cthis()
  //println("QPainter freeing ${cthis} 0 bytes")

}


/*


*/
//type QPainter.RenderHint = int
pub enum QPainterRenderHint {
  Antialiasing = 1
  TextAntialiasing = 2
  SmoothPixmapTransform = 4
  HighQualityAntialiasing = 8
  NonCosmeticDefaultPen = 16
  Qt4CompatiblePainting = 32
  LosslessImageRendering = 64
} // endof enum RenderHint


/*


*/
//type QPainter.PixmapFragmentHint = int
pub enum QPainterPixmapFragmentHint {
  OpaqueHint = 1
} // endof enum PixmapFragmentHint


/*


*/
//type QPainter.CompositionMode = int
pub enum QPainterCompositionMode {
  CompositionMode_SourceOver = 0
  CompositionMode_DestinationOver = 1
  CompositionMode_Clear = 2
  CompositionMode_Source = 3
  CompositionMode_Destination = 4
  CompositionMode_SourceIn = 5
  CompositionMode_DestinationIn = 6
  CompositionMode_SourceOut = 7
  CompositionMode_DestinationOut = 8
  CompositionMode_SourceAtop = 9
  CompositionMode_DestinationAtop = 10
  CompositionMode_Xor = 11
  CompositionMode_Plus = 12
  CompositionMode_Multiply = 13
  CompositionMode_Screen = 14
  CompositionMode_Overlay = 15
  CompositionMode_Darken = 16
  CompositionMode_Lighten = 17
  CompositionMode_ColorDodge = 18
  CompositionMode_ColorBurn = 19
  CompositionMode_HardLight = 20
  CompositionMode_SoftLight = 21
  CompositionMode_Difference = 22
  CompositionMode_Exclusion = 23
  RasterOp_SourceOrDestination = 24
  RasterOp_SourceAndDestination = 25
  RasterOp_SourceXorDestination = 26
  RasterOp_NotSourceAndNotDestination = 27
  RasterOp_NotSourceOrNotDestination = 28
  RasterOp_NotSourceXorDestination = 29
  RasterOp_NotSource = 30
  RasterOp_NotSourceAndDestination = 31
  RasterOp_SourceAndNotDestination = 32
  RasterOp_NotSourceOrDestination = 33
  RasterOp_SourceOrNotDestination = 34
  RasterOp_ClearDestination = 35
  RasterOp_SetDestination = 36
  RasterOp_NotDestination = 37
} // endof enum CompositionMode

//  body block end

//  keep block begin


fn init_unused_10173() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
