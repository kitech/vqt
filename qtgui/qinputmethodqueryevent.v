

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QInputMethodQueryEvent {
pub:
  qtcore.QEvent
}

pub interface QInputMethodQueryEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQInputMethodQueryEvent() QInputMethodQueryEvent
}
fn hotfix_QInputMethodQueryEvent_itf_name_table(this QInputMethodQueryEventITF) {
  that := QInputMethodQueryEvent{}
  hotfix_QInputMethodQueryEvent_itf_name_table(that)
}
pub fn (ptr QInputMethodQueryEvent) toQInputMethodQueryEvent() QInputMethodQueryEvent { return ptr }

pub fn (this QInputMethodQueryEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQInputMethodQueryEventFromptr(cthis voidptr) QInputMethodQueryEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QInputMethodQueryEvent{bcthis0}
}
pub fn (dummy QInputMethodQueryEvent) newFromptr(cthis voidptr) QInputMethodQueryEvent {
    return newQInputMethodQueryEventFromptr(cthis)
}

[no_inline]
pub fn deleteQInputMethodQueryEvent(this &QInputMethodQueryEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3676154321, "_ZN22QInputMethodQueryEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QInputMethodQueryEvent) freecpp() { deleteQInputMethodQueryEvent(&this) }

fn (this QInputMethodQueryEvent) free() {

  /*deleteQInputMethodQueryEvent(&this)*/

  cthis := this.get_cthis()
  //println("QInputMethodQueryEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10107() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
