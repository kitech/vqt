

module qtgui
// /usr/include/qt/QtGui/qpen.h
// #include <qpen.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QPen {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QPenITF {
    get_cthis() voidptr
    toQPen() QPen
}
fn hotfix_QPen_itf_name_table(this QPenITF) {
  that := QPen{}
  hotfix_QPen_itf_name_table(that)
}
pub fn (ptr QPen) toQPen() QPen { return ptr }

pub fn (this QPen) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQPenFromptr(cthis voidptr) QPen {
    return QPen{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QPen) newFromptr(cthis voidptr) QPen {
    return newQPenFromptr(cthis)
}
// /usr/include/qt/QtGui/qpen.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPen()
type T_ZN4QPenC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QPen) new_for_inherit_() QPen {
  //return newQPen()
  return QPen{}
}
pub fn newQPen() QPen {
    mut fnobj := T_ZN4QPenC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(46253105, "_ZN4QPenC2Ev")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis)
    rv := cthis
    vthis := newQPenFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPen)
  return vthis
}

[no_inline]
pub fn deleteQPen(this &QPen) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2669083784, "_ZN4QPenD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPen) freecpp() { deleteQPen(&this) }

fn (this QPen) free() {

  /*deleteQPen(&this)*/

  cthis := this.get_cthis()
  //println("QPen freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10153() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
