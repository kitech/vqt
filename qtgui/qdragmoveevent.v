

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QDragMoveEvent {
pub:
  QDropEvent
}

pub interface QDragMoveEventITF {
//    QDropEventITF
    get_cthis() voidptr
    toQDragMoveEvent() QDragMoveEvent
}
fn hotfix_QDragMoveEvent_itf_name_table(this QDragMoveEventITF) {
  that := QDragMoveEvent{}
  hotfix_QDragMoveEvent_itf_name_table(that)
}
pub fn (ptr QDragMoveEvent) toQDragMoveEvent() QDragMoveEvent { return ptr }

pub fn (this QDragMoveEvent) get_cthis() voidptr {
    return this.QDropEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQDragMoveEventFromptr(cthis voidptr) QDragMoveEvent {
    bcthis0 := newQDropEventFromptr(cthis)
    return QDragMoveEvent{bcthis0}
}
pub fn (dummy QDragMoveEvent) newFromptr(cthis voidptr) QDragMoveEvent {
    return newQDragMoveEventFromptr(cthis)
}

[no_inline]
pub fn deleteQDragMoveEvent(this &QDragMoveEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(278496148, "_ZN14QDragMoveEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDragMoveEvent) freecpp() { deleteQDragMoveEvent(&this) }

fn (this QDragMoveEvent) free() {

  /*deleteQDragMoveEvent(&this)*/

  cthis := this.get_cthis()
  //println("QDragMoveEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10111() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
