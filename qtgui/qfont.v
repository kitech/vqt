

module qtgui
// /usr/include/qt/QtGui/qfont.h
// #include <qfont.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QFont {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QFontITF {
    get_cthis() voidptr
    toQFont() QFont
}
fn hotfix_QFont_itf_name_table(this QFontITF) {
  that := QFont{}
  hotfix_QFont_itf_name_table(that)
}
pub fn (ptr QFont) toQFont() QFont { return ptr }

pub fn (this QFont) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQFontFromptr(cthis voidptr) QFont {
    return QFont{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QFont) newFromptr(cthis voidptr) QFont {
    return newQFontFromptr(cthis)
}
// /usr/include/qt/QtGui/qfont.h:176
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QFont(const QString &, int, int, bool)
type T_ZN5QFontC2ERK7QStringiib = fn(cthis voidptr, family voidptr, pointSize int, weight int, italic bool) 

/*

*/
pub fn (dummy QFont) new_for_inherit_(family string, pointSize int, weight int, italic bool) QFont {
  //return newQFont(family, pointSize, weight, italic)
  return QFont{}
}
pub fn newQFont(family string, pointSize int, weight int, italic bool) QFont {
    mut tmp_arg0 := qtcore.newQString5(family)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN5QFontC2ERK7QStringiib(0)
    fnobj = qtrt.sym_qtfunc6(1453900756, "_ZN5QFontC2ERK7QStringiib")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0, pointSize, weight, italic)
    rv := cthis
    vthis := newQFontFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQFont)
  return vthis
}
// /usr/include/qt/QtGui/qfont.h:176
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QFont(const QString &, int, int, bool)

/*

*/
pub fn (dummy QFont) new_for_inherit_p(family string) QFont {
  //return newQFontp(family)
  return QFont{}
}
pub fn newQFontp(family string) QFont {
    mut tmp_arg0 := qtcore.newQString5(family)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, int=Int, =Invalid, , Invalid
    pointSize := int(-1)
    // arg: 2, int=Int, =Invalid, , Invalid
    weight := int(-1)
    // arg: 3, bool=Bool, =Invalid, , Invalid
    italic := false
    mut fnobj := T_ZN5QFontC2ERK7QStringiib(0)
    fnobj = qtrt.sym_qtfunc6(1453900756, "_ZN5QFontC2ERK7QStringiib")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0, pointSize, weight, italic)
    rv := cthis
    vthis := newQFontFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQFont)
    return vthis
}
// /usr/include/qt/QtGui/qfont.h:176
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QFont(const QString &, int, int, bool)

/*

*/
pub fn (dummy QFont) new_for_inherit_p1(family string, pointSize int) QFont {
  //return newQFontp1(family, pointSize)
  return QFont{}
}
pub fn newQFontp1(family string, pointSize int) QFont {
    mut tmp_arg0 := qtcore.newQString5(family)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 2, int=Int, =Invalid, , Invalid
    weight := int(-1)
    // arg: 3, bool=Bool, =Invalid, , Invalid
    italic := false
    mut fnobj := T_ZN5QFontC2ERK7QStringiib(0)
    fnobj = qtrt.sym_qtfunc6(1453900756, "_ZN5QFontC2ERK7QStringiib")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0, pointSize, weight, italic)
    rv := cthis
    vthis := newQFontFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQFont)
    return vthis
}
// /usr/include/qt/QtGui/qfont.h:176
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QFont(const QString &, int, int, bool)

/*

*/
pub fn (dummy QFont) new_for_inherit_p2(family string, pointSize int, weight int) QFont {
  //return newQFontp2(family, pointSize, weight)
  return QFont{}
}
pub fn newQFontp2(family string, pointSize int, weight int) QFont {
    mut tmp_arg0 := qtcore.newQString5(family)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 3, bool=Bool, =Invalid, , Invalid
    italic := false
    mut fnobj := T_ZN5QFontC2ERK7QStringiib(0)
    fnobj = qtrt.sym_qtfunc6(1453900756, "_ZN5QFontC2ERK7QStringiib")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0, pointSize, weight, italic)
    rv := cthis
    vthis := newQFontFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQFont)
    return vthis
}
// /usr/include/qt/QtGui/qfont.h:187
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString family() const
type T_ZNK5QFont6familyEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QFont) family() string {
    mut fnobj := T_ZNK5QFont6familyEv(0)
    fnobj = qtrt.sym_qtfunc6(2393185171, "_ZNK5QFont6familyEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtGui/qfont.h:188
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFamily(const QString &)
type T_ZN5QFont9setFamilyERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QFont) setFamily(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN5QFont9setFamilyERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(3823958997, "_ZN5QFont9setFamilyERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtGui/qfont.h:193
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString styleName() const
type T_ZNK5QFont9styleNameEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QFont) styleName() string {
    mut fnobj := T_ZNK5QFont9styleNameEv(0)
    fnobj = qtrt.sym_qtfunc6(2670374555, "_ZNK5QFont9styleNameEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtGui/qfont.h:194
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStyleName(const QString &)
type T_ZN5QFont12setStyleNameERK7QString = fn(cthis voidptr, arg0 voidptr) /*void*/

/*

*/
pub fn (this QFont) setStyleName(arg0 string)  {
    mut tmp_arg0 := qtcore.newQString5(arg0)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN5QFont12setStyleNameERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(814338029, "_ZN5QFont12setStyleNameERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtGui/qfont.h:196
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int pointSize() const
type T_ZNK5QFont9pointSizeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) pointSize() int {
    mut fnobj := T_ZNK5QFont9pointSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(3023852881, "_ZNK5QFont9pointSizeEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:197
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPointSize(int)
type T_ZN5QFont12setPointSizeEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QFont) setPointSize(arg0 int)  {
    mut fnobj := T_ZN5QFont12setPointSizeEi(0)
    fnobj = qtrt.sym_qtfunc6(1776764880, "_ZN5QFont12setPointSizeEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:198
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qreal pointSizeF() const
type T_ZNK5QFont10pointSizeFEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QFont) pointSizeF() f64 {
    mut fnobj := T_ZNK5QFont10pointSizeFEv(0)
    fnobj = qtrt.sym_qtfunc6(314855354, "_ZNK5QFont10pointSizeFEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtGui/qfont.h:199
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPointSizeF(qreal)
type T_ZN5QFont13setPointSizeFEd = fn(cthis voidptr, arg0 f64) /*void*/

/*

*/
pub fn (this QFont) setPointSizeF(arg0 f64)  {
    mut fnobj := T_ZN5QFont13setPointSizeFEd(0)
    fnobj = qtrt.sym_qtfunc6(2339912505, "_ZN5QFont13setPointSizeFEd")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:201
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int pixelSize() const
type T_ZNK5QFont9pixelSizeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) pixelSize() int {
    mut fnobj := T_ZNK5QFont9pixelSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(197311963, "_ZNK5QFont9pixelSizeEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:202
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setPixelSize(int)
type T_ZN5QFont12setPixelSizeEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QFont) setPixelSize(arg0 int)  {
    mut fnobj := T_ZN5QFont12setPixelSizeEi(0)
    fnobj = qtrt.sym_qtfunc6(3592007514, "_ZN5QFont12setPixelSizeEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:204
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int weight() const
type T_ZNK5QFont6weightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) weight() int {
    mut fnobj := T_ZNK5QFont6weightEv(0)
    fnobj = qtrt.sym_qtfunc6(1776350790, "_ZNK5QFont6weightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:205
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWeight(int)
type T_ZN5QFont9setWeightEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QFont) setWeight(arg0 int)  {
    mut fnobj := T_ZN5QFont9setWeightEi(0)
    fnobj = qtrt.sym_qtfunc6(133781457, "_ZN5QFont9setWeightEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:207
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool bold() const
type T_ZNK5QFont4boldEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) bold() bool {
    mut fnobj := T_ZNK5QFont4boldEv(0)
    fnobj = qtrt.sym_qtfunc6(1200580973, "_ZNK5QFont4boldEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qfont.h:208
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setBold(bool)
type T_ZN5QFont7setBoldEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QFont) setBold(arg0 bool)  {
    mut fnobj := T_ZN5QFont7setBoldEb(0)
    fnobj = qtrt.sym_qtfunc6(1947504972, "_ZN5QFont7setBoldEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:210
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStyle(QFont::Style)
type T_ZN5QFont8setStyleENS_5StyleE = fn(cthis voidptr, style int) /*void*/

/*

*/
pub fn (this QFont) setStyle(style int)  {
    mut fnobj := T_ZN5QFont8setStyleENS_5StyleE(0)
    fnobj = qtrt.sym_qtfunc6(2307513941, "_ZN5QFont8setStyleENS_5StyleE")
    fnobj(this.get_cthis(), style)
}
// /usr/include/qt/QtGui/qfont.h:211
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QFont::Style style() const
type T_ZNK5QFont5styleEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) style() int {
    mut fnobj := T_ZNK5QFont5styleEv(0)
    fnobj = qtrt.sym_qtfunc6(2644565881, "_ZNK5QFont5styleEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:213
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool italic() const
type T_ZNK5QFont6italicEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) italic() bool {
    mut fnobj := T_ZNK5QFont6italicEv(0)
    fnobj = qtrt.sym_qtfunc6(4214105033, "_ZNK5QFont6italicEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qfont.h:214
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setItalic(bool)
type T_ZN5QFont9setItalicEb = fn(cthis voidptr, b bool) /*void*/

/*

*/
pub fn (this QFont) setItalic(b bool)  {
    mut fnobj := T_ZN5QFont9setItalicEb(0)
    fnobj = qtrt.sym_qtfunc6(48585686, "_ZN5QFont9setItalicEb")
    fnobj(this.get_cthis(), b)
}
// /usr/include/qt/QtGui/qfont.h:216
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool underline() const
type T_ZNK5QFont9underlineEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) underline() bool {
    mut fnobj := T_ZNK5QFont9underlineEv(0)
    fnobj = qtrt.sym_qtfunc6(3473615270, "_ZNK5QFont9underlineEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qfont.h:217
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setUnderline(bool)
type T_ZN5QFont12setUnderlineEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QFont) setUnderline(arg0 bool)  {
    mut fnobj := T_ZN5QFont12setUnderlineEb(0)
    fnobj = qtrt.sym_qtfunc6(2231558831, "_ZN5QFont12setUnderlineEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:219
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool overline() const
type T_ZNK5QFont8overlineEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) overline() bool {
    mut fnobj := T_ZNK5QFont8overlineEv(0)
    fnobj = qtrt.sym_qtfunc6(2096479559, "_ZNK5QFont8overlineEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qfont.h:220
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setOverline(bool)
type T_ZN5QFont11setOverlineEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QFont) setOverline(arg0 bool)  {
    mut fnobj := T_ZN5QFont11setOverlineEb(0)
    fnobj = qtrt.sym_qtfunc6(229563217, "_ZN5QFont11setOverlineEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:222
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool strikeOut() const
type T_ZNK5QFont9strikeOutEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) strikeOut() bool {
    mut fnobj := T_ZNK5QFont9strikeOutEv(0)
    fnobj = qtrt.sym_qtfunc6(2658375640, "_ZNK5QFont9strikeOutEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qfont.h:223
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStrikeOut(bool)
type T_ZN5QFont12setStrikeOutEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QFont) setStrikeOut(arg0 bool)  {
    mut fnobj := T_ZN5QFont12setStrikeOutEb(0)
    fnobj = qtrt.sym_qtfunc6(3564788945, "_ZN5QFont12setStrikeOutEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:225
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool fixedPitch() const
type T_ZNK5QFont10fixedPitchEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) fixedPitch() bool {
    mut fnobj := T_ZNK5QFont10fixedPitchEv(0)
    fnobj = qtrt.sym_qtfunc6(3065387023, "_ZNK5QFont10fixedPitchEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qfont.h:226
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setFixedPitch(bool)
type T_ZN5QFont13setFixedPitchEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QFont) setFixedPitch(arg0 bool)  {
    mut fnobj := T_ZN5QFont13setFixedPitchEb(0)
    fnobj = qtrt.sym_qtfunc6(3328821689, "_ZN5QFont13setFixedPitchEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:228
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool kerning() const
type T_ZNK5QFont7kerningEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) kerning() bool {
    mut fnobj := T_ZNK5QFont7kerningEv(0)
    fnobj = qtrt.sym_qtfunc6(3727955333, "_ZNK5QFont7kerningEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qfont.h:229
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setKerning(bool)
type T_ZN5QFont10setKerningEb = fn(cthis voidptr, arg0 bool) /*void*/

/*

*/
pub fn (this QFont) setKerning(arg0 bool)  {
    mut fnobj := T_ZN5QFont10setKerningEb(0)
    fnobj = qtrt.sym_qtfunc6(43048096, "_ZN5QFont10setKerningEb")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:231
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QFont::StyleHint styleHint() const
type T_ZNK5QFont9styleHintEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) styleHint() int {
    mut fnobj := T_ZNK5QFont9styleHintEv(0)
    fnobj = qtrt.sym_qtfunc6(1985110254, "_ZNK5QFont9styleHintEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:232
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QFont::StyleStrategy styleStrategy() const
type T_ZNK5QFont13styleStrategyEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) styleStrategy() int {
    mut fnobj := T_ZNK5QFont13styleStrategyEv(0)
    fnobj = qtrt.sym_qtfunc6(2409632431, "_ZNK5QFont13styleStrategyEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:233
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStyleHint(QFont::StyleHint, QFont::StyleStrategy)
type T_ZN5QFont12setStyleHintENS_9StyleHintENS_13StyleStrategyE = fn(cthis voidptr, arg0 int, arg1 int) /*void*/

/*

*/
pub fn (this QFont) setStyleHint(arg0 int, arg1 int)  {
    mut fnobj := T_ZN5QFont12setStyleHintENS_9StyleHintENS_13StyleStrategyE(0)
    fnobj = qtrt.sym_qtfunc6(4164244041, "_ZN5QFont12setStyleHintENS_9StyleHintENS_13StyleStrategyE")
    fnobj(this.get_cthis(), arg0, arg1)
}
// /usr/include/qt/QtGui/qfont.h:233
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStyleHint(QFont::StyleHint, QFont::StyleStrategy)

/*

*/
pub fn (this QFont) setStyleHintp(arg0 int)  {
    // arg: 1, QFont::StyleStrategy=Enum, QFont::StyleStrategy=Enum, , Invalid
    arg1 := 0
    mut fnobj := T_ZN5QFont12setStyleHintENS_9StyleHintENS_13StyleStrategyE(0)
    fnobj = qtrt.sym_qtfunc6(4164244041, "_ZN5QFont12setStyleHintENS_9StyleHintENS_13StyleStrategyE")
    fnobj(this.get_cthis(), arg0, arg1)
}
// /usr/include/qt/QtGui/qfont.h:234
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStyleStrategy(QFont::StyleStrategy)
type T_ZN5QFont16setStyleStrategyENS_13StyleStrategyE = fn(cthis voidptr, s int) /*void*/

/*

*/
pub fn (this QFont) setStyleStrategy(s int)  {
    mut fnobj := T_ZN5QFont16setStyleStrategyENS_13StyleStrategyE(0)
    fnobj = qtrt.sym_qtfunc6(3706440996, "_ZN5QFont16setStyleStrategyENS_13StyleStrategyE")
    fnobj(this.get_cthis(), s)
}
// /usr/include/qt/QtGui/qfont.h:236
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int stretch() const
type T_ZNK5QFont7stretchEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) stretch() int {
    mut fnobj := T_ZNK5QFont7stretchEv(0)
    fnobj = qtrt.sym_qtfunc6(1924986953, "_ZNK5QFont7stretchEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:237
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setStretch(int)
type T_ZN5QFont10setStretchEi = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QFont) setStretch(arg0 int)  {
    mut fnobj := T_ZN5QFont10setStretchEi(0)
    fnobj = qtrt.sym_qtfunc6(969599204, "_ZN5QFont10setStretchEi")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:239
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qreal letterSpacing() const
type T_ZNK5QFont13letterSpacingEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QFont) letterSpacing() f64 {
    mut fnobj := T_ZNK5QFont13letterSpacingEv(0)
    fnobj = qtrt.sym_qtfunc6(2641819184, "_ZNK5QFont13letterSpacingEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtGui/qfont.h:240
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QFont::SpacingType letterSpacingType() const
type T_ZNK5QFont17letterSpacingTypeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) letterSpacingType() int {
    mut fnobj := T_ZNK5QFont17letterSpacingTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(2189521662, "_ZNK5QFont17letterSpacingTypeEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:241
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setLetterSpacing(QFont::SpacingType, qreal)
type T_ZN5QFont16setLetterSpacingENS_11SpacingTypeEd = fn(cthis voidptr, type_ int, spacing f64) /*void*/

/*

*/
pub fn (this QFont) setLetterSpacing(type_ int, spacing f64)  {
    mut fnobj := T_ZN5QFont16setLetterSpacingENS_11SpacingTypeEd(0)
    fnobj = qtrt.sym_qtfunc6(4000308159, "_ZN5QFont16setLetterSpacingENS_11SpacingTypeEd")
    fnobj(this.get_cthis(), type_, spacing)
}
// /usr/include/qt/QtGui/qfont.h:243
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qreal wordSpacing() const
type T_ZNK5QFont11wordSpacingEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QFont) wordSpacing() f64 {
    mut fnobj := T_ZNK5QFont11wordSpacingEv(0)
    fnobj = qtrt.sym_qtfunc6(3552615348, "_ZNK5QFont11wordSpacingEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtGui/qfont.h:244
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setWordSpacing(qreal)
type T_ZN5QFont14setWordSpacingEd = fn(cthis voidptr, spacing f64) /*void*/

/*

*/
pub fn (this QFont) setWordSpacing(spacing f64)  {
    mut fnobj := T_ZN5QFont14setWordSpacingEd(0)
    fnobj = qtrt.sym_qtfunc6(2436393468, "_ZN5QFont14setWordSpacingEd")
    fnobj(this.get_cthis(), spacing)
}
// /usr/include/qt/QtGui/qfont.h:246
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setCapitalization(QFont::Capitalization)
type T_ZN5QFont17setCapitalizationENS_14CapitalizationE = fn(cthis voidptr, arg0 int) /*void*/

/*

*/
pub fn (this QFont) setCapitalization(arg0 int)  {
    mut fnobj := T_ZN5QFont17setCapitalizationENS_14CapitalizationE(0)
    fnobj = qtrt.sym_qtfunc6(2579133516, "_ZN5QFont17setCapitalizationENS_14CapitalizationE")
    fnobj(this.get_cthis(), arg0)
}
// /usr/include/qt/QtGui/qfont.h:247
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QFont::Capitalization capitalization() const
type T_ZNK5QFont14capitalizationEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) capitalization() int {
    mut fnobj := T_ZNK5QFont14capitalizationEv(0)
    fnobj = qtrt.sym_qtfunc6(74477930, "_ZNK5QFont14capitalizationEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:249
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setHintingPreference(QFont::HintingPreference)
type T_ZN5QFont20setHintingPreferenceENS_17HintingPreferenceE = fn(cthis voidptr, hintingPreference int) /*void*/

/*

*/
pub fn (this QFont) setHintingPreference(hintingPreference int)  {
    mut fnobj := T_ZN5QFont20setHintingPreferenceENS_17HintingPreferenceE(0)
    fnobj = qtrt.sym_qtfunc6(2526211871, "_ZN5QFont20setHintingPreferenceENS_17HintingPreferenceE")
    fnobj(this.get_cthis(), hintingPreference)
}
// /usr/include/qt/QtGui/qfont.h:250
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QFont::HintingPreference hintingPreference() const
type T_ZNK5QFont17hintingPreferenceEv = fn(cthis voidptr) int

/*

*/
pub fn (this QFont) hintingPreference() int {
    mut fnobj := T_ZNK5QFont17hintingPreferenceEv(0)
    fnobj = qtrt.sym_qtfunc6(1448856926, "_ZNK5QFont17hintingPreferenceEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtGui/qfont.h:258
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool exactMatch() const
type T_ZNK5QFont10exactMatchEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QFont) exactMatch() bool {
    mut fnobj := T_ZNK5QFont10exactMatchEv(0)
    fnobj = qtrt.sym_qtfunc6(2416987156, "_ZNK5QFont10exactMatchEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}

[no_inline]
pub fn deleteQFont(this &QFont) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(861828757, "_ZN5QFontD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QFont) freecpp() { deleteQFont(&this) }

fn (this QFont) free() {

  /*deleteQFont(&this)*/

  cthis := this.get_cthis()
  //println("QFont freeing ${cthis} 0 bytes")

}


/*


*/
//type QFont.StyleHint = int
pub enum QFontStyleHint {
  Helvetica = 0
  Times = 1
  Courier = 2
  OldEnglish = 3
  System = 4
  AnyStyle = 5
  Cursive = 6
  Monospace = 7
  Fantasy = 8
} // endof enum StyleHint


/*


*/
//type QFont.StyleStrategy = int
pub enum QFontStyleStrategy {
  PreferDefault = 1
  PreferBitmap = 2
  PreferDevice = 4
  PreferOutline = 8
  ForceOutline = 16
  PreferMatch = 32
  PreferQuality = 64
  PreferAntialias = 128
  NoAntialias = 256
  OpenGLCompatible = 512
  ForceIntegerMetrics = 1024
  NoSubpixelAntialias = 2048
  PreferNoShaping = 4096
  NoFontMerging = 32768
} // endof enum StyleStrategy


/*


*/
//type QFont.HintingPreference = int
pub enum QFontHintingPreference {
  PreferDefaultHinting = 0
  PreferNoHinting = 1
  PreferVerticalHinting = 2
  PreferFullHinting = 3
} // endof enum HintingPreference


/*


*/
//type QFont.Weight = int
pub enum QFontWeight {
  Thin = 0
  ExtraLight = 12
  Light = 25
  Normal = 50
  Medium = 57
  DemiBold = 63
  Bold = 75
  ExtraBold = 81
  Black = 87
} // endof enum Weight


/*


*/
//type QFont.Style = int
pub enum QFontStyle {
  StyleNormal = 0
  StyleItalic = 1
  StyleOblique = 2
} // endof enum Style


/*


*/
//type QFont.Stretch = int
pub enum QFontStretch {
  AnyStretch = 0
  UltraCondensed = 50
  ExtraCondensed = 62
  Condensed = 75
  SemiCondensed = 87
  Unstretched = 100
  SemiExpanded = 112
  Expanded = 125
  ExtraExpanded = 150
  UltraExpanded = 200
} // endof enum Stretch


/*


*/
//type QFont.Capitalization = int
pub enum QFontCapitalization {
  MixedCase = 0
  AllUppercase = 1
  AllLowercase = 2
  SmallCaps = 3
  Capitalize = 4
} // endof enum Capitalization


/*


*/
//type QFont.SpacingType = int
pub enum QFontSpacingType {
  PercentageSpacing = 0
  AbsoluteSpacing = 1
} // endof enum SpacingType


/*


*/
//type QFont.ResolveProperties = int
pub enum QFontResolveProperties {
  NoPropertiesResolved = 0
  FamilyResolved = 1
  SizeResolved = 2
  StyleHintResolved = 4
  StyleStrategyResolved = 8
  WeightResolved = 16
  StyleResolved = 32
  UnderlineResolved = 64
  OverlineResolved = 128
  StrikeOutResolved = 256
  FixedPitchResolved = 512
  StretchResolved = 1024
  KerningResolved = 2048
  CapitalizationResolved = 4096
  LetterSpacingResolved = 8192
  WordSpacingResolved = 16384
  HintingPreferenceResolved = 32768
  StyleNameResolved = 65536
  FamiliesResolved = 131072
  AllPropertiesResolved = 262143
} // endof enum ResolveProperties

//  body block end

//  keep block begin


fn init_unused_10143() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
