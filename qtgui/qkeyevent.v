

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QKeyEvent {
pub:
  QInputEvent
}

pub interface QKeyEventITF {
//    QInputEventITF
    get_cthis() voidptr
    toQKeyEvent() QKeyEvent
}
fn hotfix_QKeyEvent_itf_name_table(this QKeyEventITF) {
  that := QKeyEvent{}
  hotfix_QKeyEvent_itf_name_table(that)
}
pub fn (ptr QKeyEvent) toQKeyEvent() QKeyEvent { return ptr }

pub fn (this QKeyEvent) get_cthis() voidptr {
    return this.QInputEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQKeyEventFromptr(cthis voidptr) QKeyEvent {
    bcthis0 := newQInputEventFromptr(cthis)
    return QKeyEvent{bcthis0}
}
pub fn (dummy QKeyEvent) newFromptr(cthis voidptr) QKeyEvent {
    return newQKeyEventFromptr(cthis)
}
// /usr/include/qt/QtGui/qevent.h:387
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int key() const
type T_ZNK9QKeyEvent3keyEv = fn(cthis voidptr) int

/*

*/
pub fn (this QKeyEvent) key() int {
    mut fnobj := T_ZNK9QKeyEvent3keyEv(0)
    fnobj = qtrt.sym_qtfunc6(3921584077, "_ZNK9QKeyEvent3keyEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:392
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [8] QString text() const
type T_ZNK9QKeyEvent4textEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QKeyEvent) text() string {
    mut fnobj := T_ZNK9QKeyEvent4textEv(0)
    fnobj = qtrt.sym_qtfunc6(3489740836, "_ZNK9QKeyEvent4textEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := qtcore.newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    qtcore.deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtGui/qevent.h:393
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isAutoRepeat() const
type T_ZNK9QKeyEvent12isAutoRepeatEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QKeyEvent) isAutoRepeat() bool {
    mut fnobj := T_ZNK9QKeyEvent12isAutoRepeatEv(0)
    fnobj = qtrt.sym_qtfunc6(3618180515, "_ZNK9QKeyEvent12isAutoRepeatEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtGui/qevent.h:394
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int count() const
type T_ZNK9QKeyEvent5countEv = fn(cthis voidptr) int

/*

*/
pub fn (this QKeyEvent) count() int {
    mut fnobj := T_ZNK9QKeyEvent5countEv(0)
    fnobj = qtrt.sym_qtfunc6(2383005192, "_ZNK9QKeyEvent5countEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qevent.h:396
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] quint32 nativeScanCode() const
type T_ZNK9QKeyEvent14nativeScanCodeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QKeyEvent) nativeScanCode() int {
    mut fnobj := T_ZNK9QKeyEvent14nativeScanCodeEv(0)
    fnobj = qtrt.sym_qtfunc6(1110117359, "_ZNK9QKeyEvent14nativeScanCodeEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv) // 222
}
// /usr/include/qt/QtGui/qevent.h:397
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] quint32 nativeVirtualKey() const
type T_ZNK9QKeyEvent16nativeVirtualKeyEv = fn(cthis voidptr) int

/*

*/
pub fn (this QKeyEvent) nativeVirtualKey() int {
    mut fnobj := T_ZNK9QKeyEvent16nativeVirtualKeyEv(0)
    fnobj = qtrt.sym_qtfunc6(3927640315, "_ZNK9QKeyEvent16nativeVirtualKeyEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv) // 222
}
// /usr/include/qt/QtGui/qevent.h:398
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] quint32 nativeModifiers() const
type T_ZNK9QKeyEvent15nativeModifiersEv = fn(cthis voidptr) int

/*

*/
pub fn (this QKeyEvent) nativeModifiers() int {
    mut fnobj := T_ZNK9QKeyEvent15nativeModifiersEv(0)
    fnobj = qtrt.sym_qtfunc6(2110805296, "_ZNK9QKeyEvent15nativeModifiersEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv) // 222
}

[no_inline]
pub fn deleteQKeyEvent(this &QKeyEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(4215536018, "_ZN9QKeyEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QKeyEvent) freecpp() { deleteQKeyEvent(&this) }

fn (this QKeyEvent) free() {

  /*deleteQKeyEvent(&this)*/

  cthis := this.get_cthis()
  //println("QKeyEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10081() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
