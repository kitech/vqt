

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QToolBarChangeEvent {
pub:
  qtcore.QEvent
}

pub interface QToolBarChangeEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQToolBarChangeEvent() QToolBarChangeEvent
}
fn hotfix_QToolBarChangeEvent_itf_name_table(this QToolBarChangeEventITF) {
  that := QToolBarChangeEvent{}
  hotfix_QToolBarChangeEvent_itf_name_table(that)
}
pub fn (ptr QToolBarChangeEvent) toQToolBarChangeEvent() QToolBarChangeEvent { return ptr }

pub fn (this QToolBarChangeEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQToolBarChangeEventFromptr(cthis voidptr) QToolBarChangeEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QToolBarChangeEvent{bcthis0}
}
pub fn (dummy QToolBarChangeEvent) newFromptr(cthis voidptr) QToolBarChangeEvent {
    return newQToolBarChangeEventFromptr(cthis)
}

[no_inline]
pub fn deleteQToolBarChangeEvent(this &QToolBarChangeEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1334881248, "_ZN19QToolBarChangeEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QToolBarChangeEvent) freecpp() { deleteQToolBarChangeEvent(&this) }

fn (this QToolBarChangeEvent) free() {

  /*deleteQToolBarChangeEvent(&this)*/

  cthis := this.get_cthis()
  //println("QToolBarChangeEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10127() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
