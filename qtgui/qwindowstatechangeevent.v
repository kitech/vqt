

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QWindowStateChangeEvent {
pub:
  qtcore.QEvent
}

pub interface QWindowStateChangeEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQWindowStateChangeEvent() QWindowStateChangeEvent
}
fn hotfix_QWindowStateChangeEvent_itf_name_table(this QWindowStateChangeEventITF) {
  that := QWindowStateChangeEvent{}
  hotfix_QWindowStateChangeEvent_itf_name_table(that)
}
pub fn (ptr QWindowStateChangeEvent) toQWindowStateChangeEvent() QWindowStateChangeEvent { return ptr }

pub fn (this QWindowStateChangeEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQWindowStateChangeEventFromptr(cthis voidptr) QWindowStateChangeEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QWindowStateChangeEvent{bcthis0}
}
pub fn (dummy QWindowStateChangeEvent) newFromptr(cthis voidptr) QWindowStateChangeEvent {
    return newQWindowStateChangeEventFromptr(cthis)
}

[no_inline]
pub fn deleteQWindowStateChangeEvent(this &QWindowStateChangeEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3837143262, "_ZN23QWindowStateChangeEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QWindowStateChangeEvent) freecpp() { deleteQWindowStateChangeEvent(&this) }

fn (this QWindowStateChangeEvent) free() {

  /*deleteQWindowStateChangeEvent(&this)*/

  cthis := this.get_cthis()
  //println("QWindowStateChangeEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10131() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
