

module qtgui
// /usr/include/qt/QtGui/qpixmap.h
// #include <qpixmap.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 3
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QPixmap {
pub:
  QPaintDevice
}

pub interface QPixmapITF {
//    QPaintDeviceITF
    get_cthis() voidptr
    toQPixmap() QPixmap
}
fn hotfix_QPixmap_itf_name_table(this QPixmapITF) {
  that := QPixmap{}
  hotfix_QPixmap_itf_name_table(that)
}
pub fn (ptr QPixmap) toQPixmap() QPixmap { return ptr }

pub fn (this QPixmap) get_cthis() voidptr {
    return this.QPaintDevice.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQPixmapFromptr(cthis voidptr) QPixmap {
    bcthis0 := newQPaintDeviceFromptr(cthis)
    return QPixmap{bcthis0}
}
pub fn (dummy QPixmap) newFromptr(cthis voidptr) QPixmap {
    return newQPixmapFromptr(cthis)
}
// /usr/include/qt/QtGui/qpixmap.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPixmap()
type T_ZN7QPixmapC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QPixmap) new_for_inherit_() QPixmap {
  //return newQPixmap()
  return QPixmap{}
}
pub fn newQPixmap() QPixmap {
    mut fnobj := T_ZN7QPixmapC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(440989401, "_ZN7QPixmapC2Ev")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis)
    rv := cthis
    vthis := newQPixmapFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPixmap)
  return vthis
}
// /usr/include/qt/QtGui/qpixmap.h:66
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPixmap(int, int)
type T_ZN7QPixmapC2Eii = fn(cthis voidptr, w int, h int) 

/*

*/
pub fn (dummy QPixmap) new_for_inherit_1(w int, h int) QPixmap {
  //return newQPixmap1(w, h)
  return QPixmap{}
}
pub fn newQPixmap1(w int, h int) QPixmap {
    mut fnobj := T_ZN7QPixmapC2Eii(0)
    fnobj = qtrt.sym_qtfunc6(3559070313, "_ZN7QPixmapC2Eii")
    mut cthis := qtrt.mallocraw(32)
    fnobj(cthis, w, h)
    rv := cthis
    vthis := newQPixmapFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPixmap)
  return vthis
}
// /usr/include/qt/QtGui/qpixmap.h:86
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int width() const
type T_ZNK7QPixmap5widthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPixmap) width() int {
    mut fnobj := T_ZNK7QPixmap5widthEv(0)
    fnobj = qtrt.sym_qtfunc6(494131390, "_ZNK7QPixmap5widthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpixmap.h:87
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int height() const
type T_ZNK7QPixmap6heightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPixmap) height() int {
    mut fnobj := T_ZNK7QPixmap6heightEv(0)
    fnobj = qtrt.sym_qtfunc6(1443543034, "_ZNK7QPixmap6heightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtGui/qpixmap.h:89
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QRect rect() const
type T_ZNK7QPixmap4rectEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QPixmap) rect()  qtcore.QRect/*123*/ {
    mut fnobj := T_ZNK7QPixmap4rectEv(0)
    fnobj = qtrt.sym_qtfunc6(4207271430, "_ZNK7QPixmap4rectEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := qtcore.newQRectFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, qtcore.deleteQRect)
    return rv2
    //return qtcore.QRect{rv}
}
// /usr/include/qt/QtGui/qpixmap.h:94
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void fill(const QColor &)
type T_ZN7QPixmap4fillERK6QColor = fn(cthis voidptr, fillColor voidptr) /*void*/

/*

*/
pub fn (this QPixmap) fill(fillColor  QColor)  {
    mut conv_arg0 := voidptr(0)
    /*if fillColor != voidptr(0) && fillColor.QColor_ptr() != voidptr(0) */ {
        // conv_arg0 = fillColor.QColor_ptr().get_cthis()
      conv_arg0 = fillColor.get_cthis()
    }
    mut fnobj := T_ZN7QPixmap4fillERK6QColor(0)
    fnobj = qtrt.sym_qtfunc6(2168313289, "_ZN7QPixmap4fillERK6QColor")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtGui/qpixmap.h:94
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void fill(const QColor &)

/*

*/
pub fn (this QPixmap) fillp()  {
    // arg: 0, const QColor &=LValueReference, QColor=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN7QPixmap4fillERK6QColor(0)
    fnobj = qtrt.sym_qtfunc6(2168313289, "_ZN7QPixmap4fillERK6QColor")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQPixmap(this &QPixmap) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2275397216, "_ZN7QPixmapD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPixmap) freecpp() { deleteQPixmap(&this) }

fn (this QPixmap) free() {

  /*deleteQPixmap(&this)*/

  cthis := this.get_cthis()
  //println("QPixmap freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10149() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
