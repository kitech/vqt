

module qtgui
// /usr/include/qt/QtGui/qevent.h
// #include <qevent.h>
// #include <QtGui>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
// import github.com/kitech/qt.go/qtcore
import vqt.qtcore
//  ext block end

//  body block begin



/*

*/
pub struct QScrollEvent {
pub:
  qtcore.QEvent
}

pub interface QScrollEventITF {
//    qtcore.QEventITF
    get_cthis() voidptr
    toQScrollEvent() QScrollEvent
}
fn hotfix_QScrollEvent_itf_name_table(this QScrollEventITF) {
  that := QScrollEvent{}
  hotfix_QScrollEvent_itf_name_table(that)
}
pub fn (ptr QScrollEvent) toQScrollEvent() QScrollEvent { return ptr }

pub fn (this QScrollEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQScrollEventFromptr(cthis voidptr) QScrollEvent {
    bcthis0 := qtcore.newQEventFromptr(cthis)
    return QScrollEvent{bcthis0}
}
pub fn (dummy QScrollEvent) newFromptr(cthis voidptr) QScrollEvent {
    return newQScrollEventFromptr(cthis)
}

[no_inline]
pub fn deleteQScrollEvent(this &QScrollEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2663943836, "_ZN12QScrollEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QScrollEvent) freecpp() { deleteQScrollEvent(&this) }

fn (this QScrollEvent) free() {

  /*deleteQScrollEvent(&this)*/

  cthis := this.get_cthis()
  //println("QScrollEvent freeing ${cthis} 0 bytes")

}


/*


*/
//type QScrollEvent.ScrollState = int
pub enum QScrollEventScrollState {
  ScrollStarted = 0
  ScrollUpdated = 1
  ScrollFinished = 2
} // endof enum ScrollState

//  body block end

//  keep block begin


fn init_unused_10137() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
if false {qtcore.keepme()}
}
//  keep block end
