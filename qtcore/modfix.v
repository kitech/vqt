module qtcore

import vqt.qtrt

pub fn keepme(){}

pub fn newQString5(s string) QString {
    s2 := newQString(s)
    // qtrt.set_finalizer(&s2, deleteQString)
    return s2
}

// give qt owner and qt free it
pub fn newQEvent0(type_ int) QEvent {
    mut fnobj := T_ZN6QEventC2ENS_4TypeE(0)
    fnobj = qtrt.sym_qtfunc6(2793188191, "_ZN6QEventC2ENS_4TypeE")
    mut cthis := qtrt.real_malloc(24)
    fnobj(cthis, type_)
    rv := cthis
    vthis := newQEventFromptr(voidptr(rv))
    return vthis
}

//pub fn new_qfile_info_list_fromptr(cthis voidptr) QFileInfoList {
    //return QFileInfoList{cthis}
//}

//pub fn new_qvariant_list_fromptr(cthis voidptr) QVariantList {
    //return QVariantList{cthis}
//}

//pub fn new_qmodel_index_list_fromptr(cthis voidptr) QModelIndexList {
    //return QModelIndexList{cthis}
//}


//pub fn new_qurl_list_fromptr(cthis voidptr) QUrlList {
    //return QUrlList{cthis}
//}

//pub fn new_qobject_list_fromptr(cthis voidptr) QObjectList {
    //return QObjectList{cthis}
//}

//pub fn new_qsize_list_fromptr(cthis voidptr) QSizeList {
    //return QSizeList{cthis}
//}


//pub struct QCborMap {
    //// QObject
    //pub    mut:
    //cthis voidptr
//}

//pub fn (this QCborMap) get_cthis() voidptr { return this.cthis }

//pub fn (this QByteArrayList) get_cthis() voidptr { return this.cthis }

