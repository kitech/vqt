

module qtcore
// /usr/include/qt/QtCore/qthread.h
// #include <qthread.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QThread {
pub:
  QObject
}

pub interface QThreadITF {
//    QObjectITF
    get_cthis() voidptr
    toQThread() QThread
}
fn hotfix_QThread_itf_name_table(this QThreadITF) {
  that := QThread{}
  hotfix_QThread_itf_name_table(that)
}
pub fn (ptr QThread) toQThread() QThread { return ptr }

pub fn (this QThread) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQThreadFromptr(cthis voidptr) QThread {
    bcthis0 := newQObjectFromptr(cthis)
    return QThread{bcthis0}
}
pub fn (dummy QThread) newFromptr(cthis voidptr) QThread {
    return newQThreadFromptr(cthis)
}

[no_inline]
pub fn deleteQThread(this &QThread) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2923436468, "_ZN7QThreadD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QThread) freecpp() { deleteQThread(&this) }

fn (this QThread) free() {

  /*deleteQThread(&this)*/

  cthis := this.get_cthis()
  //println("QThread freeing ${cthis} 0 bytes")

}


/*


*/
//type QThread.Priority = int
pub enum QThreadPriority {
  IdlePriority = 0
  LowestPriority = 1
  LowPriority = 2
  NormalPriority = 3
  HighPriority = 4
  HighestPriority = 5
  TimeCriticalPriority = 6
  InheritPriority = 7
} // endof enum Priority

//  body block end

//  keep block begin


fn init_unused_10063() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
