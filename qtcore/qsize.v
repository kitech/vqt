

module qtcore
// /usr/include/qt/QtCore/qsize.h
// #include <qsize.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QSize {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QSizeITF {
    get_cthis() voidptr
    toQSize() QSize
}
fn hotfix_QSize_itf_name_table(this QSizeITF) {
  that := QSize{}
  hotfix_QSize_itf_name_table(that)
}
pub fn (ptr QSize) toQSize() QSize { return ptr }

pub fn (this QSize) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQSizeFromptr(cthis voidptr) QSize {
    return QSize{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QSize) newFromptr(cthis voidptr) QSize {
    return newQSizeFromptr(cthis)
}
// /usr/include/qt/QtCore/qsize.h:57
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QSize(int, int)
type T_ZN5QSizeC2Eii = fn(cthis voidptr, w int, h int) 

/*

*/
pub fn (dummy QSize) new_for_inherit_(w int, h int) QSize {
  //return newQSize(w, h)
  return QSize{}
}
pub fn newQSize(w int, h int) QSize {
    mut fnobj := T_ZN5QSizeC2Eii(0)
    fnobj = qtrt.sym_qtfunc6(1650702872, "_ZN5QSizeC2Eii")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, w, h)
    rv := cthis
    vthis := newQSizeFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQSize)
  return vthis
}
// /usr/include/qt/QtCore/qsize.h:63
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int width() const
type T_ZNK5QSize5widthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QSize) width() int {
    mut fnobj := T_ZNK5QSize5widthEv(0)
    fnobj = qtrt.sym_qtfunc6(978481712, "_ZNK5QSize5widthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qsize.h:64
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int height() const
type T_ZNK5QSize6heightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QSize) height() int {
    mut fnobj := T_ZNK5QSize6heightEv(0)
    fnobj = qtrt.sym_qtfunc6(1546467447, "_ZNK5QSize6heightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qsize.h:83
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int & rwidth()
type T_ZN5QSize6rwidthEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QSize) rwidth() int {
    mut fnobj := T_ZN5QSize6rwidthEv(0)
    fnobj = qtrt.sym_qtfunc6(1400378653, "_ZN5QSize6rwidthEv")
    rv :=
    fnobj(this.get_cthis())
    // return qtrt.cpretval2v("int", rv) // .(int) // 3331
    return int(rv) // 3331
}
// /usr/include/qt/QtCore/qsize.h:84
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int & rheight()
type T_ZN5QSize7rheightEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QSize) rheight() int {
    mut fnobj := T_ZN5QSize7rheightEv(0)
    fnobj = qtrt.sym_qtfunc6(3486783902, "_ZN5QSize7rheightEv")
    rv :=
    fnobj(this.get_cthis())
    // return qtrt.cpretval2v("int", rv) // .(int) // 3331
    return int(rv) // 3331
}

[no_inline]
pub fn deleteQSize(this &QSize) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2702344799, "_ZN5QSizeD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QSize) freecpp() { deleteQSize(&this) }

fn (this QSize) free() {

  /*deleteQSize(&this)*/

  cthis := this.get_cthis()
  //println("QSize freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10055() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
