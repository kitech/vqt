

module qtcore
// /usr/include/qt/QtCore/qobjectdefs.h
// #include <qobjectdefs.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QGenericReturnArgument {
pub:
  QGenericArgument
}

pub interface QGenericReturnArgumentITF {
//    QGenericArgumentITF
    get_cthis() voidptr
    toQGenericReturnArgument() QGenericReturnArgument
}
fn hotfix_QGenericReturnArgument_itf_name_table(this QGenericReturnArgumentITF) {
  that := QGenericReturnArgument{}
  hotfix_QGenericReturnArgument_itf_name_table(that)
}
pub fn (ptr QGenericReturnArgument) toQGenericReturnArgument() QGenericReturnArgument { return ptr }

pub fn (this QGenericReturnArgument) get_cthis() voidptr {
    return this.QGenericArgument.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQGenericReturnArgumentFromptr(cthis voidptr) QGenericReturnArgument {
    bcthis0 := newQGenericArgumentFromptr(cthis)
    return QGenericReturnArgument{bcthis0}
}
pub fn (dummy QGenericReturnArgument) newFromptr(cthis voidptr) QGenericReturnArgument {
    return newQGenericReturnArgumentFromptr(cthis)
}

[no_inline]
pub fn deleteQGenericReturnArgument(this &QGenericReturnArgument) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2739045802, "_ZN22QGenericReturnArgumentD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QGenericReturnArgument) freecpp() { deleteQGenericReturnArgument(&this) }

fn (this QGenericReturnArgument) free() {

  /*deleteQGenericReturnArgument(&this)*/

  cthis := this.get_cthis()
  //println("QGenericReturnArgument freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10003() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
