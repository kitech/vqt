

module qtcore
// /usr/include/qt/QtCore/qcoreevent.h
// #include <qcoreevent.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QDeferredDeleteEvent {
pub:
  QEvent
}

pub interface QDeferredDeleteEventITF {
//    QEventITF
    get_cthis() voidptr
    toQDeferredDeleteEvent() QDeferredDeleteEvent
}
fn hotfix_QDeferredDeleteEvent_itf_name_table(this QDeferredDeleteEventITF) {
  that := QDeferredDeleteEvent{}
  hotfix_QDeferredDeleteEvent_itf_name_table(that)
}
pub fn (ptr QDeferredDeleteEvent) toQDeferredDeleteEvent() QDeferredDeleteEvent { return ptr }

pub fn (this QDeferredDeleteEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQDeferredDeleteEventFromptr(cthis voidptr) QDeferredDeleteEvent {
    bcthis0 := newQEventFromptr(cthis)
    return QDeferredDeleteEvent{bcthis0}
}
pub fn (dummy QDeferredDeleteEvent) newFromptr(cthis voidptr) QDeferredDeleteEvent {
    return newQDeferredDeleteEventFromptr(cthis)
}

[no_inline]
pub fn deleteQDeferredDeleteEvent(this &QDeferredDeleteEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1062849289, "_ZN20QDeferredDeleteEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDeferredDeleteEvent) freecpp() { deleteQDeferredDeleteEvent(&this) }

fn (this QDeferredDeleteEvent) free() {

  /*deleteQDeferredDeleteEvent(&this)*/

  cthis := this.get_cthis()
  //println("QDeferredDeleteEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10035() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
