

module qtcore
// /usr/include/qt/QtCore/qurl.h
// #include <qurl.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QUrl {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QUrlITF {
    get_cthis() voidptr
    toQUrl() QUrl
}
fn hotfix_QUrl_itf_name_table(this QUrlITF) {
  that := QUrl{}
  hotfix_QUrl_itf_name_table(that)
}
pub fn (ptr QUrl) toQUrl() QUrl { return ptr }

pub fn (this QUrl) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQUrlFromptr(cthis voidptr) QUrl {
    return QUrl{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QUrl) newFromptr(cthis voidptr) QUrl {
    return newQUrlFromptr(cthis)
}
// /usr/include/qt/QtCore/qurl.h:176
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QUrl()
type T_ZN4QUrlC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QUrl) new_for_inherit_() QUrl {
  //return newQUrl()
  return QUrl{}
}
pub fn newQUrl() QUrl {
    mut fnobj := T_ZN4QUrlC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(887786197, "_ZN4QUrlC2Ev")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis)
    rv := cthis
    vthis := newQUrlFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQUrl)
  return vthis
}
// /usr/include/qt/QtCore/qurl.h:182
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QUrl(const QString &, QUrl::ParsingMode)
type T_ZN4QUrlC2ERK7QStringNS_11ParsingModeE = fn(cthis voidptr, url voidptr, mode int) 

/*

*/
pub fn (dummy QUrl) new_for_inherit_1(url string, mode int) QUrl {
  //return newQUrl1(url, mode)
  return QUrl{}
}
pub fn newQUrl1(url string, mode int) QUrl {
    mut tmp_arg0 := newQString5(url)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN4QUrlC2ERK7QStringNS_11ParsingModeE(0)
    fnobj = qtrt.sym_qtfunc6(2077668047, "_ZN4QUrlC2ERK7QStringNS_11ParsingModeE")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0, mode)
    rv := cthis
    vthis := newQUrlFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQUrl)
  return vthis
}
// /usr/include/qt/QtCore/qurl.h:182
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QUrl(const QString &, QUrl::ParsingMode)

/*

*/
pub fn (dummy QUrl) new_for_inherit_1p(url string) QUrl {
  //return newQUrl1p(url)
  return QUrl{}
}
pub fn newQUrl1p(url string) QUrl {
    mut tmp_arg0 := newQString5(url)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QUrl::ParsingMode=Enum, QUrl::ParsingMode=Enum, , Invalid
    mode := 0
    mut fnobj := T_ZN4QUrlC2ERK7QStringNS_11ParsingModeE(0)
    fnobj = qtrt.sym_qtfunc6(2077668047, "_ZN4QUrlC2ERK7QStringNS_11ParsingModeE")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0, mode)
    rv := cthis
    vthis := newQUrlFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQUrl)
    return vthis
}
// /usr/include/qt/QtCore/qurl.h:193
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setUrl(const QString &, QUrl::ParsingMode)
type T_ZN4QUrl6setUrlERK7QStringNS_11ParsingModeE = fn(cthis voidptr, url voidptr, mode int) /*void*/

/*

*/
pub fn (this QUrl) setUrl(url string, mode int)  {
    mut tmp_arg0 := newQString5(url)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN4QUrl6setUrlERK7QStringNS_11ParsingModeE(0)
    fnobj = qtrt.sym_qtfunc6(241840493, "_ZN4QUrl6setUrlERK7QStringNS_11ParsingModeE")
    fnobj(this.get_cthis(), conv_arg0, mode)
}
// /usr/include/qt/QtCore/qurl.h:193
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setUrl(const QString &, QUrl::ParsingMode)

/*

*/
pub fn (this QUrl) setUrlp(url string)  {
    mut tmp_arg0 := newQString5(url)
    mut conv_arg0 := tmp_arg0.get_cthis()
    // arg: 1, QUrl::ParsingMode=Enum, QUrl::ParsingMode=Enum, , Invalid
    mode := 0
    mut fnobj := T_ZN4QUrl6setUrlERK7QStringNS_11ParsingModeE(0)
    fnobj = qtrt.sym_qtfunc6(241840493, "_ZN4QUrl6setUrlERK7QStringNS_11ParsingModeE")
    fnobj(this.get_cthis(), conv_arg0, mode)
}

[no_inline]
pub fn deleteQUrl(this &QUrl) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2839392876, "_ZN4QUrlD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QUrl) freecpp() { deleteQUrl(&this) }

fn (this QUrl) free() {

  /*deleteQUrl(&this)*/

  cthis := this.get_cthis()
  //println("QUrl freeing ${cthis} 0 bytes")

}


/*


*/
//type QUrl.ParsingMode = int
pub enum QUrlParsingMode {
  TolerantMode = 0
  StrictMode = 1
  DecodedMode = 2
} // endof enum ParsingMode


/*


*/
//type QUrl.UrlFormattingOption = int
pub enum QUrlUrlFormattingOption {
  None = 0
  RemoveScheme = 1
  RemovePassword = 2
  RemoveUserInfo = 6
  RemovePort = 8
  RemoveAuthority = 30
  RemovePath = 32
  RemoveQuery = 64
  RemoveFragment = 128
  PreferLocalFile = 512
  StripTrailingSlash = 1024
  RemoveFilename = 2048
  NormalizePathSegments = 4096
} // endof enum UrlFormattingOption


/*


*/
//type QUrl.ComponentFormattingOption = int
pub enum QUrlComponentFormattingOption {
  PrettyDecoded = 0
  EncodeSpaces = 1048576
  EncodeUnicode = 2097152
  EncodeDelimiters = 12582912
  EncodeReserved = 16777216
  DecodeReserved = 33554432
  FullyEncoded = 32505856
  FullyDecoded = 133169152
} // endof enum ComponentFormattingOption


/*


*/
//type QUrl.UserInputResolutionOption = int
pub enum QUrlUserInputResolutionOption {
  DefaultResolution = 0
  AssumeLocalFile = 1
} // endof enum UserInputResolutionOption

//  body block end

//  keep block begin


fn init_unused_10025() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
