

module qtcore
// /usr/include/qt/QtCore/qobject.h
// #include <qobject.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 4
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QObject {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QObjectITF {
    get_cthis() voidptr
    toQObject() QObject
}
fn hotfix_QObject_itf_name_table(this QObjectITF) {
  that := QObject{}
  hotfix_QObject_itf_name_table(that)
}
pub fn (ptr QObject) toQObject() QObject { return ptr }

pub fn (this QObject) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQObjectFromptr(cthis voidptr) QObject {
    return QObject{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QObject) newFromptr(cthis voidptr) QObject {
    return newQObjectFromptr(cthis)
}
// /usr/include/qt/QtCore/qobject.h:135
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QObject(QObject *)
type T_ZN7QObjectC2EPS_ = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QObject) new_for_inherit_(parent  QObject/*777 QObject **/) QObject {
  //return newQObject(parent)
  return QObject{}
}
pub fn newQObject(parent  QObject/*777 QObject **/) QObject {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QObject_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN7QObjectC2EPS_(0)
    fnobj = qtrt.sym_qtfunc6(1614511106, "_ZN7QObjectC2EPS_")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQObjectFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQObject)
  return vthis
}
// /usr/include/qt/QtCore/qobject.h:135
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QObject(QObject *)

/*

*/
pub fn (dummy QObject) new_for_inherit_p() QObject {
  //return newQObjectp()
  return QObject{}
}
pub fn newQObjectp() QObject {
    // arg: 0, QObject *=Pointer, QObject=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN7QObjectC2EPS_(0)
    fnobj = qtrt.sym_qtfunc6(1614511106, "_ZN7QObjectC2EPS_")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQObjectFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQObject)
    return vthis
}
// /usr/include/qt/QtCore/qobject.h:150
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QString objectName() const
type T_ZNK7QObject10objectNameEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QObject) objectName() string {
    mut fnobj := T_ZNK7QObject10objectNameEv(0)
    fnobj = qtrt.sym_qtfunc6(799704896, "_ZNK7QObject10objectNameEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    /*==*/deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtCore/qobject.h:151
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setObjectName(const QString &)
type T_ZN7QObject13setObjectNameERK7QString = fn(cthis voidptr, name voidptr) /*void*/

/*

*/
pub fn (this QObject) setObjectName(name string)  {
    mut tmp_arg0 := newQString5(name)
    mut conv_arg0 := tmp_arg0.get_cthis()
    mut fnobj := T_ZN7QObject13setObjectNameERK7QString(0)
    fnobj = qtrt.sym_qtfunc6(2393916980, "_ZN7QObject13setObjectNameERK7QString")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtCore/qobject.h:153
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isWidgetType() const
type T_ZNK7QObject12isWidgetTypeEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QObject) isWidgetType() bool {
    mut fnobj := T_ZNK7QObject12isWidgetTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(4140218935, "_ZNK7QObject12isWidgetTypeEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qobject.h:154
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isWindowType() const
type T_ZNK7QObject12isWindowTypeEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QObject) isWindowType() bool {
    mut fnobj := T_ZNK7QObject12isWindowTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(2017529966, "_ZNK7QObject12isWindowTypeEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qobject.h:156
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool signalsBlocked() const
type T_ZNK7QObject14signalsBlockedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QObject) signalsBlocked() bool {
    mut fnobj := T_ZNK7QObject14signalsBlockedEv(0)
    fnobj = qtrt.sym_qtfunc6(2511842610, "_ZNK7QObject14signalsBlockedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qobject.h:157
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool blockSignals(bool)
type T_ZN7QObject12blockSignalsEb = fn(cthis voidptr, b bool) bool

/*

*/
pub fn (this QObject) blockSignals(b bool) bool {
    mut fnobj := T_ZN7QObject12blockSignalsEb(0)
    fnobj = qtrt.sym_qtfunc6(2150786866, "_ZN7QObject12blockSignalsEb")
    rv :=
    fnobj(this.get_cthis(), b)
    return rv//!=0
}
// /usr/include/qt/QtCore/qobject.h:159
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QThread * thread() const
type T_ZNK7QObject6threadEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QObject) thread()  QThread/*777 QThread **/ {
    mut fnobj := T_ZNK7QObject6threadEv(0)
    fnobj = qtrt.sym_qtfunc6(2989794423, "_ZNK7QObject6threadEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQThreadFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtCore/qobject.h:160
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void moveToThread(QThread *)
type T_ZN7QObject12moveToThreadEP7QThread = fn(cthis voidptr, thread voidptr) /*void*/

/*

*/
pub fn (this QObject) moveToThread(thread  QThread/*777 QThread **/)  {
    mut conv_arg0 := voidptr(0)
    /*if thread != voidptr(0) && thread.QThread_ptr() != voidptr(0) */ {
        // conv_arg0 = thread.QThread_ptr().get_cthis()
      conv_arg0 = thread.get_cthis()
    }
    mut fnobj := T_ZN7QObject12moveToThreadEP7QThread(0)
    fnobj = qtrt.sym_qtfunc6(2881015986, "_ZN7QObject12moveToThreadEP7QThread")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtCore/qobject.h:218
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void setParent(QObject *)
type T_ZN7QObject9setParentEPS_ = fn(cthis voidptr, parent voidptr) /*void*/

/*

*/
pub fn (this QObject) setParent(parent  QObject/*777 QObject **/)  {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QObject_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN7QObject9setParentEPS_(0)
    fnobj = qtrt.sym_qtfunc6(600858033, "_ZN7QObject9setParentEPS_")
    fnobj(this.get_cthis(), conv_arg0)
}

[no_inline]
pub fn deleteQObject(this &QObject) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1716974882, "_ZN7QObjectD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QObject) freecpp() { deleteQObject(&this) }

fn (this QObject) free() {

  /*deleteQObject(&this)*/

  cthis := this.get_cthis()
  //println("QObject freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10011() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
