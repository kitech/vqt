

module qtcore
// /usr/include/qt/QtCore/qcoreapplication.h
// #include <qcoreapplication.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QCoreApplication {
pub:
  QObject
}

pub interface QCoreApplicationITF {
//    QObjectITF
    get_cthis() voidptr
    toQCoreApplication() QCoreApplication
}
fn hotfix_QCoreApplication_itf_name_table(this QCoreApplicationITF) {
  that := QCoreApplication{}
  hotfix_QCoreApplication_itf_name_table(that)
}
pub fn (ptr QCoreApplication) toQCoreApplication() QCoreApplication { return ptr }

pub fn (this QCoreApplication) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQCoreApplicationFromptr(cthis voidptr) QCoreApplication {
    bcthis0 := newQObjectFromptr(cthis)
    return QCoreApplication{bcthis0}
}
pub fn (dummy QCoreApplication) newFromptr(cthis voidptr) QCoreApplication {
    return newQCoreApplicationFromptr(cthis)
}
// /usr/include/qt/QtCore/qcoreapplication.h:125
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Ignore Visibility=Default Availability=Available
// [-2] void postEvent(QObject *, QEvent *, int)
type T_ZN16QCoreApplication9postEventEP7QObjectP6QEventi = fn(receiver voidptr, event voidptr, priority int) /*void*/

/*

*/
pub fn (this QCoreApplication) postEvent(receiver  QObject/*777 QObject **/, event  QEvent/*777 QEvent **/, priority int)  {
    mut conv_arg0 := voidptr(0)
    /*if receiver != voidptr(0) && receiver.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = receiver.QObject_ptr().get_cthis()
      conv_arg0 = receiver.get_cthis()
    }
    mut conv_arg1 := voidptr(0)
    /*if event != voidptr(0) && event.QEvent_ptr() != voidptr(0) */ {
        // conv_arg1 = event.QEvent_ptr().get_cthis()
      conv_arg1 = event.get_cthis()
    }
    mut fnobj := T_ZN16QCoreApplication9postEventEP7QObjectP6QEventi(0)
    fnobj = qtrt.sym_qtfunc6(3008777482, "_ZN16QCoreApplication9postEventEP7QObjectP6QEventi")
    fnobj(conv_arg0, conv_arg1, priority)
}
// /usr/include/qt/QtCore/qcoreapplication.h:125
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Ignore Visibility=Default Availability=Available
// [-2] void postEvent(QObject *, QEvent *, int)

/*

*/
pub fn (this QCoreApplication) postEventp(receiver  QObject/*777 QObject **/, event  QEvent/*777 QEvent **/)  {
    mut conv_arg0 := voidptr(0)
    /*if receiver != voidptr(0) && receiver.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = receiver.QObject_ptr().get_cthis()
      conv_arg0 = receiver.get_cthis()
    }
    mut conv_arg1 := voidptr(0)
    /*if event != voidptr(0) && event.QEvent_ptr() != voidptr(0) */ {
        // conv_arg1 = event.QEvent_ptr().get_cthis()
      conv_arg1 = event.get_cthis()
    }
    // arg: 2, int=Int, =Invalid, , Invalid
    priority := 0/*Qt::NormalEventPriority*/
    mut fnobj := T_ZN16QCoreApplication9postEventEP7QObjectP6QEventi(0)
    fnobj = qtrt.sym_qtfunc6(3008777482, "_ZN16QCoreApplication9postEventEP7QObjectP6QEventi")
    fnobj(conv_arg0, conv_arg1, priority)
}
// /usr/include/qt/QtCore/qcoreapplication.h:156
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Indirect Visibility=Default Availability=Available
// [8] QString translate(const char *, const char *, const char *, int)
type T_ZN16QCoreApplication9translateEPKcS1_S1_i = fn(sretobj voidptr, context voidptr, key voidptr, disambiguation voidptr, n int) voidptr

/*

*/
pub fn (this QCoreApplication) translate(context string, key string, disambiguation string, n int) string {
    mut conv_arg0 := qtrt.cstringr(&context)
    mut conv_arg1 := qtrt.cstringr(&key)
    mut conv_arg2 := qtrt.cstringr(&disambiguation)
    mut fnobj := T_ZN16QCoreApplication9translateEPKcS1_S1_i(0)
    fnobj = qtrt.sym_qtfunc6(2590451099, "_ZN16QCoreApplication9translateEPKcS1_S1_i")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, conv_arg0, conv_arg1, conv_arg2, n)
    rv := sretobj
    rv2 := /*==*/newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    /*==*/deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtCore/qcoreapplication.h:156
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Indirect Visibility=Default Availability=Available
// [8] QString translate(const char *, const char *, const char *, int)

/*

*/
pub fn (this QCoreApplication) translatep(context string, key string) string {
    mut conv_arg0 := qtrt.cstringr(&context)
    mut conv_arg1 := qtrt.cstringr(&key)
    // arg: 2, const char *=Pointer, =Invalid, , Invalid
    mut conv_arg2 := voidptr(0)
    // arg: 3, int=Int, =Invalid, , Invalid
    n := int(-1)
    mut fnobj := T_ZN16QCoreApplication9translateEPKcS1_S1_i(0)
    fnobj = qtrt.sym_qtfunc6(2590451099, "_ZN16QCoreApplication9translateEPKcS1_S1_i")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, conv_arg0, conv_arg1, conv_arg2, n)
    rv := sretobj
    rv2 := /*==*/newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    /*==*/deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtCore/qcoreapplication.h:156
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Indirect Visibility=Default Availability=Available
// [8] QString translate(const char *, const char *, const char *, int)

/*

*/
pub fn (this QCoreApplication) translatep1(context string, key string, disambiguation string) string {
    mut conv_arg0 := qtrt.cstringr(&context)
    mut conv_arg1 := qtrt.cstringr(&key)
    mut conv_arg2 := qtrt.cstringr(&disambiguation)
    // arg: 3, int=Int, =Invalid, , Invalid
    n := int(-1)
    mut fnobj := T_ZN16QCoreApplication9translateEPKcS1_S1_i(0)
    fnobj = qtrt.sym_qtfunc6(2590451099, "_ZN16QCoreApplication9translateEPKcS1_S1_i")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, conv_arg0, conv_arg1, conv_arg2, n)
    rv := sretobj
    rv2 := /*==*/newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    /*==*/deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtCore/qcoreapplication.h:179
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Ignore Visibility=Default Availability=Available
// [-2] void quit()
type T_ZN16QCoreApplication4quitEv = fn() /*void*/

/*

*/
pub fn (this QCoreApplication) quit()  {
    mut fnobj := T_ZN16QCoreApplication4quitEv(0)
    fnobj = qtrt.sym_qtfunc6(3212895622, "_ZN16QCoreApplication4quitEv")
    fnobj()
}

[no_inline]
pub fn deleteQCoreApplication(this &QCoreApplication) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1209240029, "_ZN16QCoreApplicationD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QCoreApplication) freecpp() { deleteQCoreApplication(&this) }

fn (this QCoreApplication) free() {

  /*deleteQCoreApplication(&this)*/

  cthis := this.get_cthis()
  //println("QCoreApplication freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10037() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
