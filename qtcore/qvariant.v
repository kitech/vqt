

module qtcore
// /usr/include/qt/QtCore/qvariant.h
// #include <qvariant.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 10
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QVariant {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QVariantITF {
    get_cthis() voidptr
    toQVariant() QVariant
}
fn hotfix_QVariant_itf_name_table(this QVariantITF) {
  that := QVariant{}
  hotfix_QVariant_itf_name_table(that)
}
pub fn (ptr QVariant) toQVariant() QVariant { return ptr }

pub fn (this QVariant) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQVariantFromptr(cthis voidptr) QVariant {
    return QVariant{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QVariant) newFromptr(cthis voidptr) QVariant {
    return newQVariantFromptr(cthis)
}
// /usr/include/qt/QtCore/qvariant.h:208
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QVariant()
type T_ZN8QVariantC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_() QVariant {
  //return newQVariant()
  return QVariant{}
}
pub fn newQVariant() QVariant {
    mut fnobj := T_ZN8QVariantC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(93067653, "_ZN8QVariantC2Ev")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:210
// index:1 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(QVariant::Type)
type T_ZN8QVariantC2ENS_4TypeE = fn(cthis voidptr, type_ int) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_1(type_ int) QVariant {
  //return newQVariant1(type_)
  return QVariant{}
}
pub fn newQVariant1(type_ int) QVariant {
    mut fnobj := T_ZN8QVariantC2ENS_4TypeE(0)
    fnobj = qtrt.sym_qtfunc6(3094501539, "_ZN8QVariantC2ENS_4TypeE")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, type_)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:211
// index:2 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(int, const void *)
type T_ZN8QVariantC2EiPKv = fn(cthis voidptr, typeId int, copy voidptr) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_2(typeId int, copy voidptr /*666*/) QVariant {
  //return newQVariant2(typeId, copy)
  return QVariant{}
}
pub fn newQVariant2(typeId int, copy voidptr /*666*/) QVariant {
    mut fnobj := T_ZN8QVariantC2EiPKv(0)
    fnobj = qtrt.sym_qtfunc6(2845593041, "_ZN8QVariantC2EiPKv")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, typeId, copy)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:212
// index:3 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(int, const void *, uint)
type T_ZN8QVariantC2EiPKvj = fn(cthis voidptr, typeId int, copy voidptr, flags int) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_3(typeId int, copy voidptr /*666*/, flags int) QVariant {
  //return newQVariant3(typeId, copy, flags)
  return QVariant{}
}
pub fn newQVariant3(typeId int, copy voidptr /*666*/, flags int) QVariant {
    mut fnobj := T_ZN8QVariantC2EiPKvj(0)
    fnobj = qtrt.sym_qtfunc6(2383944132, "_ZN8QVariantC2EiPKvj")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, typeId, copy, flags)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:219
// index:4 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(int)
type T_ZN8QVariantC2Ei = fn(cthis voidptr, i int) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_4(i int) QVariant {
  //return newQVariant4(i)
  return QVariant{}
}
pub fn newQVariant4(i int) QVariant {
    mut fnobj := T_ZN8QVariantC2Ei(0)
    fnobj = qtrt.sym_qtfunc6(2290357360, "_ZN8QVariantC2Ei")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, i)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:220
// index:5 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(uint)
type T_ZN8QVariantC2Ej = fn(cthis voidptr, ui int) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_5(ui int) QVariant {
  //return newQVariant5(ui)
  return QVariant{}
}
pub fn newQVariant5(ui int) QVariant {
    mut fnobj := T_ZN8QVariantC2Ej(0)
    fnobj = qtrt.sym_qtfunc6(294471114, "_ZN8QVariantC2Ej")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, ui)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:221
// index:6 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(qlonglong)
type T_ZN8QVariantC2Ex = fn(cthis voidptr, ll i64) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_6(ll i64) QVariant {
  //return newQVariant6(ll)
  return QVariant{}
}
pub fn newQVariant6(ll i64) QVariant {
    mut fnobj := T_ZN8QVariantC2Ex(0)
    fnobj = qtrt.sym_qtfunc6(3795072130, "_ZN8QVariantC2Ex")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, ll)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:222
// index:7 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(qulonglong)
type T_ZN8QVariantC2Ey = fn(cthis voidptr, ull u64) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_7(ull u64) QVariant {
  //return newQVariant7(ull)
  return QVariant{}
}
pub fn newQVariant7(ull u64) QVariant {
    mut fnobj := T_ZN8QVariantC2Ey(0)
    fnobj = qtrt.sym_qtfunc6(2503148564, "_ZN8QVariantC2Ey")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, ull)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:223
// index:8 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(bool)
type T_ZN8QVariantC2Eb = fn(cthis voidptr, b bool) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_8(b bool) QVariant {
  //return newQVariant8(b)
  return QVariant{}
}
pub fn newQVariant8(b bool) QVariant {
    mut fnobj := T_ZN8QVariantC2Eb(0)
    fnobj = qtrt.sym_qtfunc6(525782520, "_ZN8QVariantC2Eb")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, b)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:224
// index:9 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(double)
type T_ZN8QVariantC2Ed = fn(cthis voidptr, d f64) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_9(d f64) QVariant {
  //return newQVariant9(d)
  return QVariant{}
}
pub fn newQVariant9(d f64) QVariant {
    mut fnobj := T_ZN8QVariantC2Ed(0)
    fnobj = qtrt.sym_qtfunc6(4130695373, "_ZN8QVariantC2Ed")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, d)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:225
// index:10 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(float)
type T_ZN8QVariantC2Ef = fn(cthis voidptr, f f32) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_10(f f32) QVariant {
  //return newQVariant10(f)
  return QVariant{}
}
pub fn newQVariant10(f f32) QVariant {
    mut fnobj := T_ZN8QVariantC2Ef(0)
    fnobj = qtrt.sym_qtfunc6(406522337, "_ZN8QVariantC2Ef")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, f)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:227
// index:11 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QVariant(const char *)
type T_ZN8QVariantC2EPKc = fn(cthis voidptr, str voidptr) 

/*

*/
pub fn (dummy QVariant) new_for_inherit_11(str string) QVariant {
  //return newQVariant11(str)
  return QVariant{}
}
pub fn newQVariant11(str string) QVariant {
    mut conv_arg0 := qtrt.cstringr(&str)
    mut fnobj := T_ZN8QVariantC2EPKc(0)
    fnobj = qtrt.sym_qtfunc6(2561691587, "_ZN8QVariantC2EPKc")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQVariantFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQVariant)
  return vthis
}
// /usr/include/qt/QtCore/qvariant.h:283
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QVariant::Type type() const
type T_ZNK8QVariant4typeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QVariant) type_() int {
    mut fnobj := T_ZNK8QVariant4typeEv(0)
    fnobj = qtrt.sym_qtfunc6(1071828666, "_ZNK8QVariant4typeEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtCore/qvariant.h:284
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int userType() const
type T_ZNK8QVariant8userTypeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QVariant) userType() int {
    mut fnobj := T_ZNK8QVariant8userTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(2949716066, "_ZNK8QVariant8userTypeEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qvariant.h:285
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] const char * typeName() const
type T_ZNK8QVariant8typeNameEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QVariant) typeName() string {
    mut fnobj := T_ZNK8QVariant8typeNameEv(0)
    fnobj = qtrt.sym_qtfunc6(330372657, "_ZNK8QVariant8typeNameEv")
    rv :=
    fnobj(this.get_cthis())
    return qtrt.vstringp(rv)
}
// /usr/include/qt/QtCore/qvariant.h:290
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isValid() const
type T_ZNK8QVariant7isValidEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QVariant) isValid() bool {
    mut fnobj := T_ZNK8QVariant7isValidEv(0)
    fnobj = qtrt.sym_qtfunc6(2204930347, "_ZNK8QVariant7isValidEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qvariant.h:291
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool isNull() const
type T_ZNK8QVariant6isNullEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QVariant) isNull() bool {
    mut fnobj := T_ZNK8QVariant6isNullEv(0)
    fnobj = qtrt.sym_qtfunc6(3898904494, "_ZNK8QVariant6isNullEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qvariant.h:293
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void clear()
type T_ZN8QVariant5clearEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QVariant) clear()  {
    mut fnobj := T_ZN8QVariant5clearEv(0)
    fnobj = qtrt.sym_qtfunc6(126984843, "_ZN8QVariant5clearEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtCore/qvariant.h:295
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void detach()
type T_ZN8QVariant6detachEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QVariant) detach()  {
    mut fnobj := T_ZN8QVariant6detachEv(0)
    fnobj = qtrt.sym_qtfunc6(1393086918, "_ZN8QVariant6detachEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtCore/qvariant.h:296
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isDetached() const
type T_ZNK8QVariant10isDetachedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QVariant) isDetached() bool {
    mut fnobj := T_ZNK8QVariant10isDetachedEv(0)
    fnobj = qtrt.sym_qtfunc6(3109827116, "_ZNK8QVariant10isDetachedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qvariant.h:298
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int toInt(bool *) const
type T_ZNK8QVariant5toIntEPb = fn(cthis voidptr, ok voidptr) int

/*

*/
pub fn (this QVariant) toInt(ok &bool) int {
    mut fnobj := T_ZNK8QVariant5toIntEPb(0)
    fnobj = qtrt.sym_qtfunc6(323143542, "_ZNK8QVariant5toIntEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qvariant.h:298
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int toInt(bool *) const

/*

*/
pub fn (this QVariant) toIntp() int {
    // arg: 0, bool *=Pointer, =Invalid, , Invalid
    ok := voidptr(0)
    mut fnobj := T_ZNK8QVariant5toIntEPb(0)
    fnobj = qtrt.sym_qtfunc6(323143542, "_ZNK8QVariant5toIntEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qvariant.h:299
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] uint toUInt(bool *) const
type T_ZNK8QVariant6toUIntEPb = fn(cthis voidptr, ok voidptr) int

/*

*/
pub fn (this QVariant) toUInt(ok &bool) int {
    mut fnobj := T_ZNK8QVariant6toUIntEPb(0)
    fnobj = qtrt.sym_qtfunc6(208741389, "_ZNK8QVariant6toUIntEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    return int(rv) // 222
}
// /usr/include/qt/QtCore/qvariant.h:299
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] uint toUInt(bool *) const

/*

*/
pub fn (this QVariant) toUIntp() int {
    // arg: 0, bool *=Pointer, =Invalid, , Invalid
    ok := voidptr(0)
    mut fnobj := T_ZNK8QVariant6toUIntEPb(0)
    fnobj = qtrt.sym_qtfunc6(208741389, "_ZNK8QVariant6toUIntEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    return int(rv) // 222
}
// /usr/include/qt/QtCore/qvariant.h:300
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qlonglong toLongLong(bool *) const
type T_ZNK8QVariant10toLongLongEPb = fn(cthis voidptr, ok voidptr) i64

/*

*/
pub fn (this QVariant) toLongLong(ok &bool) i64 {
    mut fnobj := T_ZNK8QVariant10toLongLongEPb(0)
    fnobj = qtrt.sym_qtfunc6(1775070467, "_ZNK8QVariant10toLongLongEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    return i64(rv) // 222
}
// /usr/include/qt/QtCore/qvariant.h:300
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qlonglong toLongLong(bool *) const

/*

*/
pub fn (this QVariant) toLongLongp() i64 {
    // arg: 0, bool *=Pointer, =Invalid, , Invalid
    ok := voidptr(0)
    mut fnobj := T_ZNK8QVariant10toLongLongEPb(0)
    fnobj = qtrt.sym_qtfunc6(1775070467, "_ZNK8QVariant10toLongLongEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    return i64(rv) // 222
}
// /usr/include/qt/QtCore/qvariant.h:301
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qulonglong toULongLong(bool *) const
type T_ZNK8QVariant11toULongLongEPb = fn(cthis voidptr, ok voidptr) u64

/*

*/
pub fn (this QVariant) toULongLong(ok &bool) u64 {
    mut fnobj := T_ZNK8QVariant11toULongLongEPb(0)
    fnobj = qtrt.sym_qtfunc6(4247319954, "_ZNK8QVariant11toULongLongEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    return u64(rv) // 222
}
// /usr/include/qt/QtCore/qvariant.h:301
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qulonglong toULongLong(bool *) const

/*

*/
pub fn (this QVariant) toULongLongp() u64 {
    // arg: 0, bool *=Pointer, =Invalid, , Invalid
    ok := voidptr(0)
    mut fnobj := T_ZNK8QVariant11toULongLongEPb(0)
    fnobj = qtrt.sym_qtfunc6(4247319954, "_ZNK8QVariant11toULongLongEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    return u64(rv) // 222
}
// /usr/include/qt/QtCore/qvariant.h:302
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool toBool() const
type T_ZNK8QVariant6toBoolEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QVariant) toBool() bool {
    mut fnobj := T_ZNK8QVariant6toBoolEv(0)
    fnobj = qtrt.sym_qtfunc6(3766730072, "_ZNK8QVariant6toBoolEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qvariant.h:303
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] double toDouble(bool *) const
type T_ZNK8QVariant8toDoubleEPb = fn(cthis voidptr, ok voidptr) f64

/*

*/
pub fn (this QVariant) toDouble(ok &bool) f64 {
    mut fnobj := T_ZNK8QVariant8toDoubleEPb(0)
    fnobj = qtrt.sym_qtfunc6(982518782, "_ZNK8QVariant8toDoubleEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("f64", rv) //.(f64) // 1111
   return f64(rv)
}
// /usr/include/qt/QtCore/qvariant.h:303
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] double toDouble(bool *) const

/*

*/
pub fn (this QVariant) toDoublep() f64 {
    // arg: 0, bool *=Pointer, =Invalid, , Invalid
    ok := voidptr(0)
    mut fnobj := T_ZNK8QVariant8toDoubleEPb(0)
    fnobj = qtrt.sym_qtfunc6(982518782, "_ZNK8QVariant8toDoubleEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("f64", rv) //.(f64) // 1111
   return f64(rv)
}
// /usr/include/qt/QtCore/qvariant.h:304
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] float toFloat(bool *) const
type T_ZNK8QVariant7toFloatEPb = fn(cthis voidptr, ok voidptr) f32

/*

*/
pub fn (this QVariant) toFloat(ok &bool) f32 {
    mut fnobj := T_ZNK8QVariant7toFloatEPb(0)
    fnobj = qtrt.sym_qtfunc6(2104130602, "_ZNK8QVariant7toFloatEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("f32", rv) //.(f32) // 1111
   return f32(rv)
}
// /usr/include/qt/QtCore/qvariant.h:304
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] float toFloat(bool *) const

/*

*/
pub fn (this QVariant) toFloatp() f32 {
    // arg: 0, bool *=Pointer, =Invalid, , Invalid
    ok := voidptr(0)
    mut fnobj := T_ZNK8QVariant7toFloatEPb(0)
    fnobj = qtrt.sym_qtfunc6(2104130602, "_ZNK8QVariant7toFloatEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("f32", rv) //.(f32) // 1111
   return f32(rv)
}
// /usr/include/qt/QtCore/qvariant.h:305
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qreal toReal(bool *) const
type T_ZNK8QVariant6toRealEPb = fn(cthis voidptr, ok voidptr) f64

/*

*/
pub fn (this QVariant) toReal(ok &bool) f64 {
    mut fnobj := T_ZNK8QVariant6toRealEPb(0)
    fnobj = qtrt.sym_qtfunc6(2923771759, "_ZNK8QVariant6toRealEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtCore/qvariant.h:305
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] qreal toReal(bool *) const

/*

*/
pub fn (this QVariant) toRealp() f64 {
    // arg: 0, bool *=Pointer, =Invalid, , Invalid
    ok := voidptr(0)
    mut fnobj := T_ZNK8QVariant6toRealEPb(0)
    fnobj = qtrt.sym_qtfunc6(2923771759, "_ZNK8QVariant6toRealEPb")
    rv :=
    fnobj(this.get_cthis(), ok)
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtCore/qvariant.h:319
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QPoint toPoint() const
type T_ZNK8QVariant7toPointEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toPoint()  QPoint/*123*/ {
    mut fnobj := T_ZNK8QVariant7toPointEv(0)
    fnobj = qtrt.sym_qtfunc6(3845935258, "_ZNK8QVariant7toPointEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := /*==*/newQPointFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQPoint)
    return rv2
    //return /*==*/QPoint{rv}
}
// /usr/include/qt/QtCore/qvariant.h:320
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QPointF toPointF() const
type T_ZNK8QVariant8toPointFEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toPointF()  QPointF/*123*/ {
    mut fnobj := T_ZNK8QVariant8toPointFEv(0)
    fnobj = qtrt.sym_qtfunc6(512643617, "_ZNK8QVariant8toPointFEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := /*==*/newQPointFFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQPointF)
    return rv2
    //return /*==*/QPointF{rv}
}
// /usr/include/qt/QtCore/qvariant.h:321
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QRect toRect() const
type T_ZNK8QVariant6toRectEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toRect()  QRect/*123*/ {
    mut fnobj := T_ZNK8QVariant6toRectEv(0)
    fnobj = qtrt.sym_qtfunc6(4043762194, "_ZNK8QVariant6toRectEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := /*==*/newQRectFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQRect)
    return rv2
    //return /*==*/QRect{rv}
}
// /usr/include/qt/QtCore/qvariant.h:322
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QSize toSize() const
type T_ZNK8QVariant6toSizeEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toSize()  QSize/*123*/ {
    mut fnobj := T_ZNK8QVariant6toSizeEv(0)
    fnobj = qtrt.sym_qtfunc6(3479111140, "_ZNK8QVariant6toSizeEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := /*==*/newQSizeFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQSize)
    return rv2
    //return /*==*/QSize{rv}
}
// /usr/include/qt/QtCore/qvariant.h:323
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QSizeF toSizeF() const
type T_ZNK8QVariant7toSizeFEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toSizeF()  QSizeF/*123*/ {
    mut fnobj := T_ZNK8QVariant7toSizeFEv(0)
    fnobj = qtrt.sym_qtfunc6(3575902903, "_ZNK8QVariant7toSizeFEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := /*==*/newQSizeFFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQSizeF)
    return rv2
    //return /*==*/QSizeF{rv}
}
// /usr/include/qt/QtCore/qvariant.h:324
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QLine toLine() const
type T_ZNK8QVariant6toLineEv = fn(cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toLine()  QLine/*123*/ {
    mut fnobj := T_ZNK8QVariant6toLineEv(0)
    fnobj = qtrt.sym_qtfunc6(3802428770, "_ZNK8QVariant6toLineEv")
    rv :=
    fnobj(this.get_cthis())
    rv2 := /*==*/newQLineFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQLine)
    return rv2
    //return /*==*/QLine{rv}
}
// /usr/include/qt/QtCore/qvariant.h:325
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [32] QLineF toLineF() const
type T_ZNK8QVariant7toLineFEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toLineF()  QLineF/*123*/ {
    mut fnobj := T_ZNK8QVariant7toLineFEv(0)
    fnobj = qtrt.sym_qtfunc6(3520409554, "_ZNK8QVariant7toLineFEv")
    mut sretobj := qtrt.mallocraw(32)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQLineFFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQLineF)
    return rv2
    //return /*==*/QLineF{rv}
}
// /usr/include/qt/QtCore/qvariant.h:326
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [32] QRectF toRectF() const
type T_ZNK8QVariant7toRectFEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QVariant) toRectF()  QRectF/*123*/ {
    mut fnobj := T_ZNK8QVariant7toRectFEv(0)
    fnobj = qtrt.sym_qtfunc6(2177105009, "_ZNK8QVariant7toRectFEv")
    mut sretobj := qtrt.mallocraw(32)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQRectFFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQRectF)
    return rv2
    //return /*==*/QRectF{rv}
}

[no_inline]
pub fn deleteQVariant(this &QVariant) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2556109116, "_ZN8QVariantD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QVariant) freecpp() { deleteQVariant(&this) }

fn (this QVariant) free() {

  /*deleteQVariant(&this)*/

  cthis := this.get_cthis()
  //println("QVariant freeing ${cthis} 0 bytes")

}


/*


*/
//type QVariant.Type = int
pub enum QVariantType {
  Invalid = 0
  Bool = 1
  Int = 2
  UInt = 3
  LongLong = 4
  ULongLong = 5
  Double = 6
  Char = 7
  Map = 8
  List = 9
  String = 10
  StringList = 11
  ByteArray = 12
  BitArray = 13
  Date = 14
  Time = 15
  DateTime = 16
  Url = 17
  Locale = 18
  Rect = 19
  RectF = 20
  Size = 21
  SizeF = 22
  Line = 23
  LineF = 24
  Point = 25
  PointF = 26
  RegExp = 27
  RegularExpression = 44
  Hash = 28
  EasingCurve = 29
  Uuid = 30
  ModelIndex = 42
  PersistentModelIndex = 50
  LastCoreType = 55
  Font = 64
  Pixmap = 65
  Brush = 66
  Color = 67
  Palette = 68
  Image = 70
  Polygon = 71
  Region = 72
  Bitmap = 73
  Cursor = 74
  KeySequence = 75
  Pen = 76
  TextLength = 77
  TextFormat = 78
  Matrix = 79
  Transform = 80
  Matrix4x4 = 81
  Vector2D = 82
  Vector3D = 83
  Vector4D = 84
  Quaternion = 85
  PolygonF = 86
  Icon = 69
  LastGuiType = 87
  SizePolicy = 121
  UserType = 1024
  LastType = -1
} // endof enum Type

//  body block end

//  keep block begin


fn init_unused_10013() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
