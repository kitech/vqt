

module qtcore
// /usr/include/qt/QtCore/qobjectdefs.h
// #include <qobjectdefs.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QMetaObject {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QMetaObjectITF {
    get_cthis() voidptr
    toQMetaObject() QMetaObject
}
fn hotfix_QMetaObject_itf_name_table(this QMetaObjectITF) {
  that := QMetaObject{}
  hotfix_QMetaObject_itf_name_table(that)
}
pub fn (ptr QMetaObject) toQMetaObject() QMetaObject { return ptr }

pub fn (this QMetaObject) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQMetaObjectFromptr(cthis voidptr) QMetaObject {
    return QMetaObject{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QMetaObject) newFromptr(cthis voidptr) QMetaObject {
    return newQMetaObjectFromptr(cthis)
}
// /usr/include/qt/QtCore/qobjectdefs.h:340
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] const char * className() const
type T_ZNK11QMetaObject9classNameEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMetaObject) className() string {
    mut fnobj := T_ZNK11QMetaObject9classNameEv(0)
    fnobj = qtrt.sym_qtfunc6(2567088323, "_ZNK11QMetaObject9classNameEv")
    rv :=
    fnobj(this.get_cthis())
    return qtrt.vstringp(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:341
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] const QMetaObject * superClass() const
type T_ZNK11QMetaObject10superClassEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMetaObject) superClass()  QMetaObject/*777 const QMetaObject **/ {
    mut fnobj := T_ZNK11QMetaObject10superClassEv(0)
    fnobj = qtrt.sym_qtfunc6(4180358434, "_ZNK11QMetaObject10superClassEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQMetaObjectFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtCore/qobjectdefs.h:344
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] QObject * cast(QObject *) const
type T_ZNK11QMetaObject4castEP7QObject = fn(cthis voidptr, obj voidptr) voidptr/*666*/

/*

*/
pub fn (this QMetaObject) cast(obj  QObject/*777 QObject **/)  QObject/*777 QObject **/ {
    mut conv_arg0 := voidptr(0)
    /*if obj != voidptr(0) && obj.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = obj.QObject_ptr().get_cthis()
      conv_arg0 = obj.get_cthis()
    }
    mut fnobj := T_ZNK11QMetaObject4castEP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(4086969823, "_ZNK11QMetaObject4castEP7QObject")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQObjectFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtCore/qobjectdefs.h:345
// index:1 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] const QObject * cast(const QObject *) const
type T_ZNK11QMetaObject4castEPK7QObject = fn(cthis voidptr, obj voidptr) voidptr/*666*/

/*

*/
pub fn (this QMetaObject) cast1(obj  QObject/*777 const QObject **/)  QObject/*777 const QObject **/ {
    mut conv_arg0 := voidptr(0)
    /*if obj != voidptr(0) && obj.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = obj.QObject_ptr().get_cthis()
      conv_arg0 = obj.get_cthis()
    }
    mut fnobj := T_ZNK11QMetaObject4castEPK7QObject(0)
    fnobj = qtrt.sym_qtfunc6(1982383341, "_ZNK11QMetaObject4castEPK7QObject")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return /*==*/newQObjectFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtCore/qobjectdefs.h:351
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int methodOffset() const
type T_ZNK11QMetaObject12methodOffsetEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) methodOffset() int {
    mut fnobj := T_ZNK11QMetaObject12methodOffsetEv(0)
    fnobj = qtrt.sym_qtfunc6(2344179760, "_ZNK11QMetaObject12methodOffsetEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:352
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int enumeratorOffset() const
type T_ZNK11QMetaObject16enumeratorOffsetEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) enumeratorOffset() int {
    mut fnobj := T_ZNK11QMetaObject16enumeratorOffsetEv(0)
    fnobj = qtrt.sym_qtfunc6(3130508702, "_ZNK11QMetaObject16enumeratorOffsetEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:353
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int propertyOffset() const
type T_ZNK11QMetaObject14propertyOffsetEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) propertyOffset() int {
    mut fnobj := T_ZNK11QMetaObject14propertyOffsetEv(0)
    fnobj = qtrt.sym_qtfunc6(2486189546, "_ZNK11QMetaObject14propertyOffsetEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:354
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int classInfoOffset() const
type T_ZNK11QMetaObject15classInfoOffsetEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) classInfoOffset() int {
    mut fnobj := T_ZNK11QMetaObject15classInfoOffsetEv(0)
    fnobj = qtrt.sym_qtfunc6(3719363778, "_ZNK11QMetaObject15classInfoOffsetEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:356
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int constructorCount() const
type T_ZNK11QMetaObject16constructorCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) constructorCount() int {
    mut fnobj := T_ZNK11QMetaObject16constructorCountEv(0)
    fnobj = qtrt.sym_qtfunc6(722228823, "_ZNK11QMetaObject16constructorCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:357
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int methodCount() const
type T_ZNK11QMetaObject11methodCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) methodCount() int {
    mut fnobj := T_ZNK11QMetaObject11methodCountEv(0)
    fnobj = qtrt.sym_qtfunc6(1964931486, "_ZNK11QMetaObject11methodCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:358
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int enumeratorCount() const
type T_ZNK11QMetaObject15enumeratorCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) enumeratorCount() int {
    mut fnobj := T_ZNK11QMetaObject15enumeratorCountEv(0)
    fnobj = qtrt.sym_qtfunc6(2750070921, "_ZNK11QMetaObject15enumeratorCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:359
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int propertyCount() const
type T_ZNK11QMetaObject13propertyCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) propertyCount() int {
    mut fnobj := T_ZNK11QMetaObject13propertyCountEv(0)
    fnobj = qtrt.sym_qtfunc6(2808090851, "_ZNK11QMetaObject13propertyCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:360
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int classInfoCount() const
type T_ZNK11QMetaObject14classInfoCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaObject) classInfoCount() int {
    mut fnobj := T_ZNK11QMetaObject14classInfoCountEv(0)
    fnobj = qtrt.sym_qtfunc6(884557363, "_ZNK11QMetaObject14classInfoCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:362
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOfConstructor(const char *) const
type T_ZNK11QMetaObject18indexOfConstructorEPKc = fn(cthis voidptr, constructor voidptr) int

/*

*/
pub fn (this QMetaObject) indexOfConstructor(constructor string) int {
    mut conv_arg0 := qtrt.cstringr(&constructor)
    mut fnobj := T_ZNK11QMetaObject18indexOfConstructorEPKc(0)
    fnobj = qtrt.sym_qtfunc6(541226815, "_ZNK11QMetaObject18indexOfConstructorEPKc")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:363
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOfMethod(const char *) const
type T_ZNK11QMetaObject13indexOfMethodEPKc = fn(cthis voidptr, method voidptr) int

/*

*/
pub fn (this QMetaObject) indexOfMethod(method string) int {
    mut conv_arg0 := qtrt.cstringr(&method)
    mut fnobj := T_ZNK11QMetaObject13indexOfMethodEPKc(0)
    fnobj = qtrt.sym_qtfunc6(1248615640, "_ZNK11QMetaObject13indexOfMethodEPKc")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:364
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOfSignal(const char *) const
type T_ZNK11QMetaObject13indexOfSignalEPKc = fn(cthis voidptr, signal voidptr) int

/*

*/
pub fn (this QMetaObject) indexOfSignal(signal string) int {
    mut conv_arg0 := qtrt.cstringr(&signal)
    mut fnobj := T_ZNK11QMetaObject13indexOfSignalEPKc(0)
    fnobj = qtrt.sym_qtfunc6(245446612, "_ZNK11QMetaObject13indexOfSignalEPKc")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:365
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOfSlot(const char *) const
type T_ZNK11QMetaObject11indexOfSlotEPKc = fn(cthis voidptr, slot voidptr) int

/*

*/
pub fn (this QMetaObject) indexOfSlot(slot string) int {
    mut conv_arg0 := qtrt.cstringr(&slot)
    mut fnobj := T_ZNK11QMetaObject11indexOfSlotEPKc(0)
    fnobj = qtrt.sym_qtfunc6(2159463968, "_ZNK11QMetaObject11indexOfSlotEPKc")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:366
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOfEnumerator(const char *) const
type T_ZNK11QMetaObject17indexOfEnumeratorEPKc = fn(cthis voidptr, name voidptr) int

/*

*/
pub fn (this QMetaObject) indexOfEnumerator(name string) int {
    mut conv_arg0 := qtrt.cstringr(&name)
    mut fnobj := T_ZNK11QMetaObject17indexOfEnumeratorEPKc(0)
    fnobj = qtrt.sym_qtfunc6(3528180915, "_ZNK11QMetaObject17indexOfEnumeratorEPKc")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:367
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOfProperty(const char *) const
type T_ZNK11QMetaObject15indexOfPropertyEPKc = fn(cthis voidptr, name voidptr) int

/*

*/
pub fn (this QMetaObject) indexOfProperty(name string) int {
    mut conv_arg0 := qtrt.cstringr(&name)
    mut fnobj := T_ZNK11QMetaObject15indexOfPropertyEPKc(0)
    fnobj = qtrt.sym_qtfunc6(2753009205, "_ZNK11QMetaObject15indexOfPropertyEPKc")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:368
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int indexOfClassInfo(const char *) const
type T_ZNK11QMetaObject16indexOfClassInfoEPKc = fn(cthis voidptr, name voidptr) int

/*

*/
pub fn (this QMetaObject) indexOfClassInfo(name string) int {
    mut conv_arg0 := qtrt.cstringr(&name)
    mut fnobj := T_ZNK11QMetaObject16indexOfClassInfoEPKc(0)
    fnobj = qtrt.sym_qtfunc6(3923681255, "_ZNK11QMetaObject16indexOfClassInfoEPKc")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qobjectdefs.h:370
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QMetaMethod constructor(int) const
type T_ZNK11QMetaObject11constructorEi = fn(cthis voidptr, index int) voidptr

/*

*/
pub fn (this QMetaObject) constructor(index int)  QMetaMethod/*123*/ {
    mut fnobj := T_ZNK11QMetaObject11constructorEi(0)
    fnobj = qtrt.sym_qtfunc6(3534876830, "_ZNK11QMetaObject11constructorEi")
    rv :=
    fnobj(this.get_cthis(), index)
    rv2 := /*==*/newQMetaMethodFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQMetaMethod)
    return rv2
    //return /*==*/QMetaMethod{rv}
}
// /usr/include/qt/QtCore/qobjectdefs.h:371
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QMetaMethod method(int) const
type T_ZNK11QMetaObject6methodEi = fn(cthis voidptr, index int) voidptr

/*

*/
pub fn (this QMetaObject) method(index int)  QMetaMethod/*123*/ {
    mut fnobj := T_ZNK11QMetaObject6methodEi(0)
    fnobj = qtrt.sym_qtfunc6(981682568, "_ZNK11QMetaObject6methodEi")
    rv :=
    fnobj(this.get_cthis(), index)
    rv2 := /*==*/newQMetaMethodFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQMetaMethod)
    return rv2
    //return /*==*/QMetaMethod{rv}
}
// /usr/include/qt/QtCore/qobjectdefs.h:372
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QMetaEnum enumerator(int) const
type T_ZNK11QMetaObject10enumeratorEi = fn(cthis voidptr, index int) voidptr

/*

*/
pub fn (this QMetaObject) enumerator(index int)  QMetaEnum/*123*/ {
    mut fnobj := T_ZNK11QMetaObject10enumeratorEi(0)
    fnobj = qtrt.sym_qtfunc6(819545584, "_ZNK11QMetaObject10enumeratorEi")
    rv :=
    fnobj(this.get_cthis(), index)
    rv2 := /*==*/newQMetaEnumFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQMetaEnum)
    return rv2
    //return /*==*/QMetaEnum{rv}
}
// /usr/include/qt/QtCore/qobjectdefs.h:373
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [32] QMetaProperty property(int) const
type T_ZNK11QMetaObject8propertyEi = fn(sretobj voidptr, cthis voidptr, index int) voidptr

/*

*/
pub fn (this QMetaObject) property(index int)  QMetaProperty/*123*/ {
    mut fnobj := T_ZNK11QMetaObject8propertyEi(0)
    fnobj = qtrt.sym_qtfunc6(588131701, "_ZNK11QMetaObject8propertyEi")
    mut sretobj := qtrt.mallocraw(32)
    fnobj(sretobj, this.get_cthis(), index)
    rv := sretobj
    rv2 := /*==*/newQMetaPropertyFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQMetaProperty)
    return rv2
    //return /*==*/QMetaProperty{rv}
}
// /usr/include/qt/QtCore/qobjectdefs.h:374
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [16] QMetaClassInfo classInfo(int) const
type T_ZNK11QMetaObject9classInfoEi = fn(cthis voidptr, index int) voidptr

/*

*/
pub fn (this QMetaObject) classInfo(index int)  QMetaClassInfo/*123*/ {
    mut fnobj := T_ZNK11QMetaObject9classInfoEi(0)
    fnobj = qtrt.sym_qtfunc6(1372127368, "_ZNK11QMetaObject9classInfoEi")
    rv :=
    fnobj(this.get_cthis(), index)
    rv2 := /*==*/newQMetaClassInfoFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQMetaClassInfo)
    return rv2
    //return /*==*/QMetaClassInfo{rv}
}
// /usr/include/qt/QtCore/qobjectdefs.h:375
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [32] QMetaProperty userProperty() const
type T_ZNK11QMetaObject12userPropertyEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QMetaObject) userProperty()  QMetaProperty/*123*/ {
    mut fnobj := T_ZNK11QMetaObject12userPropertyEv(0)
    fnobj = qtrt.sym_qtfunc6(4052217834, "_ZNK11QMetaObject12userPropertyEv")
    mut sretobj := qtrt.mallocraw(32)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQMetaPropertyFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQMetaProperty)
    return rv2
    //return /*==*/QMetaProperty{rv}
}

[no_inline]
pub fn deleteQMetaObject(this &QMetaObject) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2735102144, "_ZN11QMetaObjectD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMetaObject) freecpp() { deleteQMetaObject(&this) }

fn (this QMetaObject) free() {

  /*deleteQMetaObject(&this)*/

  cthis := this.get_cthis()
  //println("QMetaObject freeing ${cthis} 0 bytes")

}


/*


*/
//type QMetaObject.Call = int
pub enum QMetaObjectCall {
  InvokeMetaMethod = 0
  ReadProperty = 1
  WriteProperty = 2
  ResetProperty = 3
  QueryPropertyDesignable = 4
  QueryPropertyScriptable = 5
  QueryPropertyStored = 6
  QueryPropertyEditable = 7
  QueryPropertyUser = 8
  CreateInstance = 9
  IndexOfMethod = 10
  RegisterPropertyMetaType = 11
  RegisterMethodArgumentMetaType = 12
} // endof enum Call

//  body block end

//  keep block begin


fn init_unused_10005() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
