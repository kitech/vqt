

module qtcore
// /usr/include/qt/QtCore/qrect.h
// #include <qrect.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QRect {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QRectITF {
    get_cthis() voidptr
    toQRect() QRect
}
fn hotfix_QRect_itf_name_table(this QRectITF) {
  that := QRect{}
  hotfix_QRect_itf_name_table(that)
}
pub fn (ptr QRect) toQRect() QRect { return ptr }

pub fn (this QRect) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQRectFromptr(cthis voidptr) QRect {
    return QRect{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QRect) newFromptr(cthis voidptr) QRect {
    return newQRectFromptr(cthis)
}
// /usr/include/qt/QtCore/qrect.h:65
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isNull() const
type T_ZNK5QRect6isNullEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QRect) isNull() bool {
    mut fnobj := T_ZNK5QRect6isNullEv(0)
    fnobj = qtrt.sym_qtfunc6(3424420210, "_ZNK5QRect6isNullEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qrect.h:66
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isEmpty() const
type T_ZNK5QRect7isEmptyEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QRect) isEmpty() bool {
    mut fnobj := T_ZNK5QRect7isEmptyEv(0)
    fnobj = qtrt.sym_qtfunc6(1044706320, "_ZNK5QRect7isEmptyEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qrect.h:67
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isValid() const
type T_ZNK5QRect7isValidEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QRect) isValid() bool {
    mut fnobj := T_ZNK5QRect7isValidEv(0)
    fnobj = qtrt.sym_qtfunc6(204306394, "_ZNK5QRect7isValidEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qrect.h:69
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int left() const
type T_ZNK5QRect4leftEv = fn(cthis voidptr) int

/*

*/
pub fn (this QRect) left() int {
    mut fnobj := T_ZNK5QRect4leftEv(0)
    fnobj = qtrt.sym_qtfunc6(3169620863, "_ZNK5QRect4leftEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qrect.h:70
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int top() const
type T_ZNK5QRect3topEv = fn(cthis voidptr) int

/*

*/
pub fn (this QRect) top() int {
    mut fnobj := T_ZNK5QRect3topEv(0)
    fnobj = qtrt.sym_qtfunc6(3329531928, "_ZNK5QRect3topEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qrect.h:71
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int right() const
type T_ZNK5QRect5rightEv = fn(cthis voidptr) int

/*

*/
pub fn (this QRect) right() int {
    mut fnobj := T_ZNK5QRect5rightEv(0)
    fnobj = qtrt.sym_qtfunc6(3515267020, "_ZNK5QRect5rightEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qrect.h:72
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int bottom() const
type T_ZNK5QRect6bottomEv = fn(cthis voidptr) int

/*

*/
pub fn (this QRect) bottom() int {
    mut fnobj := T_ZNK5QRect6bottomEv(0)
    fnobj = qtrt.sym_qtfunc6(3137250108, "_ZNK5QRect6bottomEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qrect.h:75
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int x() const
type T_ZNK5QRect1xEv = fn(cthis voidptr) int

/*

*/
pub fn (this QRect) x() int {
    mut fnobj := T_ZNK5QRect1xEv(0)
    fnobj = qtrt.sym_qtfunc6(1050399888, "_ZNK5QRect1xEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qrect.h:76
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int y() const
type T_ZNK5QRect1yEv = fn(cthis voidptr) int

/*

*/
pub fn (this QRect) y() int {
    mut fnobj := T_ZNK5QRect1yEv(0)
    fnobj = qtrt.sym_qtfunc6(1062846119, "_ZNK5QRect1yEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qrect.h:81
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setX(int)
type T_ZN5QRect4setXEi = fn(cthis voidptr, x int) /*void*/

/*

*/
pub fn (this QRect) setX(x int)  {
    mut fnobj := T_ZN5QRect4setXEi(0)
    fnobj = qtrt.sym_qtfunc6(2433102519, "_ZN5QRect4setXEi")
    fnobj(this.get_cthis(), x)
}
// /usr/include/qt/QtCore/qrect.h:82
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setY(int)
type T_ZN5QRect4setYEi = fn(cthis voidptr, y int) /*void*/

/*

*/
pub fn (this QRect) setY(y int)  {
    mut fnobj := T_ZN5QRect4setYEi(0)
    fnobj = qtrt.sym_qtfunc6(2428786816, "_ZN5QRect4setYEi")
    fnobj(this.get_cthis(), y)
}
// /usr/include/qt/QtCore/qrect.h:126
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setWidth(int)
type T_ZN5QRect8setWidthEi = fn(cthis voidptr, w int) /*void*/

/*

*/
pub fn (this QRect) setWidth(w int)  {
    mut fnobj := T_ZN5QRect8setWidthEi(0)
    fnobj = qtrt.sym_qtfunc6(940159144, "_ZN5QRect8setWidthEi")
    fnobj(this.get_cthis(), w)
}
// /usr/include/qt/QtCore/qrect.h:127
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setHeight(int)
type T_ZN5QRect9setHeightEi = fn(cthis voidptr, h int) /*void*/

/*

*/
pub fn (this QRect) setHeight(h int)  {
    mut fnobj := T_ZN5QRect9setHeightEi(0)
    fnobj = qtrt.sym_qtfunc6(1082198786, "_ZN5QRect9setHeightEi")
    fnobj(this.get_cthis(), h)
}

[no_inline]
pub fn deleteQRect(this &QRect) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3544253908, "_ZN5QRectD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QRect) freecpp() { deleteQRect(&this) }

fn (this QRect) free() {

  /*deleteQRect(&this)*/

  cthis := this.get_cthis()
  //println("QRect freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10059() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
