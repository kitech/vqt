

module qtcore
// /usr/include/qt/QtCore/qpoint.h
// #include <qpoint.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 3
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QPoint {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QPointITF {
    get_cthis() voidptr
    toQPoint() QPoint
}
fn hotfix_QPoint_itf_name_table(this QPointITF) {
  that := QPoint{}
  hotfix_QPoint_itf_name_table(that)
}
pub fn (ptr QPoint) toQPoint() QPoint { return ptr }

pub fn (this QPoint) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQPointFromptr(cthis voidptr) QPoint {
    return QPoint{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QPoint) newFromptr(cthis voidptr) QPoint {
    return newQPointFromptr(cthis)
}
// /usr/include/qt/QtCore/qpoint.h:58
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isNull() const
type T_ZNK6QPoint6isNullEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QPoint) isNull() bool {
    mut fnobj := T_ZNK6QPoint6isNullEv(0)
    fnobj = qtrt.sym_qtfunc6(1390804939, "_ZNK6QPoint6isNullEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qpoint.h:60
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int x() const
type T_ZNK6QPoint1xEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPoint) x() int {
    mut fnobj := T_ZNK6QPoint1xEv(0)
    fnobj = qtrt.sym_qtfunc6(359386261, "_ZNK6QPoint1xEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qpoint.h:61
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int y() const
type T_ZNK6QPoint1yEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPoint) y() int {
    mut fnobj := T_ZNK6QPoint1yEv(0)
    fnobj = qtrt.sym_qtfunc6(346662562, "_ZNK6QPoint1yEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qpoint.h:62
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setX(int)
type T_ZN6QPoint4setXEi = fn(cthis voidptr, x int) /*void*/

/*

*/
pub fn (this QPoint) setX(x int)  {
    mut fnobj := T_ZN6QPoint4setXEi(0)
    fnobj = qtrt.sym_qtfunc6(148123695, "_ZN6QPoint4setXEi")
    fnobj(this.get_cthis(), x)
}
// /usr/include/qt/QtCore/qpoint.h:63
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setY(int)
type T_ZN6QPoint4setYEi = fn(cthis voidptr, y int) /*void*/

/*

*/
pub fn (this QPoint) setY(y int)  {
    mut fnobj := T_ZN6QPoint4setYEi(0)
    fnobj = qtrt.sym_qtfunc6(152459800, "_ZN6QPoint4setYEi")
    fnobj(this.get_cthis(), y)
}
// /usr/include/qt/QtCore/qpoint.h:65
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int manhattanLength() const
type T_ZNK6QPoint15manhattanLengthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QPoint) manhattanLength() int {
    mut fnobj := T_ZNK6QPoint15manhattanLengthEv(0)
    fnobj = qtrt.sym_qtfunc6(3380245230, "_ZNK6QPoint15manhattanLengthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}

[no_inline]
pub fn deleteQPoint(this &QPoint) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2151685267, "_ZN6QPointD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPoint) freecpp() { deleteQPoint(&this) }

fn (this QPoint) free() {

  /*deleteQPoint(&this)*/

  cthis := this.get_cthis()
  //println("QPoint freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10039() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
