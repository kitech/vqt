

// +build !minimal

module qtcore
// /usr/include/qt/QtCore/qabstractitemmodel.h
// #include <qabstractitemmodel.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 12
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QPersistentModelIndex {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QPersistentModelIndexITF {
    get_cthis() voidptr
    toQPersistentModelIndex() QPersistentModelIndex
}
fn hotfix_QPersistentModelIndex_itf_name_table(this QPersistentModelIndexITF) {
  that := QPersistentModelIndex{}
  hotfix_QPersistentModelIndex_itf_name_table(that)
}
pub fn (ptr QPersistentModelIndex) toQPersistentModelIndex() QPersistentModelIndex { return ptr }

pub fn (this QPersistentModelIndex) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQPersistentModelIndexFromptr(cthis voidptr) QPersistentModelIndex {
    return QPersistentModelIndex{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QPersistentModelIndex) newFromptr(cthis voidptr) QPersistentModelIndex {
    return newQPersistentModelIndexFromptr(cthis)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:111
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QPersistentModelIndex()
type T_ZN21QPersistentModelIndexC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QPersistentModelIndex) new_for_inherit_() QPersistentModelIndex {
  //return newQPersistentModelIndex()
  return QPersistentModelIndex{}
}
pub fn newQPersistentModelIndex() QPersistentModelIndex {
    mut fnobj := T_ZN21QPersistentModelIndexC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(4053232108, "_ZN21QPersistentModelIndexC2Ev")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis)
    rv := cthis
    vthis := newQPersistentModelIndexFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQPersistentModelIndex)
  return vthis
}

[no_inline]
pub fn deleteQPersistentModelIndex(this &QPersistentModelIndex) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1816154453, "_ZN21QPersistentModelIndexD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPersistentModelIndex) freecpp() { deleteQPersistentModelIndex(&this) }

fn (this QPersistentModelIndex) free() {

  /*deleteQPersistentModelIndex(&this)*/

  cthis := this.get_cthis()
  //println("QPersistentModelIndex freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10017() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
