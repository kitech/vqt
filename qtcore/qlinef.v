

module qtcore
// /usr/include/qt/QtCore/qline.h
// #include <qline.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QLineF {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QLineFITF {
    get_cthis() voidptr
    toQLineF() QLineF
}
fn hotfix_QLineF_itf_name_table(this QLineFITF) {
  that := QLineF{}
  hotfix_QLineF_itf_name_table(that)
}
pub fn (ptr QLineF) toQLineF() QLineF { return ptr }

pub fn (this QLineF) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQLineFFromptr(cthis voidptr) QLineF {
    return QLineF{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QLineF) newFromptr(cthis voidptr) QLineF {
    return newQLineFFromptr(cthis)
}

[no_inline]
pub fn deleteQLineF(this &QLineF) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2019197402, "_ZN6QLineFD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QLineF) freecpp() { deleteQLineF(&this) }

fn (this QLineF) free() {

  /*deleteQLineF(&this)*/

  cthis := this.get_cthis()
  //println("QLineF freeing ${cthis} 0 bytes")

}


/*


*/
//type QLineF.IntersectType = int
pub enum QLineFIntersectType {
  NoIntersection = 0
  BoundedIntersection = 1
  UnboundedIntersection = 2
} // endof enum IntersectType

//  body block end

//  keep block begin


fn init_unused_10045() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
