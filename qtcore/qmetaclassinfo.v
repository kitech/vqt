

module qtcore
// /usr/include/qt/QtCore/qmetaobject.h
// #include <qmetaobject.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QMetaClassInfo {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QMetaClassInfoITF {
    get_cthis() voidptr
    toQMetaClassInfo() QMetaClassInfo
}
fn hotfix_QMetaClassInfo_itf_name_table(this QMetaClassInfoITF) {
  that := QMetaClassInfo{}
  hotfix_QMetaClassInfo_itf_name_table(that)
}
pub fn (ptr QMetaClassInfo) toQMetaClassInfo() QMetaClassInfo { return ptr }

pub fn (this QMetaClassInfo) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQMetaClassInfoFromptr(cthis voidptr) QMetaClassInfo {
    return QMetaClassInfo{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QMetaClassInfo) newFromptr(cthis voidptr) QMetaClassInfo {
    return newQMetaClassInfoFromptr(cthis)
}

[no_inline]
pub fn deleteQMetaClassInfo(this &QMetaClassInfo) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1030872570, "_ZN14QMetaClassInfoD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMetaClassInfo) freecpp() { deleteQMetaClassInfo(&this) }

fn (this QMetaClassInfo) free() {

  /*deleteQMetaClassInfo(&this)*/

  cthis := this.get_cthis()
  //println("QMetaClassInfo freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10053() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
