

// +build !minimal

module qtcore
// /usr/include/qt/QtCore/qabstractitemmodel.h
// #include <qabstractitemmodel.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 29
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QAbstractTableModel {
pub:
  QAbstractItemModel
}

pub interface QAbstractTableModelITF {
//    QAbstractItemModelITF
    get_cthis() voidptr
    toQAbstractTableModel() QAbstractTableModel
}
fn hotfix_QAbstractTableModel_itf_name_table(this QAbstractTableModelITF) {
  that := QAbstractTableModel{}
  hotfix_QAbstractTableModel_itf_name_table(that)
}
pub fn (ptr QAbstractTableModel) toQAbstractTableModel() QAbstractTableModel { return ptr }

pub fn (this QAbstractTableModel) get_cthis() voidptr {
    return this.QAbstractItemModel.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQAbstractTableModelFromptr(cthis voidptr) QAbstractTableModel {
    bcthis0 := newQAbstractItemModelFromptr(cthis)
    return QAbstractTableModel{bcthis0}
}
pub fn (dummy QAbstractTableModel) newFromptr(cthis voidptr) QAbstractTableModel {
    return newQAbstractTableModelFromptr(cthis)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:392
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractTableModel(QObject *)
type T_ZN19QAbstractTableModelC2EP7QObject = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAbstractTableModel) new_for_inherit_(parent  QObject/*777 QObject **/) QAbstractTableModel {
  //return newQAbstractTableModel(parent)
  return QAbstractTableModel{}
}
pub fn newQAbstractTableModel(parent  QObject/*777 QObject **/) QAbstractTableModel {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QObject_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN19QAbstractTableModelC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(2295160465, "_ZN19QAbstractTableModelC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractTableModelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractTableModel)
    // qtrt.connect_destroyed(gothis, "QAbstractTableModel")
  return vthis
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:392
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractTableModel(QObject *)

/*

*/
pub fn (dummy QAbstractTableModel) new_for_inherit_p() QAbstractTableModel {
  //return newQAbstractTableModelp()
  return QAbstractTableModel{}
}
pub fn newQAbstractTableModelp() QAbstractTableModel {
    // arg: 0, QObject *=Pointer, QObject=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN19QAbstractTableModelC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(2295160465, "_ZN19QAbstractTableModelC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractTableModelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractTableModel)
    // qtrt.connect_destroyed(gothis, "QAbstractTableModel")
    return vthis
}

[no_inline]
pub fn deleteQAbstractTableModel(this &QAbstractTableModel) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2737077743, "_ZN19QAbstractTableModelD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAbstractTableModel) freecpp() { deleteQAbstractTableModel(&this) }

fn (this QAbstractTableModel) free() {

  /*deleteQAbstractTableModel(&this)*/

  cthis := this.get_cthis()
  //println("QAbstractTableModel freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10021() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
