

// +build !minimal

module qtcore
// /usr/include/qt/QtCore/qabstractitemmodel.h
// #include <qabstractitemmodel.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 36
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QModelIndex {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QModelIndexITF {
    get_cthis() voidptr
    toQModelIndex() QModelIndex
}
fn hotfix_QModelIndex_itf_name_table(this QModelIndexITF) {
  that := QModelIndex{}
  hotfix_QModelIndex_itf_name_table(that)
}
pub fn (ptr QModelIndex) toQModelIndex() QModelIndex { return ptr }

pub fn (this QModelIndex) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQModelIndexFromptr(cthis voidptr) QModelIndex {
    return QModelIndex{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QModelIndex) newFromptr(cthis voidptr) QModelIndex {
    return newQModelIndexFromptr(cthis)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:60
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QModelIndex()
type T_ZN11QModelIndexC2Ev = fn(cthis voidptr) 

/*

*/
pub fn (dummy QModelIndex) new_for_inherit_() QModelIndex {
  //return newQModelIndex()
  return QModelIndex{}
}
pub fn newQModelIndex() QModelIndex {
    mut fnobj := T_ZN11QModelIndexC2Ev(0)
    fnobj = qtrt.sym_qtfunc6(756365278, "_ZN11QModelIndexC2Ev")
    mut cthis := qtrt.mallocraw(24)
    fnobj(cthis)
    rv := cthis
    vthis := newQModelIndexFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQModelIndex)
  return vthis
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:62
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int row() const
type T_ZNK11QModelIndex3rowEv = fn(cthis voidptr) int

/*

*/
pub fn (this QModelIndex) row() int {
    mut fnobj := T_ZNK11QModelIndex3rowEv(0)
    fnobj = qtrt.sym_qtfunc6(3724840192, "_ZNK11QModelIndex3rowEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:63
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int column() const
type T_ZNK11QModelIndex6columnEv = fn(cthis voidptr) int

/*

*/
pub fn (this QModelIndex) column() int {
    mut fnobj := T_ZNK11QModelIndex6columnEv(0)
    fnobj = qtrt.sym_qtfunc6(3046022154, "_ZNK11QModelIndex6columnEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:64
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] quintptr internalId() const
type T_ZNK11QModelIndex10internalIdEv = fn(cthis voidptr) u64

/*

*/
pub fn (this QModelIndex) internalId() u64 {
    mut fnobj := T_ZNK11QModelIndex10internalIdEv(0)
    fnobj = qtrt.sym_qtfunc6(47055808, "_ZNK11QModelIndex10internalIdEv")
    rv :=
    fnobj(this.get_cthis())
    return u64(rv) // 222
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:65
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] void * internalPointer() const
type T_ZNK11QModelIndex15internalPointerEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QModelIndex) internalPointer() voidptr /*666*/ {
    mut fnobj := T_ZNK11QModelIndex15internalPointerEv(0)
    fnobj = qtrt.sym_qtfunc6(2841223468, "_ZNK11QModelIndex15internalPointerEv")
    rv :=
    fnobj(this.get_cthis())
    return voidptr(rv)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:66
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [24] QModelIndex parent() const
type T_ZNK11QModelIndex6parentEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QModelIndex) parent()  QModelIndex/*123*/ {
    mut fnobj := T_ZNK11QModelIndex6parentEv(0)
    fnobj = qtrt.sym_qtfunc6(1304276158, "_ZNK11QModelIndex6parentEv")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:67
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [24] QModelIndex sibling(int, int) const
type T_ZNK11QModelIndex7siblingEii = fn(sretobj voidptr, cthis voidptr, row int, column int) voidptr

/*

*/
pub fn (this QModelIndex) sibling(row int, column int)  QModelIndex/*123*/ {
    mut fnobj := T_ZNK11QModelIndex7siblingEii(0)
    fnobj = qtrt.sym_qtfunc6(128912225, "_ZNK11QModelIndex7siblingEii")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row, column)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:68
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [24] QModelIndex siblingAtColumn(int) const
type T_ZNK11QModelIndex15siblingAtColumnEi = fn(sretobj voidptr, cthis voidptr, column int) voidptr

/*

*/
pub fn (this QModelIndex) siblingAtColumn(column int)  QModelIndex/*123*/ {
    mut fnobj := T_ZNK11QModelIndex15siblingAtColumnEi(0)
    fnobj = qtrt.sym_qtfunc6(2281150179, "_ZNK11QModelIndex15siblingAtColumnEi")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), column)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:69
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [24] QModelIndex siblingAtRow(int) const
type T_ZNK11QModelIndex12siblingAtRowEi = fn(sretobj voidptr, cthis voidptr, row int) voidptr

/*

*/
pub fn (this QModelIndex) siblingAtRow(row int)  QModelIndex/*123*/ {
    mut fnobj := T_ZNK11QModelIndex12siblingAtRowEi(0)
    fnobj = qtrt.sym_qtfunc6(3167847458, "_ZNK11QModelIndex12siblingAtRowEi")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:73
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [16] QVariant data(int) const
type T_ZNK11QModelIndex4dataEi = fn(sretobj voidptr, cthis voidptr, role int) voidptr

/*

*/
pub fn (this QModelIndex) data(role int)  QVariant/*123*/ {
    mut fnobj := T_ZNK11QModelIndex4dataEi(0)
    fnobj = qtrt.sym_qtfunc6(2419095415, "_ZNK11QModelIndex4dataEi")
    mut sretobj := qtrt.mallocraw(16)
    fnobj(sretobj, this.get_cthis(), role)
    rv := sretobj
    rv2 := /*==*/newQVariantFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQVariant)
    return rv2
    //return /*==*/QVariant{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:73
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [16] QVariant data(int) const

/*

*/
pub fn (this QModelIndex) datap()  QVariant/*123*/ {
    // arg: 0, int=Int, =Invalid, , Invalid
    role := 0/*Qt::DisplayRole*/
    mut fnobj := T_ZNK11QModelIndex4dataEi(0)
    fnobj = qtrt.sym_qtfunc6(2419095415, "_ZNK11QModelIndex4dataEi")
    mut sretobj := qtrt.mallocraw(16)
    fnobj(sretobj, this.get_cthis(), role)
    rv := sretobj
    rv2 := /*==*/newQVariantFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQVariant)
    return rv2
    //return /*==*/QVariant{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:75
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] const QAbstractItemModel * model() const
type T_ZNK11QModelIndex5modelEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QModelIndex) model()  QAbstractItemModel/*777 const QAbstractItemModel **/ {
    mut fnobj := T_ZNK11QModelIndex5modelEv(0)
    fnobj = qtrt.sym_qtfunc6(662887749, "_ZNK11QModelIndex5modelEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQAbstractItemModelFromptr(voidptr(rv)) // 444
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:76
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isValid() const
type T_ZNK11QModelIndex7isValidEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QModelIndex) isValid() bool {
    mut fnobj := T_ZNK11QModelIndex7isValidEv(0)
    fnobj = qtrt.sym_qtfunc6(603683773, "_ZNK11QModelIndex7isValidEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}

[no_inline]
pub fn deleteQModelIndex(this &QModelIndex) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2965507943, "_ZN11QModelIndexD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QModelIndex) freecpp() { deleteQModelIndex(&this) }

fn (this QModelIndex) free() {

  /*deleteQModelIndex(&this)*/

  cthis := this.get_cthis()
  //println("QModelIndex freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10015() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
