

// +build !minimal

module qtcore
// /usr/include/qt/QtCore/qabstractitemmodel.h
// #include <qabstractitemmodel.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin

// QModelIndex createIndex(int, int, void *)
pub fn (this QAbstractItemModel) inherit_createIndex(f fn(row int, column int, data voidptr /*666*/) voidptr) {
  // qtrt.set_all_inherit_callback(this, "createIndex", f)
}

// void beginResetModel()
pub fn (this QAbstractItemModel) inherit_beginResetModel(f fn() /*void*/) {
  // qtrt.set_all_inherit_callback(this, "beginResetModel", f)
}

// void endResetModel()
pub fn (this QAbstractItemModel) inherit_endResetModel(f fn() /*void*/) {
  // qtrt.set_all_inherit_callback(this, "endResetModel", f)
}



/*

*/
pub struct QAbstractItemModel {
pub:
  QObject
}

pub interface QAbstractItemModelITF {
//    QObjectITF
    get_cthis() voidptr
    toQAbstractItemModel() QAbstractItemModel
}
fn hotfix_QAbstractItemModel_itf_name_table(this QAbstractItemModelITF) {
  that := QAbstractItemModel{}
  hotfix_QAbstractItemModel_itf_name_table(that)
}
pub fn (ptr QAbstractItemModel) toQAbstractItemModel() QAbstractItemModel { return ptr }

pub fn (this QAbstractItemModel) get_cthis() voidptr {
    return this.QObject.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQAbstractItemModelFromptr(cthis voidptr) QAbstractItemModel {
    bcthis0 := newQObjectFromptr(cthis)
    return QAbstractItemModel{bcthis0}
}
pub fn (dummy QAbstractItemModel) newFromptr(cthis voidptr) QAbstractItemModel {
    return newQAbstractItemModelFromptr(cthis)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:178
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractItemModel(QObject *)
type T_ZN18QAbstractItemModelC2EP7QObject = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAbstractItemModel) new_for_inherit_(parent  QObject/*777 QObject **/) QAbstractItemModel {
  //return newQAbstractItemModel(parent)
  return QAbstractItemModel{}
}
pub fn newQAbstractItemModel(parent  QObject/*777 QObject **/) QAbstractItemModel {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QObject_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModelC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(1538334395, "_ZN18QAbstractItemModelC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractItemModelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractItemModel)
    // qtrt.connect_destroyed(gothis, "QAbstractItemModel")
  return vthis
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:178
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractItemModel(QObject *)

/*

*/
pub fn (dummy QAbstractItemModel) new_for_inherit_p() QAbstractItemModel {
  //return newQAbstractItemModelp()
  return QAbstractItemModel{}
}
pub fn newQAbstractItemModelp() QAbstractItemModel {
    // arg: 0, QObject *=Pointer, QObject=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN18QAbstractItemModelC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(1538334395, "_ZN18QAbstractItemModelC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractItemModelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractItemModel)
    // qtrt.connect_destroyed(gothis, "QAbstractItemModel")
    return vthis
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:181
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool hasIndex(int, int, const QModelIndex &) const
type T_ZNK18QAbstractItemModel8hasIndexEiiRK11QModelIndex = fn(cthis voidptr, row int, column int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) hasIndex(row int, column int, parent  QModelIndex) bool {
    mut conv_arg2 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = parent.QModelIndex_ptr().get_cthis()
      conv_arg2 = parent.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel8hasIndexEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(281611678, "_ZNK18QAbstractItemModel8hasIndexEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, column, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:181
// index:0 inlined:false externc:Language=CPlusPlus
// Public Extend Visibility=Default Availability=Available
// [1] bool hasIndex(int, int, const QModelIndex &) const

/*

*/
pub fn (this QAbstractItemModel) hasIndexp(row int, column int) bool {
    // arg: 2, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg2 := newQModelIndex()
    mut fnobj := T_ZNK18QAbstractItemModel8hasIndexEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(281611678, "_ZNK18QAbstractItemModel8hasIndexEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, column, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:182
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Indirect Visibility=Default Availability=Available
// [24] QModelIndex index(int, int, const QModelIndex &) const
type T_ZNK18QAbstractItemModel5indexEiiRK11QModelIndex = fn(sretobj voidptr, cthis voidptr, row int, column int, parent voidptr) voidptr

/*

*/
pub fn (this QAbstractItemModel) index(row int, column int, parent  QModelIndex)  QModelIndex/*123*/ {
    mut conv_arg2 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = parent.QModelIndex_ptr().get_cthis()
      conv_arg2 = parent.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel5indexEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(164512793, "_ZNK18QAbstractItemModel5indexEiiRK11QModelIndex")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row, column, conv_arg2)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:182
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Indirect Visibility=Default Availability=Available
// [24] QModelIndex index(int, int, const QModelIndex &) const

/*

*/
pub fn (this QAbstractItemModel) indexp(row int, column int)  QModelIndex/*123*/ {
    // arg: 2, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg2 := newQModelIndex()
    mut fnobj := T_ZNK18QAbstractItemModel5indexEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(164512793, "_ZNK18QAbstractItemModel5indexEiiRK11QModelIndex")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row, column, conv_arg2)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:184
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Indirect Visibility=Default Availability=Available
// [24] QModelIndex parent(const QModelIndex &) const
type T_ZNK18QAbstractItemModel6parentERK11QModelIndex = fn(sretobj voidptr, cthis voidptr, child voidptr) voidptr

/*

*/
pub fn (this QAbstractItemModel) parent(child  QModelIndex)  QModelIndex/*123*/ {
    mut conv_arg0 := voidptr(0)
    /*if child != voidptr(0) && child.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = child.QModelIndex_ptr().get_cthis()
      conv_arg0 = child.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel6parentERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(75286673, "_ZNK18QAbstractItemModel6parentERK11QModelIndex")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), conv_arg0)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:186
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Indirect Visibility=Default Availability=Available
// [24] QModelIndex sibling(int, int, const QModelIndex &) const
type T_ZNK18QAbstractItemModel7siblingEiiRK11QModelIndex = fn(sretobj voidptr, cthis voidptr, row int, column int, idx voidptr) voidptr

/*

*/
pub fn (this QAbstractItemModel) sibling(row int, column int, idx  QModelIndex)  QModelIndex/*123*/ {
    mut conv_arg2 := voidptr(0)
    /*if idx != voidptr(0) && idx.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = idx.QModelIndex_ptr().get_cthis()
      conv_arg2 = idx.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel7siblingEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(783199811, "_ZNK18QAbstractItemModel7siblingEiiRK11QModelIndex")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row, column, conv_arg2)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:187
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Direct Visibility=Default Availability=Available
// [4] int rowCount(const QModelIndex &) const
type T_ZNK18QAbstractItemModel8rowCountERK11QModelIndex = fn(cthis voidptr, parent voidptr) int

/*

*/
pub fn (this QAbstractItemModel) rowCount(parent  QModelIndex) int {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QModelIndex_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel8rowCountERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1828919860, "_ZNK18QAbstractItemModel8rowCountERK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:187
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Direct Visibility=Default Availability=Available
// [4] int rowCount(const QModelIndex &) const

/*

*/
pub fn (this QAbstractItemModel) rowCountp() int {
    // arg: 0, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg0 := newQModelIndex()
    mut fnobj := T_ZNK18QAbstractItemModel8rowCountERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1828919860, "_ZNK18QAbstractItemModel8rowCountERK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:188
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Direct Visibility=Default Availability=Available
// [4] int columnCount(const QModelIndex &) const
type T_ZNK18QAbstractItemModel11columnCountERK11QModelIndex = fn(cthis voidptr, parent voidptr) int

/*

*/
pub fn (this QAbstractItemModel) columnCount(parent  QModelIndex) int {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QModelIndex_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel11columnCountERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(678361467, "_ZNK18QAbstractItemModel11columnCountERK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:188
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Direct Visibility=Default Availability=Available
// [4] int columnCount(const QModelIndex &) const

/*

*/
pub fn (this QAbstractItemModel) columnCountp() int {
    // arg: 0, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg0 := newQModelIndex()
    mut fnobj := T_ZNK18QAbstractItemModel11columnCountERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(678361467, "_ZNK18QAbstractItemModel11columnCountERK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:189
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool hasChildren(const QModelIndex &) const
type T_ZNK18QAbstractItemModel11hasChildrenERK11QModelIndex = fn(cthis voidptr, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) hasChildren(parent  QModelIndex) bool {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QModelIndex_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel11hasChildrenERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1032754129, "_ZNK18QAbstractItemModel11hasChildrenERK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:189
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool hasChildren(const QModelIndex &) const

/*

*/
pub fn (this QAbstractItemModel) hasChildrenp() bool {
    // arg: 0, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg0 := newQModelIndex()
    mut fnobj := T_ZNK18QAbstractItemModel11hasChildrenERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1032754129, "_ZNK18QAbstractItemModel11hasChildrenERK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:191
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Indirect Visibility=Default Availability=Available
// [16] QVariant data(const QModelIndex &, int) const
type T_ZNK18QAbstractItemModel4dataERK11QModelIndexi = fn(sretobj voidptr, cthis voidptr, index voidptr, role int) voidptr

/*

*/
pub fn (this QAbstractItemModel) data(index  QModelIndex, role int)  QVariant/*123*/ {
    mut conv_arg0 := voidptr(0)
    /*if index != voidptr(0) && index.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = index.QModelIndex_ptr().get_cthis()
      conv_arg0 = index.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel4dataERK11QModelIndexi(0)
    fnobj = qtrt.sym_qtfunc6(2517699839, "_ZNK18QAbstractItemModel4dataERK11QModelIndexi")
    mut sretobj := qtrt.mallocraw(16)
    fnobj(sretobj, this.get_cthis(), conv_arg0, role)
    rv := sretobj
    rv2 := /*==*/newQVariantFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQVariant)
    return rv2
    //return /*==*/QVariant{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:191
// index:0 inlined:false externc:Language=CPlusPlus
// Public purevirtual virtual Indirect Visibility=Default Availability=Available
// [16] QVariant data(const QModelIndex &, int) const

/*

*/
pub fn (this QAbstractItemModel) datap(index  QModelIndex)  QVariant/*123*/ {
    mut conv_arg0 := voidptr(0)
    /*if index != voidptr(0) && index.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = index.QModelIndex_ptr().get_cthis()
      conv_arg0 = index.get_cthis()
    }
    // arg: 1, int=Int, =Invalid, , Invalid
    role := 0/*Qt::DisplayRole*/
    mut fnobj := T_ZNK18QAbstractItemModel4dataERK11QModelIndexi(0)
    fnobj = qtrt.sym_qtfunc6(2517699839, "_ZNK18QAbstractItemModel4dataERK11QModelIndexi")
    mut sretobj := qtrt.mallocraw(16)
    fnobj(sretobj, this.get_cthis(), conv_arg0, role)
    rv := sretobj
    rv2 := /*==*/newQVariantFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQVariant)
    return rv2
    //return /*==*/QVariant{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:192
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool setData(const QModelIndex &, const QVariant &, int)
type T_ZN18QAbstractItemModel7setDataERK11QModelIndexRK8QVarianti = fn(cthis voidptr, index voidptr, value voidptr, role int) bool

/*

*/
pub fn (this QAbstractItemModel) setData(index  QModelIndex, value  QVariant, role int) bool {
    mut conv_arg0 := voidptr(0)
    /*if index != voidptr(0) && index.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = index.QModelIndex_ptr().get_cthis()
      conv_arg0 = index.get_cthis()
    }
    mut conv_arg1 := voidptr(0)
    /*if value != voidptr(0) && value.QVariant_ptr() != voidptr(0) */ {
        // conv_arg1 = value.QVariant_ptr().get_cthis()
      conv_arg1 = value.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel7setDataERK11QModelIndexRK8QVarianti(0)
    fnobj = qtrt.sym_qtfunc6(279704600, "_ZN18QAbstractItemModel7setDataERK11QModelIndexRK8QVarianti")
    rv :=
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, role)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:192
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool setData(const QModelIndex &, const QVariant &, int)

/*

*/
pub fn (this QAbstractItemModel) setDatap(index  QModelIndex, value  QVariant) bool {
    mut conv_arg0 := voidptr(0)
    /*if index != voidptr(0) && index.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = index.QModelIndex_ptr().get_cthis()
      conv_arg0 = index.get_cthis()
    }
    mut conv_arg1 := voidptr(0)
    /*if value != voidptr(0) && value.QVariant_ptr() != voidptr(0) */ {
        // conv_arg1 = value.QVariant_ptr().get_cthis()
      conv_arg1 = value.get_cthis()
    }
    // arg: 2, int=Int, =Invalid, , Invalid
    role := 0/*Qt::EditRole*/
    mut fnobj := T_ZN18QAbstractItemModel7setDataERK11QModelIndexRK8QVarianti(0)
    fnobj = qtrt.sym_qtfunc6(279704600, "_ZN18QAbstractItemModel7setDataERK11QModelIndexRK8QVarianti")
    rv :=
    fnobj(this.get_cthis(), conv_arg0, conv_arg1, role)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:219
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool insertRows(int, int, const QModelIndex &)
type T_ZN18QAbstractItemModel10insertRowsEiiRK11QModelIndex = fn(cthis voidptr, row int, count int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) insertRows(row int, count int, parent  QModelIndex) bool {
    mut conv_arg2 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = parent.QModelIndex_ptr().get_cthis()
      conv_arg2 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel10insertRowsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(843062727, "_ZN18QAbstractItemModel10insertRowsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:219
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool insertRows(int, int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) insertRowsp(row int, count int) bool {
    // arg: 2, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg2 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel10insertRowsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(843062727, "_ZN18QAbstractItemModel10insertRowsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:220
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool insertColumns(int, int, const QModelIndex &)
type T_ZN18QAbstractItemModel13insertColumnsEiiRK11QModelIndex = fn(cthis voidptr, column int, count int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) insertColumns(column int, count int, parent  QModelIndex) bool {
    mut conv_arg2 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = parent.QModelIndex_ptr().get_cthis()
      conv_arg2 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel13insertColumnsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(3123320753, "_ZN18QAbstractItemModel13insertColumnsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:220
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool insertColumns(int, int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) insertColumnsp(column int, count int) bool {
    // arg: 2, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg2 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel13insertColumnsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(3123320753, "_ZN18QAbstractItemModel13insertColumnsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:221
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool removeRows(int, int, const QModelIndex &)
type T_ZN18QAbstractItemModel10removeRowsEiiRK11QModelIndex = fn(cthis voidptr, row int, count int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) removeRows(row int, count int, parent  QModelIndex) bool {
    mut conv_arg2 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = parent.QModelIndex_ptr().get_cthis()
      conv_arg2 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel10removeRowsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(3635885257, "_ZN18QAbstractItemModel10removeRowsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:221
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool removeRows(int, int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) removeRowsp(row int, count int) bool {
    // arg: 2, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg2 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel10removeRowsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(3635885257, "_ZN18QAbstractItemModel10removeRowsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:222
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool removeColumns(int, int, const QModelIndex &)
type T_ZN18QAbstractItemModel13removeColumnsEiiRK11QModelIndex = fn(cthis voidptr, column int, count int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) removeColumns(column int, count int, parent  QModelIndex) bool {
    mut conv_arg2 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = parent.QModelIndex_ptr().get_cthis()
      conv_arg2 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel13removeColumnsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(3609969157, "_ZN18QAbstractItemModel13removeColumnsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:222
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool removeColumns(int, int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) removeColumnsp(column int, count int) bool {
    // arg: 2, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg2 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel13removeColumnsEiiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(3609969157, "_ZN18QAbstractItemModel13removeColumnsEiiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, count, conv_arg2)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:228
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool insertRow(int, const QModelIndex &)
type T_ZN18QAbstractItemModel9insertRowEiRK11QModelIndex = fn(cthis voidptr, row int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) insertRow(row int, parent  QModelIndex) bool {
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QModelIndex_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel9insertRowEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(2197740031, "_ZN18QAbstractItemModel9insertRowEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:228
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool insertRow(int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) insertRowp(row int) bool {
    // arg: 1, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg1 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel9insertRowEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(2197740031, "_ZN18QAbstractItemModel9insertRowEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:229
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool insertColumn(int, const QModelIndex &)
type T_ZN18QAbstractItemModel12insertColumnEiRK11QModelIndex = fn(cthis voidptr, column int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) insertColumn(column int, parent  QModelIndex) bool {
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QModelIndex_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel12insertColumnEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(2279385328, "_ZN18QAbstractItemModel12insertColumnEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:229
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool insertColumn(int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) insertColumnp(column int) bool {
    // arg: 1, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg1 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel12insertColumnEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(2279385328, "_ZN18QAbstractItemModel12insertColumnEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:230
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool removeRow(int, const QModelIndex &)
type T_ZN18QAbstractItemModel9removeRowEiRK11QModelIndex = fn(cthis voidptr, row int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) removeRow(row int, parent  QModelIndex) bool {
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QModelIndex_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel9removeRowEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(4007650377, "_ZN18QAbstractItemModel9removeRowEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:230
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool removeRow(int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) removeRowp(row int) bool {
    // arg: 1, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg1 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel9removeRowEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(4007650377, "_ZN18QAbstractItemModel9removeRowEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), row, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:231
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool removeColumn(int, const QModelIndex &)
type T_ZN18QAbstractItemModel12removeColumnEiRK11QModelIndex = fn(cthis voidptr, column int, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) removeColumn(column int, parent  QModelIndex) bool {
    mut conv_arg1 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg1 = parent.QModelIndex_ptr().get_cthis()
      conv_arg1 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel12removeColumnEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1619948254, "_ZN18QAbstractItemModel12removeColumnEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:231
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool removeColumn(int, const QModelIndex &)

/*

*/
pub fn (this QAbstractItemModel) removeColumnp(column int) bool {
    // arg: 1, const QModelIndex &=LValueReference, QModelIndex=Record, , Invalid
    mut conv_arg1 := newQModelIndex()
    mut fnobj := T_ZN18QAbstractItemModel12removeColumnEiRK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1619948254, "_ZN18QAbstractItemModel12removeColumnEiRK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), column, conv_arg1)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:232
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool moveRow(const QModelIndex &, int, const QModelIndex &, int)
type T_ZN18QAbstractItemModel7moveRowERK11QModelIndexiS2_i = fn(cthis voidptr, sourceParent voidptr, sourceRow int, destinationParent voidptr, destinationChild int) bool

/*

*/
pub fn (this QAbstractItemModel) moveRow(sourceParent  QModelIndex, sourceRow int, destinationParent  QModelIndex, destinationChild int) bool {
    mut conv_arg0 := voidptr(0)
    /*if sourceParent != voidptr(0) && sourceParent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = sourceParent.QModelIndex_ptr().get_cthis()
      conv_arg0 = sourceParent.get_cthis()
    }
    mut conv_arg2 := voidptr(0)
    /*if destinationParent != voidptr(0) && destinationParent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = destinationParent.QModelIndex_ptr().get_cthis()
      conv_arg2 = destinationParent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel7moveRowERK11QModelIndexiS2_i(0)
    fnobj = qtrt.sym_qtfunc6(3803393986, "_ZN18QAbstractItemModel7moveRowERK11QModelIndexiS2_i")
    rv :=
    fnobj(this.get_cthis(), conv_arg0, sourceRow, conv_arg2, destinationChild)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:234
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool moveColumn(const QModelIndex &, int, const QModelIndex &, int)
type T_ZN18QAbstractItemModel10moveColumnERK11QModelIndexiS2_i = fn(cthis voidptr, sourceParent voidptr, sourceColumn int, destinationParent voidptr, destinationChild int) bool

/*

*/
pub fn (this QAbstractItemModel) moveColumn(sourceParent  QModelIndex, sourceColumn int, destinationParent  QModelIndex, destinationChild int) bool {
    mut conv_arg0 := voidptr(0)
    /*if sourceParent != voidptr(0) && sourceParent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = sourceParent.QModelIndex_ptr().get_cthis()
      conv_arg0 = sourceParent.get_cthis()
    }
    mut conv_arg2 := voidptr(0)
    /*if destinationParent != voidptr(0) && destinationParent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg2 = destinationParent.QModelIndex_ptr().get_cthis()
      conv_arg2 = destinationParent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel10moveColumnERK11QModelIndexiS2_i(0)
    fnobj = qtrt.sym_qtfunc6(1894749172, "_ZN18QAbstractItemModel10moveColumnERK11QModelIndexiS2_i")
    rv :=
    fnobj(this.get_cthis(), conv_arg0, sourceColumn, conv_arg2, destinationChild)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:237
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Ignore Visibility=Default Availability=Available
// [-2] void fetchMore(const QModelIndex &)
type T_ZN18QAbstractItemModel9fetchMoreERK11QModelIndex = fn(cthis voidptr, parent voidptr) /*void*/

/*

*/
pub fn (this QAbstractItemModel) fetchMore(parent  QModelIndex)  {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QModelIndex_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractItemModel9fetchMoreERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(93984695, "_ZN18QAbstractItemModel9fetchMoreERK11QModelIndex")
    fnobj(this.get_cthis(), conv_arg0)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:238
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool canFetchMore(const QModelIndex &) const
type T_ZNK18QAbstractItemModel12canFetchMoreERK11QModelIndex = fn(cthis voidptr, parent voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) canFetchMore(parent  QModelIndex) bool {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QModelIndex_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel12canFetchMoreERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(2951622094, "_ZNK18QAbstractItemModel12canFetchMoreERK11QModelIndex")
    rv :=
    fnobj(this.get_cthis(), conv_arg0)
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:241
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Indirect Visibility=Default Availability=Available
// [24] QModelIndex buddy(const QModelIndex &) const
type T_ZNK18QAbstractItemModel5buddyERK11QModelIndex = fn(sretobj voidptr, cthis voidptr, index voidptr) voidptr

/*

*/
pub fn (this QAbstractItemModel) buddy(index  QModelIndex)  QModelIndex/*123*/ {
    mut conv_arg0 := voidptr(0)
    /*if index != voidptr(0) && index.QModelIndex_ptr() != voidptr(0) */ {
        // conv_arg0 = index.QModelIndex_ptr().get_cthis()
      conv_arg0 = index.get_cthis()
    }
    mut fnobj := T_ZNK18QAbstractItemModel5buddyERK11QModelIndex(0)
    fnobj = qtrt.sym_qtfunc6(1354301694, "_ZNK18QAbstractItemModel5buddyERK11QModelIndex")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), conv_arg0)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:299
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Extend Visibility=Default Availability=Available
// [1] bool submit()
type T_ZN18QAbstractItemModel6submitEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QAbstractItemModel) submit() bool {
    mut fnobj := T_ZN18QAbstractItemModel6submitEv(0)
    fnobj = qtrt.sym_qtfunc6(2465187961, "_ZN18QAbstractItemModel6submitEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:300
// index:0 inlined:false externc:Language=CPlusPlus
// Public virtual Ignore Visibility=Default Availability=Available
// [-2] void revert()
type T_ZN18QAbstractItemModel6revertEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAbstractItemModel) revert()  {
    mut fnobj := T_ZN18QAbstractItemModel6revertEv(0)
    fnobj = qtrt.sym_qtfunc6(380932091, "_ZN18QAbstractItemModel6revertEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:311
// index:0 inlined:true externc:Language=CPlusPlus
// Protected inline Indirect Visibility=Default Availability=Available
// [24] QModelIndex createIndex(int, int, void *) const
type T_ZNK18QAbstractItemModel11createIndexEiiPv = fn(sretobj voidptr, cthis voidptr, row int, column int, data voidptr) voidptr

/*

*/
pub fn (this QAbstractItemModel) createIndex(row int, column int, data voidptr /*666*/)  QModelIndex/*123*/ {
    mut fnobj := T_ZNK18QAbstractItemModel11createIndexEiiPv(0)
    fnobj = qtrt.sym_qtfunc6(1298301286, "_ZNK18QAbstractItemModel11createIndexEiiPv")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row, column, data)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:311
// index:0 inlined:true externc:Language=CPlusPlus
// Protected inline Indirect Visibility=Default Availability=Available
// [24] QModelIndex createIndex(int, int, void *) const

/*

*/
pub fn (this QAbstractItemModel) createIndexp(row int, column int)  QModelIndex/*123*/ {
    // arg: 2, void *=Pointer, =Invalid, , Invalid
    data := voidptr(0)
    mut fnobj := T_ZNK18QAbstractItemModel11createIndexEiiPv(0)
    fnobj = qtrt.sym_qtfunc6(1298301286, "_ZNK18QAbstractItemModel11createIndexEiiPv")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row, column, data)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:312
// index:1 inlined:true externc:Language=CPlusPlus
// Protected inline Indirect Visibility=Default Availability=Available
// [24] QModelIndex createIndex(int, int, quintptr) const
type T_ZNK18QAbstractItemModel11createIndexEiiy = fn(sretobj voidptr, cthis voidptr, row int, column int, id u64) voidptr

/*

*/
pub fn (this QAbstractItemModel) createIndex1(row int, column int, id u64)  QModelIndex/*123*/ {
    mut fnobj := T_ZNK18QAbstractItemModel11createIndexEiiy(0)
    fnobj = qtrt.sym_qtfunc6(2637747804, "_ZNK18QAbstractItemModel11createIndexEiiy")
    mut sretobj := qtrt.mallocraw(24)
    fnobj(sretobj, this.get_cthis(), row, column, id)
    rv := sretobj
    rv2 := /*==*/newQModelIndexFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQModelIndex)
    return rv2
    //return /*==*/QModelIndex{rv}
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:344
// index:0 inlined:false externc:Language=CPlusPlus
// Protected Ignore Visibility=Default Availability=Available
// [-2] void beginResetModel()
type T_ZN18QAbstractItemModel15beginResetModelEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAbstractItemModel) beginResetModel()  {
    mut fnobj := T_ZN18QAbstractItemModel15beginResetModelEv(0)
    fnobj = qtrt.sym_qtfunc6(2958635407, "_ZN18QAbstractItemModel15beginResetModelEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:345
// index:0 inlined:false externc:Language=CPlusPlus
// Protected Ignore Visibility=Default Availability=Available
// [-2] void endResetModel()
type T_ZN18QAbstractItemModel13endResetModelEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QAbstractItemModel) endResetModel()  {
    mut fnobj := T_ZN18QAbstractItemModel13endResetModelEv(0)
    fnobj = qtrt.sym_qtfunc6(2568843013, "_ZN18QAbstractItemModel13endResetModelEv")
    fnobj(this.get_cthis())
}

[no_inline]
pub fn deleteQAbstractItemModel(this &QAbstractItemModel) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(23633593, "_ZN18QAbstractItemModelD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAbstractItemModel) freecpp() { deleteQAbstractItemModel(&this) }

fn (this QAbstractItemModel) free() {

  /*deleteQAbstractItemModel(&this)*/

  cthis := this.get_cthis()
  //println("QAbstractItemModel freeing ${cthis} 0 bytes")

}


/*


*/
//type QAbstractItemModel.LayoutChangeHint = int
pub enum QAbstractItemModelLayoutChangeHint {
  NoLayoutChangeHint = 0
  VerticalSortHint = 1
  HorizontalSortHint = 2
} // endof enum LayoutChangeHint


/*


*/
//type QAbstractItemModel.CheckIndexOption = int
pub enum QAbstractItemModelCheckIndexOption {
  NoOption = 0
  IndexIsValid = 1
  DoNotUseParent = 2
  ParentIsInvalid = 4
} // endof enum CheckIndexOption

//  body block end

//  keep block begin


fn init_unused_10019() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
