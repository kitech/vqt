

module qtcore
// /usr/include/qt/QtCore/qmetaobject.h
// #include <qmetaobject.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 14
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QMetaEnum {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QMetaEnumITF {
    get_cthis() voidptr
    toQMetaEnum() QMetaEnum
}
fn hotfix_QMetaEnum_itf_name_table(this QMetaEnumITF) {
  that := QMetaEnum{}
  hotfix_QMetaEnum_itf_name_table(that)
}
pub fn (ptr QMetaEnum) toQMetaEnum() QMetaEnum { return ptr }

pub fn (this QMetaEnum) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQMetaEnumFromptr(cthis voidptr) QMetaEnum {
    return QMetaEnum{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QMetaEnum) newFromptr(cthis voidptr) QMetaEnum {
    return newQMetaEnumFromptr(cthis)
}

[no_inline]
pub fn deleteQMetaEnum(this &QMetaEnum) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(859374498, "_ZN9QMetaEnumD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMetaEnum) freecpp() { deleteQMetaEnum(&this) }

fn (this QMetaEnum) free() {

  /*deleteQMetaEnum(&this)*/

  cthis := this.get_cthis()
  //println("QMetaEnum freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10049() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
