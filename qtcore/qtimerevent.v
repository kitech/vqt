

module qtcore
// /usr/include/qt/QtCore/qcoreevent.h
// #include <qcoreevent.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 8
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QTimerEvent {
pub:
  QEvent
}

pub interface QTimerEventITF {
//    QEventITF
    get_cthis() voidptr
    toQTimerEvent() QTimerEvent
}
fn hotfix_QTimerEvent_itf_name_table(this QTimerEventITF) {
  that := QTimerEvent{}
  hotfix_QTimerEvent_itf_name_table(that)
}
pub fn (ptr QTimerEvent) toQTimerEvent() QTimerEvent { return ptr }

pub fn (this QTimerEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQTimerEventFromptr(cthis voidptr) QTimerEvent {
    bcthis0 := newQEventFromptr(cthis)
    return QTimerEvent{bcthis0}
}
pub fn (dummy QTimerEvent) newFromptr(cthis voidptr) QTimerEvent {
    return newQTimerEventFromptr(cthis)
}

[no_inline]
pub fn deleteQTimerEvent(this &QTimerEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1009211296, "_ZN11QTimerEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QTimerEvent) freecpp() { deleteQTimerEvent(&this) }

fn (this QTimerEvent) free() {

  /*deleteQTimerEvent(&this)*/

  cthis := this.get_cthis()
  //println("QTimerEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10029() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
