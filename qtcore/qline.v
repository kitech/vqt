

module qtcore
// /usr/include/qt/QtCore/qline.h
// #include <qline.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 6
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QLine {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QLineITF {
    get_cthis() voidptr
    toQLine() QLine
}
fn hotfix_QLine_itf_name_table(this QLineITF) {
  that := QLine{}
  hotfix_QLine_itf_name_table(that)
}
pub fn (ptr QLine) toQLine() QLine { return ptr }

pub fn (this QLine) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQLineFromptr(cthis voidptr) QLine {
    return QLine{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QLine) newFromptr(cthis voidptr) QLine {
    return newQLineFromptr(cthis)
}

[no_inline]
pub fn deleteQLine(this &QLine) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2630803797, "_ZN5QLineD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QLine) freecpp() { deleteQLine(&this) }

fn (this QLine) free() {

  /*deleteQLine(&this)*/

  cthis := this.get_cthis()
  //println("QLine freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10043() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
