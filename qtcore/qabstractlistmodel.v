

// +build !minimal

module qtcore
// /usr/include/qt/QtCore/qabstractitemmodel.h
// #include <qabstractitemmodel.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 1
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QAbstractListModel {
pub:
  QAbstractItemModel
}

pub interface QAbstractListModelITF {
//    QAbstractItemModelITF
    get_cthis() voidptr
    toQAbstractListModel() QAbstractListModel
}
fn hotfix_QAbstractListModel_itf_name_table(this QAbstractListModelITF) {
  that := QAbstractListModel{}
  hotfix_QAbstractListModel_itf_name_table(that)
}
pub fn (ptr QAbstractListModel) toQAbstractListModel() QAbstractListModel { return ptr }

pub fn (this QAbstractListModel) get_cthis() voidptr {
    return this.QAbstractItemModel.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQAbstractListModelFromptr(cthis voidptr) QAbstractListModel {
    bcthis0 := newQAbstractItemModelFromptr(cthis)
    return QAbstractListModel{bcthis0}
}
pub fn (dummy QAbstractListModel) newFromptr(cthis voidptr) QAbstractListModel {
    return newQAbstractListModelFromptr(cthis)
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:418
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractListModel(QObject *)
type T_ZN18QAbstractListModelC2EP7QObject = fn(cthis voidptr, parent voidptr) 

/*

*/
pub fn (dummy QAbstractListModel) new_for_inherit_(parent  QObject/*777 QObject **/) QAbstractListModel {
  //return newQAbstractListModel(parent)
  return QAbstractListModel{}
}
pub fn newQAbstractListModel(parent  QObject/*777 QObject **/) QAbstractListModel {
    mut conv_arg0 := voidptr(0)
    /*if parent != voidptr(0) && parent.QObject_ptr() != voidptr(0) */ {
        // conv_arg0 = parent.QObject_ptr().get_cthis()
      conv_arg0 = parent.get_cthis()
    }
    mut fnobj := T_ZN18QAbstractListModelC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(3627761914, "_ZN18QAbstractListModelC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractListModelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractListModel)
    // qtrt.connect_destroyed(gothis, "QAbstractListModel")
  return vthis
}
// /usr/include/qt/QtCore/qabstractitemmodel.h:418
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QAbstractListModel(QObject *)

/*

*/
pub fn (dummy QAbstractListModel) new_for_inherit_p() QAbstractListModel {
  //return newQAbstractListModelp()
  return QAbstractListModel{}
}
pub fn newQAbstractListModelp() QAbstractListModel {
    // arg: 0, QObject *=Pointer, QObject=Record, , Invalid
    mut conv_arg0 := voidptr(0)
    mut fnobj := T_ZN18QAbstractListModelC2EP7QObject(0)
    fnobj = qtrt.sym_qtfunc6(3627761914, "_ZN18QAbstractListModelC2EP7QObject")
    mut cthis := qtrt.mallocraw(16)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQAbstractListModelFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQAbstractListModel)
    // qtrt.connect_destroyed(gothis, "QAbstractListModel")
    return vthis
}

[no_inline]
pub fn deleteQAbstractListModel(this &QAbstractListModel) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(319317266, "_ZN18QAbstractListModelD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QAbstractListModel) freecpp() { deleteQAbstractListModel(&this) }

fn (this QAbstractListModel) free() {

  /*deleteQAbstractListModel(&this)*/

  cthis := this.get_cthis()
  //println("QAbstractListModel freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10023() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
