

module qtcore
// /usr/include/qt/QtCore/qbytearray.h
// #include <qbytearray.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 26
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QByteArray {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QByteArrayITF {
    get_cthis() voidptr
    toQByteArray() QByteArray
}
fn hotfix_QByteArray_itf_name_table(this QByteArrayITF) {
  that := QByteArray{}
  hotfix_QByteArray_itf_name_table(that)
}
pub fn (ptr QByteArray) toQByteArray() QByteArray { return ptr }

pub fn (this QByteArray) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQByteArrayFromptr(cthis voidptr) QByteArray {
    return QByteArray{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QByteArray) newFromptr(cthis voidptr) QByteArray {
    return newQByteArrayFromptr(cthis)
}
// /usr/include/qt/QtCore/qbytearray.h:181
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QByteArray(const char *, int)
type T_ZN10QByteArrayC2EPKci = fn(cthis voidptr, arg0 voidptr, size int) 

/*

*/
pub fn (dummy QByteArray) new_for_inherit_(arg0 string, size int) QByteArray {
  //return newQByteArray(arg0, size)
  return QByteArray{}
}
pub fn newQByteArray(arg0 string, size int) QByteArray {
    mut conv_arg0 := qtrt.cstringr(&arg0)
    mut fnobj := T_ZN10QByteArrayC2EPKci(0)
    fnobj = qtrt.sym_qtfunc6(1185669528, "_ZN10QByteArrayC2EPKci")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0, size)
    rv := cthis
    vthis := newQByteArrayFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQByteArray)
  return vthis
}
// /usr/include/qt/QtCore/qbytearray.h:181
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QByteArray(const char *, int)

/*

*/
pub fn (dummy QByteArray) new_for_inherit_p(arg0 string) QByteArray {
  //return newQByteArrayp(arg0)
  return QByteArray{}
}
pub fn newQByteArrayp(arg0 string) QByteArray {
    mut conv_arg0 := qtrt.cstringr(&arg0)
    // arg: 1, int=Int, =Invalid, , Invalid
    size := int(-1)
    mut fnobj := T_ZN10QByteArrayC2EPKci(0)
    fnobj = qtrt.sym_qtfunc6(1185669528, "_ZN10QByteArrayC2EPKci")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0, size)
    rv := cthis
    vthis := newQByteArrayFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQByteArray)
    return vthis
}
// /usr/include/qt/QtCore/qbytearray.h:196
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int size() const
type T_ZNK10QByteArray4sizeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QByteArray) size() int {
    mut fnobj := T_ZNK10QByteArray4sizeEv(0)
    fnobj = qtrt.sym_qtfunc6(2381602376, "_ZNK10QByteArray4sizeEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qbytearray.h:210
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] char * data()
type T_ZN10QByteArray4dataEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QByteArray) data() string {
    mut fnobj := T_ZN10QByteArray4dataEv(0)
    fnobj = qtrt.sym_qtfunc6(3584956573, "_ZN10QByteArray4dataEv")
    rv :=
    fnobj(this.get_cthis())
    return qtrt.vstringp(rv)
}

[no_inline]
pub fn deleteQByteArray(this &QByteArray) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(87055785, "_ZN10QByteArrayD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QByteArray) freecpp() { deleteQByteArray(&this) }

fn (this QByteArray) free() {

  /*deleteQByteArray(&this)*/

  cthis := this.get_cthis()
  //println("QByteArray freeing ${cthis} 0 bytes")

}


/*


*/
//type QByteArray.Base64Option = int
pub enum QByteArrayBase64Option {
  Base64Encoding = 0
  Base64UrlEncoding = 1
  OmitTrailingEquals = 2
  AbortOnBase64DecodingErrors = 4
} // endof enum Base64Option


/*


*/
//type QByteArray.Base64DecodingStatus = int
pub enum QByteArrayBase64DecodingStatus {
  Ok = 0
  IllegalInputLength = 1
  IllegalCharacter = 2
  IllegalPadding = 3
} // endof enum Base64DecodingStatus

//  body block end

//  keep block begin


fn init_unused_10007() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
