

module qtcore
// /usr/include/qt/QtCore/qsize.h
// #include <qsize.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 5
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QSizeF {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QSizeFITF {
    get_cthis() voidptr
    toQSizeF() QSizeF
}
fn hotfix_QSizeF_itf_name_table(this QSizeFITF) {
  that := QSizeF{}
  hotfix_QSizeF_itf_name_table(that)
}
pub fn (ptr QSizeF) toQSizeF() QSizeF { return ptr }

pub fn (this QSizeF) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQSizeFFromptr(cthis voidptr) QSizeF {
    return QSizeF{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QSizeF) newFromptr(cthis voidptr) QSizeF {
    return newQSizeFFromptr(cthis)
}

[no_inline]
pub fn deleteQSizeF(this &QSizeF) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2561848487, "_ZN6QSizeFD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QSizeF) freecpp() { deleteQSizeF(&this) }

fn (this QSizeF) free() {

  /*deleteQSizeF(&this)*/

  cthis := this.get_cthis()
  //println("QSizeF freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10057() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
