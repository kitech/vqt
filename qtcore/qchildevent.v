

module qtcore
// /usr/include/qt/QtCore/qcoreevent.h
// #include <qcoreevent.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QChildEvent {
pub:
  QEvent
}

pub interface QChildEventITF {
//    QEventITF
    get_cthis() voidptr
    toQChildEvent() QChildEvent
}
fn hotfix_QChildEvent_itf_name_table(this QChildEventITF) {
  that := QChildEvent{}
  hotfix_QChildEvent_itf_name_table(that)
}
pub fn (ptr QChildEvent) toQChildEvent() QChildEvent { return ptr }

pub fn (this QChildEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQChildEventFromptr(cthis voidptr) QChildEvent {
    bcthis0 := newQEventFromptr(cthis)
    return QChildEvent{bcthis0}
}
pub fn (dummy QChildEvent) newFromptr(cthis voidptr) QChildEvent {
    return newQChildEventFromptr(cthis)
}

[no_inline]
pub fn deleteQChildEvent(this &QChildEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2956324060, "_ZN11QChildEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QChildEvent) freecpp() { deleteQChildEvent(&this) }

fn (this QChildEvent) free() {

  /*deleteQChildEvent(&this)*/

  cthis := this.get_cthis()
  //println("QChildEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10031() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
