

module qtcore
// /usr/include/qt/QtCore/qobjectdefs.h
// #include <qobjectdefs.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QGenericArgument {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QGenericArgumentITF {
    get_cthis() voidptr
    toQGenericArgument() QGenericArgument
}
fn hotfix_QGenericArgument_itf_name_table(this QGenericArgumentITF) {
  that := QGenericArgument{}
  hotfix_QGenericArgument_itf_name_table(that)
}
pub fn (ptr QGenericArgument) toQGenericArgument() QGenericArgument { return ptr }

pub fn (this QGenericArgument) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQGenericArgumentFromptr(cthis voidptr) QGenericArgument {
    return QGenericArgument{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QGenericArgument) newFromptr(cthis voidptr) QGenericArgument {
    return newQGenericArgumentFromptr(cthis)
}

[no_inline]
pub fn deleteQGenericArgument(this &QGenericArgument) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2610054240, "_ZN16QGenericArgumentD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QGenericArgument) freecpp() { deleteQGenericArgument(&this) }

fn (this QGenericArgument) free() {

  /*deleteQGenericArgument(&this)*/

  cthis := this.get_cthis()
  //println("QGenericArgument freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10001() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
