

module qtcore
// /usr/include/qt/QtCore/qcoreevent.h
// #include <qcoreevent.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QDynamicPropertyChangeEvent {
pub:
  QEvent
}

pub interface QDynamicPropertyChangeEventITF {
//    QEventITF
    get_cthis() voidptr
    toQDynamicPropertyChangeEvent() QDynamicPropertyChangeEvent
}
fn hotfix_QDynamicPropertyChangeEvent_itf_name_table(this QDynamicPropertyChangeEventITF) {
  that := QDynamicPropertyChangeEvent{}
  hotfix_QDynamicPropertyChangeEvent_itf_name_table(that)
}
pub fn (ptr QDynamicPropertyChangeEvent) toQDynamicPropertyChangeEvent() QDynamicPropertyChangeEvent { return ptr }

pub fn (this QDynamicPropertyChangeEvent) get_cthis() voidptr {
    return this.QEvent.get_cthis()
}
  // ignore GetCthis for 1 base
[no_inline]
pub fn newQDynamicPropertyChangeEventFromptr(cthis voidptr) QDynamicPropertyChangeEvent {
    bcthis0 := newQEventFromptr(cthis)
    return QDynamicPropertyChangeEvent{bcthis0}
}
pub fn (dummy QDynamicPropertyChangeEvent) newFromptr(cthis voidptr) QDynamicPropertyChangeEvent {
    return newQDynamicPropertyChangeEventFromptr(cthis)
}

[no_inline]
pub fn deleteQDynamicPropertyChangeEvent(this &QDynamicPropertyChangeEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3558902021, "_ZN27QDynamicPropertyChangeEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QDynamicPropertyChangeEvent) freecpp() { deleteQDynamicPropertyChangeEvent(&this) }

fn (this QDynamicPropertyChangeEvent) free() {

  /*deleteQDynamicPropertyChangeEvent(&this)*/

  cthis := this.get_cthis()
  //println("QDynamicPropertyChangeEvent freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10033() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
