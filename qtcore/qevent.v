

module qtcore
// /usr/include/qt/QtCore/qcoreevent.h
// #include <qcoreevent.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 3
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QEvent {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QEventITF {
    get_cthis() voidptr
    toQEvent() QEvent
}
fn hotfix_QEvent_itf_name_table(this QEventITF) {
  that := QEvent{}
  hotfix_QEvent_itf_name_table(that)
}
pub fn (ptr QEvent) toQEvent() QEvent { return ptr }

pub fn (this QEvent) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQEventFromptr(cthis voidptr) QEvent {
    return QEvent{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QEvent) newFromptr(cthis voidptr) QEvent {
    return newQEventFromptr(cthis)
}
// /usr/include/qt/QtCore/qcoreevent.h:297
// index:0 inlined:false externc:Language=CPlusPlus
// Public Visibility=Default Availability=Available
// [-2] void QEvent(QEvent::Type)
type T_ZN6QEventC2ENS_4TypeE = fn(cthis voidptr, type_ int) 

/*

*/
pub fn (dummy QEvent) new_for_inherit_(type_ int) QEvent {
  //return newQEvent(type_)
  return QEvent{}
}
pub fn newQEvent(type_ int) QEvent {
    mut fnobj := T_ZN6QEventC2ENS_4TypeE(0)
    fnobj = qtrt.sym_qtfunc6(2793188191, "_ZN6QEventC2ENS_4TypeE")
    mut cthis := qtrt.mallocraw(24)
    fnobj(cthis, type_)
    rv := cthis
    vthis := newQEventFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQEvent)
  return vthis
}
// /usr/include/qt/QtCore/qcoreevent.h:301
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] QEvent::Type type() const
type T_ZNK6QEvent4typeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QEvent) type_() int {
    mut fnobj := T_ZNK6QEvent4typeEv(0)
    fnobj = qtrt.sym_qtfunc6(4240343207, "_ZNK6QEvent4typeEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtCore/qcoreevent.h:302
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool spontaneous() const
type T_ZNK6QEvent11spontaneousEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QEvent) spontaneous() bool {
    mut fnobj := T_ZNK6QEvent11spontaneousEv(0)
    fnobj = qtrt.sym_qtfunc6(3979599173, "_ZNK6QEvent11spontaneousEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qcoreevent.h:304
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setAccepted(bool)
type T_ZN6QEvent11setAcceptedEb = fn(cthis voidptr, accepted bool) /*void*/

/*

*/
pub fn (this QEvent) setAccepted(accepted bool)  {
    mut fnobj := T_ZN6QEvent11setAcceptedEb(0)
    fnobj = qtrt.sym_qtfunc6(2738784580, "_ZN6QEvent11setAcceptedEb")
    fnobj(this.get_cthis(), accepted)
}
// /usr/include/qt/QtCore/qcoreevent.h:305
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isAccepted() const
type T_ZNK6QEvent10isAcceptedEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QEvent) isAccepted() bool {
    mut fnobj := T_ZNK6QEvent10isAcceptedEv(0)
    fnobj = qtrt.sym_qtfunc6(608985925, "_ZNK6QEvent10isAcceptedEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qcoreevent.h:307
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void accept()
type T_ZN6QEvent6acceptEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QEvent) accept()  {
    mut fnobj := T_ZN6QEvent6acceptEv(0)
    fnobj = qtrt.sym_qtfunc6(1915523935, "_ZN6QEvent6acceptEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtCore/qcoreevent.h:308
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void ignore()
type T_ZN6QEvent6ignoreEv = fn(cthis voidptr) /*void*/

/*

*/
pub fn (this QEvent) ignore()  {
    mut fnobj := T_ZN6QEvent6ignoreEv(0)
    fnobj = qtrt.sym_qtfunc6(340108874, "_ZN6QEvent6ignoreEv")
    fnobj(this.get_cthis())
}
// /usr/include/qt/QtCore/qcoreevent.h:310
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Direct Visibility=Default Availability=Available
// [4] int registerEventType(int)
type T_ZN6QEvent17registerEventTypeEi = fn(hint int) int

/*

*/
pub fn (this QEvent) registerEventType(hint int) int {
    mut fnobj := T_ZN6QEvent17registerEventTypeEi(0)
    fnobj = qtrt.sym_qtfunc6(3887961503, "_ZN6QEvent17registerEventTypeEi")
    rv :=
    fnobj(hint)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qcoreevent.h:310
// index:0 inlined:false externc:Language=CPlusPlus
// Public static Direct Visibility=Default Availability=Available
// [4] int registerEventType(int)

/*

*/
pub fn (this QEvent) registerEventTypep() int {
    // arg: 0, int=Int, =Invalid, , Invalid
    hint := int(-1)
    mut fnobj := T_ZN6QEvent17registerEventTypeEi(0)
    fnobj = qtrt.sym_qtfunc6(3887961503, "_ZN6QEvent17registerEventTypeEi")
    rv :=
    fnobj(hint)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}

[no_inline]
pub fn deleteQEvent(this &QEvent) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3259194329, "_ZN6QEventD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QEvent) freecpp() { deleteQEvent(&this) }

fn (this QEvent) free() {

  /*deleteQEvent(&this)*/

  cthis := this.get_cthis()
  //println("QEvent freeing ${cthis} 0 bytes")

}


/*


*/
//type QEvent.Type = int
pub enum QEventType {
  None = 0
  Timer = 1
  MouseButtonPress = 2
  MouseButtonRelease = 3
  MouseButtonDblClick = 4
  MouseMove = 5
  KeyPress = 6
  KeyRelease = 7
  FocusIn = 8
  FocusOut = 9
  FocusAboutToChange = 23
  Enter = 10
  Leave = 11
  Paint = 12
  Move = 13
  Resize = 14
  Create = 15
  Destroy = 16
  Show = 17
  Hide = 18
  Close = 19
  Quit = 20
  ParentChange = 21
  ParentAboutToChange = 131
  ThreadChange = 22
  WindowActivate = 24
  WindowDeactivate = 25
  ShowToParent = 26
  HideToParent = 27
  Wheel = 31
  WindowTitleChange = 33
  WindowIconChange = 34
  ApplicationWindowIconChange = 35
  ApplicationFontChange = 36
  ApplicationLayoutDirectionChange = 37
  ApplicationPaletteChange = 38
  PaletteChange = 39
  Clipboard = 40
  Speech = 42
  MetaCall = 43
  SockAct = 50
  WinEventAct = 132
  DeferredDelete = 52
  DragEnter = 60
  DragMove = 61
  DragLeave = 62
  Drop = 63
  DragResponse = 64
  ChildAdded = 68
  ChildPolished = 69
  ChildRemoved = 71
  ShowWindowRequest = 73
  PolishRequest = 74
  Polish = 75
  LayoutRequest = 76
  UpdateRequest = 77
  UpdateLater = 78
  EmbeddingControl = 79
  ActivateControl = 80
  DeactivateControl = 81
  ContextMenu = 82
  InputMethod = 83
  TabletMove = 87
  LocaleChange = 88
  LanguageChange = 89
  LayoutDirectionChange = 90
  Style = 91
  TabletPress = 92
  TabletRelease = 93
  OkRequest = 94
  HelpRequest = 95
  IconDrag = 96
  FontChange = 97
  EnabledChange = 98
  ActivationChange = 99
  StyleChange = 100
  IconTextChange = 101
  ModifiedChange = 102
  MouseTrackingChange = 109
  WindowBlocked = 103
  WindowUnblocked = 104
  WindowStateChange = 105
  ReadOnlyChange = 106
  ToolTip = 110
  WhatsThis = 111
  StatusTip = 112
  ActionChanged = 113
  ActionAdded = 114
  ActionRemoved = 115
  FileOpen = 116
  Shortcut = 117
  ShortcutOverride = 51
  WhatsThisClicked = 118
  ToolBarChange = 120
  ApplicationActivate = 121
  ApplicationDeactivate = 122
  QueryWhatsThis = 123
  EnterWhatsThisMode = 124
  LeaveWhatsThisMode = 125
  ZOrderChange = 126
  HoverEnter = 127
  HoverLeave = 128
  HoverMove = 129
  AcceptDropsChange = 152
  ZeroTimerEvent = 154
  GraphicsSceneMouseMove = 155
  GraphicsSceneMousePress = 156
  GraphicsSceneMouseRelease = 157
  GraphicsSceneMouseDoubleClick = 158
  GraphicsSceneContextMenu = 159
  GraphicsSceneHoverEnter = 160
  GraphicsSceneHoverMove = 161
  GraphicsSceneHoverLeave = 162
  GraphicsSceneHelp = 163
  GraphicsSceneDragEnter = 164
  GraphicsSceneDragMove = 165
  GraphicsSceneDragLeave = 166
  GraphicsSceneDrop = 167
  GraphicsSceneWheel = 168
  KeyboardLayoutChange = 169
  DynamicPropertyChange = 170
  TabletEnterProximity = 171
  TabletLeaveProximity = 172
  NonClientAreaMouseMove = 173
  NonClientAreaMouseButtonPress = 174
  NonClientAreaMouseButtonRelease = 175
  NonClientAreaMouseButtonDblClick = 176
  MacSizeChange = 177
  ContentsRectChange = 178
  MacGLWindowChange = 179
  FutureCallOut = 180
  GraphicsSceneResize = 181
  GraphicsSceneMove = 182
  CursorChange = 183
  ToolTipChange = 184
  NetworkReplyUpdated = 185
  GrabMouse = 186
  UngrabMouse = 187
  GrabKeyboard = 188
  UngrabKeyboard = 189
  MacGLClearDrawable = 191
  StateMachineSignal = 192
  StateMachineWrapped = 193
  TouchBegin = 194
  TouchUpdate = 195
  TouchEnd = 196
  NativeGesture = 197
  RequestSoftwareInputPanel = 199
  CloseSoftwareInputPanel = 200
  WinIdChange = 203
  Gesture = 198
  GestureOverride = 202
  ScrollPrepare = 204
  Scroll = 205
  Expose = 206
  InputMethodQuery = 207
  OrientationChange = 208
  TouchCancel = 209
  ThemeChange = 210
  SockClose = 211
  PlatformPanel = 212
  StyleAnimationUpdate = 213
  ApplicationStateChange = 214
  WindowChangeInternal = 215
  ScreenChangeInternal = 216
  PlatformSurface = 217
  Pointer = 218
  TabletTrackingChange = 219
  User = 1000
  MaxUser = 65535
} // endof enum Type

//  body block end

//  keep block begin


fn init_unused_10027() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
