

module qtcore
// /usr/include/qt/QtCore/qrect.h
// #include <qrect.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 13
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QRectF {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QRectFITF {
    get_cthis() voidptr
    toQRectF() QRectF
}
fn hotfix_QRectF_itf_name_table(this QRectFITF) {
  that := QRectF{}
  hotfix_QRectF_itf_name_table(that)
}
pub fn (ptr QRectF) toQRectF() QRectF { return ptr }

pub fn (this QRectF) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQRectFFromptr(cthis voidptr) QRectF {
    return QRectF{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QRectF) newFromptr(cthis voidptr) QRectF {
    return newQRectFFromptr(cthis)
}

[no_inline]
pub fn deleteQRectF(this &QRectF) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3802840456, "_ZN6QRectFD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QRectF) freecpp() { deleteQRectF(&this) }

fn (this QRectF) free() {

  /*deleteQRectF(&this)*/

  cthis := this.get_cthis()
  //println("QRectF freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10061() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
