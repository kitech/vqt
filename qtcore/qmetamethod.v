

module qtcore
// /usr/include/qt/QtCore/qmetaobject.h
// #include <qmetaobject.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QMetaMethod {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QMetaMethodITF {
    get_cthis() voidptr
    toQMetaMethod() QMetaMethod
}
fn hotfix_QMetaMethod_itf_name_table(this QMetaMethodITF) {
  that := QMetaMethod{}
  hotfix_QMetaMethod_itf_name_table(that)
}
pub fn (ptr QMetaMethod) toQMetaMethod() QMetaMethod { return ptr }

pub fn (this QMetaMethod) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQMetaMethodFromptr(cthis voidptr) QMetaMethod {
    return QMetaMethod{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QMetaMethod) newFromptr(cthis voidptr) QMetaMethod {
    return newQMetaMethodFromptr(cthis)
}
// /usr/include/qt/QtCore/qmetaobject.h:59
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QByteArray methodSignature() const
type T_ZNK11QMetaMethod15methodSignatureEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QMetaMethod) methodSignature()  QByteArray/*123*/ {
    mut fnobj := T_ZNK11QMetaMethod15methodSignatureEv(0)
    fnobj = qtrt.sym_qtfunc6(2549705587, "_ZNK11QMetaMethod15methodSignatureEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQByteArrayFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQByteArray)
    return rv2
    //return /*==*/QByteArray{rv}
}
// /usr/include/qt/QtCore/qmetaobject.h:60
// index:0 inlined:false externc:Language=CPlusPlus
// Public Indirect Visibility=Default Availability=Available
// [8] QByteArray name() const
type T_ZNK11QMetaMethod4nameEv = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QMetaMethod) name()  QByteArray/*123*/ {
    mut fnobj := T_ZNK11QMetaMethod4nameEv(0)
    fnobj = qtrt.sym_qtfunc6(2756351335, "_ZNK11QMetaMethod4nameEv")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQByteArrayFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQByteArray)
    return rv2
    //return /*==*/QByteArray{rv}
}
// /usr/include/qt/QtCore/qmetaobject.h:61
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] const char * typeName() const
type T_ZNK11QMetaMethod8typeNameEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMetaMethod) typeName() string {
    mut fnobj := T_ZNK11QMetaMethod8typeNameEv(0)
    fnobj = qtrt.sym_qtfunc6(1716461499, "_ZNK11QMetaMethod8typeNameEv")
    rv :=
    fnobj(this.get_cthis())
    return qtrt.vstringp(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:62
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int returnType() const
type T_ZNK11QMetaMethod10returnTypeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaMethod) returnType() int {
    mut fnobj := T_ZNK11QMetaMethod10returnTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(857633469, "_ZNK11QMetaMethod10returnTypeEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:63
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int parameterCount() const
type T_ZNK11QMetaMethod14parameterCountEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaMethod) parameterCount() int {
    mut fnobj := T_ZNK11QMetaMethod14parameterCountEv(0)
    fnobj = qtrt.sym_qtfunc6(3953292363, "_ZNK11QMetaMethod14parameterCountEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:64
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int parameterType(int) const
type T_ZNK11QMetaMethod13parameterTypeEi = fn(cthis voidptr, index int) int

/*

*/
pub fn (this QMetaMethod) parameterType(index int) int {
    mut fnobj := T_ZNK11QMetaMethod13parameterTypeEi(0)
    fnobj = qtrt.sym_qtfunc6(1621940323, "_ZNK11QMetaMethod13parameterTypeEi")
    rv :=
    fnobj(this.get_cthis(), index)
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:65
// index:0 inlined:false externc:Language=CPlusPlus
// Public Ignore Visibility=Default Availability=Available
// [-2] void getParameterTypes(int *) const
type T_ZNK11QMetaMethod17getParameterTypesEPi = fn(cthis voidptr, types voidptr) /*void*/

/*

*/
pub fn (this QMetaMethod) getParameterTypes(types voidptr /*666*/)  {
    mut fnobj := T_ZNK11QMetaMethod17getParameterTypesEPi(0)
    fnobj = qtrt.sym_qtfunc6(4219375380, "_ZNK11QMetaMethod17getParameterTypesEPi")
    fnobj(this.get_cthis(), types)
}
// /usr/include/qt/QtCore/qmetaobject.h:68
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [8] const char * tag() const
type T_ZNK11QMetaMethod3tagEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMetaMethod) tag() string {
    mut fnobj := T_ZNK11QMetaMethod3tagEv(0)
    fnobj = qtrt.sym_qtfunc6(2225931727, "_ZNK11QMetaMethod3tagEv")
    rv :=
    fnobj(this.get_cthis())
    return qtrt.vstringp(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:70
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QMetaMethod::Access access() const
type T_ZNK11QMetaMethod6accessEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaMethod) access() int {
    mut fnobj := T_ZNK11QMetaMethod6accessEv(0)
    fnobj = qtrt.sym_qtfunc6(1453354162, "_ZNK11QMetaMethod6accessEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:72
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] QMetaMethod::MethodType methodType() const
type T_ZNK11QMetaMethod10methodTypeEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaMethod) methodType() int {
    mut fnobj := T_ZNK11QMetaMethod10methodTypeEv(0)
    fnobj = qtrt.sym_qtfunc6(1406394018, "_ZNK11QMetaMethod10methodTypeEv")
    rv :=
    fnobj(this.get_cthis())
    return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:74
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int attributes() const
type T_ZNK11QMetaMethod10attributesEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaMethod) attributes() int {
    mut fnobj := T_ZNK11QMetaMethod10attributesEv(0)
    fnobj = qtrt.sym_qtfunc6(4009498167, "_ZNK11QMetaMethod10attributesEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:75
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int methodIndex() const
type T_ZNK11QMetaMethod11methodIndexEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaMethod) methodIndex() int {
    mut fnobj := T_ZNK11QMetaMethod11methodIndexEv(0)
    fnobj = qtrt.sym_qtfunc6(2608998004, "_ZNK11QMetaMethod11methodIndexEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:76
// index:0 inlined:false externc:Language=CPlusPlus
// Public Direct Visibility=Default Availability=Available
// [4] int revision() const
type T_ZNK11QMetaMethod8revisionEv = fn(cthis voidptr) int

/*

*/
pub fn (this QMetaMethod) revision() int {
    mut fnobj := T_ZNK11QMetaMethod8revisionEv(0)
    fnobj = qtrt.sym_qtfunc6(245548466, "_ZNK11QMetaMethod8revisionEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qmetaobject.h:78
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] const QMetaObject * enclosingMetaObject() const
type T_ZNK11QMetaMethod19enclosingMetaObjectEv = fn(cthis voidptr) voidptr/*666*/

/*

*/
pub fn (this QMetaMethod) enclosingMetaObject()  QMetaObject/*777 const QMetaObject **/ {
    mut fnobj := T_ZNK11QMetaMethod19enclosingMetaObjectEv(0)
    fnobj = qtrt.sym_qtfunc6(2832786517, "_ZNK11QMetaMethod19enclosingMetaObjectEv")
    rv :=
    fnobj(this.get_cthis())
    return /*==*/newQMetaObjectFromptr(voidptr(rv)) // 444
}

[no_inline]
pub fn deleteQMetaMethod(this &QMetaMethod) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(3696685076, "_ZN11QMetaMethodD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMetaMethod) freecpp() { deleteQMetaMethod(&this) }

fn (this QMetaMethod) free() {

  /*deleteQMetaMethod(&this)*/

  cthis := this.get_cthis()
  //println("QMetaMethod freeing ${cthis} 0 bytes")

}


/*


*/
//type QMetaMethod.Access = int
pub enum QMetaMethodAccess {
  Private = 0
  Protected = 1
  Public = 2
} // endof enum Access


/*


*/
//type QMetaMethod.MethodType = int
pub enum QMetaMethodMethodType {
  Method = 0
  Signal = 1
  Slot = 2
  Constructor = 3
} // endof enum MethodType


/*


*/
//type QMetaMethod.Attributes = int
pub enum QMetaMethodAttributes {
  Compatibility = 1
  Cloned = 2
  Scriptable = 4
} // endof enum Attributes

//  body block end

//  keep block begin


fn init_unused_10047() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
