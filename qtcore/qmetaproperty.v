

module qtcore
// /usr/include/qt/QtCore/qmetaobject.h
// #include <qmetaobject.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 0
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QMetaProperty {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QMetaPropertyITF {
    get_cthis() voidptr
    toQMetaProperty() QMetaProperty
}
fn hotfix_QMetaProperty_itf_name_table(this QMetaPropertyITF) {
  that := QMetaProperty{}
  hotfix_QMetaProperty_itf_name_table(that)
}
pub fn (ptr QMetaProperty) toQMetaProperty() QMetaProperty { return ptr }

pub fn (this QMetaProperty) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQMetaPropertyFromptr(cthis voidptr) QMetaProperty {
    return QMetaProperty{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QMetaProperty) newFromptr(cthis voidptr) QMetaProperty {
    return newQMetaPropertyFromptr(cthis)
}

[no_inline]
pub fn deleteQMetaProperty(this &QMetaProperty) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(1340264886, "_ZN13QMetaPropertyD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QMetaProperty) freecpp() { deleteQMetaProperty(&this) }

fn (this QMetaProperty) free() {

  /*deleteQMetaProperty(&this)*/

  cthis := this.get_cthis()
  //println("QMetaProperty freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10051() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
