

module qtcore
// /usr/include/qt/QtCore/qstring.h
// #include <qstring.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 3
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QString {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QStringITF {
    get_cthis() voidptr
    toQString() QString
}
fn hotfix_QString_itf_name_table(this QStringITF) {
  that := QString{}
  hotfix_QString_itf_name_table(that)
}
pub fn (ptr QString) toQString() QString { return ptr }

pub fn (this QString) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQStringFromptr(cthis voidptr) QString {
    return QString{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QString) newFromptr(cthis voidptr) QString {
    return newQStringFromptr(cthis)
}
// /usr/include/qt/QtCore/qstring.h:279
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [4] int length() const
type T_ZNK7QString6lengthEv = fn(cthis voidptr) int

/*

*/
pub fn (this QString) length() int {
    mut fnobj := T_ZNK7QString6lengthEv(0)
    fnobj = qtrt.sym_qtfunc6(1867525835, "_ZNK7QString6lengthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("int", rv) //.(int) // 1111
   return int(rv)
}
// /usr/include/qt/QtCore/qstring.h:683
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Indirect Visibility=Default Availability=Available
// [8] QByteArray toUtf8() const
type T_ZNKR7QString6toUtf8Ev = fn(sretobj voidptr, cthis voidptr) voidptr

/*

*/
pub fn (this QString) toUtf8()  QByteArray/*123*/ {
    mut fnobj := T_ZNKR7QString6toUtf8Ev(0)
    fnobj = qtrt.sym_qtfunc6(637795128, "_ZNKR7QString6toUtf8Ev")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, this.get_cthis())
    rv := sretobj
    rv2 := /*==*/newQByteArrayFromptr(voidptr(rv)) // 333
    qtrt.set_finalizer(&rv2, /*==*/deleteQByteArray)
    return rv2
    //return /*==*/QByteArray{rv}
}
// /usr/include/qt/QtCore/qstring.h:704
// index:0 inlined:true externc:Language=CPlusPlus
// Public static inline Indirect Visibility=Default Availability=Available
// [8] QString fromUtf8(const char *, int)
type T_ZN7QString8fromUtf8EPKci = fn(sretobj voidptr, str voidptr, size int) voidptr

/*

*/
pub fn (this QString) fromUtf8(str string, size int) string {
    mut conv_arg0 := qtrt.cstringr(&str)
    mut fnobj := T_ZN7QString8fromUtf8EPKci(0)
    fnobj = qtrt.sym_qtfunc6(2549669065, "_ZN7QString8fromUtf8EPKci")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, conv_arg0, size)
    rv := sretobj
    rv2 := /*==*/newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    /*==*/deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtCore/qstring.h:704
// index:0 inlined:true externc:Language=CPlusPlus
// Public static inline Indirect Visibility=Default Availability=Available
// [8] QString fromUtf8(const char *, int)

/*

*/
pub fn (this QString) fromUtf8p(str string) string {
    mut conv_arg0 := qtrt.cstringr(&str)
    // arg: 1, int=Int, =Invalid, , Invalid
    size := int(-1)
    mut fnobj := T_ZN7QString8fromUtf8EPKci(0)
    fnobj = qtrt.sym_qtfunc6(2549669065, "_ZN7QString8fromUtf8EPKci")
    mut sretobj := qtrt.mallocraw(8)
    fnobj(sretobj, conv_arg0, size)
    rv := sretobj
    rv2 := /*==*/newQStringFromptr(voidptr(rv))
    rv3 := rv2.toUtf8().data()
    /*==*/deleteQString(&rv2)
    return rv3
}
// /usr/include/qt/QtCore/qstring.h:835
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Visibility=Default Availability=Available
// [-2] void QString(const char *)
type T_ZN7QStringC2EPKc = fn(cthis voidptr, ch voidptr) 

/*

*/
pub fn (dummy QString) new_for_inherit_(ch string) QString {
  //return newQString(ch)
  return QString{}
}
pub fn newQString(ch string) QString {
    mut conv_arg0 := qtrt.cstringr(&ch)
    mut fnobj := T_ZN7QStringC2EPKc(0)
    fnobj = qtrt.sym_qtfunc6(2640353187, "_ZN7QStringC2EPKc")
    mut cthis := qtrt.mallocraw(8)
    fnobj(cthis, conv_arg0)
    rv := cthis
    vthis := newQStringFromptr(voidptr(rv))
    qtrt.set_finalizer(&vthis, deleteQString)
  return vthis
}

[no_inline]
pub fn deleteQString(this &QString) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(2000556505, "_ZN7QStringD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QString) freecpp() { deleteQString(&this) }

fn (this QString) free() {

  /*deleteQString(&this)*/

  cthis := this.get_cthis()
  //println("QString freeing ${cthis} 0 bytes")

}


/*


*/
//type QString.SectionFlag = int
pub enum QStringSectionFlag {
  SectionDefault = 0
  SectionSkipEmpty = 1
  SectionIncludeLeadingSep = 2
  SectionIncludeTrailingSep = 4
  SectionCaseInsensitiveSeps = 8
} // endof enum SectionFlag


/*


*/
//type QString.SplitBehavior = int
pub enum QStringSplitBehavior {
  KeepEmptyParts = 0
  SkipEmptyParts = 1
} // endof enum SplitBehavior


/*


*/
//type QString.NormalizationForm = int
pub enum QStringNormalizationForm {
  NormalizationForm_D = 0
  NormalizationForm_C = 1
  NormalizationForm_KD = 2
  NormalizationForm_KC = 3
} // endof enum NormalizationForm

//  body block end

//  keep block begin


fn init_unused_10009() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
