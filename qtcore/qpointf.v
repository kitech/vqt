

module qtcore
// /usr/include/qt/QtCore/qpoint.h
// #include <qpoint.h>
// #include <QtCore>

//  header block end

//  main block begin

//  main block end

//  use block begin

//  use block end

//  ext block begin


/*
#include <stdlib.h>
// extern C begin: 6
*/
// import "C"
// import vsafe
// import reflect
import fmt
// import log
// import github.com/kitech/qt.go/qtrt
import vqt.qtrt
//  ext block end

//  body block begin



/*

*/
pub struct QPointF {
    // mut: CObject &qtrt.CObject
    pub: qtrt.CObject
}

pub interface QPointFITF {
    get_cthis() voidptr
    toQPointF() QPointF
}
fn hotfix_QPointF_itf_name_table(this QPointFITF) {
  that := QPointF{}
  hotfix_QPointF_itf_name_table(that)
}
pub fn (ptr QPointF) toQPointF() QPointF { return ptr }

pub fn (this QPointF) get_cthis() voidptr {
    return this.CObject.get_cthis()
}
  // ignore GetCthis for 0 base
[no_inline]
pub fn newQPointFFromptr(cthis voidptr) QPointF {
    return QPointF{qtrt.newCObjectFromptr(cthis)}
}
pub fn (dummy QPointF) newFromptr(cthis voidptr) QPointF {
    return newQPointFFromptr(cthis)
}
// /usr/include/qt/QtCore/qpoint.h:228
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] qreal manhattanLength() const
type T_ZNK7QPointF15manhattanLengthEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QPointF) manhattanLength() f64 {
    mut fnobj := T_ZNK7QPointF15manhattanLengthEv(0)
    fnobj = qtrt.sym_qtfunc6(2157545085, "_ZNK7QPointF15manhattanLengthEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtCore/qpoint.h:230
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Extend Visibility=Default Availability=Available
// [1] bool isNull() const
type T_ZNK7QPointF6isNullEv = fn(cthis voidptr) bool

/*

*/
pub fn (this QPointF) isNull() bool {
    mut fnobj := T_ZNK7QPointF6isNullEv(0)
    fnobj = qtrt.sym_qtfunc6(1609692812, "_ZNK7QPointF6isNullEv")
    rv :=
    fnobj(this.get_cthis())
    return rv//!=0
}
// /usr/include/qt/QtCore/qpoint.h:232
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] qreal x() const
type T_ZNK7QPointF1xEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QPointF) x() f64 {
    mut fnobj := T_ZNK7QPointF1xEv(0)
    fnobj = qtrt.sym_qtfunc6(92750711, "_ZNK7QPointF1xEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtCore/qpoint.h:233
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Direct Visibility=Default Availability=Available
// [8] qreal y() const
type T_ZNK7QPointF1yEv = fn(cthis voidptr) f64

/*

*/
pub fn (this QPointF) y() f64 {
    mut fnobj := T_ZNK7QPointF1yEv(0)
    fnobj = qtrt.sym_qtfunc6(71641408, "_ZNK7QPointF1yEv")
    rv :=
    fnobj(this.get_cthis())
    //return qtrt.cretval2v("f64", rv) // .(f64) // 1111
   return 0
}
// /usr/include/qt/QtCore/qpoint.h:234
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setX(qreal)
type T_ZN7QPointF4setXEd = fn(cthis voidptr, x f64) /*void*/

/*

*/
pub fn (this QPointF) setX(x f64)  {
    mut fnobj := T_ZN7QPointF4setXEd(0)
    fnobj = qtrt.sym_qtfunc6(2852303336, "_ZN7QPointF4setXEd")
    fnobj(this.get_cthis(), x)
}
// /usr/include/qt/QtCore/qpoint.h:235
// index:0 inlined:true externc:Language=CPlusPlus
// Public inline Ignore Visibility=Default Availability=Available
// [-2] void setY(qreal)
type T_ZN7QPointF4setYEd = fn(cthis voidptr, y f64) /*void*/

/*

*/
pub fn (this QPointF) setY(y f64)  {
    mut fnobj := T_ZN7QPointF4setYEd(0)
    fnobj = qtrt.sym_qtfunc6(2881543135, "_ZN7QPointF4setYEd")
    fnobj(this.get_cthis(), y)
}

[no_inline]
pub fn deleteQPointF(this &QPointF) {
    mut fnobj := qtrt.TCppDtor(0)
    fnobj = qtrt.sym_qtfunc6(134628044, "_ZN7QPointFD2Ev")
    fnobj(this.get_cthis())
    mut that := this
    //that.cthis = voidptr(0)
}

pub fn (this QPointF) freecpp() { deleteQPointF(&this) }

fn (this QPointF) free() {

  /*deleteQPointF(&this)*/

  cthis := this.get_cthis()
  //println("QPointF freeing ${cthis} 0 bytes")

}

//  body block end

//  keep block begin


fn init_unused_10041() {
  // if false {reflect.keepme()}
  // if false {reflect.TypeOf(123)}
  // if false {reflect.TypeOf(vsafe.sizeof(0))}
  // if false {fmt.println(123)}
  if false {/*log.println(123)*/}
  if false {qtrt.keepme()}
}
//  keep block end
